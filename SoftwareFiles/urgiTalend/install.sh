#/bin/bash

ETC=./etc

# get version of the job
JOB_NAME=BioMercatorMetaQTLToGnpMapMetaQTL

VERSION=`find . -name *.zip`
VERSION=`echo $VERSION | sed s/"\.\/$JOB_NAME\_"//g | sed s/.zip//`
JAR_VERSION=`echo $VERSION | tr . _`

INFLATED_DIR=${JOB_NAME}_${VERSION}

# clean previous installation
if [ -d $ETC ]; then
	rm -rf $ETC
fi
if [ -d $INFLATED_DIR ]; then
	rm -rf $INFLATED_DIR
fi

# create temporary directory
mkdir $ETC

# prepare all files which will be included in the jar file
unzip *.zip
cp $INFLATED_DIR/lib/*.jar $ETC
rm $ETC/geronimo-stax-api_1.0_spec-1.0.jar
cp $INFLATED_DIR/$JOB_NAME/biomercatormetaqtltognpmapmetaqtl_${JAR_VERSION}.jar $ETC
cp MANIFEST.MF $ETC

# build the jar file
cd $ETC
for i in *.jar; do 
	jar xvf $i; 
done
rm *.jar
rm -rf META-INF/ NOTICE.txt LICENSE.txt 
jar cvfm biomercator2gnpmap.jar ./MANIFEST.MF `find . -type f`

# install jar under user maven local repository
mvn install:install-file -Dfile=biomercator2gnpmap.jar -DgroupId=fr.inra.urgi -DartifactId=biomercator2gnpmap -Dversion=$VERSION -Dpackaging=jar

# clean
cd -
rm -rf $ETC $INFLATED_DIR

exit 0