/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.biom_proj;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Locus;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;

/**
 *
 * @author sosnowski
 */
public class Projection2 {
	private ArrayList	listeCouplesDeGroupes;
	private MapGene		carteRef;
	private MapGene		carteAProjeter;
	private MapGene		carteProjetee;
	private String		dirName;
	private MapGene		carteMarqCommuns;
	private String		typeProjection;
	private ArrayList	commonMarkersVectorsList;
	private Object		tabCorresp[][];
	private File		outputFile;
	private PrintWriter outWriter;
	private boolean		markersProj;
	private boolean		QTLProj;
	private boolean		ESTProj;
	private String		newMapName;
	private boolean		ignoreMarkInversion;
	private String		inversionsList;
	private boolean		inversionExist;
	private ArrayList	discardedMarkers;

    /** Creates new Projector */
    public Projection2(ArrayList	listeCouplesGroupes,
						MapGene		cartRef,
						MapGene		cartAProj,
						String		dirName,
						File		OutputFile,
						boolean		markersProj,
						boolean		QTLProj,
						boolean		ESTProj,
						String		newMap,
						boolean		ignoreMarkInversion)throws Exception{
        this.listeCouplesDeGroupes = listeCouplesGroupes;
        this.carteRef = cartRef;
        this.carteAProjeter = cartAProj;
        this.dirName = dirName;
        this.outputFile = OutputFile;
        this.markersProj = markersProj;
        this.QTLProj = QTLProj;
        this.ESTProj = ESTProj;
        this.newMapName = newMap;
        this.ignoreMarkInversion = ignoreMarkInversion;
        discardedMarkers = new ArrayList();
        //printWriter pour ecrire vers un fichier output avec messages de realisation ou d'erreur
        try
        {
           outWriter = new PrintWriter(new BufferedWriter(new FileWriter(outputFile)));
        }
        catch(java.io.IOException ioe){}
        chromPairsEnumerator(listeCouplesDeGroupes);
		if (!doCommunMarkersExist()){
			throw new Exception("Aucun marqueur commun n'a ??t?? trouv??.");
		}
        if (inversionExist == false)
        {
            projectionLaunch();
        }
        else
        {
			String	expStr = "Inversion de marqueurs : \n\n" + inversionsList;
			throw new Exception(expStr, null);
			//System.out.println("inversionExist == true");
			/*
            if (ignoreMarkInversion == true)
            {
                InversionsListDialog ild = new InversionsListDialog(interfa,
                                                    "List of markers inversions",
                                                    true,
                                                    inversionsList,
                                                    "Launch projection");
                //determiner nouveaux R et eliminer m.communs inverses
                //lancer la projection avec marqueurs englobants;
            }
            else
            {
                InversionsListDialog ild = new InversionsListDialog(interfa,
                                                    "List of markers inversions",
                                                    true,
                                                    inversionsList,
                                                    "OK");
			  
            }*/
        }
        outWriter.close();
    }

	public final boolean	doCommunMarkersExist(){
		boolean		res = false;
		ArrayList	v = null;

		if (null != commonMarkersVectorsList){
			for (int i = 0; i < commonMarkersVectorsList.size(); ++i){
				v = (ArrayList)commonMarkersVectorsList.get(i);
				if (v.size() > 0){
					res = true;
				}
			}
		}

		return res;
	}

    /**
     * For each chromosome pair:
     * -detect common markers and create a vector with them. Add this vector
     * to a global vector commonMarkersVectorList
     * -determines if common markers are in the same order
     */
    public final void chromPairsEnumerator( ArrayList listeCouplesGroupes )
    {
        inversionExist = false;
        inversionsList = "";
        commonMarkersVectorsList = new ArrayList();

        //enumeration of chromosome pairs
        Enumeration enumListeCouplesGroupes = Collections.enumeration(listeCouplesDeGroupes);
        while ( enumListeCouplesGroupes.hasMoreElements() )
        {
            Object obj1 = enumListeCouplesGroupes.nextElement();
            GroupsPair coupleGroupes = (GroupsPair) obj1;
            Chromosome groupeCarteRef = coupleGroupes.getGroupeCarteRef();
            Chromosome groupeCarteAProj = coupleGroupes.getGroupeCarteAProj();
            ArrayList marqueursCommunsDuGroupe = detectCommonMarkers(groupeCarteRef, groupeCarteAProj);//comparaison des 2 groupes
            if (ignoreMarkInversion == false)
            {
                commonMarkersVectorsList.add(marqueursCommunsDuGroupe);
                areMarkersInverted(marqueursCommunsDuGroupe, groupeCarteRef.getName());
            }
            else //automatically resolve inversions
            {
                //System.out.println("ON PASSE dans la resolution d'inversions...");
                marqueursCommunsDuGroupe = resolveInversions(marqueursCommunsDuGroupe);
                commonMarkersVectorsList.add(marqueursCommunsDuGroupe);
                inversionExist = false;
            }
        }
    }


    /**
     * Looks for common markers between groups. For each correspondence, an object
     * CommonMarker is created (with marker positions on both maps, rank on
     * ref map, R value) and added to vector listeMarqueursCommuns.
     *
     */
    public ArrayList detectCommonMarkers( Chromosome groupeCarteRef, Chromosome groupeCarteAProj )
    {
        CommonMarker corresp;
        //System.out.println("\n\n________________________________________________");
        //System.out.println("COMPARISON BETWEEN CHROMOSOMES " + groupeCarteRef.getNom()+ " "+groupeCarteAProj.getNom()+"\n");
        //outWriter.println("\n\n________________________________________________");
        //outWriter.println("\n********* COMPARISON BETWEEN CHROMOSOMES " + groupeCarteRef.getNom() + " ********\n");

        ArrayList listeMarqueursCommuns = new ArrayList();

        //Enumeration enumListeLocusCarteAProj = groupeCarteAProj.getListeLocus().elements();
        //System.out.println("nbr locus CAP: "+ groupeCarteAProj.getListeLocus().size());
        boolean premierMarqueurCommun = true;
        boolean commonMarkerFound;
        int rangSurCarteAProj = 1;
        //parcours du groupe de la carte ??? projeter
		LinkageGroup lkgAProj = groupeCarteAProj.getDefaultLkg();
		for (Iterator<Locus> it = lkgAProj.iterator(); it.hasNext();) {
			Locus obj1 = it.next();

            if ( obj1 instanceof Marker )
            {
                Marker marqueurQ = (Marker) obj1;
                commonMarkerFound = false;
                //Enumeration enumListeLocusGenes = groupeCarteRef.getListeLocus().elements();
                int rangSurCarteGenes = 1;
                //parcours du groupe de la carte de genes
				LinkageGroup lkgRef = groupeCarteRef.getDefaultLkg();
				//groupeCarteRef.getDefaultLkg().iteratorStart();
				for (Iterator<Locus> itRef = lkgRef.iterator(); itRef.hasNext();) {
					Locus obj2 = itRef.next();

                    if ( obj2 instanceof Marker )
                    {
                        Marker marqueurG = (Marker) obj2;

                        if (marqueurQ.getName().equals(marqueurG.getName())) //si egalite des noms de marqueurs
                        {   //nouvelle ligne de correspondance entre les marqueurs
                            corresp = new CommonMarker(marqueurG,
                                                       marqueurG.getName(),
                                                       marqueurG.getPosition(),
                                                       marqueurQ.getPosition(),
                                                       rangSurCarteGenes,
                                                       rangSurCarteAProj,
                                                       0,
                                                       groupeCarteRef);
                           // System.out.println("NOUVEAU marqueur commun: "+marqueurG.getNom()
                             //                   +" rang CG: "+rangSurCarteGenes
                               //                 +" rang CAP: "+rangSurCarteAProj);
                            corresp.setGroupe(groupeCarteRef);
                            if (premierMarqueurCommun == false)
                            {   //calcul de la valeur du facteur de proportionnalite R precedent :
                                //on recupere l'objet precedent du vecteur
								Object obj = listeMarqueursCommuns.get(listeMarqueursCommuns.size()-1);
                                CommonMarker correspPrecedente = (CommonMarker) obj;
                                double posPrecedenteMG = correspPrecedente.getPositionMarqueurG();
                                double posPrecedenteMQ = correspPrecedente.getPositionMarqueurQ();
                                //rapport (intervalle M carte genes) / (intervalle M carte QTL)
                                correspPrecedente.setR( (marqueurG.getPosition() - posPrecedenteMG )
                                                        / (marqueurQ.getPosition() - posPrecedenteMQ ) );
                            }
                            listeMarqueursCommuns.add(corresp);//ajout de la ligne au vecteur
                            premierMarqueurCommun = false;
                            commonMarkerFound = true;
                        }
                        rangSurCarteGenes ++;
                    }
                }
                if (commonMarkerFound) rangSurCarteAProj ++;
            }
        }

		//compute global R for each interval, put in last common marker of the vector
        if (!listeMarqueursCommuns.isEmpty()){
        	Object objP = listeMarqueursCommuns.get(0);
        	CommonMarker premiereCorresp = (CommonMarker) objP;
        	double posPremierMarqQ = premiereCorresp.getPositionMarqueurQ();
        	double posPremierMarqG = premiereCorresp.getPositionMarqueurG();
        	Object objD = listeMarqueursCommuns.get(listeMarqueursCommuns.size()-1);
        	CommonMarker derniereCorresp = (CommonMarker) objD;
        	double posDernierMarqQ = derniereCorresp.getPositionMarqueurQ();
        	double posDernierMarqG = derniereCorresp.getPositionMarqueurG();
        	//rapport (intervalle M carte genes) / (intervalle M carte QTL)
        	derniereCorresp.setR( (posDernierMarqG - posPremierMarqG )
                                / (posDernierMarqQ - posPremierMarqQ ) );

        	//print common markers and R in file and Output
        	/*if (listeMarqueursCommuns.size() > 1)
        	{
				for (int i=0; i < (listeMarqueursCommuns.size()-1); i++)
				{
					CommonMarker cm = (CommonMarker) listeMarqueursCommuns.elementAt(i);
					CommonMarker nextCm = (CommonMarker) listeMarqueursCommuns.elementAt(i+1);
					System.out.println(cm.getNomMarqueur()+" - "+nextCm.getNomMarqueur()+": R= "+cm.getR());
					outWriter.println(cm.getNomMarqueur()+" - "+nextCm.getNomMarqueur()+": R= "+cm.getR());
				}
				CommonMarker lastCm = (CommonMarker) listeMarqueursCommuns.lastElement();
				outWriter.println("Global R: "+lastCm.getR());
				System.out.println("R global: "+lastCm.getR());
        	}*/
        }
		//detect if some common markers are colocated in either map. In this case, discard them
		listeMarqueursCommuns = discardColocatedMarkers(listeMarqueursCommuns);

		//System.out.println("NUMBER OF COMMON MARKERS: " + listeMarqueursCommuns.size()+"\n" );
		outWriter.println("NUMBER OF COMMON MARKERS: " + listeMarqueursCommuns.size()+"\n" );

        if (listeMarqueursCommuns.size() == 1)
        {
           // System.out.println("WARNING: ONE SINGLE COMMON MARKER ON CHROMOSOME " + groupeCarteAProj.getNom() );
            outWriter.println("\n WARNING: ONE SINGLE COMMON MARKER ON CHROMOSOME " + groupeCarteAProj.getName() );
        }
        return listeMarqueursCommuns;
    }


    /**
     * parcours du tableau de marqueurs communs, determine s'ils sont dans le
     * meme ordre
     */
    public void areMarkersInverted( ArrayList listeMarqueursCommuns, String nomGroupe )
    {
        int rangPrecedent = 0;
        String nomMarqueurPrecedent = "";
        Enumeration enummarqcommuns = Collections.enumeration(listeMarqueursCommuns);
        while ( enummarqcommuns.hasMoreElements() )
        {
             Object obj = enummarqcommuns.nextElement();
             CommonMarker corr = (CommonMarker) obj;
             int rangMarqueur = corr.getRang();
             if (rangPrecedent > rangMarqueur)
             {
                 System.out.println("WARNING: INVERSION BETWEEN MARKERS "
                                     + nomMarqueurPrecedent + " AND "
                                     + corr.getNomMarqueur() );
                 outWriter.println("WARNING: INVERSION BETWEEN MARKERS "
                                    + nomMarqueurPrecedent + " AND "
                                    + corr.getNomMarqueur() );
                 inversionsList += nomGroupe + " : Inversion entre les marqueurs "
                                    + nomMarqueurPrecedent + " et "
                                    + corr.getNomMarqueur()+"\n\n";
                 inversionExist = true;
             }
             nomMarqueurPrecedent = corr.getNomMarqueur();
             rangPrecedent = rangMarqueur;
        }
    }

    /**
     * If some common markers colocate (either in ref map or in map to project),
     * discard them as they won't be useful in R computing,
     * and can even lead to division by 0 (if located on map to project)
     *
     * @param commonMarkers
     * @return
     */
	public ArrayList discardColocatedMarkers(ArrayList commonMarkers){
		//boolean discarded;
		Enumeration enumCM = Collections.enumeration(commonMarkers);
		while (enumCM.hasMoreElements()){
			CommonMarker cm = (CommonMarker) enumCM.nextElement();
			double r = cm.getR();
			Double RDouble = new Double(r);
			if ((r == 0) | (RDouble.isInfinite()) | (RDouble.isNaN())){
				commonMarkers.remove(cm);
				enumCM = Collections.enumeration(commonMarkers);
				discardedMarkers.add(cm.getNomMarqueur());
				System.out.println("Enlev??? marqueur commun colocated: "+cm.getNomMarqueur());
				outWriter.println("Discarded common marker "+cm.getNomMarqueur()+ " because of unuseful R value");
				Enumeration enumCM2 = Collections.enumeration(commonMarkers);
				while (enumCM2.hasMoreElements()){
					CommonMarker cm2 = (CommonMarker) enumCM2.nextElement();
					if (cm2.getY() > cm.getY()) cm2.setY(cm2.getY()-1);
					if (cm2.getX() > cm.getX()) cm2.setX(cm2.getX()-1);
				}
			}
		}
		recomputeR(commonMarkers);
		Enumeration enumCM3 = Collections.enumeration(commonMarkers);
		while(enumCM3.hasMoreElements()){
			CommonMarker cm = (CommonMarker) enumCM3.nextElement();
			//System.out.println("CM: name: "+cm.getNomMarqueur()+" X: "+ cm.getX()+ " Y: "+cm.getY());
		}
		return commonMarkers;
	}

    /**
     * Resolves inversions of markers. Markers showing the most inversions with
     * other markers are discarded. Coordinates X = carteRef.
     * Coordinates Y = carteAProj
     */
    public ArrayList resolveInversions(ArrayList commonMarkers)
    {
        //recompute relative positions of common markers on ref map (not the rank
        //in the chromosome, but rank in the vector of common markers)
        if (!commonMarkers.isEmpty()){
        	CommonMarker tabSortedCommonMarkers[];
        	tabSortedCommonMarkers = sortMarkersOnRefMap(commonMarkers);
        	commonMarkers = recomputePositions(commonMarkers, tabSortedCommonMarkers);
       		int diagonaleX = 1;
        	int diagonaleY = 1;
        	for (int i=0; i < commonMarkers.size(); i++)
        	{
				//System.out.println("NOUVELLE DIAGONALE: " + diagonaleX+" "+diagonaleY);
            	CommonMarker cmXi = searchCommonMarkerWhereXis(diagonaleX, commonMarkers);
				//System.out.println("cmXi:" + cmXi.getNomMarqueur());
            	CommonMarker cmYi = searchCommonMarkerWhereYis(diagonaleY, commonMarkers);
				//System.out.println("cmYi:" + cmYi.getNomMarqueur());
            	//if commonMarker is not in the diagonale
            	while ( ((cmXi.getNomMarqueur() !=null) && (cmYi.getNomMarqueur() != null)) && ( ! cmXi.getNomMarqueur().equals(cmYi.getNomMarqueur()) ))
            	{
                	int deplacementX = cmXi.getY() - diagonaleY;
                	int deplacementY = cmYi.getX() - diagonaleX;

					//System.out.println("resolution de l'inversion entre "+cmXi.getNomMarqueur()
					//									+" et "+cmYi.getNomMarqueur());

                	if (deplacementX > deplacementY)
                	{
                		String listInvertedMarkers="";
                		for(int j=diagonaleX;j<(diagonaleX+deplacementX);j++){
							CommonMarker cma = searchCommonMarkerWhereYis(j, commonMarkers);
                			listInvertedMarkers += cma.getNomMarqueur()+" ";
                		}
                		//System.out.println("Resolution de l'inversion entre "+cmXi.getNomMarqueur()
						//					+" et "+listInvertedMarkers);
						outWriter.println("Resolution of inversion between "+cmXi.getNomMarqueur()
											+" and "+listInvertedMarkers);

                    	commonMarkers = discard(cmXi, commonMarkers);
                	}
                	else if ( (deplacementX == deplacementY) & (deplacementX == 2) ) //3 markers inverted
                	{
						CommonMarker thirdMarker = searchCommonMarkerWhereXis((diagonaleX+1), commonMarkers);
						//System.out.println("\nResolution de l'inversion entre "+cmXi.getNomMarqueur()
						//							+", "+cmYi.getNomMarqueur()+ " et "+thirdMarker.getNomMarqueur());
						outWriter.println("\nResolution of inversion between "+cmXi.getNomMarqueur()
													+", "+cmYi.getNomMarqueur()+ " and "+thirdMarker.getNomMarqueur());
                    	commonMarkers = discard(cmXi, commonMarkers);
                    	commonMarkers = discard(cmYi, commonMarkers);
                    	CommonMarker middleCommonMarker = searchCommonMarkerWhereXis(diagonaleX, commonMarkers);
						commonMarkers = discard(middleCommonMarker, commonMarkers);
                	}
					else if (deplacementX == deplacementY) //2 markers inverted
					{
						//System.out.println("\nResolution de l'inversion entre "+cmXi.getNomMarqueur()
						//								+" et "+cmYi.getNomMarqueur());
						outWriter.println("\nResolution of inversion between "+cmXi.getNomMarqueur()
											+" and "+cmYi.getNomMarqueur());

						commonMarkers = discard(cmXi, commonMarkers);
						commonMarkers = discard(cmYi, commonMarkers);
					}
                	else
                	{
						String listInvertedMarkers="";
						for(int j=diagonaleY;j<(diagonaleY+deplacementY);j++){
							CommonMarker cma = searchCommonMarkerWhereXis(j, commonMarkers);
							listInvertedMarkers += cma.getNomMarqueur()+" ";
						}
						//System.out.println("resolution de l'inversion entre "+cmYi.getNomMarqueur()
						//					+" et "+listInvertedMarkers);
						outWriter.println("Resolution of inversion between "+cmYi.getNomMarqueur()
											+" and "+listInvertedMarkers);

                    	commonMarkers = discard(cmYi, commonMarkers);
                	}

					cmXi = searchCommonMarkerWhereXis(diagonaleX, commonMarkers);
					cmYi = searchCommonMarkerWhereYis(diagonaleY, commonMarkers);
            	}
            	diagonaleX ++;
            	diagonaleY ++;

        	}
        	//recompute R value with present common markers
        	commonMarkers = recomputeR(commonMarkers);
        }
        return commonMarkers;
    }

    /**
     * Search for the CommonMarker where X = diagX
     */
    public CommonMarker searchCommonMarkerWhereXis(int diagX, ArrayList commonMarkers)
    {
        CommonMarker cmFound = new CommonMarker();
        Enumeration enu = Collections.enumeration(commonMarkers);
        while (enu.hasMoreElements() )
        {
            Object obj = enu.nextElement();
            CommonMarker cm = (CommonMarker) obj;
            if (cm.getX() == diagX)
            {
                //System.out.println("trouve le cm avec X= "+diagX+ ": "+cm.getNomMarqueur());
                cmFound = cm;
                break;
            }
        }
        return cmFound;
    }

    /**
     * Search for the CommonMarker where Y = diagY
     */
    public CommonMarker searchCommonMarkerWhereYis(int diagY, ArrayList commonMarkers)
    {
        CommonMarker cmFound = new CommonMarker();
        Enumeration enu = Collections.enumeration(commonMarkers);
        while (enu.hasMoreElements() )
        {
            Object obj = enu.nextElement();
            CommonMarker cm = (CommonMarker) obj;
            if (cm.getY() == diagY)
            {
                //System.out.println("trouve le cm avec Y= "+diagY+ ": "+cm.getNomMarqueur());
                cmFound = cm;
                break;
            }
        }
        return cmFound;
    }

    /*
     * Sort markers by crescent positions on reference map
     */
    public CommonMarker[] sortMarkersOnRefMap(ArrayList cm) {
            //transform vector in an array
            CommonMarker tabCommonMarkers[];
            tabCommonMarkers = new CommonMarker[cm.size()];
            //System.out.println("nombre Marker: "+group.getListeMarker().size());
            int i = 0;
            Enumeration enumMarkCommuns = Collections.enumeration(cm);
            while (enumMarkCommuns.hasMoreElements())
            {
                Object objMC = enumMarkCommuns.nextElement();
                CommonMarker cma = (CommonMarker) objMC;
                tabCommonMarkers[i] = cma;
                //put markers of vector in an array
                i++;
            }
            //sort markers according to crescent positions on ref map
            for (int j=1; j<tabCommonMarkers.length; j++) {
                CommonMarker cmToPlace = (CommonMarker) tabCommonMarkers[j];
                //System.out.println("Marker a placer: "+(Marker) tab[j]);
                int indicePrec = j-1;
                //System.out.println("tab_indice_prec: "+((Marker) tab[indicePrec]).getPosition());
                while(indicePrec >= 0 && ( (CommonMarker) tabCommonMarkers[indicePrec]).getX() > cmToPlace.getX() ) {
                    tabCommonMarkers[indicePrec+1] = tabCommonMarkers[indicePrec];
                    indicePrec = indicePrec-1;
                    //System.out.println("Marker " + MarkerAPlacer.getNom() + " deplac???");
            }
            tabCommonMarkers[indicePrec+1] = cmToPlace;
        }
        return tabCommonMarkers;
    }
    /**
     * Replaces original positions of markers on gene map by positions related to
     * the group of common markers
     */
    public ArrayList recomputePositions(ArrayList commonMarkers, CommonMarker tabSortedCommonMarkers[])
    {
        CommonMarker cmHomol = new CommonMarker();
        Enumeration enumCommMark = Collections.enumeration(commonMarkers);
        while (enumCommMark.hasMoreElements()){
            Object obj = enumCommMark.nextElement();
            CommonMarker cm = (CommonMarker) obj;
            int pos = positionOfHomologousCM(cm, tabSortedCommonMarkers);
            cm.setX(pos);
            //System.out.println("POSITIONS CORRIGEES POUR "+cm.getNomMarqueur()
            //+" CG: "+pos + " CAP: "+cm.getY());
        }
        return commonMarkers;
    }

    /**
     * Find the homologous CommonMarker in the array, and return its position
     */
    public int positionOfHomologousCM(CommonMarker cmToFind,
                                      CommonMarker tabSortedCommonMarkers[])
    {
        int position = 0;
        for (int j=0; j<tabSortedCommonMarkers.length; j++) {
                CommonMarker cm = (CommonMarker) tabSortedCommonMarkers[j];
                if (cm.getNomMarqueur().equals(cmToFind.getNomMarqueur()))
                    position = j+1;
        }
        return position;
    }

    /**
     * Discard the CommonMarker from vector of commonMarkers
     */
    public ArrayList discard(CommonMarker cm, ArrayList commonMarkers)
    {
        int coordXcm = cm.getX();
        int coordYcm = cm.getY();
        commonMarkers.remove(cm);
        //System.out.println("elimination du marqueur "+cm.getNomMarqueur());
        outWriter.println("  Marker "+cm.getNomMarqueur() + " discarded");
        Enumeration enu = Collections.enumeration(commonMarkers);
        while (enu.hasMoreElements() )
        {
            Object obj = enu.nextElement();
            CommonMarker cma = (CommonMarker) obj;
            if (cma.getX() > coordXcm){
            	cma.setX( (cma.getX() - 1));
            }
            if (cma.getY() > coordYcm){
            	cma.setY( (cma.getY() - 1));
            }

        }
        discardedMarkers.add(cm.getNomMarqueur());
        return commonMarkers;
    }


    /**
     * After common markers discarding, recomputes values of R for each interval
     * of actual common markers
     */
    public ArrayList recomputeR(ArrayList commonMarkers)
    {
		outWriter.println("\nNEW VALUES FOR R: \n");
        if (commonMarkers.size() > 1)
        {
            for (int i=0; i < (commonMarkers.size()-1); i++)
            {
                CommonMarker cm = (CommonMarker) commonMarkers.get(i);
                CommonMarker nextCm = (CommonMarker) commonMarkers.get(i+1);
                //rapport (intervalle M carte genes) / (intervalle M carte QTL)
                cm.setR( (nextCm.getPositionMarqueurG() - cm.getPositionMarqueurG())
                        /(nextCm.getPositionMarqueurQ() - cm.getPositionMarqueurQ()));
				outWriter.println(cm.getNomMarqueur()+" - "+nextCm.getNomMarqueur()+": R= "+cm.getR());
            }
            CommonMarker firstCm = (CommonMarker) commonMarkers.get(0);
            CommonMarker lastCm = (CommonMarker) commonMarkers.get(commonMarkers.size()-1);
            lastCm.setR( (lastCm.getPositionMarqueurG() - firstCm.getPositionMarqueurG())
                        /(lastCm.getPositionMarqueurQ() - firstCm.getPositionMarqueurQ()));
            outWriter.println("Global R: "+lastCm.getR());
        }
        return commonMarkers;
    }

    /**
     * Launches projection processes. For each of them, enumerates the list of
     * chromosomes and calls the appropriate projection method.
     */
    public final void projectionLaunch()
    {
        //creation of a new projected map
        //carteProjetee = new MapGene("tmp");
		carteProjetee = carteRef.copy();
		carteProjetee.setName(newMapName);
        //carteProjetee = copieCarte(carteRef, carteProjetee);
        //carteProjetee.getMarkersSettingsClass().setProjectionExist(true);

        //createCommonMarkersMap();//Creates map with only common markers (demande M. Falque)

        outWriter.println("\n*************** PROJECTION RESULTS ******************\n");
        if (markersProj == true)
        {
            Enumeration enumListeCouplesGroupes = Collections.enumeration(listeCouplesDeGroupes);
            while ( enumListeCouplesGroupes.hasMoreElements() )
            {
                Object obj1 = enumListeCouplesGroupes.nextElement();
                GroupsPair coupleGroupes = (GroupsPair) obj1;
                Chromosome groupeCarteRef = coupleGroupes.getGroupeCarteRef();
                Chromosome groupeCarteAProj = coupleGroupes.getGroupeCarteAProj();
                //find corresponding common markers vector
                ArrayList commonMarkers = findCommonMarkersVector(groupeCarteRef);
                if (commonMarkers.size() > 1)
                {
                    //transform vector in an array
                    tabCorresp = vectorToTable(commonMarkers);
                    markersProjector(groupeCarteAProj, groupeCarteRef, tabCorresp);
                }
            }
			carteProjetee.sort();
        }

        if (QTLProj == true)
        {
            Enumeration enumListeCouplesGroupes = Collections.enumeration(listeCouplesDeGroupes);
            while ( enumListeCouplesGroupes.hasMoreElements() )
            {
                Object obj1 = enumListeCouplesGroupes.nextElement();
                GroupsPair coupleGroupes = (GroupsPair) obj1;
                Chromosome groupeCarteRef = coupleGroupes.getGroupeCarteRef();
                Chromosome groupeCarteAProj = coupleGroupes.getGroupeCarteAProj();
                //find corresponding common markers vector
                ArrayList commonMarkers = findCommonMarkersVector(groupeCarteRef);
                if (commonMarkers.size() > 1)
                {
                    //transform vector in an array
                    tabCorresp = vectorToTable(commonMarkers);
                    QTLProjector(groupeCarteAProj, groupeCarteRef, tabCorresp);
                }
            }
			//carteProjetee.sortMapMarker();
            //update trait list in the map (add the new ones)
            //carteProjetee.updateQtlTraitsList();

            //set default colors for QTL traits
            //carteProjetee.getQTLSettingsClass().setDefaultQTLColors();
        }

        if (ESTProj == true)
        {
            Enumeration enumListeCouplesGroupes = Collections.enumeration(listeCouplesDeGroupes);
            while ( enumListeCouplesGroupes.hasMoreElements() )
            {
                Object obj1 = enumListeCouplesGroupes.nextElement();
                GroupsPair coupleGroupes = (GroupsPair) obj1;
                Chromosome groupeCarteRef = coupleGroupes.getGroupeCarteRef();
                Chromosome groupeCarteAProj = coupleGroupes.getGroupeCarteAProj();
                //find corresponding common markers vector
                ArrayList commonMarkers = findCommonMarkersVector(groupeCarteRef);
                if (commonMarkers.size() > 1)
                {
                    //transform vector in an array
                    tabCorresp = vectorToTable(commonMarkers);
                    ESTProjector(groupeCarteAProj, groupeCarteRef, tabCorresp);
                }
            }
			carteProjetee.sort();
        }
        //update markers types for this map
        //carteProjetee.getMarkersSettingsClass().updateMarkerTypes();
        //adds the new projected map to JTree
        /*interfa.getLoadedMaps().addElement(carteProjetee);
        interfa.setCarteAffichee(carteProjetee);
        interfa.getTop().addChildToTree(selectedProject, interfa.getCarteAffichee(), true);
        Workspace currentWorkspace = interfa.getWorkspace();
        Project currentProject = currentWorkspace.getProject1(selectedProject);
        currentProject.addMap(carteProjetee);
        interfa.updateWorkspaceCfFile();
        interfa.displayMap(carteProjetee);
        outputCreation();
		 */
    }

	public MapGene	getResult(){
		return carteProjetee;
	}

    /**
     * recalcul des positions de marqueurs relativement a la carte de genes de r???f???rence et ajout
     * a la carte projetee (clone de carteRef)
     */
    public void markersProjector(Chromosome groupeCarteAProjeter, Chromosome groupeCarteRef, Object[][] tabCorresp)
    {
        outWriter.println("\n---MARKERS ON CHROMOSOME :" + groupeCarteRef.getName() + "\n");
        //Enumeration enumListeMarker = groupeCarteAProjeter.getListeMarker().elements();
		LinkageGroup lkgAProj = groupeCarteAProjeter.getDefaultLkg();
		for (Iterator<Locus> itProj = lkgAProj.iterator(); itProj.hasNext();) {
			Locus obj1 = itProj.next();
			boolean isMarqueurCommun = false;

            if ( obj1 instanceof Marker )
            {
                Marker marq = (Marker) obj1;

				if (! (marq.getType().equalsIgnoreCase("EST"))
                  & !(discardedMarkers.contains(marq.getName())) )//do not project discarded inverted markers
                {
                    double posActuelle = marq.getPosition();
                    double posSurCarteGenes = 0;
                    int derniereColonne = tabCorresp.length - 1;
                    double RGlobal = ( (Double) tabCorresp[derniereColonne][4] ).doubleValue();

                    /*si la position du marqueur se trouve avant le 1er marqueur commun-->
                     application du R global. Formule:
                     Qgene = MCommungene - (R * (MCommunqtl - Qqtl) ) ;
                     R = (McommunDernierGene-McommunPremierGene) / (McommunDernierQTL - MCommunPremierQTL) */
                    if ( posActuelle < ((Double)tabCorresp[0][2]).doubleValue() )
                    {
                        if (marq.getName().equals( (String) tabCorresp[0][0] ))
                        {isMarqueurCommun = true;}
                        else
                        {
                            posSurCarteGenes = ((Double) tabCorresp[0][1]).doubleValue() - RGlobal * ( ((Double)tabCorresp[0][2]).doubleValue() - posActuelle);
                            outWriter.println("New position for marker " + marq.getName() + " : " + posSurCarteGenes);
                        }
                    }

                    /*si la position du marqueur se trouve apres le DERNIER marqueur commun -->
                     application du R global. Formule:
                     Qgene = (R * (Qqtl - MCommunqtl ) ) + MCommungene ;
                     R = (McommunDernierGene-McommunPremierGene) / (McommunDernierQTL - MCommunPremierQTL)*/
                    if ( posActuelle > ((Double)tabCorresp[derniereColonne][2]).doubleValue() )
                    {
                        if (marq.getName().equals( (String) tabCorresp[derniereColonne][0]))
                        {isMarqueurCommun = true;}
                        else
                        {
                            posSurCarteGenes = RGlobal * ( posActuelle - ((Double)tabCorresp[derniereColonne][2]).doubleValue() ) + ((Double) tabCorresp[derniereColonne][1]).doubleValue() ;
                            outWriter.println("New position for marker " + marq.getName() + " : " + posSurCarteGenes);
                        }
                    }

                    /*si la position du marqueur se trouve dans les intervalles de marqueurs communs
                     formule: Qgene = (R * (Qqtl - MCommunqtl ) ) + MCommungene ;
                     R = (McommunGene[i-1]-McommunGene[i]) / (McommunQTL[i-1] - MCommunQTL[i])*/
                    for ( int i = 1; i < tabCorresp.length; i ++ )
                    {
                        if( (posActuelle >= ((Double) tabCorresp[i-1][2]).doubleValue()) & (posActuelle <= ( (Double) tabCorresp[i][2]).doubleValue()) )
                        {
                            if( (marq.getName().equals( (String) tabCorresp[i-1][0]))
                                                        | (marq.getName().equals( (String) tabCorresp[i][0])))
                            {isMarqueurCommun = true;}
                            else
                            {
								posSurCarteGenes = ((Double) tabCorresp[i-1][4]).doubleValue() * ( posActuelle - ((Double)tabCorresp[i-1][2]).doubleValue() ) + ((Double) tabCorresp[i-1][1]).doubleValue() ;
								outWriter.println("New position for marker " + marq.getName() + " : " + posSurCarteGenes);
                            }
                        }
                    }
                    if( isMarqueurCommun == false )
                    {
                        Marker newMarqueur = new Marker(marq.getName());
						newMarqueur.setPosition(posSurCarteGenes);
                        newMarqueur.setProjected(true);
                        /*if (marq.getOriginalMap() != "")
                        {
                            newMarqueur.setOriginalMap(marq.getOriginalMap());
                        }
                        else
                        {
                            newMarqueur.setOriginalMap(carteAProjeter.getName());
                        }*/
                        Chromosome groupeCarteProjetee = findHomologousGroup(groupeCarteRef);
                        //newMarqueur.setSonGroupe(groupeCarteProjetee);
						groupeCarteProjetee.getDefaultLkg().add(newMarqueur);
                    }
                }
            }
        }
    }

    /**
     * recalcul des positions de QTL relativement a la carte de genes et ajout
     * a une nouvelle carte (copie de carte de ref et ses marqueurs)
     */
    public void QTLProjector(Chromosome groupeCarteAProj, Chromosome groupeCarteRef, Object[][] tabCorresp)
    {
        outWriter.println("\n---QTL ON CHROMOSOME :" + groupeCarteRef.getName() + "\n");
        //Enumeration enumListeMarkerQTL = groupeCarteAProj.getListeMarker().elements();
		LinkageGroup lkgAProj = groupeCarteAProj.getDefaultLkg();
		for (Iterator<Locus> itProj = lkgAProj.iterator(); itProj.hasNext();) {
			Locus obj1 = itProj.next();

            if ( obj1 instanceof Qtl )
            {
                Qtl qtl = (Qtl) obj1;
                double posActuelle = qtl.getPosition();
                double ICDebutActuel = qtl.getPositionStart();
                double ICFinActuel = qtl.getPositionEnd();
                double posSurCarteGenes = 0;
                double ICDebutSurCarteGenes = 0;
                double ICFinSurCarteGenes = 0;
                int derniereColonne = tabCorresp.length - 1;
                double RGlobal = ( (Double) tabCorresp[derniereColonne][4] ).doubleValue();

                /*si une ou plsrs des 3 coordonnees du QTL se trouve avant le
                 1er marqueur commun--> application du R global
                 formule: Qgene = MCommungene - (R * (MCommunqtl - Qqtl) ) ;
                 R = (McommunDernierGene-McommunPremierGene) / (McommunDernierQTL - MCommunPremierQTL)*/
                if ( ICDebutActuel < ( (Double)tabCorresp[0][2]).doubleValue() )
                {
                    ICDebutSurCarteGenes = ((Double) tabCorresp[0][1]).doubleValue() - RGlobal * ( ((Double)tabCorresp[0][2]).doubleValue() - ICDebutActuel);
                    outWriter.println("New CI-beginning for QTL " + qtl.getTrait() + " : " + ICDebutSurCarteGenes);
                }

                if ( posActuelle < ((Double)tabCorresp[0][2]).doubleValue() )
                {
                    posSurCarteGenes = ((Double) tabCorresp[0][1]).doubleValue() - RGlobal * ( ((Double)tabCorresp[0][2]).doubleValue() - posActuelle);
                    outWriter.println("New position for QTL " + qtl.getTrait() + " : " + posSurCarteGenes);
                }

                if ( ICFinActuel < ((Double)tabCorresp[0][2]).doubleValue() )
                {
                    ICFinSurCarteGenes = ((Double) tabCorresp[0][1]).doubleValue() - RGlobal * ( ((Double)tabCorresp[0][2]).doubleValue() - ICFinActuel);
                    outWriter.println("New CI-end for QTL " + qtl.getTrait() + " : " + ICFinSurCarteGenes);
                }

                /*si une ou plsrs des 3 coordonnees du QTL se trouve apres le
                 DERNIER marqueur commun --> application du R global
                 formule: Qgene = (R * (Qqtl - MCommunqtl ) ) + MCommungene ;
                 R = (McommunDernierGene-McommunPremierGene) / (McommunDernierQTL - MCommunPremierQTL)*/
                if ( ICDebutActuel > ((Double)tabCorresp[derniereColonne][2]).doubleValue() )
                {
                    ICDebutSurCarteGenes = RGlobal * ( ICDebutActuel - ((Double)tabCorresp[derniereColonne][2]).doubleValue() ) + ((Double) tabCorresp[derniereColonne][1]).doubleValue();
                    outWriter.println("New CI-beginning for QTL " + qtl.getTrait() + " : " + ICDebutSurCarteGenes);
                }
                if ( posActuelle > ((Double)tabCorresp[derniereColonne][2]).doubleValue() )
                {
                    posSurCarteGenes = RGlobal * ( posActuelle - ((Double)tabCorresp[derniereColonne][2]).doubleValue() ) + ((Double) tabCorresp[derniereColonne][1]).doubleValue() ;
                    outWriter.println("New position for QTL " + qtl.getTrait() + " : " + posSurCarteGenes);
                }
                if ( ICFinActuel > ((Double)tabCorresp[derniereColonne][2]).doubleValue() )
                {
                    ICFinSurCarteGenes = RGlobal * ( ICFinActuel - ((Double)tabCorresp[derniereColonne][2]).doubleValue() ) + ((Double) tabCorresp[derniereColonne][1]).doubleValue();
                    outWriter.println("New CI-end for QTL " + qtl.getTrait() + " : " + ICFinSurCarteGenes);
                }

                /*si une ou plsrs des 3 coordonnees du QTL se trouve(nt) dans les
                 intervalles de marqueurs communs
                 formule: Qgene = (R * (Qqtl - MCommunqtl ) ) + MCommungene ;
                 R = (McommunGene[i-1]-McommunGene[i]) / (McommunQTL[i-1] - MCommunQTL[i])*/
                for ( int i = 1; i < tabCorresp.length; i ++ )
                {
                    if( (ICDebutActuel >= ((Double) tabCorresp[i-1][2]).doubleValue()) & (ICDebutActuel <= ( (Double) tabCorresp[i][2]).doubleValue()) )
                    {
                        ICDebutSurCarteGenes = ((Double) tabCorresp[i-1][4]).doubleValue() * ( ICDebutActuel - ((Double)tabCorresp[i-1][2]).doubleValue() ) + ((Double) tabCorresp[i-1][1]).doubleValue() ;
                        outWriter.println("New CI-beginning for QTL " + qtl.getTrait() + " : " + ICDebutSurCarteGenes);
                    }
                    if( (posActuelle >= ((Double) tabCorresp[i-1][2]).doubleValue()) & (posActuelle <= ( (Double) tabCorresp[i][2]).doubleValue()) )
                    {
                        posSurCarteGenes = ((Double) tabCorresp[i-1][4]).doubleValue() * ( posActuelle - ((Double)tabCorresp[i-1][2]).doubleValue() ) + ((Double) tabCorresp[i-1][1]).doubleValue() ;
                        outWriter.println("New position for QTL "+ qtl.getTrait() +" : " + posSurCarteGenes);
                    }
                    if( (ICFinActuel >= ((Double) tabCorresp[i-1][2]).doubleValue()) & (ICFinActuel <= ( (Double) tabCorresp[i][2]).doubleValue()) )
                    {
                        ICFinSurCarteGenes = ((Double) tabCorresp[i-1][4]).doubleValue() * ( ICFinActuel - ((Double)tabCorresp[i-1][2]).doubleValue() ) + ((Double) tabCorresp[i-1][1]).doubleValue() ;
                        outWriter.println("New CI-end for QTL " + qtl.getTrait() +" : " + ICFinSurCarteGenes);
                    }
                }
                //copy attributes of the original QTL in the projected QTL
                Qtl newQTL = qtl.copy();
                newQTL.setPosition( posSurCarteGenes );
                //newQTL.setProjected(true);
                /*if (qtl.getOriginalMap() != "")
                {
                    newQTL.setOriginalMap(qtl.getOriginalMap());
                }
                else
                {
                    newQTL.setOriginalMap(carteAProjeter.getName());
                }*/
                newQTL.setPositionStart( ICDebutSurCarteGenes );
                newQTL.setPositionEnd( ICFinSurCarteGenes );
                Chromosome groupeCarteProjetee = findHomologousGroup(groupeCarteRef);
                //newQTL.setSonGroupe(groupeCarteProjetee);
				groupeCarteProjetee.getDefaultLkg().add(newQTL);
            }
        }
    }

     /**
     * recalcul des positions d'EST relativement a la carte de genes de r???f???rence et ajout
     * a la carte de r???f???rence
     */
    public void ESTProjector(Chromosome groupeCarteEST, Chromosome groupeCarteRef, Object[][] tabCorresp)
    {
        outWriter.println("\n---EST ON CHROMOSOME :" + groupeCarteRef.getName() + "\n");
        //Enumeration enumListeMarker = groupeCarteEST.getListeMarker().elements();
		LinkageGroup lkgEST = groupeCarteEST.getDefaultLkg();
		for (Iterator<Locus> itEST = lkgEST.iterator(); itEST.hasNext();) {	//parcours du groupe de la carte a projeter
			Locus obj1 = itEST.next();

            if ( obj1 instanceof Marker )
            {
                Marker marq = (Marker) obj1;
                if (marq.getType().equalsIgnoreCase("EST"))
                {
                    double posActuelle = marq.getPosition();
                    double posSurCarteGenes = 0;
                    int derniereColonne = tabCorresp.length - 1;
                    double RGlobal = ( (Double) tabCorresp[derniereColonne][4] ).doubleValue();

                    /*si la position de l'EST se trouve avant le 1er marqueur commun-->
                     application du R global
                     formule: Qgene = MCommungene - (R * (MCommunqtl - Qqtl) ) ;
                     R = (McommunDernierGene-McommunPremierGene) / (McommunDernierQTL - MCommunPremierQTL)*/
                    if ( posActuelle < ((Double)tabCorresp[0][2]).doubleValue() )
                    {
                        posSurCarteGenes = ((Double) tabCorresp[0][1]).doubleValue() - RGlobal * ( ((Double)tabCorresp[0][2]).doubleValue() - posActuelle);
                        outWriter.println("New position for EST " + marq.getName() + " : " + posSurCarteGenes);
                    }

                    /*si la position de l'EST se trouve apres le DERNIER marqueur commun -->
                     application du R global
                     formule: Qgene = (R * (Qqtl - MCommunqtl ) ) + MCommungene ;
                     R = (McommunDernierGene-McommunPremierGene) / (McommunDernierQTL - MCommunPremierQTL)*/
                    if ( posActuelle > ((Double)tabCorresp[derniereColonne][2]).doubleValue() )
                    {
                        posSurCarteGenes = RGlobal * ( posActuelle - ((Double)tabCorresp[derniereColonne][2]).doubleValue() ) + ((Double) tabCorresp[derniereColonne][1]).doubleValue() ;
                        outWriter.println("New position for EST " + marq.getName() + " : " + posSurCarteGenes);
                    }

                    /*si la position de l'EST se trouve dans les intervalles de marqueurs communs
                     formule: Qgene = (R * (Qqtl - MCommunqtl ) ) + MCommungene ;
                     R = (McommunGene[i-1]-McommunGene[i]) / (McommunQTL[i-1] - MCommunQTL[i])*/
                    for ( int i = 1; i < tabCorresp.length; i ++ )
                    {
                        if( (posActuelle >= ((Double) tabCorresp[i-1][2]).doubleValue()) & (posActuelle <= ( (Double) tabCorresp[i][2]).doubleValue()) )
                        {
                            posSurCarteGenes = ((Double) tabCorresp[i-1][4]).doubleValue() * ( posActuelle - ((Double)tabCorresp[i-1][2]).doubleValue() ) + ((Double) tabCorresp[i-1][1]).doubleValue() ;
                            outWriter.println("New position for EST "+ marq.getName() +" : " + posSurCarteGenes);
                        }
                    }
                    Marker newEST = new Marker(marq.getName());
                    newEST.setPosition( posSurCarteGenes );
                    newEST.setType("EST");
                    newEST.setProjected(true);
                    /*if (marq.getOriginalMap() != "")
                    {
                        newEST.setOriginalMap(marq.getOriginalMap());
                    }
                    else
                    {
                        newEST.setOriginalMap(carteAProjeter.getNom());
                    }*/
                    Chromosome groupeCarteProjetee = findHomologousGroup(groupeCarteRef);
                    //newEST.setSonGroupe(groupeCarteProjetee);
                    groupeCarteProjetee.getDefaultLkg().add( newEST );
                }
            }
        }
    }



    /**
     * Find the group of carteProjetee homologous to the group of carteRef
     * given in argument
     */
    public Chromosome findHomologousGroup(Chromosome groupeCarteRef)
    {
		Chromosome matchedGroup = new Chromosome("TMP");
		matchedGroup.add(new LinkageGroup());

		for (Iterator<Chromosome> itProj = carteProjetee.iterator(); itProj.hasNext();) {
			Chromosome groupeCarteProjetee = itProj.next();

			if (groupeCarteProjetee.getName().equalsIgnoreCase(groupeCarteRef.getName())){
				matchedGroup = groupeCarteProjetee;
			}
		}

        return matchedGroup;
    }

     /**
     * Find the vector of common markers corresponding to the group of carteRef
     * given in argument
     */
    public ArrayList findCommonMarkersVector(Chromosome groupeCarteRef)
    {
        ArrayList commonMarkers = new ArrayList();
        Enumeration enumListeVectors = Collections.enumeration(commonMarkersVectorsList);
        while ( enumListeVectors.hasMoreElements() )
        {
            Object obj = enumListeVectors.nextElement();
            ArrayList commonMarkersVector = (ArrayList) obj;
            if (commonMarkersVector.size()>1){
            	CommonMarker cm = (CommonMarker) commonMarkersVector.get(0);
            	if ( cm.getGroupe().equals(groupeCarteRef))
            	{
                	commonMarkers = commonMarkersVector;
                	break;
            	}
        	}
        }
        return commonMarkers;
    }

    /**
     * Transforms the content of vector commonMarkers in an array
     * (speed up computations)
     */
    public Object[][] vectorToTable(ArrayList listeMarqueursCommuns)
    {
        Object tabCorres[][];
        int tailleVecteurListeMarqCommuns = listeMarqueursCommuns.size();
        tabCorres = new Object[tailleVecteurListeMarqCommuns][5];
        for ( int i = 0; i < tailleVecteurListeMarqCommuns; i ++ )
        {
            CommonMarker corresp = (CommonMarker) listeMarqueursCommuns.get(i);
            tabCorres[i][0] = (String) corresp.getNomMarqueur(); //[i][0] : nom marqueur commun
            tabCorres[i][1] = (Double) new Double( corresp.getPositionMarqueurG() ); //[i][1] : pos marqueur commun sur carte gene
            tabCorres[i][2] = (Double) new Double( corresp.getPositionMarqueurQ() ); //[i][2] : pos marqueur commun sur carte QTL
            tabCorres[i][3] = (Integer) new Integer( corresp.getRang() ); //[i][3] : rang du marqueur commun sur carte gene
            tabCorres[i][4] = (Double) new Double( corresp.getR() ); //[i][4] : facteur Proportionnalit??? dans l'intervalle entre ce marqueur et le suivant
        }
        return tabCorres;
    }

   }
