/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.biom_proj;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.Locus;


/**
 *
 * @author  Anne ARCADE
 * INRA SGV Moulon
 * @version 
 *
 * Quadruplet decrivant pour un couple de groupes de liaison: le marqueur commun,
 * sa position sur la carte de genes, sa position sur la carte de QTL, son rang
 * sur la carte de genes, le facteur de proportionnalit??? r
 *
 */

public class CommonMarker 
{
    /** Creates new CorrespMarqueur */
    public CommonMarker(Locus		marqG,
                        String		nomMarq,
                        double		positionMarqG,
                        double		positionMarqQ,
                        int			rangSurCarteG,
                        int			rangSurCarteAProj,
                        double		r,
                        Chromosome	groupeCarteRef)
    {
        marqueurG = marqG;
        nomMarqueur = nomMarq;/**/
        posMarqueurG = positionMarqG;
        posMarqueurQ = positionMarqQ;
        rangSurCarteGenes = rangSurCarteG;
        this.rangSurCarteAProj = rangSurCarteAProj;
        this.groupeCarteRef = groupeCarteRef;
    }
    
    public CommonMarker() {}
    
    //accesseurs
    public Locus getMarqueur()
    {
        return marqueurG;
    }
    public String getNomMarqueur()
    {
        return nomMarqueur;
    }
    
    public double getPositionMarqueurG()
    {
        return posMarqueurG;
    }
    
    public double getPositionMarqueurQ()
    {
        return posMarqueurQ;
    }
    
    public int getRang()
    {
        return rangSurCarteGenes;
    }
    
    public double getR()
    {
        return R;
    }
    
    public int getX()
    {
        return rangSurCarteGenes;
    }
    
    public int getY()
    {
        return rangSurCarteAProj;
    }
    
    public Chromosome getGroupe()
    {
        return this.groupeCarteRef;
    }
    
    
    public void setMarqueur(Locus marqueur)
    {
        marqueurG = marqueur;
    }
    
    public void setNomMarqueur(String nomMarqueur)
    {
        this.nomMarqueur = nomMarqueur;
    }
    
    public void setPositionMarqueurG(double posMarqueurG)
    {
        this.posMarqueurG = posMarqueurG;
    }
    
    public void setPositionMarqueurQ(double posMarqueurQ)
    {
        this.posMarqueurQ = posMarqueurQ;
    }
    
    public void setRang(int rang)
    {
        rangSurCarteGenes = rang;
    }
    
    public void setX(int rangSurCG)
    {
        rangSurCarteGenes = rangSurCG;
    }
    
    public void setY(int rangSurCAP)
    {
        rangSurCarteAProj = rangSurCAP;
    }
    
    public void setR(double R)
    {
        this.R = R;
    }
    
    public void setGroupe(Chromosome gl)
    {
        groupeCarteRef = gl;
    }

    private Locus		marqueurG;
    private String		nomMarqueur;
    private double		posMarqueurG;
    private double		posMarqueurQ;
    private int			rangSurCarteGenes;
    private int			rangSurCarteAProj;
    private double		R;
    private Chromosome	groupeCarteRef;
}
