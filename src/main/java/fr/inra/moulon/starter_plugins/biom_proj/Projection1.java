/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.biom_proj;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author sosnowski
 */
public class Projection1 {
    /** Creates new GroupsMatcher */
    public Projection1(	MapGene	mapRef,
						MapGene	mapToProj,
						String	dirName,
						boolean	markersProj,
						boolean	QTLProj,
						boolean	ESTProj,
						String	newMap,
						boolean	ignoreMarkInversion) {
        _mapRef = mapRef;
		_mapToProj = mapToProj;
        _dirName = dirName;
        _markersProj = markersProj;
        _QTLProj = QTLProj;
        _ESTProj = ESTProj;
        _newMapName = newMap;
        _ignoreMarkInversion = ignoreMarkInversion;
        _outputFile = new File(dirName + (File.separator) + newMap + "Report.out");
        System.out.println(_outputFile.getPath());
        //printWriter pour ecrire vers un fichier output avec messages de realisation ou d'erreur
        try
        {
           _outWriter = new PrintWriter(new BufferedWriter(new FileWriter(_outputFile)));
        }
        catch(java.io.IOException ioe){
			System.out.println(ioe.getLocalizedMessage());
		}

    }

    /**
     * looks for correspondence between the groups of the 2 maps
     */
    public void execute() throws Exception{
		_listeCouplesDeGroupes = new ArrayList();
		//Parcours des GL de la carte de genes un par un, et comparaison avec chacun des GL de la carte de QTL
		for (Iterator<Chromosome> itRef = _mapRef.iterator(); itRef.hasNext();) {
			Chromosome chrRef = itRef.next();

			boolean trouveGroupeCAProj = false;
			if (trouveGroupeCAProj == false){
				for (Iterator<Chromosome> itProj = _mapToProj.iterator(); itProj.hasNext();) {
					Chromosome chrToProj = itProj.next();

					if (chrRef.getName().equalsIgnoreCase(chrToProj.getName())){
						_outWriter.println("Correspondence between " + chrRef.getName() + " and " + chrToProj.getName());
						trouveGroupeCAProj = true;
						GroupsPair coupleGroupes = new GroupsPair(chrRef, chrToProj);
						_listeCouplesDeGroupes.add(coupleGroupes);
					}
				}
			}
			//Test avec les 2 derniers caracteres
			if (trouveGroupeCAProj == false && chrRef.getName().length() > 2){
				String subString = chrRef.getName().substring((chrRef.getName().length() - 2), chrRef.getName().length());
				for (Iterator<Chromosome> itProj = _mapToProj.iterator(); itProj.hasNext();) {
					Chromosome chrToProj = itProj.next();
					if (chrRef.getName().endsWith(subString)){
						_outWriter.println("Correspondence between " + chrRef.getName() + " and " + chrToProj.getName());
						trouveGroupeCAProj = true;
						GroupsPair coupleGroupes = new GroupsPair(chrRef, chrToProj);
						_listeCouplesDeGroupes.add(coupleGroupes);
					}
				}
			}
			//Test avec le dernier caractere
			if (trouveGroupeCAProj == false && chrRef.getName().length() > 1){
				String subString = chrRef.getName().substring((chrRef.getName().length() - 1), chrRef.getName().length());
				for (Iterator<Chromosome> itProj = _mapToProj.iterator(); itProj.hasNext();) {
					Chromosome chrToProj = itProj.next();
					if (chrRef.getName().endsWith(subString)){
						_outWriter.println("Correspondence between " + chrRef.getName() + " and " + chrToProj.getName());
						trouveGroupeCAProj = true;
						GroupsPair coupleGroupes = new GroupsPair(chrRef, chrToProj);
						_listeCouplesDeGroupes.add(coupleGroupes);
					}
				}
			}
		}

		/*int nombreCouplesGroupes = _listeCouplesDeGroupes.size();
		if (nombreCouplesGroupes != chrsRefNames.size()){
			_outWriter.println("WARNING: FAILED TO FIND A CORRESPONDENCE BETWEEN ALL CHROMOSOMES");
		}*/
		_outWriter.close();
		Projection2 proj = new Projection2(_listeCouplesDeGroupes,
											_mapRef,
											_mapToProj,
											_dirName,
											_outputFile,
											_markersProj,
											_QTLProj,
											_ESTProj,
											_newMapName,
											_ignoreMarkInversion
											);
		_mapRes = proj.getResult();
	}

	public MapGene	getResult(){
		return _mapRes;
	}


	private MapGene		_mapRef;
	private MapGene		_mapToProj;
	private MapGene		_mapRes;
    private String		_dirName;
    private ArrayList	_listeCouplesDeGroupes;
    private File		_outputFile;
    private PrintWriter	_outWriter;
    private boolean		_markersProj;
    private boolean		_QTLProj;
    private boolean		_ESTProj;
    private String		_newMapName;
    private boolean		_ignoreMarkInversion;

}
