/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.wizards;

import fr.inra.moulon.starter.gui.wizards.Wizard_Analysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.ControllerGUI;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.utils.HTMLTools;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter_plugins.tasks.TaskMetaQTL_MMapView;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTree;


public class Wizard_MetaQTL_MMapView extends Wizard_Analysis{
	private	final	static	Dimension	WIZARD_MAIN	= new Dimension(700, 300);

	public Wizard_MetaQTL_MMapView(String path, String dialogTitle){
		super(path, dialogTitle, true);
		super.addDialogSize(1, WIZARD_MAIN);
	}

	@Override
	public	long getAnalysisUID() {
		return 4L;
	}

	@Override
	protected	void			launch(int panelId){
		TaskMetaQTL_MMapView	task	= null;
		List<MapGene>			mapRef	= null;
		List<MapGene>			maps	= null;

		super.launch(panelId);
		if (1 == panelId){	// The analysis is to be launched
			maps = ControllerGUI.getCheckedMaps(_treeCheck);
			mapRef = ControllerGUI.getCheckedMaps(_treeRadio);

			if (null != maps && null != mapRef && mapRef.size() == 1){
				task = new TaskMetaQTL_MMapView(this);
				task.setMaps(maps, mapRef.get(0));
				task.setChromosomeName((String)_chrName.getSelectedItem());
				task.setOutputName(_fileResName.getText());
				task.execute();
				_task = task;
			}
		}
	}

	/**
	 * THE WIZARD'S MAIN ANALYSIS STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	@Override
	protected	final	void	addAnalysisComponentsOnPanel(final JPanel pane){
		GridBagConstraints		c		= new GridBagConstraints();
		JLabel					label	= null;
		JSeparator				sep		= null;
		int						iRow	= 0;

		_treeRadio = ControllerGUI.createCheckTree(false);
		_treeCheck = ControllerGUI.createCheckTree(true);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		label = new JLabel(HTMLTools.setFontBold(_bundle.getString("wizard_input_maps")));
		GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(label, c);

		label = new JLabel(HTMLTools.setFontBold(_bundle.getString("wizard_analysis_metaqtl_mmapview_map_ref")));
		GridBagLayoutUtils.set_gbc(c, 1, iRow, 1, 1, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(label, c);

		addFieldVerification(_treeRadio);
		GridBagLayoutUtils.set_gbc(c, 0, ++iRow, 1, 4, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.weightx = 1;
		pane.add(new JScrollPane(_treeRadio), c);

		addFieldVerification(_treeCheck);
		GridBagLayoutUtils.set_gbc(c, 1, iRow, 1, 4, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		pane.add(new JScrollPane(_treeCheck), c);

		c.weightx = 0.3;
		label = new JLabel("Mrkt (2)");
		GridBagLayoutUtils.set_gbc(c, 2, iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_mrkt = new JTextField("2");
		addFieldVerification(_mrkt, FIELD_INTEGER);
		GridBagLayoutUtils.set_gbc(c, 3, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_mrkt, c);

		label = new JLabel(_bundle.getString("misc_chr"));
		GridBagLayoutUtils.set_gbc(c, 2, ++iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		/**
		 * TODO
		 * Change the static names of chromosomes to dynamic.
		 */
		_chrName = new JComboBox(new String[]{"1","2","3","4","5","6","7","8","10"});
		GridBagLayoutUtils.set_gbc(c, 3, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_chrName, c);

		sep = new JSeparator(JSeparator.HORIZONTAL);
		GridBagLayoutUtils.set_gbc(c, 2, ++iRow, 2, 1, GridBagConstraints.HORIZONTAL);
		c.weighty = 0.5;
		pane.add(sep, c);

		label = new JLabel(_bundle.getString("misc_result_map"));
		GridBagLayoutUtils.set_gbc(c, 2, ++iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_fileResName = new JTextField("img_connect");
		addFieldVerification(_fileResName, FIELD_FILE_RES);
		GridBagLayoutUtils.set_gbc(c, 3, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_fileResName, c);
	}

	@Override
	public	String	getAnalysisName(){
		return _bundle.getString("wizard_analysis_metaqtl_mmapview_name");
	}

	private			JTextField		_mrkt			= null; // TextField for the mrkt parameter
	private			JTextField		_fileResName	= null; // TextField for the resulting file name
	private			JTree			_treeRadio		= null;	// Tree for the input map
	private			JTree			_treeCheck		= null;	// Tree for the input maps
	private			JComboBox		_chrName		= null;	// Tree for the input maps
	private	static	ResourceBundle	_bundle			= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);

	@Override
	protected void addResult(AnalysisResult res) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
