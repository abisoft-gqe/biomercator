/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.wizards;

import fr.inra.moulon.starter.gui.wizards.Wizard_Analysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.ControllerGUI;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.wizards.WizardQTLChoice;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter_plugins.tasks.TaskBioM_MetaAnalysis;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;


public class Wizard_BioM_MetaAnalysis extends Wizard_Analysis{
	private	final	static	Dimension	WIZARD_MAIN	= new Dimension(315, 300);

	public Wizard_BioM_MetaAnalysis(String path, String dialogTitle){
		super(path, dialogTitle, false);
		super.addDialogSize(0, WIZARD_MAIN);
	}

	@Override
	public	long getAnalysisUID() {
		return 2L;
	}

	@Override
	protected	void			launch(int panelId){
		TaskBioM_MetaAnalysis	task	= null;
		LinkageGroup			lkg		= null;
		Project					project	= null;

		super.launch(panelId);
		if (0 == panelId){	// The analysis is to be launched
			lkg = (LinkageGroup)_combos[3].getSelectedItem();
			project = (Project)_combos[0].getSelectedItem();

			_projectName = project.getName();
			task = new TaskBioM_MetaAnalysis(this);
			task.setDubiousQtl(_dubiousQtls);
			task.setMetaAName(_metaName.getText());
			task.setLkg(lkg);
			task.execute();
			_task = task;
		}
	}

	/**
	 * THE WIZARD'S MAIN ANALYSIS STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	@Override
	protected	final	void	addAnalysisComponentsOnPanel(final JPanel pane){
		GridBagConstraints		c			= new GridBagConstraints();
		JLabel					label		= null;
		JSeparator				sep			= null;
		JButton					qtlChoice	= new JButton(_bundle.getString("wizard_analysis_biom_meta_qtls"));

		_combos	= ControllerGUI.getLkgChoiceBoxes(qtlChoice);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		label = new JLabel(_bundle.getString("misc_meta_analysis"));
		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(label, c);

		_metaName = new JTextField();
		addFieldVerification(_metaName, FIELD_META_RES);
		GridBagLayoutUtils.set_gbc(c, 1, 0, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(_metaName, c);

		sep = new JSeparator(JSeparator.HORIZONTAL);
		GridBagLayoutUtils.set_gbc(c, 0, 1, 2, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.CENTER;
		pane.add(sep, c);


		label = new JLabel(_bundle.getString("misc_project"));
		GridBagLayoutUtils.set_gbc(c, 0, 2, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[0]);
		GridBagLayoutUtils.set_gbc(c, 1, 2, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[0], c);

		label = new JLabel(_bundle.getString("misc_map"));
		GridBagLayoutUtils.set_gbc(c, 0, 3, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[1]);
		GridBagLayoutUtils.set_gbc(c, 1, 3, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[1], c);

		label = new JLabel(_bundle.getString("misc_chr"));
		GridBagLayoutUtils.set_gbc(c, 0, 4, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[2]);
		GridBagLayoutUtils.set_gbc(c, 1, 4, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[2], c);

		label = new JLabel(_bundle.getString("misc_lkg"));
		GridBagLayoutUtils.set_gbc(c, 0, 5, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[3]);
		GridBagLayoutUtils.set_gbc(c, 1, 5, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[3], c);

		qtlChoice.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WizardQTLChoice wizard = new WizardQTLChoice(_bundle.getString("wizard_analysis_biom_meta_qtls"),
															((Container)_combos[0].getSelectedItem()).getName(),
															((Container)_combos[1].getSelectedItem()).getName(),
															((Container)_combos[2].getSelectedItem()).getName(),
															((Container)_combos[3].getSelectedItem()).getName(),
															_dubiousQtls);
				wizard.show();
				_dubiousQtls = wizard.getDubiousQtls();
			}
		});

		GridBagLayoutUtils.set_gbc(c, 0, 6, 2, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.CENTER;
		pane.add(qtlChoice, c);
	}

	@Override
	protected	void	addResult(AnalysisResult analysisResult){
		MetaAnalysis	metaA	= null;
		Project			project	= (Project)_combos[0].getSelectedItem();
		MapGene			mapGene	= (MapGene)_combos[1].getSelectedItem();
		LinkageGroup	lkg		= (LinkageGroup)_combos[3].getSelectedItem();

		if (AnalysisResult.META_ANALYSIS_ID == analysisResult.type){
			metaA = (MetaAnalysis)analysisResult.obj;
			lkg.addMetaAnalysis(metaA);
			FileManager.writeMap(mapGene, project.getName());
		}
	}

	@Override
	public	String	getAnalysisName(){
		return _bundle.getString("wizard_analysis_biom_meta_name");
	}

	private					List<Qtl>		_dubiousQtls		= null;
	private					JTextField		_metaName			= null; // TextField for the analysis map name
	private					JComboBox[]		_combos				= null;	// ComboBoxes for the linkage group to analysis
	private	static			ResourceBundle	_bundle				= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
