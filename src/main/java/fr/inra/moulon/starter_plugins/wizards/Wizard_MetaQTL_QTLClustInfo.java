/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.wizards;

import fr.inra.moulon.starter.gui.wizards.Wizard_Analysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.ControllerGUI;
import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter_plugins.tasks.TaskMetaQTL_QTLClustInfo;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;


public class Wizard_MetaQTL_QTLClustInfo extends Wizard_Analysis{
	private	final	static	Dimension	WIZARD_PANEL_SIZE	= new Dimension(450, 350);

	public Wizard_MetaQTL_QTLClustInfo(String path, String dialogTitle){
		super(path, dialogTitle, false);
		super.addDialogSize(0, WIZARD_PANEL_SIZE);
	}

	@Override
	public	long getAnalysisUID() {
		return 7L;
	}

	@Override
	protected	void				launch(int panelId){
		TaskMetaQTL_QTLClustInfo	task		= null;
		Project						project		= null;
		LinkageGroup				lkg			= null;
		Chromosome             chromosome = null;
		MetaAnalysis				metaA		= null;
		String						trait		= null;
		String						resFilePath	= null;

		super.launch(panelId);
		if (0 == panelId){	// The analysis is to be launched
			project	= (Project)(_combos[0].getSelectedItem());
			chromosome = (Chromosome) _combos[2].getSelectedItem();
			lkg		= (LinkageGroup)_combos[3].getSelectedItem();
			metaA	= (MetaAnalysis)_combos[4].getSelectedItem();
			trait	= (String)_combos[5].getSelectedItem();

			_projectName = project.getName();
			resFilePath = FileManager.DIR_SOFT_MAP
						+ _projectName + "/"
						+ metaA.getName() + "/"
						+ metaA.getName()
						+ "_res.txt";

			task = new TaskMetaQTL_QTLClustInfo(this);
			task.setParameters(	chromosome.getName()+"%"+lkg.getName(),
								trait,
								resFilePath,
								metaA,
								Integer.valueOf(_best.getText()),
								Integer.valueOf(_kMin.getText()),
								Integer.valueOf(_kMax.getText())
								);
			task.execute();
			_task = task;
		}
	}

	/**
	 * THE WIZARD'S MAIN ANALYSIS STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	@Override
	protected	final	void	addAnalysisComponentsOnPanel(final JPanel pane){
		GridBagConstraints		c		= new GridBagConstraints();
		JLabel					label	= null;
		JSeparator				sep		= null;
		int						iCol	= 0;
		int						iLine	= 0;

		_combos	= ControllerGUI.getMetaAResFileChoiceBoxes();

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		// LINKAGE GROUP
		label = new JLabel(_bundle.getString("misc_project"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[0]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[0], c);

		label = new JLabel(_bundle.getString("misc_map"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[1]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[1], c);

		label = new JLabel(_bundle.getString("misc_chr"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[2]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[2], c);

		label = new JLabel(_bundle.getString("misc_lkg"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[3]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[3], c);

		label = new JLabel(_bundle.getString("misc_meta_analysis"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[4]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[4], c);

		label = new JLabel(_bundle.getString("misc_trait"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[5]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[5], c);

		// SEPARATOR
		iCol += 2;
		iLine = 2;
		sep = new JSeparator(JSeparator.VERTICAL);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 5, GridBagConstraints.HORIZONTAL);
		c.weightx = 0.5;
		c.anchor = GridBagConstraints.CENTER;
		pane.add(sep, c);


		// PARAMETERS
		label = new JLabel("kMin");
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.weightx = 0.5;
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_kMin = new JTextField("1");
		addFieldVerification(_kMin, FIELD_INTEGER);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_kMin, c);

		label = new JLabel("kMax",new ImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_HELP)),JLabel.LEFT);
		label.setHorizontalTextPosition(JLabel.LEFT);
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);
		label.setToolTipText(_bundle.getString("wizard_analysis_metqtl_kmax"));

		_kMax = new JTextField("10");
		addFieldVerification(_kMax, FIELD_INTEGER);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_kMax, c);

		label = new JLabel("best");
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_best = new JTextField();
		addFieldVerification(_best, FIELD_INTEGER);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_best, c);

	}

	@Override
	protected	void	addResult(AnalysisResult analysisResult){
		Project			project	= (Project)_combos[0].getSelectedItem();
		MapGene			map		= (MapGene)_combos[1].getSelectedItem();

		if (AnalysisResult.META_ANALYSIS_ID == analysisResult.type){
			FileManager.writeMap(map, project.getName());
		}
	}

	@Override
	public	String	getAnalysisName(){
		return _bundle.getString("wizard_analysis_metqtl_meta2_2_name");
	}

	private			JComboBox[]		_combos	= null;	// ComboBoxes for the linkage group to analysis
	private			JTextField		_kMin	= null; // TextField for the "kMin" parameter
	private			JTextField		_kMax	= null; // TextField for the "kMax" parameter
	private			JTextField		_best	= null; // TextField for the "best" parameter
	private	static	ResourceBundle	_bundle	= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
