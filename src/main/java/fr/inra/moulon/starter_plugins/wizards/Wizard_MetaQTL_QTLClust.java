/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.wizards;

import fr.inra.moulon.starter.gui.wizards.Wizard_Analysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.ControllerGUI;
import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.JFileChooserTools;
import fr.inra.moulon.starter.gui.wizards.WizardQTLChoice;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter_plugins.tasks.TaskMetaQTL_QTLClust;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;


public class Wizard_MetaQTL_QTLClust extends Wizard_Analysis{
	private	final	static	Dimension	WIZARD_PANEL_SIZE	= new Dimension(600, 450);

	public Wizard_MetaQTL_QTLClust(String path, String dialogTitle){
		super(path, dialogTitle, false);
		super.addDialogSize(0, WIZARD_PANEL_SIZE);
	}

	@Override
	public	long getAnalysisUID() {
		return 7L;
	}

	@Override
	protected	void			launch(int panelId){
		TaskMetaQTL_QTLClust	task				= null;
		MapGene					map					= null;
		Chromosome				chr					= null;
		LinkageGroup			lkg					= null;
		Project					project				= null;
		String					ontologyFileName	= null;

		super.launch(panelId);
		if (0 == panelId){	// The analysis is to be launched
			project	= (Project)_combos[0].getSelectedItem();
			map		= (MapGene)_combos[1].getSelectedItem();
			chr		= (Chromosome)_combos[2].getSelectedItem();
			lkg		= (LinkageGroup)_combos[3].getSelectedItem();
			_projectName = project.getName();

			// Getting the correct ontology group type
			switch (_groupType){
				case NONE:
					break;
				case ALL:
					//Create ontology file for all traits
					ontologyFileName = createOntologyFile(lkg, _metaTraitName.getText());
					break;
				case FILE:
					ontologyFileName = _ontology.getText();
			}

			// Launching analysis
			task = new TaskMetaQTL_QTLClust(this);
			task.setParameters(	_metaName.getText(),
								map,
								lkg,
								chr.getName(),
								lkg.getName(),
								Integer.valueOf(_kMax.getText()),
								Integer.valueOf(_ci_mode.getText()),
								Integer.valueOf(_ci_miss.getText()),
								Integer.valueOf(_emrs.getText()),
								Double.valueOf(_emeps.getText()),
								ontologyFileName,
								_dubiousQtls);
			task.execute();
			_task = task;
		}
	}

	private	String	createOntologyFile(LinkageGroup lkg, String metaTrait){
		VisitorEltGet<Qtl>	v			= new VisitorEltGet<Qtl>(Qtl.class);
		String				fileName	= FileManager.DIR_SOFT_TMP + "ontology_" + metaTrait + ".xml";
		Set<String>			traits		= new HashSet<String>();
		SAXWriter			writer		= new SAXWriter(fileName);
		int					idTrait		= 0;

		lkg.accept(v);
		for (Iterator<Content> it = v.getList().iterator(); it.hasNext();) {
			traits.add(((Qtl)it.next()).getTrait());
		}

		writer.startDocument();
		writer.startElement(ONTOLOGY, "name", "");
		writer.startElement(ONTOLOGY_TERM,
							new String[]{"name", "id"},
							new String[]{"trait ontology", String.valueOf(++idTrait)});

		writer.addElement(	PROPERTY,
							new String[]{"name", "value"},
							new String[]{"term.synonyms", ""});
		writer.addElement(	PROPERTY,
							new String[]{"name", "value"},
							new String[]{"term.definition", ""});

		writer.startElement(ONTOLOGY_TERM,
							new String[]{"name", "id"},
							new String[]{metaTrait, String.valueOf(++idTrait)});

		writer.addElement(	PROPERTY,
							new String[]{"name", "value"},
							new String[]{"term.synonyms", "metaTrait from MetaAnalysis"});
		writer.addElement(	PROPERTY,
							new String[]{"name", "value"},
							new String[]{"term.definition", ""});


		for (Iterator<String> it = traits.iterator(); it.hasNext();) {
			String trait = it.next();

			writer.startElement(ONTOLOGY_TERM,
								new String[]{"name", "id"},
								new String[]{trait, String.valueOf(++idTrait)});
			writer.addElement(	PROPERTY,
								new String[]{"name", "value"},
								new String[]{"term.synonyms", trait});
			writer.addElement(	PROPERTY,
								new String[]{"name", "value"},
								new String[]{"term.definition", ""});
			writer.endElement(ONTOLOGY_TERM);

		}
		writer.endElement(ONTOLOGY_TERM);
		writer.endElement(ONTOLOGY_TERM);
		writer.endElement(ONTOLOGY);
		writer.endDocument();

		return fileName;
	}

	private	static	String	ONTOLOGY		= "ontology";
	private	static	String	ONTOLOGY_TERM	= "ontology-term";
	private	static	String	PROPERTY		= "property";

	/**
	 * THE WIZARD'S MAIN ANALYSIS STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	@Override
	protected	final	void	addAnalysisComponentsOnPanel(final JPanel pane){
		GridBagConstraints		c				= new GridBagConstraints();
		JButton					qtlChoice		= new JButton(_bundle.getString("wizard_analysis_biom_meta_qtls"));
		JRadioButton			radio			= null;
		ButtonGroup				radioGrp		= new ButtonGroup();
		JLabel					label			= null;
		JSeparator				sep				= null;
		int						iCol			= 0;
		int						iLine			= 0;

		_combos	= ControllerGUI.getLkgChoiceBoxes(qtlChoice);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		label = new JLabel(_bundle.getString("misc_meta_analysis"));
		GridBagLayoutUtils.set_gbc(c, 0, 0, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(label, c);

		_metaName = new JTextField();
		addFieldVerification(_metaName, FIELD_META_RES);
		GridBagLayoutUtils.set_gbc(c, 2, iLine++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(_metaName, c);

		sep = new JSeparator(JSeparator.HORIZONTAL);
		GridBagLayoutUtils.set_gbc(c, 0, iLine++, 4, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.CENTER;
		pane.add(sep, c);

		label = new JLabel(_bundle.getString("misc_project"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[0]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[0], c);

		label = new JLabel(_bundle.getString("misc_map"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[1]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[1], c);

		label = new JLabel(_bundle.getString("misc_chr"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[2]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[2], c);

		label = new JLabel(_bundle.getString("misc_lkg"));
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		addFieldVerification(_combos[3]);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combos[3], c);

		qtlChoice.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				WizardQTLChoice wizard = new WizardQTLChoice(_bundle.getString("wizard_analysis_biom_meta_qtls"),
															((Container)_combos[0].getSelectedItem()).getName(),
															((Container)_combos[1].getSelectedItem()).getName(),
															((Container)_combos[2].getSelectedItem()).getName(),
															((Container)_combos[3].getSelectedItem()).getName(),
															_dubiousQtls);
				wizard.show();
				_dubiousQtls = wizard.getDubiousQtls();
			}
		});

		GridBagLayoutUtils.set_gbc(c, 0, 6, 2, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.CENTER;
		pane.add(qtlChoice, c);

		// SEPARATOR
		iCol += 2;
		iLine = 2;
		c.insets = new Insets(10, 0, 10, 0);
		sep = new JSeparator(JSeparator.VERTICAL);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 5, GridBagConstraints.HORIZONTAL);
		c.weightx = 0.5;
		c.anchor = GridBagConstraints.CENTER;
		pane.add(sep, c);
		c.insets = new Insets(0, 0, 0, 0);

		// PARAMETERS
		label = new JLabel("kMax");
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.weightx = 0.5;
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_kMax = new JTextField("10");
		addFieldVerification(_kMax, FIELD_INTEGER);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_kMax, c);

		label = new JLabel("ci mode");
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_ci_mode = new JTextField("1");
		addFieldVerification(_ci_mode, FIELD_INTEGER);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_ci_mode, c);

		label = new JLabel("ci miss");
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_ci_miss = new JTextField("1");
		addFieldVerification(_ci_miss, FIELD_INTEGER);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_ci_miss, c);

		label = new JLabel("emrs");
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_emrs = new JTextField("50");
		addFieldVerification(_emrs, FIELD_INTEGER);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_emrs, c);

		label = new JLabel("emeps");
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_emeps = new JTextField("1.e-8");
		addFieldVerification(_emeps, FIELD_DOUBLE);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iLine++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_emeps, c);

		///////////////////////////////////////////////
		sep = new JSeparator(JSeparator.HORIZONTAL);
		c.insets = new Insets(10, 0, 10, 0);
		GridBagLayoutUtils.set_gbc(c, 0, iLine++, 5, 1, GridBagConstraints.HORIZONTAL);
		c.weightx = 0.5;
		c.anchor = GridBagConstraints.CENTER;
		pane.add(sep, c);
		c.insets = new Insets(0, 0, 0, 0);
		///////////////////////////////////////////////

		// ONTOLOGY
		//  Radio 1
		c.insets = new Insets(10, 0, 0, 0);
		_groupType = OntologyGroupType.NONE;
		radio = new JRadioButton(_bundle.getString("wizard_analysis_metqtl_meta1_2_regroup_none"), true);
		radioGrp.add(radio);
		GridBagLayoutUtils.set_gbc(c, 0, iLine++, 2, 1, GridBagConstraints.HORIZONTAL);
		pane.add(radio, c);
		setRadioListener(radio, OntologyGroupType.NONE);

		//  radio 2
		c.insets = new Insets(10, 0, 0, 0);
		radio = new JRadioButton(_bundle.getString("wizard_analysis_metqtl_meta1_2_regroup_all"));
		radioGrp.add(radio);
		GridBagLayoutUtils.set_gbc(c, 0, iLine++, 2, 1, GridBagConstraints.HORIZONTAL);
		pane.add(radio, c);
		setRadioListener(radio, OntologyGroupType.ALL);

		c.insets = new Insets(0, 30, 0, 0);
		label = new JLabel(_bundle.getString("misc_meta_trait"));
		GridBagLayoutUtils.set_gbc(c, 0, iLine, 2, 1, GridBagConstraints.HORIZONTAL);
		pane.add(label, c);

		_metaTraitName = new JTextField();
		GridBagLayoutUtils.set_gbc(c, 1, iLine++, 2, 1, GridBagConstraints.HORIZONTAL);
		pane.add(_metaTraitName, c);

		//  radio 3
		c.insets = new Insets(10, 0, 0, 0);
		radio = new JRadioButton(_bundle.getString("wizard_analysis_metqtl_meta1_2_regroup_file"));
		radioGrp.add(radio);
		GridBagLayoutUtils.set_gbc(c, 0, iLine++, 2, 1, GridBagConstraints.HORIZONTAL);
		pane.add(radio, c);
		setRadioListener(radio, OntologyGroupType.FILE);

		_ontologyChoice	= new JButton(_bundle.getString("wizard_analysis_metqtl_meta1_2_choose_onto"));
		_ontologyChoice.setEnabled(false);
		c.insets = new Insets(0, 30, 0, 0);
		GridBagLayoutUtils.set_gbc(c, 0, iLine, 2, 1, GridBagConstraints.HORIZONTAL);
		pane.add(_ontologyChoice, c);

		_ontologyChoice.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser	fileChooser = null;

				fileChooser = JFileChooserTools.createFileChooser(	_bundle.getString("wizard_analysis_metqtl_meta1_2_choose_onto"),
																	_bundle.getString("misc_choose"),
																	false,
																	null);
				if (fileChooser.showOpenDialog(pane) == JFileChooser.APPROVE_OPTION){
					Session.instance().setValue(Session.CHOOSER_FILE_DIR, fileChooser.getCurrentDirectory());
					_ontology.setText(fileChooser.getSelectedFile().getAbsolutePath());
				}
			}
		});

		c.insets = new Insets(0, 0, 0, 0);
		_ontology = new JTextField();
		_ontology.setEnabled(false);
		GridBagLayoutUtils.set_gbc(c, iCol, iLine++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.CENTER;
		pane.add(_ontology, c);
	}

	@Override
	protected	void	addResult(AnalysisResult analysisResult){
		MetaAnalysis	metaA	= null;
		Project			project	= (Project)_combos[0].getSelectedItem();
		MapGene			mapGene	= (MapGene)_combos[1].getSelectedItem();
		LinkageGroup	lkg		= (LinkageGroup)_combos[3].getSelectedItem();

		if (AnalysisResult.META_ANALYSIS_ID == analysisResult.type){
			metaA = (MetaAnalysis)analysisResult.obj;
			lkg.addMetaAnalysis(metaA);
			FileManager.writeMap(mapGene, project.getName());
		}
	}


	private	void	setRadioListener(JRadioButton radio, final OntologyGroupType type){
		radio.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					_groupType = type;
					switch (type){
						case NONE:
							_ontology.setEnabled(false);
							_ontologyChoice.setEnabled(false);
							_metaTraitName.setEnabled(false);
							break;
						case ALL:
							_ontology.setEnabled(false);
							_ontologyChoice.setEnabled(false);
							_metaTraitName.setEnabled(true);
							break;
						case FILE:
							_ontology.setEnabled(true);
							_ontologyChoice.setEnabled(true);
							_metaTraitName.setEnabled(false);
							break;
					}
				}
			}
		});
	}

	@Override
	public	String	getAnalysisName(){
		return _bundle.getString("wizard_analysis_metqtl_meta1_2_name");
	}

	enum OntologyGroupType {NONE, ALL, FILE};

	private			List<Qtl>			_dubiousQtls		= null;
	private			JTextField			_metaName			= null; // TextField for the analysis map name
	private			JComboBox[]			_combos				= null;	// ComboBoxes for the linkage group to analysis
	private			JTextField			_kMax				= null; // TextField for the "kMax" parameter
	private			JTextField			_ci_mode			= null; // TextField for the "ci mode" parameter
	private			JTextField			_ci_miss			= null; // TextField for the "ci miss" parameter
	private			JTextField			_emrs				= null; // TextField for the "emrs" parameter
	private			JTextField			_emeps				= null; // TextField for the "emeps" parameter
	private			JTextField			_metaTraitName		= null;	// TextField for the meta trait name
	private			JTextField			_ontology			= null;	// TextField for the "ontology" file path
	private			JButton				_ontologyChoice		= null;	// Button for ontology file browsing
	private	static	OntologyGroupType	_groupType			= OntologyGroupType.NONE;
	private	static	ResourceBundle		_bundle				= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
