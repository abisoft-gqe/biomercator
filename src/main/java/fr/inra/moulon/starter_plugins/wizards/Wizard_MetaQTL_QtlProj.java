/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.wizards;

import fr.inra.moulon.starter.gui.wizards.Wizard_Analysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.ControllerGUI;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.utils.HTMLTools;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter_plugins.tasks.TaskMetaQTL_QtlProj;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTree;


public class Wizard_MetaQTL_QtlProj extends Wizard_Analysis{
	private	final	static	Dimension	WIZARD_MAIN	= new Dimension(600, 400);

	public Wizard_MetaQTL_QtlProj(String path, String dialogTitle){
		super(path, dialogTitle, true);
		super.addDialogSize(1, WIZARD_MAIN);
	}

	@Override
	public	long getAnalysisUID() {
		return 6L;
	}

	@Override
	protected void resetWizardParameters() {
		super.resetWizardParameters();
		_combos = ControllerGUI.getProjectNMapComboBoxes();
	}

	@Override
	protected	void		launch(int panelId){
		TaskMetaQTL_QtlProj	task	= null;
		MapGene				mapRef	= null;
		List<MapGene>		maps	= null;

		super.launch(panelId);
		if (1 == panelId){	// The analysis is to be launched
			maps = ControllerGUI.getCheckedMaps(_tree);
			mapRef = (MapGene)_combos[1].getSelectedItem();
			task = new TaskMetaQTL_QtlProj(this);
			
			task.setMaps(maps, mapRef);
			task.setMapName(_mapResName.getText());
			task.execute();
			_task = task;
		}
	}

	/**
	 * THE WIZARD'S MAIN ANALYSIS STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	@Override
	protected	final	void	addAnalysisComponentsOnPanel(final JPanel pane){
		GridBagConstraints		c		= new GridBagConstraints();
		JLabel					label	= null;
		JSeparator				sep		= null;

		_tree = ControllerGUI.createCheckTree(true);
		addFieldVerification(_tree);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.CENTER;
		pane.add(new JLabel(HTMLTools.setFontBold(_bundle.getString("wizard_input_maps_qtls"))), c);

		GridBagLayoutUtils.set_gbc(c, 0, 1, 1, 6, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.weightx = 1;
		c.insets = new Insets(0, 0, 0, 10);
		pane.add(new JScrollPane(_tree), c);
		c.insets = new Insets(0, 0, 0, 0);

		c.weightx = 0.2;
		label = new JLabel(_bundle.getString("misc_project"));
		GridBagLayoutUtils.set_gbc(c, 1, 0, 4, 1, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(new JLabel(HTMLTools.setFontBold(_bundle.getString("wizard_target_map_qtls"))), c);
		GridBagLayoutUtils.set_gbc(c, 1, 1, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		c.weightx = 0.4;
		_combos[0].setSize(_WIZARD_COMBO);
		addFieldVerification(_combos[0]);
		GridBagLayoutUtils.set_gbc(c, 2, 1, 1, 1, GridBagConstraints.NONE);
		pane.add(_combos[0], c);

		c.weightx = 0;
		label = new JLabel(_bundle.getString("misc_map"));
		GridBagLayoutUtils.set_gbc(c, 1, 2, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_combos[1].setSize(_WIZARD_COMBO);
		addFieldVerification(_combos[1]);
		GridBagLayoutUtils.set_gbc(c, 2, 2, 1, 1, GridBagConstraints.NONE);
		pane.add(_combos[1], c);

		label = new JLabel("Ratio (0.25)");
		GridBagLayoutUtils.set_gbc(c, 1, 3, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_ratio = new JTextField("0.25");
		addFieldVerification(_ratio, FIELD_DOUBLE);
		GridBagLayoutUtils.set_gbc(c, 2, 3, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_ratio, c);

		label = new JLabel("pValue (0.5)");
		GridBagLayoutUtils.set_gbc(c, 1, 4, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_pValue = new JTextField("0.5");
		addFieldVerification(_pValue, FIELD_DOUBLE);
		GridBagLayoutUtils.set_gbc(c, 2, 4, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_pValue, c);

		// FOURTH LINE
		sep = new JSeparator(JSeparator.HORIZONTAL);
		GridBagLayoutUtils.set_gbc(c, 1, 5, 2, 1, GridBagConstraints.HORIZONTAL);
		c.weighty = 0.5;
		pane.add(sep, c);

		// SIXTH LINE
		label = new JLabel(_bundle.getString("misc_result_map"));
		GridBagLayoutUtils.set_gbc(c, 1, 6, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_mapResName = new JTextField();
		addFieldVerification(_mapResName, FIELD_MAP_RES);
		GridBagLayoutUtils.set_gbc(c, 2, 6, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_mapResName, c);
	}

	@Override
	public	String	getAnalysisName(){
		return _bundle.getString("wizard_analysis_metaqtl_qtlproj_name");
	}

	private					JTextField		_mapResName			= null; // TextField for the resulting map name
	private					JTextField		_ratio				= null; // TextField for the ratio parameter
	private					JTextField		_pValue				= null; // TextField for the pValue parameter
	private					JTree			_tree				= null;	// Tree for the input maps
	private					JComboBox[]		_combos				= null;	// ComboBoxes for the reference map
	private	final	static	Dimension		_WIZARD_COMBO		= new Dimension(50, 20);
	private	static			ResourceBundle	_bundle			= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);

	@Override
	protected void addResult(AnalysisResult res) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
