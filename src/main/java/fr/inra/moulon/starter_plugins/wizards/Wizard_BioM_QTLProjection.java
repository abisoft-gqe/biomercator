/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.wizards;

import fr.inra.moulon.starter.gui.wizards.Wizard_Analysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.ControllerGUI;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter_plugins.tasks.TaskBioM_QTLProjection;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;


public class Wizard_BioM_QTLProjection extends Wizard_Analysis{
	private	final	static	Dimension	WIZARD_MAIN	= new Dimension(520, 300);

	public Wizard_BioM_QTLProjection(String path, String dialogTitle){
		super(path, dialogTitle, true);
		super.addDialogSize(1, WIZARD_MAIN);
	}

	@Override
	public	long getAnalysisUID() {
		return 1L;
	}

	@Override
	protected	void	launch(int panelId){
		TaskBioM_QTLProjection	task = null;

		super.launch(panelId);
		if (1 == panelId){	// The analysis is to be launched
			MapGene	mapRef	= (MapGene)_combosRef[1].getSelectedItem();
			MapGene	mapFrom	= (MapGene)_combosProj[1].getSelectedItem();

			task = new TaskBioM_QTLProjection(this);
			task.setMaps(mapRef, mapFrom);
			task.setMapName(_mapResName.getText());
			task.setParams(_autoInversionCheck.isSelected());
			task.execute();
			_task = task;
		}
	}

	/**
	 * THE WIZARD'S MAIN ANALYSIS STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	@Override
	protected	final	void	addAnalysisComponentsOnPanel(final JPanel pane){
		GridBagConstraints		c			= new GridBagConstraints();
		JLabel					label		= new JLabel();
		JSeparator				sep			= null;
		int						iCol		= 0;
		int						iLine		= 0;
		Font					boldFont	= label.getFont().deriveFont(Font.BOLD);

		_combosRef	= ControllerGUI.getProjectNMapComboBoxes();
		_combosProj	= ControllerGUI.getProjectNMapComboBoxes();

		addFieldVerification(_combosRef[0]);
		addFieldVerification(_combosRef[1]);
		addFieldVerification(_combosProj[0]);
		addFieldVerification(_combosProj[1]);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		// FISRT LINE
		iCol = 0;
		iLine = 0;
		c.weightx = 0.4;
		c.weighty = 0.7;

		label = new JLabel(_bundle.getString("wizard_analysis_biom_proj_map_ref"));
		label.setFont(boldFont);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(label, c);

		iCol++;
		sep = new JSeparator(JSeparator.VERTICAL);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 3, GridBagConstraints.VERTICAL);
		pane.add(sep, c);


		label = new JLabel(_bundle.getString("wizard_analysis_biom_proj_map_proj"));
		label.setFont(boldFont);
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(label, c);

		// SECOND LINE
		iCol = 0;
		iLine = 1;
		c.weighty = 0;

		label = new JLabel(_bundle.getString("misc_project"));
		label.setFont(boldFont);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_combosRef[0].setSize(_WIZARD_COMBO);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(_combosRef[0], c);

		iCol++;
		_combosProj[0].setSize(_WIZARD_COMBO);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_END;
		pane.add(_combosProj[0], c);

		label = new JLabel(_bundle.getString("misc_project"));
		label.setFont(boldFont);
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		// THIRD LINE
		iCol = 0;
		iLine = 2;

		label = new JLabel(_bundle.getString("misc_map"));
		label.setFont(boldFont);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_combosRef[1].setSize(_WIZARD_COMBO);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(_combosRef[1], c);

		iCol++;
		_combosProj[1].setSize(_WIZARD_COMBO);
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_END;
		pane.add(_combosProj[1], c);

		label = new JLabel(_bundle.getString("misc_map"));
		label.setFont(boldFont);
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		// FOURTH LINE
		iCol = 0;
		iLine = 3;

		c.weighty = 1;
		sep = new JSeparator(JSeparator.HORIZONTAL);
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 5, 1, GridBagConstraints.HORIZONTAL);
		pane.add(sep, c);

		// FIFTH LINE
		iCol = 0;
		iLine = 4;
		c.weighty = 0;

		label = new JLabel(_bundle.getString("wizard_analysis_biom_proj_auto_inversion"));
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		iCol++;
		_autoInversionCheck = new JCheckBox();
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_autoInversionCheck, c);

		// SIXTH LINE
		iCol = 0;
		iLine = 5;

		label = new JLabel(_bundle.getString("misc_result_map"));
		GridBagLayoutUtils.set_gbc(c, iCol++, iLine, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		iCol++;
		_mapResName = new JTextField();
		addFieldVerification(_mapResName, FIELD_MAP_RES);
		GridBagLayoutUtils.set_gbc(c, iCol, iLine, 2, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_mapResName, c);
	}

	@Override
	public	String	getAnalysisName(){
		return _bundle.getString("wizard_analysis_biom_proj_name");
	}

	private					JCheckBox		_autoInversionCheck	= null;	// CheckBox for automatic markers inversions resolution
	private					JTextField		_mapResName			= null; // TextField for the resulting map name
	private					JComboBox[]		_combosProj			= null;	// ComboBoxes for the map to project
	private					JComboBox[]		_combosRef			= null;	// ComboBoxes for the reference map
	private	final	static	Dimension		_WIZARD_COMBO		= new Dimension(50, 20);
	private	static			ResourceBundle	_bundle				= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);

	@Override
	protected void addResult(AnalysisResult res) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
