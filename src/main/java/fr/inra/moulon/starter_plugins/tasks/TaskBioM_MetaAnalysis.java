/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 8, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.tasks;

import fr.inra.moulon.starter.gui.wizards.TaskAnalysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.logging.Parameters;
import fr.inra.moulon.starter_plugins.biom_meta_a.TreatmentMetaAnalysis;
import fr.inra.moulon.starter_plugins.wizards.Wizard_BioM_MetaAnalysis;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;


public class TaskBioM_MetaAnalysis extends TaskAnalysis {
	public	TaskBioM_MetaAnalysis(Wizard_BioM_MetaAnalysis wizard){
		super(wizard);
	}

	@Override
	public	String	getAnalysisName(){
		return "Meta analysis - Gerber & Goffinet";
	}

	public void	setLkg(LinkageGroup lkg){
		_lkg = lkg;
	}

	public void	setDubiousQtl(List<Qtl> dubiousQtls){
		_dubiousQtls = dubiousQtls;
	}

	public	void	setMetaAName(String metaAName){
		_metaAName = metaAName;
	}

	@Override
	public	void				executeAnalysis() throws Exception{
		TreatmentMetaAnalysis	metaA		= new TreatmentMetaAnalysis(_lkg, _dubiousQtls);
		File					outputDir	= new File(FileManager.DIR_SOFT_TMP + "1");

		FileManager.createDirectoryIfNeeded(outputDir.getAbsolutePath());
		FileManager.emptyDirectory(outputDir);

		_outputPath = outputDir.getAbsolutePath() + "/" + _metaAName + "/";
		FileManager.createDirectoryIfNeeded(_outputPath);

		metaA.execute();
		_metaAnalysis = metaA.getResults();
		_metaAnalysis.setName(_metaAName);
		_outputFile = metaA.createFileTable(_outputPath + "/" + _metaAName + ".txt");
	}

	@Override
	public void fillAnalysisResultList() {
		_results.add(new AnalysisResult(AnalysisResult.DIRECTORY_ID, _outputPath));
		_results.add(new AnalysisResult(AnalysisResult.META_ANALYSIS_ID, _metaAnalysis));
	}

	@Override
	protected	Parameters	getInputsPrettyPrint(){
		Parameters			inputs		= new Parameters();
		List				qtlsList	= null;
		SortedSet<String>	qtlsSet		= new TreeSet<String>();

		qtlsList = Controller.getElements(_lkg, Qtl.class);
		for (Iterator it = qtlsList.iterator(); it.hasNext();) {
			Qtl qtl = (Qtl)it.next();
			qtlsSet.add(qtl.getName());
		}

		if (null != _dubiousQtls){
			for (Iterator<Qtl> it = _dubiousQtls.iterator(); it.hasNext();) {
				String qtlName = it.next().getName();

				if (qtlsSet.contains(qtlName)){
					qtlsSet.remove(qtlName);
				}
			}
		}

		inputs.addParam("Meta analysis version", "Gerber & Goffinet");
		inputs.addParam("Linkage group used", _lkg.getName());
		inputs.addParam("Output meta analysis name", _metaAName);
		inputs.addParam("Qtls used", qtlsSet);

		return inputs;
	}

	@Override
	protected	Parameters	getOutputsPrettyPrint(){
		Parameters			outputs = new Parameters();

		if (null != _metaAnalysis){
			outputs.addParam("Meta analysis", "Successful");
			outputs.addParam("Meta analysis name", _metaAName);
		} else{
			outputs.addParam("Meta analysis", "Problem");
		}

		return outputs;
	}

	private	File			_outputFile		= null;
	private	MetaAnalysis	_metaAnalysis	= null;
	private	LinkageGroup	_lkg			= null;
	private	List<Qtl>		_dubiousQtls	= null;
	private	String			_metaAName		= null;
	private	String			_outputPath		= null;
}
