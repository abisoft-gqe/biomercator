/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 8, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.tasks;

import fr.inra.moulon.starter.gui.wizards.TaskAnalysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.logging.Parameters;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_QtlProj;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;


import java.util.List;
import org.metaqtl.main.QTLProj;


public class TaskMetaQTL_QtlProj extends TaskAnalysis {
	private	List<MapGene>	_maps		= null;
	private	MapGene			_mapRef		= null;
	private	double			_ratio		= 0.5;
	private	double			_pValue		= 0.01;
	private	String			_mapName	= null;

	public	TaskMetaQTL_QtlProj(Wizard_MetaQTL_QtlProj wizard){
		super(wizard);
	}

	@Override
	public	String	getAnalysisName(){
		return "QTL projection - MetaQTL (QTLProj)";
	}

	public void	setMaps(List<MapGene> maps, MapGene mapRef){
		_maps = maps;
		_mapRef = mapRef;
	}

	public void	setMapName(String mapName){
		_mapName = mapName;
	}

	/**
	 * Sets the ratio parameter.
	 * @param ratio The ratio
	 */
	public	void	setRatio(double ratio){
		_ratio = ratio;
	}

	/**
	 * Sets the pValue parameter.
	 * @param pValue The pValue
	 */
	public	void	setPValue(double pValue){
		_pValue = pValue;
	}

	@Override
	public	void		executeAnalysis() throws Exception{
		File			qtlsDir		= new File(FileManager.DIR_SOFT_TMP + "1");
		File			mapRefDir	= new File(FileManager.DIR_SOFT_TMP + "2");
		File			outputDir	= new File(FileManager.DIR_SOFT_TMP + "3");

		_outputFileRes = outputDir.getAbsolutePath() + "/" + _mapName;


		FileManager.createDirectoryIfNeeded(qtlsDir.getAbsolutePath());
		FileManager.createDirectoryIfNeeded(mapRefDir.getAbsolutePath());
		FileManager.createDirectoryIfNeeded(outputDir.getAbsolutePath());

		FileManager.emptyDirectory(qtlsDir);
		FileManager.emptyDirectory(mapRefDir);
		FileManager.emptyDirectory(outputDir);

		if (null != _maps && null != _mapRef){
			for (Iterator<MapGene> it = _maps.iterator(); it.hasNext();) {
				MapGene mapGene = it.next();

				FileManager.writeToMetaQTLXML(mapGene, qtlsDir.getAbsolutePath() + "/" + mapGene.getName() + ".xml");
			}
			FileManager.writeToMetaQTLXML(_mapRef, mapRefDir.getAbsolutePath() + "/"  + _mapRef.getName() + ".xml");


			QTLProj.main(new String[]{	"-m", mapRefDir.getAbsolutePath() + "/"  + _mapRef.getName() + ".xml",
										"-q", qtlsDir.getAbsolutePath(),
										"-r", String.valueOf(_ratio),
										"-p", String.valueOf(_pValue),
										"-o", _outputFileRes});
		}
	}

	@Override
	public	void	fillAnalysisResultList(){
		_results.add(new AnalysisResult(AnalysisResult.FILE_MAP_GENE_STD_ID, _outputFileRes + _ext, _mapName));
	}

	@Override
	public	void		done() {
		File			compfile		= new File(_outputFileRes + _extMetaQtl);

		// Convert into standard XML format
		FileManager.convert(compfile, FileManager.FileFormat.XML_METAQTL, "3", _mapName);
		super.done();
	}

	@Override
	protected	Parameters	getInputsPrettyPrint(){
		Parameters			inputs		= new Parameters();
		List<String>		mapsName	= new ArrayList<String>();

		for (Iterator<MapGene> it = _maps.iterator(); it.hasNext();) {
			mapsName.add(it.next().getName());
		}

		inputs.addParam("Map reference", _mapRef.getName());
		inputs.addParam("Maps projected", mapsName);
		inputs.addParam("Ratio", Double.toString(_ratio));
		inputs.addParam("pValue", Double.toString(_pValue));
		inputs.addParam("Output map name", _mapName);

		return inputs;
	}

	@Override
	protected	Parameters	getOutputsPrettyPrint(){
		return null;
	}

	private			String	_outputFileRes	= null;
	private	static	String	_extMetaQtl		= "_map.xml";
	private	static	String	_ext			= ".xml";
}
