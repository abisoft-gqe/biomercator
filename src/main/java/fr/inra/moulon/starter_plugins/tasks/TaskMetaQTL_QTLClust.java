/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 8, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.tasks;

import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.gui.wizards.TaskAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.logging.Parameters;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_QTLClust;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import org.metaqtl.main.QTLClust;


public class TaskMetaQTL_QTLClust extends TaskAnalysis {
	public	TaskMetaQTL_QTLClust(Wizard_MetaQTL_QTLClust wizard){
		super(wizard);
	}

	@Override
	public	String	getAnalysisName(){
		return "Meta analysis 1/2 - Veyrieras (QTLClust)";
	}

	public	void	setParameters(	String			metaAName,
									MapGene			map,
									LinkageGroup	lkg,
									String			chrName,
									String			lkgName,
									int				kMax,
									int				ci_mode,
									int				ci_miss,
									int				emrs,
									double			emeps,
									String			ontologyFilePath,
									List<Qtl>		dubiousQtls){
		_metaAName			= metaAName;
		_map				= map;
		_lkg				= lkg;
		_chrName			= chrName;
		_lkgName			= lkgName;
		_kMax				= String.valueOf(kMax);
		_ontologyFilePath	= ontologyFilePath;
		_dubiousQtls		= dubiousQtls;
	}

	@Override
	public	void		executeAnalysis() throws Exception{
		File			mapRefDir	= new File(FileManager.DIR_SOFT_TMP + "1");
		File			outputDir	= new File(FileManager.DIR_SOFT_TMP + "2");
		String			mapPath		= null;
		List<String>	args		= new ArrayList<String>();

		_outputPath = outputDir.getAbsolutePath() + "/" + _metaAName + "/";

		FileManager.createDirectoryIfNeeded(mapRefDir.getAbsolutePath());
		FileManager.createDirectoryIfNeeded(outputDir.getAbsolutePath());

		FileManager.emptyDirectory(mapRefDir);
		FileManager.emptyDirectory(outputDir);

		FileManager.createDirectoryIfNeeded(_outputPath);
		FileManager.createDirectoryIfNeeded(mapRefDir.getAbsolutePath());

		if (null != _map){
			mapPath = mapRefDir.getAbsolutePath() + "/"  + _map.getName() + ".xml";
			FileManager.writeToMetaQTLXML(_map, mapPath, _dubiousQtls);

			args.add("-q");
			args.add(mapPath);

			if (null != _ontologyFilePath){
				args.add("-t");
				args.add(_ontologyFilePath);
			}

			args.add("-k");
			args.add(_kMax);

			args.add("-c");
			args.add(_chrName+"%"+_lkgName);

			args.add("-o");
			//args.add(_outputName);
			args.add(_outputPath + _metaAName);

			QTLClust.main(args.toArray(new String[args.size()]));
//			QTLClust.main(new String[]{	"-q", mapPath,
//										"-t", _ontologyFilePath,
//										"-k", _kMax,
//										"-c", _lkgName,
//										"-o", _outputName});
		}
	}

	/**
	 * Creates a metaAnalysis and fill it with the used QTLs.
	 */
	private	void		createMetaAnalysis(){
		List<Qtl>		qtls = null;
		VisitorEltGet	vQtl = null;

		vQtl = new VisitorEltGet(Qtl.class, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
		_lkg.accept(vQtl);
		qtls = vQtl.getList();
		if (null != _dubiousQtls){
			for (Iterator<Qtl> it = _dubiousQtls.iterator(); it.hasNext();) {
				qtls.remove(it.next());
			}
		}
		_metaAnalysis = new MetaAnalysis(_metaAName, "Veyrieras");
		for (Iterator<Qtl> it = qtls.iterator(); it.hasNext();) {
			_metaAnalysis.addUsedQtls(it.next());
		}
	}

	@Override
	public	void	fillAnalysisResultList(){
		createMetaAnalysis();
//		_results.add(new AnalysisResult(AnalysisResult.FILE_ID, _outputPath + _metaAName + "_res.txt"));
//		_results.add(new AnalysisResult(AnalysisResult.FILE_ID, _outputPath + _metaAName + "_crit.txt"));
//		_results.add(new AnalysisResult(AnalysisResult.FILE_ID, _outputPath + _metaAName + "_model.txt"));
		_results.add(new AnalysisResult(AnalysisResult.DIRECTORY_ID, _outputPath));
		_results.add(new AnalysisResult(AnalysisResult.META_ANALYSIS_ID, _metaAnalysis));
	}

	@Override
	protected	Parameters	getInputsPrettyPrint(){
		Parameters			inputs	= new Parameters();
		List				qtlsList	= null;
		SortedSet<String>	qtlsSet	= new TreeSet<String>();

		qtlsList = Controller.getElements(_map.get(_chrName).get(_lkgName), Qtl.class);
		for (Iterator it = qtlsList.iterator(); it.hasNext();) {
			Qtl qtl = (Qtl)it.next();
			qtlsSet.add(qtl.getName());
		}

		if (null != _dubiousQtls){
			for (Iterator<Qtl> it = _dubiousQtls.iterator(); it.hasNext();) {
				String qtlName = it.next().getName();

				if (qtlsSet.contains(qtlName)){
					qtlsSet.remove(qtlName);
				}
			}
		}

		inputs.addParam("Map name", _map.getName());
		inputs.addParam("Chromosome name", _chrName);
		inputs.addParam("Linkage group name", _lkgName);
		inputs.addParam("KMax", _kMax);
		inputs.addParam("Ontology file path", _ontologyFilePath);
		inputs.addParam("Used qtl", qtlsSet);
		inputs.addParam("Meta analysis name", _metaAName);

		return inputs;
	}

	@Override
	protected	Parameters	getOutputsPrettyPrint(){
		return null;
	}

	private	String			_ontologyFilePath	= null;
	//private	String			_outputName			= null;
	private	String			_outputPath			= null;
	private	String			_chrName			= null;
	private	String			_lkgName			= null;
	private	String			_kMax				= null;
	private	MapGene			_map				= null;
	private	List<Qtl>		_dubiousQtls		= null;
	private	String			_metaAName			= null;
	private	MetaAnalysis	_metaAnalysis		= null;
	private	LinkageGroup	_lkg				= null;
}
