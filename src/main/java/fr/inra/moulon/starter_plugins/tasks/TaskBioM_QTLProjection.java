/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 8, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.tasks;

import fr.inra.moulon.starter.gui.wizards.TaskAnalysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.logging.Parameters;
import fr.inra.moulon.starter_plugins.biom_proj.Projection1;
import fr.inra.moulon.starter_plugins.wizards.Wizard_BioM_QTLProjection;
import java.util.Iterator;
import java.util.List;


public class TaskBioM_QTLProjection extends TaskAnalysis {
	private	MapGene	_mapRef			= null;
	private	MapGene	_mapFrom		= null;
	private	boolean	_inversionAuto	= false;
	private	String	_mapName		= null;

	public	TaskBioM_QTLProjection(Wizard_BioM_QTLProjection wizard){
		super(wizard);
	}

	@Override
	public	String	getAnalysisName(){
		return "Map projection";
	}

	public void	setMaps(MapGene mapRef, MapGene mapFrom){
		_mapRef = mapRef;
		_mapFrom = mapFrom;
	}

	public void	setMapName(String mapName){
		_mapName = mapName;
	}

	public void	setParams(boolean	inversionAuto){
		_inversionAuto = inversionAuto;
	}

	@Override
	public	void	executeAnalysis() throws Exception{
		Projection1	projection = null;

		projection = new Projection1(_mapRef, _mapFrom, FileManager.DIR_RES_ANALYSES, true, true, false, _mapName, _inversionAuto);
		projection.execute();
		_mapQtlProj = projection.getResult();
		for (Iterator<Chromosome> it = _mapQtlProj.iterator(); it.hasNext();) {
			LinkageGroup lkg = it.next().getDefaultLkg();
			lkg.setBelongingMap(_mapQtlProj);
			lkg.sort();
		}
		_mapQtlProj.sort();
		_mapQtlProj.addMapUsed(_mapFrom.getName());
	}

	@Override
	public void fillAnalysisResultList() {
		_results.add(new AnalysisResult(AnalysisResult.MAP_GENE_ID, _mapQtlProj, _mapName));
	}

	@Override
	protected	Parameters	getInputsPrettyPrint(){
		Parameters			inputs = new Parameters();

		inputs.addParam("Map from", _mapFrom.getName());
		inputs.addParam("Map reference", _mapRef.getName());
		inputs.addParam("Inversion auto resolution", Boolean.toString(_inversionAuto));
		inputs.addParam("Output map name", _mapName);

		return inputs;
	}

	@Override
	protected	Parameters	getOutputsPrettyPrint(){
		Parameters			outputs = new Parameters();
		List				loci	= null;

		if (null != _mapQtlProj){
			loci = Controller.getElements(_mapQtlProj, Marker.class);

			outputs.addParam("Projection", "Successful");
			outputs.addParam("Map name", _mapQtlProj.getName());
			outputs.addParam("locus number", Integer.toString((null != loci)?loci.size():0));
		} else{
			outputs.addParam("Projection", "Problem");
		}

		return outputs;
	}

	private	MapGene	_mapQtlProj	= null;
}
