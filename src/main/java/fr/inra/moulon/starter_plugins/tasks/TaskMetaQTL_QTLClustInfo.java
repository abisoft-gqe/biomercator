/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 8, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.tasks;

import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.gui.wizards.TaskAnalysis;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.logging.Parameters;
import fr.inra.moulon.starter.utils.StringTools;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_QTLClustInfo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.metaqtl.main.QTLClustInfo;


public class TaskMetaQTL_QTLClustInfo extends TaskAnalysis {
	public	TaskMetaQTL_QTLClustInfo(Wizard_MetaQTL_QTLClustInfo wizard){
		super(wizard);
	}

	@Override
	public	String	getAnalysisName(){
		return "Meta analysis 2/2 - Veyrieras (QTLClust)";
	}

	public	void	setParameters(	String			lkgName,
									String			trait,
									String			resFile,
									MetaAnalysis	metaAnalysis,
									int				best,
									int				kMin,
									int				kMax){
		_lkgName		= lkgName;
		_trait			= trait;
		_resFile		= resFile;
		_metaAnalysis	= metaAnalysis;
		_best			= String.valueOf(best);
		_kMin			= String.valueOf(kMin);
		_kMax			= String.valueOf(kMax);
	}

	@Override
	public	void	executeAnalysis() throws Exception{
		File		outputDir	= new File(FileManager.DIR_SOFT_TMP + "1");

		FileManager.createDirectoryIfNeeded(outputDir.getAbsolutePath());
		_outputPath = outputDir.getAbsolutePath() + "/" + _metaAnalysis.getName() + "/";
		FileManager.createDirectoryIfNeeded(_outputPath);
		FileManager.emptyDirectory(new File(_outputPath));

		_outputName = _resFile.substring(_resFile.lastIndexOf("/")+1);
		_outputName	= _outputPath + "/"
				+ _outputName.replaceAll("_res.txt$", "_" + _trait + "_" + _best + "_table.txt");

		QTLClustInfo.main(new String[]{	"-c",		_lkgName,
										"-t",		_trait,
										"-r",		_resFile,
										"-b",		_best,
										"--kmin",	_kMin,
										"--kmax",	_kMax,
										"-o",		_outputName});
	}

	/**
	 * Parses the file created by the "QTLClustInfo" routine and returns a filled
	 * MetaAnalysis.
	 * @return The MetaAnalysis created by "QTLClustInfo".
	 */
	private	void		setMetaAnalysis(String outputPath){
		MetaModel		metaModel		= null;
		MetaQtl			metaQtl			= null;
		int				nbMetaQtls		= Integer.valueOf(_best);
		File			metaAFile		= new File(_outputName);
		String			line			= null;
		BufferedReader	buf				= null;
		int				offsetID		= BEFORE_MODELS;
		Pattern			pattern			= null;
		Matcher			matcher			= null;
		double			position		= 0;
		double			ci				= 0;
		int				iTrait			= 1;
//		PrintWriter		writer			= null;
		String			sep				= "\t";

		if (metaAFile.exists() && metaAFile.canRead() && metaAFile.length() != 0){
			metaModel = new MetaModel(String.valueOf(nbMetaQtls));
			try{
//				writer = new PrintWriter(new BufferedWriter(new FileWriter(outputPath)));
//				writer.println(	""
//						+ "Element"		+ sep
//						+ "Position"	+ sep
//						+ "CI"			+ sep
//						+ "Start"		+ sep
//						+ "Stop"
//						);
				buf = new BufferedReader(new FileReader(metaAFile));
				pattern = Pattern.compile("([0-9]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)");
				while (offsetID != AFTER_MODELS && null != (line = buf.readLine())){
					matcher = pattern.matcher(line);
					if (matcher.find()){
						offsetID	= INSIDE_MODELS;
						metaQtl		= new MetaQtl(matcher.group(1), _trait + "_" + iTrait++);
						position	= Double.valueOf(matcher.group(2));
						ci			= Double.valueOf(matcher.group(5));

						metaQtl.setPosition(position);
						metaQtl.setPositionStart(position-ci/2);
						metaQtl.setPositionEnd(position+ci/2);
						metaModel.add(metaQtl);
//						writer.println(""
//								+ "MetaQTL" + metaQtl.getName()								+ sep
//								+ metaQtl.getPosition()										+ sep
//								+ (metaQtl.getPositionEnd() - metaQtl.getPositionStart())	+ sep
//								+ metaQtl.getPositionStart()								+ sep
//								+ metaQtl.getPositionEnd()
//								);
					}else{
						if (INSIDE_MODELS == offsetID){
							offsetID = AFTER_MODELS;
						}
					}
				}

//				writer.println();
//				writer.print(	""
//						+ "QTL"			+ sep
//						+ "Trait"		+ sep
//						+ "Position"	+ sep
//						+ "Start"		+ sep
//						+ "Stop"		+ sep
//						);
//				for (int i = 1; i <= nbMetaQtls; ++i){
//					writer.print("Membership MetaQTL " + i);
//				}
//				writer.println();

				pattern = Pattern.compile("([^\t]+)" + StringTools.repeatString("\t([^\t]+)", 3+nbMetaQtls));
				while (null != (line = buf.readLine())){
					matcher = pattern.matcher(line);
					if (matcher.find()){
//						_metaAnalysis.addUsedQtls(null);
//						writer.print(	""
//						+ matcher.group(1)	+ sep
//						+ "Trait"			+ sep
//						+ matcher.group(2)	+ sep
//						+ "Start"		+ sep
//						+ "Stop"		+ sep
//						);

						for (int i = 0; i < nbMetaQtls; ++i){
							metaModel.get(i).addMembership(matcher.group(1), Double.valueOf(matcher.group(i+5)));
//							writer.print(matcher.group(i+5) + sep);
						}
//						writer.println();
					}
				}

				_metaAnalysis.add(metaModel);
//				writer.close();
			}catch (IOException e){
				System.out.println(e.getMessage());
			}
		}
	}

	@Override
	public void fillAnalysisResultList() {
		String	outputFileRes	= _outputName.replaceAll("_table.txt$", ".txt");
		String	outputDir		= outputFileRes.substring(0, outputFileRes.lastIndexOf("/"));

		setMetaAnalysis(outputFileRes);
		_results.add(new AnalysisResult(AnalysisResult.META_ANALYSIS_ID, _metaAnalysis));
		_results.add(new AnalysisResult(AnalysisResult.DIRECTORY_ID, outputDir));
	}

	@Override
	protected	Parameters	getInputsPrettyPrint(){
		Parameters			inputs = new Parameters();

		inputs.addParam("Linkage group name", _lkgName);
		inputs.addParam("Meta trait", _trait);
		inputs.addParam("Best model", _best);
		inputs.addParam("KMin", _kMin);
		inputs.addParam("KMax", _kMax);
		inputs.addParam("Output res file", _resFile);

		return inputs;
	}

	@Override
	protected	Parameters	getOutputsPrettyPrint(){
		return null;
	}

	private	MetaAnalysis	_metaAnalysis	= null;
	private	String			_lkgName		= null;
	private	String			_trait			= null;
	private	String			_resFile		= null;
	private	String			_kMin			= null;
	private	String			_kMax			= null;
	private	String			_best			= null;
	private	String			_outputName		= null;
	private	String			_outputPath		= null;

	private	final	static	int	BEFORE_MODELS	= 0;
	private	final	static	int	INSIDE_MODELS	= 1;
	private	final	static	int	AFTER_MODELS	= 2;
}
