/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 8, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.tasks;

import fr.inra.moulon.starter.gui.wizards.TaskAnalysis;
import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.logging.Parameters;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_Cons;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;


import java.util.List;
import org.metaqtl.main.ConsMap;


public class TaskMetaQTL_Cons extends TaskAnalysis {
	private	List<MapGene>	_maps		= null;
	private	MapGene			_mapRef		= null;
	private	String			_mapName	= null;

	public	TaskMetaQTL_Cons(Wizard_MetaQTL_Cons wizard){
		super(wizard);
	}

	@Override
	public	String	getAnalysisName(){
		return "MetaQTL compilation (ConsMap)";
	}

	public void	setMaps(List<MapGene> maps, MapGene mapRef){
		_maps = maps;
		_mapRef = mapRef;
	}

	public void	setMapName(String mapName){
		_mapName = mapName;
	}

	@Override
	public	void		executeAnalysis() throws Exception{
		File			mapsDir		= new File(FileManager.DIR_SOFT_TMP + "1");
		File			mapRefDir	= new File(FileManager.DIR_SOFT_TMP + "2");
		File			outputDir	= new File(FileManager.DIR_SOFT_TMP + "3");
		String			mapRefPath	= null;
		List<String>	params	= new ArrayList<String>();

		_outputFileRes = outputDir.getAbsolutePath() + "/" + _mapName;

		FileManager.createDirectoryIfNeeded(mapsDir.getAbsolutePath());
		FileManager.createDirectoryIfNeeded(mapRefDir.getAbsolutePath());
		FileManager.createDirectoryIfNeeded(outputDir.getAbsolutePath());

		FileManager.emptyDirectory(mapsDir);
		FileManager.emptyDirectory(mapRefDir);
		FileManager.emptyDirectory(outputDir);

		if (null != _maps){
			for (Iterator<MapGene> it = _maps.iterator(); it.hasNext();) {
				MapGene mapGene = it.next();

				FileManager.writeToMetaQTLXML(mapGene, mapsDir.getAbsolutePath() + "/" + mapGene.getName() + ".xml");
			}
			if (null != _mapRef){
				mapRefPath = mapRefDir.getAbsolutePath() + "/"  + _mapRef.getName() + ".xml";
				FileManager.writeToMetaQTLXML(_mapRef, mapRefPath);
			}

			params.add("-m");
			params.add(mapsDir.getAbsolutePath());

			if (null != mapRefPath){
				params.add("-r");
				params.add(mapRefPath);
			}

			params.add("-t");
			params.add("1");

			params.add("-o");
			params.add(_outputFileRes);

			ConsMap.main(params.toArray(new String[]{}));
		}
	}

	@Override
	public	void	fillAnalysisResultList(){
		_results.add(new AnalysisResult(AnalysisResult.FILE_MAP_GENE_STD_ID, _outputFileRes + _ext, _mapName));
	}

	@Override
	public	void		done() {
		File			mQTLfile		= new File(_outputFileRes + _extMetaQtl);

		FileManager.convert(mQTLfile, FileManager.FileFormat.XML_METAQTL, "3", _mapName);
		mQTLfile.delete();

		super.done();
	}


	@Override
	protected	Parameters	getInputsPrettyPrint(){
		Parameters			inputs = new Parameters();
		List<String>		mapsName	= new ArrayList<String>();

		for (Iterator<MapGene> it = _maps.iterator(); it.hasNext();) {
			mapsName.add(it.next().getName());
		}

		inputs.addParam("Map reference", (null != _mapRef?_mapRef.getName():"None"));
		inputs.addParam("Maps to be projected", mapsName);
		inputs.addParam("Output map name", _mapName);

		return inputs;
	}

	@Override
	protected	Parameters	getOutputsPrettyPrint(){
		return null;
	}

	private	String			_outputFileRes	= null;
	private	static String	_extMetaQtl		= "_map.xml";
	private	static String	_ext			= ".xml";
}
