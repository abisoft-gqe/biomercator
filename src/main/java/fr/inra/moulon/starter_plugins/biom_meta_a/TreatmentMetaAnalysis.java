/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter_plugins.biom_meta_a;

import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.datamodel.entities.utils.LocusComparator;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TreatmentMetaAnalysis {
	public TreatmentMetaAnalysis(	LinkageGroup	lkg,
									List<Qtl>		dubious){
		_averageArray = new double[5][4][3];
		_split1 = new int[3];
		_split2 = new int[3];
		_split3 = new int[3];
		_split4 = new int[3];
		initQtlArrays(lkg, dubious);
	}

	public void		setOutputFile(String outFile){
		_outFile = outFile;
	}

	private void		initQtlArrays(	LinkageGroup	lkg,
										List<Qtl>		dubious){
		List<Qtl>		qtls = null;
		VisitorEltGet	vQtl = null;

		vQtl = new VisitorEltGet(Qtl.class, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
		lkg.accept(vQtl);
		qtls = vQtl.getList();
		if (null != dubious){
			for (Iterator<Qtl> it = dubious.iterator(); it.hasNext();) {
				qtls.remove(it.next());
			}
		}
		_metaAnalysis = new MetaAnalysis(_outFile, "Gerber S., Goffinet B.");
		for (Iterator<Qtl> it = qtls.iterator(); it.hasNext();) {
			_metaAnalysis.addUsedQtls(it.next());
		}

		_nbQTL = qtls.size();
		_QTLArray = new double[_nbQTL][3];
		_QTLArrayName = new String[_nbQTL];

		Collections.sort(qtls, new LocusComparator());
		for (int i = 0; i < qtls.size(); ++i){
			_QTLArray[i][0] = qtls.get(i).getPosition();
			_QTLArray[i][1] = qtls.get(i).getPositionStart();
			_QTLArray[i][2] = qtls.get(i).getPositionEnd();
			_QTLArrayName[i] = qtls.get(i).getName();
		}
		_varianceQTL = new double[_nbQTL];
	}

	public void	execute(){
		VarianceIC();
		factor1();
		factor2();
		factor3();
		factor4();
		factorN();
	}

	public MetaAnalysis		getResults(){
		MetaModel			metaModel		= null;
		MetaQtl				metaQtl			= null;
		double				posStart		= 0;
		double				posEnd			= 0;
		double				posConf			= 0;

		for (int i = 0; i < 4; ++i){
			metaModel = new MetaModel(String.valueOf(i+1));
			_metaAnalysis.add(metaModel);
		}
		for (int i = 0; i < 4; ++i){
			for (int k = i; k < 4; ++k){
				posConf = _averageArray[k][i][0];
				posConf = (double)((int)(posConf*100))/100;
				posStart = _averageArray[k][i][1];
				posStart = (double)((int)(posStart*100))/100;
				posEnd = _averageArray[k][i][2];
				posEnd = (double)((int)(posEnd*100))/100;
				metaQtl = new MetaQtl("name" + (4-i) + "_" + (4-k), "trait_" + (4-i));
				metaQtl.setPosition(posConf);
				metaQtl.setPositionStart(posStart);
				metaQtl.setPositionEnd(posEnd);
				if (-1 != posConf){
					_metaAnalysis.get(String.valueOf(k+1)).add(metaQtl);
				}
			}
		}
		if (_aic1 > 1000000){
//			metaAnalyses.set(0, null);
		}
		if (_aic2 > 1000000){
//			metaAnalyses.set(1, null);
		}
		if (_aic3 > 1000000){
//			metaAnalyses.set(2, null);
		}
		if (_aic4 > 1000000){
//			metaAnalyses.set(3, null);
		}
		setMemberships(_metaAnalysis);

		return _metaAnalysis;
	}

	/**
     * Set colors to QTL of the new meta-analysis map: all real QTL
     * are represented in gray, meta-analysis QTL are represented in red
     */
    private void	setMemberships(MetaAnalysis metaAnalysis) {
		MetaModel	metaModel	= null;
		MetaQtl		metaQtl		= null;
    	int			index		= 0;
		String		name		= null;
		int			i			= 0;

		while (i < _nbQTL) {
			name = _QTLArrayName[i];
			// One MAnalysis QTL case
			if (null != (metaModel = metaAnalysis.get("1"))){
				if (null != (metaQtl = metaModel.get(0))){
					metaQtl.addMembership(name, 1.0);
				}
			}

			// Two MAnalysis QTL case
			if ((_split2[0] > 0) && (_split2[1]== 0)){
				if (null != (metaModel = metaAnalysis.get("2"))){
					if (index < _split2[0]){
						if (null != (metaQtl = metaModel.get(0))){
							metaQtl.addMembership(name, 1.0);
						}
					}else{
						if (null != (metaQtl = metaModel.get(1))){
							metaQtl.addMembership(name, 1.0);
						}
					}
				}
			}

			// Three MAnalysis QTL case
			if ((_split3[0] > 0) && (_split3[1] > 0) && (_split3[2] == 0)){
				if (null != (metaModel = metaAnalysis.get("3"))){
					if (index < _split3[0]){
						if (null != (metaQtl = metaModel.get(0))){
							metaQtl.addMembership(name, 1.0);
						}
					} else if ((index >= _split3[0]) && (index < _split3[1])){
						if (null != (metaQtl = metaModel.get(1))){
							metaQtl.addMembership(name, 1.0);
						}
					} else if (index >= _split3[1]){
						if (null != (metaQtl = metaModel.get(2))){
							metaQtl.addMembership(name, 1.0);
						}
					}
				}
			}

			// Four MAnalaysis QTL case
			if ((_split4[0] > 0) && (_split4[1] > 0) && (_split4[2] > 0)){
			if (null != (metaModel = metaAnalysis.get("4"))){
					if (index < _split4[0]){
						if (null != (metaQtl = metaModel.get(0))){
							metaQtl.addMembership(name, 1.0);
						}
					} else if ((index >= _split4[0]) && (index < _split4[1])){
						if (null != (metaQtl = metaModel.get(1))){
							metaQtl.addMembership(name, 1.0);
						}
					} else if ((index >= _split4[1]) && (index < _split4[2])){
						if (null != (metaQtl = metaModel.get(2))){
							metaQtl.addMembership(name, 1.0);
						}
					} else if (index >= _split4[2]){
						if (null != (metaQtl = metaModel.get(3))){
							metaQtl.addMembership(name, 1.0);
						}
					}
				}
			}
			++index;
			++i;
		}
	}

	public File		createFileTable(String path){
		String		str = null;
		double		a, b, c;
		File		file = null;
		PrintWriter	writer = null;
		String		sep = "\t";
		String		indent = "\t";

		try{
			writer = new PrintWriter(new BufferedWriter(new FileWriter(path)));

			writer.print(" " + sep);
			for (int i = 1; i <= 4; ++i){
				writer.print("Model " + i + sep);
			}
			writer.println("Model N");

			writer.print("AIC Value" + sep);
			if (_aic1 >= 1000000) str = "infinity";
			else str = String.valueOf((double)((int)(_aic1*100))/100);
			writer.print(str + sep);

			if (_aic2 >= 1000000) str = "infinity";
			else str = String.valueOf((double)((int)(_aic2*100))/100);
			writer.print(str + sep);

			if (_aic3 >= 1000000) str = "infinity";
			else str = String.valueOf((double)((int)(_aic3*100))/100);
			writer.print(str + sep);

			if (_aic4 >= 1000000) str = "infinity";
			else str = String.valueOf((double)((int)(_aic4*100))/100);
			writer.print(str + sep);

			if (_aicN >= 1000000) str = "infinity";
			else str = String.valueOf((double)((int)(_aicN*100))/100);
			writer.println(str);

			for (int i = 0; i < 4; ++i){
				writer.print("Mean position " + i + " (C.I.)" + indent);
				for (int k = i; k < 4; ++k){
					a = _averageArray[k][i][0];
					b = _averageArray[k][i][1];
					c = _averageArray[k][i][2];
					a = (double)((int)(a*100))/100;
					b = (double)((int)(b*100))/100;
					c = (double)((int)(c*100))/100;
					if (-1 != a){
						str = a + "(" + b + " - " + c + ")";
					}else{
						str = "-";
					}
					writer.print(str + sep);
				}
				writer.println();
				indent += sep;
			}

			writer.close();
			file = new File(path);
		}catch(IOException e){
		}

		return file;
	}

	private void	VarianceIC(){
        for(int i = 0; i < _nbQTL; i++) {
            _varianceQTL[i] = Math.pow((_QTLArray[i][2] - _QTLArray[i][1]) / 3.92, 2);
            if (_varianceQTL[i] == 0)
            	_varianceQTL[i] = 1;
        }
    }

	private	void	factor1(){
		double		vraisGlob = 1;
		int[]		nArray = null;

		calculateAverageN(nArray);
		vraisGlob = calculateLikelihoodN(nArray);
		_aic1 = -2* (Math.log(vraisGlob) - 1);
		if (vraisGlob > 0.0) {
			computeIC(0, _averageArray[1-1], 0, (_nbQTL-1));
        	_split1[0] = 0;
		}else{
        	_averageArray[1-1][0][0] = -1;
		}
	}

	private	void	factor2(){
		double		vraisMax = 0;
		double		vraisGlob = 0;
		int			nSeparation = 0;
		int[]		nArray = new int[1];

		for (int n2=0; n2 < (_nbQTL-1); n2++) {
			nArray[0] = n2;
			calculateAverageN(nArray);
			vraisGlob = calculateLikelihoodN(nArray);
			if( vraisGlob > vraisMax) {
				vraisMax = vraisGlob;
				nSeparation = n2;
				_split2[0] = n2 +1;
			}
		}

		_aic2 = -2 * (Math.log(vraisMax) - 0.7 * _nbQTL);
		if (vraisMax > 0.0) {
			nArray[0] = nSeparation;
			calculateAverageN(nArray);

        	computeIC(0, _averageArray[2-1], 0, nSeparation);
			computeIC(1, _averageArray[2-1], (nSeparation+1), (_nbQTL-1));

        	_split2[1] = 0;
        	_split2[2] = 0;
        }else{
        	_averageArray[2-1][0][0] = -1;
        	_averageArray[2-1][1][0] = -1;
        }
	}

	private	void	factor3(){
		double		vraisMax = 0.0;
		double		vraisGlob = 0;
        int			nSeparation1 = 0;
        int			nSeparation2 = 0;
        int[]		nArray = new int[2];

        for (int n31=0; n31 < (_nbQTL-2); n31++) {
            for(int n32=(n31+1); n32 < (_nbQTL-1); n32++) {
            	nArray[0] = n31;
            	nArray[1] = n32;
            	calculateAverageN(nArray);
                vraisGlob = calculateLikelihoodN(nArray);
				if( vraisGlob > vraisMax) {
		    		vraisMax = vraisGlob;
		    		nSeparation1 = n31;
		    		_split3[0] = n31 + 1;
		    		nSeparation2 = n32;
		    		_split3[1] = n32 + 1;
				}
            }                        
        }

        _aic3 = -2 * (Math.log(vraisMax) - 1.11 * _nbQTL);
		if (vraisMax > 0.0) {
			nArray[0] = nSeparation1;
			nArray[1] = nSeparation2;
			calculateAverageN(nArray);

        	//calcul des IC
			computeIC(0, _averageArray[3-1], 0, nSeparation1);
			computeIC(1, _averageArray[3-1], (nSeparation1 + 1), nSeparation2);
			computeIC(2, _averageArray[3-1], (nSeparation2 + 1), (_nbQTL-1));

        	_split3[2] = 0;
		}else{
        	_averageArray[3-1][0][0] = -1;
        	_averageArray[3-1][1][0] = -1;
        	_averageArray[3-1][2][0] = -1;
		}
	}

	private	void	factor4(){
		double		vraisMax = 0.0;
		double		vraisGlob = 0;
		int			nSeparation1 = 0;
		int			nSeparation2 = 0;
		int			nSeparation3 = 0;
		int[]		nArray = new int[3];

		for(int n41=0; n41 < (_nbQTL-3); n41++) {
			for(int n42=(n41+1); n42 < (_nbQTL-2); n42++) {
				for(int n43=(n42+1); n43 < (_nbQTL-1); n43++) {
					nArray[0] = n41;
					nArray[1] = n42;
					nArray[2] = n43;
					calculateAverageN(nArray);
					vraisGlob = calculateLikelihoodN(nArray);
					if( vraisGlob > vraisMax) {
						vraisMax = vraisGlob;
						nSeparation1 = n41;
						_split4[0] = n41 + 1;
						nSeparation2 = n42;
						_split4[1] = n42 + 1;
						nSeparation3 = n43;
						_split4[2] = n43 + 1;
					}
				}
			}
		}
		_aic4 = -2 * (Math.log(vraisMax) - 1.44 * _nbQTL);

		if (vraisMax > 0.0) {
			nArray[0] = nSeparation1;
			nArray[1] = nSeparation2;
			nArray[2] = nSeparation3;
			calculateAverageN(nArray);

        	//calcul des IC 
			computeIC(0, _averageArray[4-1], 0, nSeparation1);
			computeIC(1, _averageArray[4-1], (nSeparation1 + 1), nSeparation2);
			computeIC(2, _averageArray[4-1], (nSeparation2 + 1), nSeparation3);
			computeIC(3, _averageArray[4-1], (nSeparation3 + 1), (_nbQTL-1));
		}else{
	    	_averageArray[4-1][0][0] = -1;
        	_averageArray[4-1][1][0] = -1;
        	_averageArray[4-1][2][0] = -1;
        	_averageArray[4-1][3][0] = -1;
		}
	}

	private	void	factorN(){
		double		vraisGlobale = 1;
		double		probaPos = 0;

		for (int i = 0; i < _nbQTL; i++) {
            probaPos = 1/Math.sqrt(2*Math.PI*_varianceQTL[i]);
            vraisGlobale = vraisGlobale * probaPos;
        }
        _aicN = -2 * (Math.log(vraisGlobale) - 2.27 * _nbQTL);
	}

	private void	calculateAverageN(int[] indexes){
		int			index = 0;
		int			index_prev = 0;
		int			len = 0;
		int			i = 0;

		if (null == indexes){
			len = 0;
		}else{
			len = indexes.length;
		}

		for (int k = 0; k < len; ++k){
			index = indexes[k];
			calculateAverage(index_prev, index, len, i++);
			index_prev = index+1;
		}
		calculateAverage(index_prev, _nbQTL-1, len, i);
	}

	private void	calculateAverage(int start, int end, int n, int i){
		double		sum = 0;
		double		average = 0;

		if (start <= end){
			for (int k = start; k <= end; ++k){
				average += _QTLArray[k][0]/_varianceQTL[k];
				sum += 1/_varianceQTL[k];
			}
			_averageArray[n][i][0] = average/sum;
		}
	}

	private double	calculateLikelihoodN(int[] indexes){
		int			index = 0;
		int			index_prev = 0;
		int			len = 0;
		int			i = 0;
		double		likelihoodGrp = 1;

		if (null == indexes)
			len = 0;
		else
			len = indexes.length;

		for (int k = 0; k < len; ++k){
			index = indexes[k];
			likelihoodGrp *= calculateLikelihood(index_prev, index, len, i++);
			index_prev = index+1;
		}
		likelihoodGrp *= calculateLikelihood(index_prev, _nbQTL-1, len, i);

        return likelihoodGrp;
	}

	private double	calculateLikelihood(int start, int end, int n, int i){
		double		likelihoodGrp = 1;
		double		probaPos = 0;

		for (int k = start; k <= end; ++k){
			probaPos = (1 / Math.sqrt(2 * Math.PI * _varianceQTL[k])) * Math.exp(-(Math.pow((_QTLArray[k][0] - _averageArray[n][i][0]),2)/(2*_varianceQTL[k])));
			likelihoodGrp *= probaPos;
		}

		return likelihoodGrp;
	}

	/**
	 * Recomputes variance of a position if multiple QTL are included
	 * in the distribution. Attributes the initial confidence interval if
	 * there is only one QTL
	 */
	public void computeIC(int set, double moy[][], int begin, int last ) {
		if (last-begin > 0) { 
			double ic1 = calcIC(variancePosConsensus(moy[set][0], begin, last));
			moy[set][1] = moy[set][0] - (ic1/2);
			moy[set][2] = moy[set][0] + (ic1/2);
		}
		else {
			moy[set][1] = _QTLArray[begin][1];
			moy[set][2] = _QTLArray[begin][2];
		}
	}

	/**
	 * Computes variance of consensus position, for the set of QTL of a distribution
	 * (index n1 to n2 (n2 included))
	 */
	private	double	variancePosConsensus( double moy, int n1, int n2) {
		double		variance = 0;
		double		sumInvVar = 0;

		for (int i = n1; i <= n2; i++)
			sumInvVar += (1/_varianceQTL[i]);
		variance = 1/sumInvVar;

		return variance;
    }

	/**
	 * Deduces IC for consensus position from variance value
	 * (95% : CI = 3.92 * sqrt(variance)
	 */
	public double calcIC(double variance) {
		double ic = 3.92 * Math.sqrt(variance);

		return ic;
	}

	private	int				_nbQTL			= 0;
	private	int[]			_split1			= null;
	private	int[]			_split2			= null;
	private	int[]			_split3			= null;
	private	int[]			_split4			= null;
	private double			_aic1			= 0;
	private double			_aic2			= 0;
	private double			_aic3			= 0;
	private double			_aic4			= 0;
	private double			_aicN			= 0;
	private	double[]		_varianceQTL	= null;
	private String[]		_QTLArrayName	= null;
	private	double[][]		_QTLArray		= null;
	private	double[][][]	_averageArray	= null; //[0]=mean position [1]=ICbeginning [2]=ICend
	private String			_chrName		= null;
	private String			_outFile		= null;
	private	MetaAnalysis	_metaAnalysis	= null;
}
