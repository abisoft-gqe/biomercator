/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 19-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards.utils;

import javax.swing.JComponent;

/**
 * This class is capable to return if the given JComponent is null or not.
 * This class is abstract, a subclass must be created for each new component
 * class (Jradio, jtree, JcomboBox...) the application wants to use.
 *
 * @author sosnowski
 */
public abstract	class JComponentAutoTest {
	protected	JComponentAutoTest(JComponent comp){
		_comp = comp;
	}

	public	abstract	String	getText();

	public	abstract	boolean	isNotNull();

	public	void	setValidity(boolean validity){
//		System.out.println("comp / border : " + _comp + " / " + _comp.getBorder());
//		_comp.setBorder(new LineBorder(validity?Color.BLUE:Color.RED));
	}

	protected	JComponent	_comp = null;
}
