/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 5, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.database.Pair;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.utils.HTMLTools;
import fr.inra.moulon.starter.utils.PhysicDataTypes;
import fr.inra.moulon.starter.utils.PhysicDataTypesTools;
import fr.inra.moulon.starter.utils.Session;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;


public class WizardGenomeVersionManagement extends AbstractWizard {
	@Override
	protected void setDialogParameters() {
		_dialog.setSize(new Dimension(500, 300));
		_dialog.setTitle(_bundle.getString("wizard_genome_version_management"));

		_combosAll = new ArrayList<JComboBox>();
		_buttonsAll = new ArrayList<JButton>();

		_combos = new EnumMap<PhysicDataTypes, JComboBox>(PhysicDataTypes.class);
		_buttons = new EnumMap<PhysicDataTypes, JButton>(PhysicDataTypes.class);

		_combos.put(PhysicDataTypes.STRUCTURAL_ANNOTATIONS, new JComboBox());
		_buttons.put(PhysicDataTypes.STRUCTURAL_ANNOTATIONS, new JButton(_bundle.getString("misc_suppress")));
		_combosAll.add(_combos.get(PhysicDataTypes.STRUCTURAL_ANNOTATIONS));
		_buttonsAll.add(_buttons.get(PhysicDataTypes.STRUCTURAL_ANNOTATIONS));

		_combos.put(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS, new JComboBox());
		_buttons.put(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS, new JButton(_bundle.getString("misc_suppress")));
		_combosAll.add(_combos.get(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS));
		_buttonsAll.add(_buttons.get(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS));

		_combos.put(PhysicDataTypes.ANCHORS, new JComboBox());
		_buttons.put(PhysicDataTypes.ANCHORS, new JButton(_bundle.getString("misc_suppress")));
		_combosAll.add(_combos.get(PhysicDataTypes.ANCHORS));
		_buttonsAll.add(_buttons.get(PhysicDataTypes.ANCHORS));
		
		_combos.put(PhysicDataTypes.DBLINKS, new JComboBox());
		_buttons.put(PhysicDataTypes.DBLINKS, new JButton(_bundle.getString("misc_suppress")));
		_combosAll.add(_combos.get(PhysicDataTypes.DBLINKS));
		_buttonsAll.add(_buttons.get(PhysicDataTypes.DBLINKS));
	}

	@Override
	protected	void		addComponentsToMainPane(Container pane) {
		GridBagConstraints	c							= new GridBagConstraints();
		JLabel				label						= null;
		JButton				button						= null;
		int					iRow						= 0;

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		// PROGRESS BAR
		_progress = new JProgressBar();
		_progress.setVisible(false);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 3, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_progress, c);

		//	GENOME VERSION
		label = new JLabel(HTMLTools.setFontBold(_bundle.getString("tab_genome_version")));
		GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		_combosAll.add(_comboGenomeVersion);
		GridBagLayoutUtils.set_gbc(c, 1, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_comboGenomeVersion, c);

		button = new JButton(_bundle.getString("misc_suppress"));
		_buttonsAll.add(button);
		GridBagLayoutUtils.set_gbc(c, 2, iRow++, 2, 1, GridBagConstraints.NONE);
		pane.add(button, c);

		button.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void	actionPerformed(java.awt.event.ActionEvent evt) {
				Pair pair = (Pair)_comboGenomeVersion.getSelectedItem();
				if (pair.id != null){
					if (showValidationDialog(pair.comments, null)){
						DatabaseManager.instance().deleteGenomeVersion(pair.id);
						fillCombos();
					}
				}
            }
        });

		for (final PhysicDataTypes type : PhysicDataTypes.values()){
			label = new JLabel(HTMLTools.setFontBold(PhysicDataTypesTools.typeToString(type)));
			GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.NONE);
			c.anchor = GridBagConstraints.LINE_START;
			pane.add(label, c);

			GridBagLayoutUtils.set_gbc(c, 1, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
			c.anchor = GridBagConstraints.LINE_START;
			pane.add(_combos.get(type), c);

			GridBagLayoutUtils.set_gbc(c, 2, iRow++, 2, 1, GridBagConstraints.NONE);
			pane.add(_buttons.get(type), c);

			_buttons.get(type).addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void	actionPerformed(java.awt.event.ActionEvent evt) {
					Pair pair = (Pair)_combos.get(type).getSelectedItem();

					if (pair.id != null){
						List<Pair> genomeVersions = DatabaseManager.instance().getGenomeVersionsUsing(type, pair.id);
						if (showValidationDialog(pair.comments, genomeVersions)){
							new TaskDeleting(pair.id, type, genomeVersions).execute();
						}
					}
				}
			});
		}

		fillCombos();

		// Informative text about the '*'
		c.insets = new Insets(10, 0, 10, 0);
		label = new JLabel("<html><i>" + _bundle.getString("wizard_genome_version_info_star") + "</i>");
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 3, 1, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		// Close button
		c.insets = new Insets(0, 0, 0, 0);
		button = new JButton(_bundle.getString("misc_close"));
		_buttonsAll.add(button);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 3, 1, GridBagConstraints.NONE);
		pane.add(button, c);

		// Actions performed
		button.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void	actionPerformed(java.awt.event.ActionEvent evt) {
				_dialog.setVisible(false);
            }
        });
	}

	private	boolean	showValidationDialog(String fileToDelete, List<Pair> genomeVersions){
		String		warning	= "<html>" + _bundle.getString("wizard_genome_version_warning") + " : <ul><li>" + fileToDelete + "</li></ul>";
		int			res		= -1;
		Object[]	options = {	_bundle.getString("misc_yes"),
								_bundle.getString("misc_no")};
		
		if (null != genomeVersions && genomeVersions.size() > 0){
			// Warning label for genome versions using this element
			warning += "<p>";
			if (genomeVersions.size() == 1){
				warning += _bundle.getString("wizard_genome_version_warning_singular2");
			}else{
				warning += _bundle.getString("wizard_genome_version_warning_plural2");
			}
			warning += " : <ul>";
			for (int i = 0; i < genomeVersions.size(); i++) {
				Pair pair = genomeVersions.get(i);

				warning += "<li>" + pair.comments + "</li>";
			}
			warning += "</ul>";
			warning += "<p><i>" + _bundle.getString("wizard_genome_version_suppress1")
					+ "\"" + fileToDelete + "\""
					+ _bundle.getString("wizard_genome_version_suppress2") + ".</i>";
		}

		res = JOptionPane.showOptionDialog(
							null,
							warning,
							_bundle.getString("misc_suppression"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							null,     //do not use a custom Icon
							options,  //the titles of buttons
							options[0]); //default button title

		return 0 == res;
	}

	@Override
	protected void resetWizardParameters() {
	}

	private	static	void	fillCombos(){
		fillCombo((DefaultComboBoxModel)_comboGenomeVersion.getModel(), DatabaseManager.instance().getGenomeVersionsPairs());
		fillCombo((DefaultComboBoxModel)_combos.get(PhysicDataTypes.STRUCTURAL_ANNOTATIONS).getModel(), DatabaseManager.instance().getStructuralAnnotationPairs());
		fillCombo((DefaultComboBoxModel)_combos.get(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS).getModel(), DatabaseManager.instance().getFunctionalAnnotationPairs());
		fillCombo((DefaultComboBoxModel)_combos.get(PhysicDataTypes.ANCHORS).getModel(), DatabaseManager.instance().getAnchorsPairs());
		fillCombo((DefaultComboBoxModel)_combos.get(PhysicDataTypes.DBLINKS).getModel(), DatabaseManager.instance().getDBLinksPairs());
	}

	private	static	void		fillCombo(	DefaultComboBoxModel	model,
											List<Pair>				pairs){
		model.removeAllElements();
		if (null != pairs){
			model.addElement(new Pair(null, _bundle.getString("misc_select")));
			for (int i = 0; i < pairs.size(); i++) {
				model.addElement(pairs.get(i));
			}
		}
	}

	public static class TaskDeleting extends SwingWorker<Void, Void>{
		public	TaskDeleting(	int				id,
								PhysicDataTypes type,
								List<Pair>		genomeVersions){
			_id = id;
			_type = type;
			_genomeVersions = genomeVersions;
		}

		@Override
		protected	Void	doInBackground() throws Exception {
			for (int i = 0; i < _buttonsAll.size(); i++) {
				_buttonsAll.get(i).setEnabled(false);
			}
			for (int i = 0; i < _combosAll.size(); i++) {
				_combosAll.get(i).setEnabled(false);
			}
			_progress.setVisible(true);
			_progress.setIndeterminate(true);
			DatabaseManager.instance().deletePhysicalType(_type, _id, _genomeVersions);

			return null;
		}

		@Override
		public	void	done(){
			fillCombos();
			for (int i = 0; i < _buttonsAll.size(); i++) {
				_buttonsAll.get(i).setEnabled(true);
			}
			for (int i = 0; i < _combosAll.size(); i++) {
				_combosAll.get(i).setEnabled(true);
			}
			_progress.setVisible(false);
		}

		private	int				_id	= -1;
		private	PhysicDataTypes _type;
		private	List<Pair>		_genomeVersions = null;
	}

	private	static	Map<PhysicDataTypes, JComboBox>	_combos				= null;
	private	static	Map<PhysicDataTypes, JButton>	_buttons			= null;
	private	static	List<JComboBox>					_combosAll			= null;
	private	static	List<JButton>					_buttonsAll			= null;
	private	static	JComboBox						_comboGenomeVersion	= new JComboBox();
	private	static	JProgressBar					_progress			= null;
	private	static	ResourceBundle					_bundle				= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
