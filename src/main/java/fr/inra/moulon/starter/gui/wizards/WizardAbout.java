/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 5, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.utils.Session;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JLabel;


public class WizardAbout extends AbstractWizard {
	@Override
	protected void setDialogParameters() {
		_dialog.setSize(new Dimension(400, 400));
		_dialog.setTitle("About...");
	}

	@Override
	protected	void		addComponentsToMainPane(Container pane) {
		GridBagConstraints	c			= new GridBagConstraints();
		JLabel				label		= null;
		JButton				close		= new JButton("Close");

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		label = new JLabel(new javax.swing.ImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_LOGO_UMRGV)));
		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.NONE);
		c.insets = new Insets(5, 5, 5, 5);
		pane.add(label, c);

		label = new JLabel("BioMercator");
		label.setFont(new Font("Lucida Grande", 0, 24));
		GridBagLayoutUtils.set_gbc(c, 1, 0, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		label = new JLabel(	"<html><body>" +
							"BioMercator is a genetic map compilation and meta-analysis software.<br/>" +
							"It includes directly algorithms from BioMercator previous version v2.1, and MetaQTL's algorithms in a plugin way.");
		GridBagLayoutUtils.set_gbc(c, 0, 1, 2, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(20, 5, 10, 5);
		pane.add(label, c);

		label = new JLabel(	"<html><body>" + 
							"<b>Version : </b>" + Session.instance().getValue(Session.VERSION) + "<br/>" +
							"<b>Authors : </b>Yannick DE OLIVEIRA, Lydia AIT BRAHAM, Olivier SOSNOWSKI, Johann JOETS<br/>" +
							"<b>Company : </b>INRA - UMR GQE-Le Moulon<br/>" +
							"<b>Date : </b>" + Session.instance().getValue(Session.DATE) + "<br/>" +
								"</body></html>");
		GridBagLayoutUtils.set_gbc(c, 0, 2, 2, 1, GridBagConstraints.HORIZONTAL);
		c.weighty = 1;
		c.insets = new Insets(10, 5, 10, 5);
		pane.add(label, c);


		GridBagLayoutUtils.set_gbc(c, 0, 3, 2, 1, GridBagConstraints.NONE);
		pane.add(close, c);

		// Actions performed
		close.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void	actionPerformed(java.awt.event.ActionEvent evt) {
				_dialog.setVisible(false);
            }
        });
	}

	@Override
	protected void resetWizardParameters() {
		
	}
}
