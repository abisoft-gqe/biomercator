/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.dragNdrop;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.file.FileRes;
import fr.inra.moulon.starter.gui.Canvas;
import fr.inra.moulon.starter.gui.MainFrame;

/**
 * This class is the dropping process handler; it's design to handle drops on
 * the 'JTabbedPane' component containing one or more Canvas; when dropped a
 * chromosome or map are dropped, a new tab is created.
 * @author Sosnow
 */
public class DropTransferHandlerTabbedCanvas extends DropTransferHandler {
	public DropTransferHandlerTabbedCanvas(int action) {
		super(action);
	}

	@Override
	public	void	addLinkageGroup(LinkageGroup lkg, MetaAnalysis metaAnalysis, MetaModel metaModel) {
/**
 * IF YOU DECIDE TO USE SEVERAL PANELS, YOU NEED TO RESOLVE THE PHYSICAL
 * DISPLAY BUG VISIBLE WHEN UNCOMMENTING THE LINES BELOW.
 */

/*		Canvas		canvas = MainFrame.createCanvas();

		canvas.addLinkageGroup(lkg, metaAnalysis, metaModel);
		super.updateMainFrameFields();
 */
	}

	@Override
	public	void	addFile(FileRes fileRes) {
		Canvas		canvas = MainFrame.createCanvas();

		canvas.addFile(fileRes);
	}
}
