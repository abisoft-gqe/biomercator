/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 5, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.controller.ControllerDataBase;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.graphic.designers.GOStatistics;
import fr.inra.moulon.starter.graphic.designers.GOStatisticsManager;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesigner;
import fr.inra.moulon.starter.graphic.designers.PhysicalManager;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.JFileChooserTools;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter.utils.StringTools;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.util.ResourceBundle;
import java.util.Set;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.UIManager;


public class WizardExportGFF extends AbstractDefaultWizard {
	private	final	static	Dimension	WIZARD_PANEL_SIZE	= new Dimension(600, 250);

	public	WizardExportGFF(String dialogTitle){
		super(null, dialogTitle, 2);
		super.addDialogSize(0, WIZARD_PANEL_SIZE);
	}

	@Override
	protected	void	addWizardPanels(JPanel panelFrame) {
		JPanel			panelExport1 = new JPanel();
		JPanel			panelExport2 = new JPanel();

		addGFFExportPanel1OnPanel(panelExport1);
		panelFrame.add(panelExport1, "1");

		addGFFExportPanel2OnPanel(panelExport2);
		panelFrame.add(panelExport2, "2");
	}

	@Override
	protected	void	launch(int panelId) {
		String			path			= null;
		boolean			genesOnly		= false;
		Double			start			= null;
		Double			end				= null;
		Set<String>		goGenesFamily	= null;
		String			lkgName			= null;
		MetaModel		metaModel		= null;

		super.launch(panelId);
		if (0 == panelId){
			metaModel = _lkgDes.getMetaModel();
			lkgName = _lkgDes.getLkgName();
			if (_features[1].isSelected()){
				// Export only genes
				genesOnly = true;
			}
			if (_radiosPos[0].isSelected()){
				// Export only the window
				double[] extrems = _lkgDes.getExtremWindowPositions();
				start = extrems[0];
				end = extrems[1];
			}
			if (_radiosPos[2].isSelected()){
				lkgName = null;
			}
			if (_radiosGo[1].isSelected()){
				// Export only features with chosen GOTerm
				String goSelected = GOStatisticsManager.instance().getInfo(GOStatistics.INFO_GOTERM_ID);
				goGenesFamily = PhysicalManager.instance().getGenesFamily(goSelected);
			}
			path = _fileName.getText();
			path = StringTools.addExtensionIfNeeded(path, "gff");
			new TaskExporting(	path,
								_structural_annotations_id,
								lkgName,
								genesOnly,
								start,
								end,
								goGenesFamily,
								metaModel).execute();
		}
	}

	public	void	setParams(	int						structural_annotations_id,
								LinkageGroupDesigner	lkgDes,
								LinkageGroupDesigner	lkgDesSon){
		_structural_annotations_id = structural_annotations_id;
		_lkgDes = lkgDes;
		_lkgDesSon = lkgDesSon;
	}

	/**
	 * Adds the components for GFF exporting onto the given panel.
	 * @param panelFrame The panel where to place the components.
	 */
	private	void			addGFFExportPanel1OnPanel(final JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();
		ButtonGroup			radioGrpFeature	= new ButtonGroup();
		ButtonGroup			radioGrpPos		= new ButtonGroup();
		ButtonGroup			radioGrpGO		= new ButtonGroup();
		JLabel				label			= null;
		JButton				browseButton	= new JButton(_bundle.getString("misc_browse"));
		String				goTermSelected	= GOStatisticsManager.instance().getInfo(GOStatistics.INFO_GOTERM_ID);
		String				radioStr		= null;
		Insets				sepInsets		= new Insets(0, 20, 0, 20);
		Insets				titlesInsets	= new Insets(0, 0, 20, 0);
		Insets				defaultInsets	= new Insets(0, 0, 0, 0);
		int					iRow			= 0;
		int					iCol			= 0;

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		_features = new JRadioButton[]{	new JRadioButton(_bundle.getString("misc_all")),
										new JRadioButton(_bundle.getString("misc_genes"))};

		// First filter column
		label = new JLabel("<html><b>Features</b>");
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.BOTH);
		c.insets = titlesInsets;
		pane.add(label, c);
		c.insets = defaultInsets;

		radioGrpFeature.add(_features[0]);
		c.anchor = GridBagConstraints.LINE_START;
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		_features[0].setSelected(true);
		pane.add(_features[0], c);

		radioGrpFeature.add(_features[1]);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(_features[1], c);

		++iCol;
		iRow = 0;
		GridBagLayoutUtils.set_gbc(c, iCol++, iRow, 1, 4, GridBagConstraints.VERTICAL);
		c.insets = sepInsets;
		pane.add(new JSeparator(JSeparator.VERTICAL), c);
		c.insets = defaultInsets;

		// Second filter column
		_radiosPos = new JRadioButton[]{new JRadioButton(_bundle.getString("wizard_gff_windows")),
										new JRadioButton(_bundle.getString("wizard_gff_chromosome")),
										new JRadioButton(_bundle.getString("wizard_gff_genome"))
										};
		label = new JLabel("<html><b>Position</b>");
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.BOTH);
		c.insets = titlesInsets;
		pane.add(label, c);
		c.insets = defaultInsets;

		radioGrpPos.add(_radiosPos[0]);
		_radiosPos[0].setSelected(true);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(_radiosPos[0], c);

		radioGrpPos.add(_radiosPos[1]);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(_radiosPos[1], c);

//		_radiosPos[2].setEnabled(false);
		radioGrpPos.add(_radiosPos[2]);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(_radiosPos[2], c);

		++iCol;
		iRow = 0;
		GridBagLayoutUtils.set_gbc(c, iCol++, iRow, 1, 4, GridBagConstraints.VERTICAL);
		c.insets = sepInsets;
		pane.add(new JSeparator(JSeparator.VERTICAL), c);
		c.insets = defaultInsets;

		// Third filter column
		_radiosGo = new JRadioButton[]{	new JRadioButton(_bundle.getString("wizard_gff_no_filter")),
										new JRadioButton()
										};

		label = new JLabel("<html><b>" + _bundle.getString("wizard_gff_go_filter") + "</b>");
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.BOTH);
		c.insets = titlesInsets;
		pane.add(label, c);
		c.insets = defaultInsets;

		radioGrpGO.add(_radiosGo[0]);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(_radiosGo[0], c);

		if (null != goTermSelected && !goTermSelected.equals("")){
			radioStr = _bundle.getString("wizard_gff_only_term") + " : " + goTermSelected;
			_radiosGo[1].setSelected(true);
		}else{
			radioStr = _bundle.getString("wizard_gff_term_info");
			_radiosGo[1].setEnabled(false);
			_radiosGo[0].setSelected(true);
		}
		_radiosGo[1].setText(radioStr);
		radioGrpGO.add(_radiosGo[1]);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(_radiosGo[1], c);

		// Browse button
		GridBagLayoutUtils.set_gbc(c, 0, 5, 1, 1, GridBagConstraints.NONE);
		pane.add(browseButton, c);

		GridBagLayoutUtils.set_gbc(c, 1, 5, 3, 1, GridBagConstraints.HORIZONTAL);
		pane.add(_fileName, c);
		addFieldVerification(_fileName, FIELD_NOT_NULL);

		//Actions performed
		browseButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void			actionPerformed(java.awt.event.ActionEvent evt) {
				String			exportStr = _bundle.getString("wizard_gff_export");
				JFileChooser	fileChooser = JFileChooserTools.createFileChooser(exportStr + "...", exportStr, false, "my_gff.gff");

				if (fileChooser.showOpenDialog(pane) == JFileChooser.APPROVE_OPTION){
					Session.instance().setValue(Session.CHOOSER_FILE_DIR, fileChooser.getCurrentDirectory());
					File file = fileChooser.getSelectedFile();
					_fileName.setText(file.getAbsolutePath());
				}
            }
        });
	}

	/**
	 * Adds the components for GFF exporting onto the given panel.
	 * @param panelFrame The panel where to place the components.
	 */
	private	void			addGFFExportPanel2OnPanel(final JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();
		Insets				titlesInsets	= new Insets(0, 0, 20, 0);
		int					iRow			= 0;
		int					iCol			= 0;

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		// PROGRESS BAR
		_progress = new JProgressBar();
		_progress.setVisible(false);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = titlesInsets;
		pane.add(_progress, c);

		// Label
		_label = new JLabel(_bundle.getString("wizard_gff_exporting") + "...",
							UIManager.getIcon("OptionPane.informationIcon"),
							JLabel.CENTER);
		c.anchor = GridBagConstraints.LINE_START;
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(_label, c);
	}

	public static class TaskExporting extends SwingWorker<Void, Void>{
		public	TaskExporting(	String		path,
								Integer		structural_annotations_id,
								String		seqname,
								boolean		genesOnly,
								Double		start_pb,
								Double		end_pb,
								Set<String>	goGenesFamily,
								MetaModel	metaModel){
			t_path						= path;
			t_structural_annotations_id	= structural_annotations_id;
			t_seqname					= seqname;
			t_genesOnly					= genesOnly;
			t_start_pb					= start_pb;
			t_end_pb					= end_pb;
			t_goGenesFamily				= goGenesFamily;
			t_metaModel					= metaModel;
		}

		@Override
		protected	Void	doInBackground() throws Exception {
			try{
				_progress.setVisible(true);
				_progress.setIndeterminate(true);
				ControllerDataBase.writeInFileGenes(
									t_path,
									t_structural_annotations_id,
									t_seqname,
									t_genesOnly,
									t_start_pb,
									t_end_pb,
									t_goGenesFamily,
									t_metaModel);
			}catch (Throwable t){
				t.printStackTrace(System.out);
			}
			return null;
		}

		@Override
		public	void	done(){
			_progress.setIndeterminate(false);
			_progress.setVisible(false);
			_label.setText(_bundle.getString("wizard_gff_exported"));
		}

		private	String		t_path;
		private	Integer		t_structural_annotations_id;
		private	String		t_seqname;
		private	boolean		t_genesOnly;
		private	Double		t_start_pb;
		private	Double		t_end_pb;
		private	Set<String>	t_goGenesFamily;
		private	MetaModel	t_metaModel;
	}

	private	int						_structural_annotations_id	= 0;
	private	LinkageGroupDesigner	_lkgDes						= null;
	private	LinkageGroupDesigner	_lkgDesSon					= null;
	private	JTextField				_fileName					= new JTextField();
	private	JRadioButton[]			_features					= null;
	private	JRadioButton[]			_radiosPos					= null;
	private	JRadioButton[]			_radiosGo					= null;
	private	static	JLabel			_label						= null;
	private	static	JProgressBar	_progress					= new JProgressBar();
	private	static	ResourceBundle	_bundle						= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
