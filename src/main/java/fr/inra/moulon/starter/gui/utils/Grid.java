/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.utils;

import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.controller.visitors.VisitorExtremsPosition;
import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesigner;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesignerGenetic;
import fr.inra.moulon.starter.graphic.utils.Anchor;
import fr.inra.moulon.starter.graphic.utils.FontTools;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignLinked;
import fr.inra.moulon.starter.utils.Session;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 *
 * @author sosnowski
 */
public class Grid {
	public Grid(){
		_gridCellsList = new ArrayList<GridCell>();
		_interCellsShapes = new ArrayList<ShapeDesign>();
	}

	/**
	 * Returns an iterator browsing all grid cells.
	 * @return An iterator browsing all grid cells
	 */
	public	Iterator<GridCell>	iterator(){
		return _gridCellsList.iterator();
	}

//	/**
//	 * Returns an iterator on the shapeDesigns corresponding to the dataId.
//	 * @return an iterator on the shapeDesigns corresponding to the dataId
//	 */
//	public	Iterator<ShapeDesign>	iteratorInterCellShapes(){
//		return _interCellsShapes.iterator();
//	}

	/**
	 * Transforms the given linkage group into linkage group designers, and
	 * adds them on the next cell.
	 * The method also sets the scrollers to control the correct linkage group
	 * designers.
	 * @param lkg The linkage group to add
	 */
	public	void				addLinkageGroup(final	LinkageGroup	lkg,
												final	MetaAnalysis	metaAnalysis,
												final	MetaModel		metaModel){
		LinkageGroupDesigner	lkgDesGen	= new LinkageGroupDesignerGenetic(lkg, metaAnalysis, metaModel);
		VisitorExtremsPosition	v			= new VisitorExtremsPosition();
		double					bioSize		= 0;
		double					pxlSize		= 0;
		double					maxPxlSize	= (Double)Session.instance().getValue(Session.LKGDES_SIZE);
		boolean					reDrawAll	= false;

		lkg.accept(v);
		bioSize = v.getExtrems()[1];
		if (bioSize <= _maxBioSize){
			pxlSize = bioSize*maxPxlSize/_maxBioSize;
			lkgDesGen.setMaxPxlSize(pxlSize);
			lkgDesGen.recreateShapes();
		}else{
			reDrawAll = true;
			_maxBioSize = bioSize;
		}

		_gridCellsList.add(new GridCell(lkgDesGen, null, null, 0, 0));

		if (reDrawAll){
			reProportion();
		}
		rearrange();
		findAnchors();
	}

	/**
	 * Redraw the linkage groups in the grid for a maximum possible size.
	 */
	public	void	reProportion(){
		double		bioSize		= 0;
		double		pxlSize		= 0;
		double		maxBioSize	= 0;
		double		maxPxlSize	= (Double)Session.instance().getValue(Session.LKGDES_SIZE);

		for (int i = 0; i < _gridCellsList.size(); i++) {
			GridCell cell = _gridCellsList.get(i);

			bioSize = cell.lkgDesignerGen.getLinkageGroup().getBioSize();
			maxBioSize = Math.max(maxBioSize, bioSize);
		}
		_maxBioSize = maxBioSize;
		for (int i = 0; i < _gridCellsList.size(); i++) {
			GridCell cell = _gridCellsList.get(i);

			bioSize = cell.lkgDesignerGen.getLinkageGroup().getBioSize();
			pxlSize = bioSize*maxPxlSize/_maxBioSize;
			cell.lkgDesignerGen.setMaxPxlSize(pxlSize);
			cell.lkgDesignerGen.recreateShapes();
		}
	}

//	/**
//	 * Redraw the linkage groups in the grid for a maximum possible size.
//	 */
//	public	void				reProportion(){
//		VisitorExtremsPosition	v			= new VisitorExtremsPosition();
//		double					bioSize		= 0;
//		double					pxlSize		= 0;
//		double					maxBioSize	= 0;
//		double					maxPxlSize	= (Double)Session.instance().getValue(Session.LKGDES_SIZE);
//
//		for (int i = 0; i < _gridCellsList.size(); i++) {
//			GridCell cell = _gridCellsList.get(i);
//
//			cell.lkgDesignerGen.getLinkageGroup().accept(v);
//			bioSize = v.getExtrems()[1];
//			maxBioSize = Math.max(maxBioSize, bioSize);
//		}
//		_maxBioSize = maxBioSize;
//		for (int i = 0; i < _gridCellsList.size(); i++) {
//			GridCell cell = _gridCellsList.get(i);
//
//			cell.lkgDesignerGen.getLinkageGroup().accept(v);
//			bioSize = v.getExtrems()[1];
//			pxlSize = bioSize*maxPxlSize/_maxBioSize;
//			cell.lkgDesignerGen.setMaxPxlSize(pxlSize);
//			cell.lkgDesignerGen.recreateShapes();
//		}
//	}

	/**
	 * This methods checks if the given linkage group designer is another
	 * @param lkgDesRef
	 * @return
	 */
	private	boolean	isSon(LinkageGroupDesigner lkgDesRef){
		boolean		res	= false;

		for (Iterator<GridCell> it = _gridCellsList.iterator(); it.hasNext() && !res;) {
			LinkageGroupDesigner lkgDes = it.next().lkgDesignerGen;

			if (null != lkgDes && lkgDes.hasSon(lkgDesRef)){
				res = true;
			}

		}

		return res;
	}

	/**.
	 * Manages the cascading zoom : if the linkage group is the only one present
	 * on the drawing area (other than a previous cascading zoomed version of
	 * it), a cascading zoom is created.
	 * @param lkgDesGenClicked
	 * @param posY_pxl
	 * @param height_pxl
	 */	public	void				manageCascadingZoom(LinkageGroupDesignerGenetic lkgDesGenClicked,
														double						posY_pxl,
														double						height_pxl){
		LinkageGroupDesignerGenetic	lkgDesGen	= null;

		if (null != lkgDesGenClicked && isCascadingAvailable()){
			if (!lkgDesGenClicked.hasSon()){// Create a new linkage group designer
				if (!isSon(lkgDesGenClicked)){
					lkgDesGenClicked.zoomOut();
				}
				lkgDesGen = new LinkageGroupDesignerGenetic(lkgDesGenClicked.getLinkageGroup(),
															lkgDesGenClicked.getMetaAnalysis(),
															lkgDesGenClicked.getMetaModel());
				lkgDesGen.setAnnealing(lkgDesGenClicked.getAnnealing());
				lkgDesGenClicked.initScroller(lkgDesGen, null, Anchor.CM_TO_CM);
				lkgDesGenClicked.enableScroller(posY_pxl, height_pxl, false);
				_gridCellsList.add(new GridCell(lkgDesGen, null, null, 0, 0));
				lkgDesGen.setMaxPxlSize(lkgDesGenClicked.getMaxPxlSize());
				lkgDesGen.recreateShapes();
				rearrange();
			}else{							// Set the scroller to the clicked position
				lkgDesGenClicked.enableScroller(posY_pxl, false);
				lkgDesGenClicked.recreateShapes();
			}
		}
	}

	/**
	 * This methods is here to check if the cascading zooming is available, ie
	 * if the linkage groups already present (if any) are different views of the
	 * same one.
	 * @return if the cascading option is available.
	 */
	private	boolean				isCascadingAvailable(){
		boolean					res		= true;
		LinkageGroupDesigner	lkgPrev	= null;

		if (_gridCellsList.size() > 0){
			lkgPrev = _gridCellsList.get(0).lkgDesignerGen;
			if (null != lkgPrev && null == _gridCellsList.get(0).lkgDesignerPhy){
				for (int i = 1; i < _gridCellsList.size() && res; ++i) {
					LinkageGroupDesigner lkgCur = _gridCellsList.get(i).lkgDesignerGen;

					if (!lkgPrev.hasSon(lkgCur)){
						res = false;
					}
					lkgPrev = lkgCur;
				}
			}
		}

		return res;
	}

	/**
	 * This methods checks if a single zoom (when mouse wheeling and dragging
	 * inside) is available; ie if the cascading view isn't possible.
	 * @return if the single view is available.
	 */
	public	boolean	isSingleZoomAvailable(){
		return (!isCascadingAvailable());
	}

	/**
	 * This methods gives the cells number the grid contains.
	 * @return The cells number the grid contains.
	 */
	public	int	size(){
		return _gridCellsList.size();
	}

	/**
	 * Sets the mouseOn attributes of all cells;
	 * If no cell is found at the given point position, all cells will be
	 * considered as MOUSE OFF.
	 * If a cell is found, corresponding to the point position, the cell will be
	 * considered as MOUSE_ON or MOUSE_ON_EDGE.
	 * @param point The mouse point
	 * @return If a change has been done and the panel needs a repaint()
	 */
	public	boolean	setMouseOn(Point p){
		Point		pTranslated	= null;
		boolean		res			= false;

		for (Iterator<GridCell> it = _gridCellsList.iterator(); it.hasNext();) {
			GridCell gridCell = it.next();

			pTranslated = new Point(p.x - (int)(gridCell.xPos),
									p.y - (int)(gridCell.yPos));
			res |= gridCell.setMousePosition(pTranslated);
		}

		return res;
	}

	/**
	 * Sets the mouseOn attributes of all cells;
	 * If no cell is found at the given point position, all cells will be
	 * considered as MOUSE OFF.
	 * If a cell is found, corresponding to the point position, the cell will be
	 * considered as MOUSE_ON or MOUSE_ON_EDGE.
	 * @param point The mouse point
	 * @return If a change has been done and the panel needs a repaint()
	 */
	public	boolean	setMouseOff(){
		for (Iterator<GridCell> it = _gridCellsList.iterator(); it.hasNext();) {
			GridCell gridCell = it.next();
			gridCell.setMouseOff();
		}

		return true;
	}

	/**
	 * Launches the redraw of all cells.
	 */
	public	void	redrawAllCells(){
		for (Iterator<GridCell> it = _gridCellsList.iterator(); it.hasNext();) {
			it.next().reDrawShapes();
		}
	}
	/**
	 * Rearranges the gridCell order and positions.
	 */
	public	Dimension	rearrange(){
		int				xCur	= 0;
		int				yCur	= 0;
		int				xMax	= _widthMax;
		int				hMax	= 0;
		int				xReal	= 0;

		for (Iterator<GridCell> it = _gridCellsList.iterator(); it.hasNext();) {
			GridCell gridCell = it.next();

//			gridCell.reDrawShapes();
			// Change line if the total width is above the maximum
			if (xCur + gridCell.getWidth() >= xMax){
				xCur = 0;
				yCur += hMax;
				hMax = 0;
			}
			// Sets the grid cell coordinates
			if (!gridCell.getSelected()){
				gridCell.xPos = xCur;
				gridCell.yPos = yCur;
			}
			xCur += gridCell.getWidth();
			xReal = Math.max(xReal, xCur);
			hMax = Math.max(hMax, (int)gridCell.getHeight());
		}

		return new Dimension(xReal, yCur+hMax);
	}

	/**
	 * Removes all grid cells from the grid.
	 */
	public	void	clean(){
		_gridCellsList.clear();
		_maxBioSize = 0;
	}

	class Ranked<T>{
		public	Ranked(T obj, int rankLeft, int rankRight){
			this.obj		= obj;
			this.rankLeft	= rankLeft;
			this.rankRight	= rankRight;
		}

		T		obj;
		int		rankLeft;
		int		rankRight;
		boolean	enabled;
	}

	/**
	 * Empties and fill the inter cells shapes with links for dynamic
	 * comparisons. The anchors must already be found using findAnchors().
	 */
	public	void					createAnchorsShapes(){
		LinkageGroupDesignerGenetic	curLkgGenDes	= null;
		LinkageGroupDesignerGenetic	prevLkgGenDes	= null;
		GridCell					gridCellRight	= null;
		GridCell					gridCellLeft	= null;
		Iterator<GridCell>			itCells			= _gridCellsList.iterator();
		ShapeDesignLinked			shapeLeft		= null;
		ShapeDesignLinked			shapeRight		= null;
		int							iAnchorList		= 0;
		int							longestStrWidth	= 0;
		Color						color			= null;
		AnchorGen					anchor			= null;
		Map<String, AnchorGen>		anchors			= null;

		_interCellsShapes.clear();
		if (itCells.hasNext()){
			gridCellRight = itCells.next();
		}
		while (itCells.hasNext()) {
			gridCellLeft = gridCellRight;
			gridCellRight = itCells.next();
			prevLkgGenDes = (LinkageGroupDesignerGenetic)gridCellLeft.lkgDesignerGen;
			curLkgGenDes = (LinkageGroupDesignerGenetic)gridCellRight.lkgDesignerGen;

			if (displayLinks(prevLkgGenDes, curLkgGenDes)){
				anchors = _anchorsList.get(iAnchorList++);

				// Finds the longest anchor name
				longestStrWidth = 0;
				for (Iterator<String> itMarkersName = prevLkgGenDes.iteratorMarkersName(); itMarkersName.hasNext();) {
					String markersNamePrev = itMarkersName.next();

					longestStrWidth = Math.max(longestStrWidth, FontTools.getWidth(markersNamePrev));
				}

				for (Iterator<String> itMarkersName = prevLkgGenDes.iteratorMarkersName(); itMarkersName.hasNext();) {
					String markersNamePrev = itMarkersName.next();

					if (	null != (anchor = anchors.get(markersNamePrev))
						&&	null != (shapeLeft = prevLkgGenDes.getMarkerNameShape(markersNamePrev))
						&&	null != (shapeRight = curLkgGenDes.getMarkerNameShape(markersNamePrev))){
						double	xStart = gridCellLeft.xPos + gridCellLeft.getWidth() - LinkageGroupDesigner.RIGHT_MARGIN;
						double	xEnd = LinkageGroupDesigner.LEFT_MARGIN + (shapeRight.x + gridCellRight.xPos);
						double	width = xEnd - xStart;
						double	yStart = shapeLeft.strY + gridCellLeft.yPos - FontTools.getHeight()/3;

						color = anchor.inverted?new Color(200, 0, 0, 150):new Color(0, 0, 200, 150);
						_interCellsShapes.add(
							new ShapeDesign(ShapeDesign.LINE,
											xStart,
											yStart,
											width,
											(shapeRight.y + gridCellRight.yPos) - yStart,
											color));

						xEnd = xStart;
						xStart = xStart - (longestStrWidth - FontTools.getWidth(anchor.name));
						width = xEnd - xStart;
						_interCellsShapes.add(new ShapeDesign(	ShapeDesign.LINE,
																xStart,
																yStart,
																width,
																0,
																color));
					}

				}
			}
		}
	}

	/**
	 * Returns an iterator on the shapeDesigns corresponding to the dataId.
	 * @return an iterator on the shapeDesigns corresponding to the dataId
	 */
	public	Iterator<ShapeDesign>	iteratorInterCellShapes(){
		return _interCellsShapes.iterator();
	}

	/**
	 * Finds and sets the common markers list between the grid cells.
	 */
	public	void					findAnchors(){
		VisitorEltGet<Marker>		visitor				= new VisitorEltGet<Marker>(Marker.class);
		LinkageGroupDesignerGenetic	lkgGenDesRight		= null;
		LinkageGroupDesignerGenetic	lkgGenDesLeft		= null;
		GridCell					gridCellRight		= null;
		GridCell					gridCellLeft		= null;
		Iterator<GridCell>			itCells				= _gridCellsList.iterator();
		List<AnchorGen>				anchors				= null;
		int							rank				= 0;
		Marker						markerLeft			= null;
		Marker						markerRight			= null;

		_anchorsList = new ArrayList<Map<String, AnchorGen>>();
		if (itCells.hasNext()){
			gridCellRight = itCells.next();
		}
		while (itCells.hasNext()) {
			gridCellLeft = gridCellRight;
			gridCellRight = itCells.next();
			lkgGenDesLeft = (LinkageGroupDesignerGenetic)gridCellLeft.lkgDesignerGen;
			lkgGenDesRight = (LinkageGroupDesignerGenetic)gridCellRight.lkgDesignerGen;
			anchors = new ArrayList<AnchorGen>();

			// Parsing and checking for right and left linkage group's common markers.
			visitor.resetList();
			lkgGenDesLeft.getLinkageGroup().accept(visitor);
			if (null != visitor.getList()){
				for (Iterator<Content> itContent = visitor.getList().iterator(); itContent.hasNext();) {
					try{
						markerLeft = (Marker)itContent.next();
						markerRight = (Marker)lkgGenDesRight.getLinkageGroup().get(markerLeft.getName());

						if (null != markerRight){
							anchors.add(new AnchorGen(	markerLeft.getName(),
														markerLeft.getPosition(),
														markerRight.getPosition()));
						}
					} catch(Exception e){}
				}
			}

			Collections.sort(anchors, new AnchorGenComparator(AnchorGenComparator.POSITION.RIGHT));
			rank = 0;
			for (Iterator<AnchorGen> it = anchors.iterator(); it.hasNext();) {
				it.next().rankRight = rank++;
			}
			setInvertedAnchors(anchors);
			Map<String, AnchorGen> anchorsMap = new HashMap<String, AnchorGen>();
			for (Iterator<AnchorGen> it = anchors.iterator(); it.hasNext();) {
				AnchorGen anchorGen = it.next();
				anchorsMap.put(anchorGen.name, anchorGen);
			}
			_anchorsList.add(anchorsMap);
		}
	}

	/**
	 * Sets the inverted attribute for all given anchors.
	 * @param anchorsRight The anchors list ordered by right position.
	 */
	private	void			setInvertedAnchors(List<AnchorGen> anchorsRight){
		List<AnchorGen>		anchorsLeft		= new LinkedList<AnchorGen>();
		Iterator<AnchorGen>	it_gen			= null;
		Iterator<AnchorGen>	it_phy			= null;
		AnchorGen			anchorGen		= null;
		AnchorGen			anchorPhy		= null;
		int					rank_diff		= 0;
		boolean				incr_anchor_gen	= true;
		boolean				incr_anchor_phy	= true;
		boolean				end_of_anchors	= false;

		if (null != anchorsRight){
			for (int i = 0; i < anchorsRight.size(); ++i){
				anchorsLeft.add(anchorsRight.get(i));
			}
			Collections.sort(anchorsLeft, new AnchorGenComparator(AnchorGenComparator.POSITION.LEFT));
			for (int i = 0; i < anchorsLeft.size(); ++i){
				anchorsLeft.get(i).rankLeft = i;
			}

			it_gen = anchorsLeft.iterator();
			it_phy = anchorsRight.iterator();
			while (it_gen.hasNext() && it_phy.hasNext()){
				if (incr_anchor_gen){
					anchorGen = it_gen.next();
					while (!anchorGen.enabled && !end_of_anchors){
						try{
							anchorGen = it_gen.next();
						}catch (NoSuchElementException e){
							end_of_anchors = true;
						}
					}
				}
				if (incr_anchor_phy){
					anchorPhy = it_phy.next();
					while (!anchorPhy.enabled && !end_of_anchors){
						try{
							anchorPhy = it_phy.next();
						}catch (NoSuchElementException e){
							end_of_anchors = true;
						}
					}
				}

				if (!end_of_anchors){
					if (anchorGen != anchorPhy){
						rank_diff = anchorGen.rankRight - anchorPhy.rankLeft;
						if (rank_diff > 0){
							anchorGen.enabled = false;
							anchorGen.inverted = true;
							incr_anchor_gen = true;
							incr_anchor_phy = false;
						}else if (rank_diff < 0){
							anchorPhy.enabled = false;
							anchorPhy.inverted = true;
							incr_anchor_gen = false;
							incr_anchor_phy = true;
						}else{
							anchorGen.enabled = false;
							anchorPhy.enabled = false;
							anchorGen.inverted = true;
							anchorPhy.inverted = true;
							incr_anchor_gen = true;
							incr_anchor_phy = true;
						}
					}else{
						incr_anchor_gen = true;
						incr_anchor_phy = true;
					}
				}
			}
		}
	}

	public class AnchorGen {
		public	AnchorGen(	String				name,
							Double				posLeft,
							Double				posRight){
			this.name			= name;
			this.posLeft		= posLeft;
			this.posRight		= posRight;
		}

		String				name;
		Double				posLeft;
		Double				posRight;
		Integer				rankLeft;
		Integer				rankRight;
		Boolean				enabled		= Boolean.TRUE;
		Boolean				inverted	= Boolean.FALSE;
	}

	public static class AnchorGenComparator implements Comparator<AnchorGen>{
		public enum POSITION {LEFT, RIGHT}

		AnchorGenComparator(POSITION position){
			super();
			_position = position;
		}

		@Override
		public int	compare(AnchorGen a1, AnchorGen a2){
			int		res = 0;

			switch (_position){
				case LEFT:
					res = (a1.posLeft < a2.posLeft)?-1:+1;
					break;
				case RIGHT:
					res = (a1.posRight < a2.posRight)?-1:+1;
					break;
			}

			return  res;
		}

		private	POSITION	_position = POSITION.LEFT;
	}

	private	boolean	displayLinks(LinkageGroupDesigner prevLkgDes, LinkageGroupDesigner curLkgDes){
		boolean		res = true;

		if (prevLkgDes.hasSon(curLkgDes)){
			res = false;
		}

		return res;
	}

	/**
	 * Returns a unselected GridCell at the Point coordinates; null otherwise.
	 * @param point The point where the cell might be
	 * @return The GridCell at the Point coordinates; null otherwise
	 */
	public	GridCell	getGridCell(Point point){
		GridCell		gridCellRes = null;
		Point			pTranslated	= null;

		for (Iterator<GridCell> it = _gridCellsList.iterator(); it.hasNext() && gridCellRes == null;) {
			GridCell gridCell = it.next();

			pTranslated = new Point(point.x - (int)(gridCell.xPos),
									point.y - (int)(gridCell.yPos));
			if (gridCell.contains(pTranslated) && !gridCell.getSelected()){
				gridCellRes = gridCell;
			}
		}
		return gridCellRes;
	}

	/**
	 * Returns the first GridCell found; null if no GridCells is present.
	 * @return The first GridCell found; null if no GridCells i present.
	 */
	public	GridCell	getFirstGridCell(){
		GridCell		gridCellRes = null;

		if (_gridCellsList.size() > 0){
			gridCellRes = _gridCellsList.get(0);
		}

		return gridCellRes;
	}

	/**
	 * Removes the cell from the grid; rearrange the grid afterwards.
	 * @param cell The cell to remove
	 */
	public	void	remove(GridCell cellToRemove){
		boolean		found = false;

		for (Iterator<GridCell> it = _gridCellsList.iterator(); it.hasNext() && !found;) {
			GridCell gridCell = it.next();

			if (cellToRemove == gridCell){
				it.remove();
				found = true;
			}
		}
		for (Iterator<GridCell> it = _gridCellsList.iterator(); it.hasNext();) {
			GridCell gridCell = it.next();

			if (gridCell.lkgDesignerGen.hasSon(cellToRemove.lkgDesignerGen)){
				gridCell.lkgDesignerGen.disableScroller();
				gridCell.lkgDesignerGen.removeSon();
			}
		}
		if (found){
			reProportion();
			rearrange();
			findAnchors();
		}
	}

	public	void	switchCells(GridCell cell1, GridCell cell2){
		int			index1 = -1;
		int			index2 = -1;

		for (int i = 0; i < _gridCellsList.size() && (-1 == index1 || -1 == index2); ++i) {
			GridCell gridCell = _gridCellsList.get(i);

			if (gridCell == cell1){
				index1 = i;
			}
			if (gridCell == cell2){
				index2 = i;
			}
		}

		if (-1 != index1 && -1 != index2){
			_gridCellsList.set(index1, cell2);
			_gridCellsList.set(index2, cell1);
			rearrange();
			findAnchors();
		}
	}

	/**
	 * Sets the maximum width the grid is allowed to use for drawing.
	 * @param widthMax
	 */
	public	void	setWidthMax(int widthMax){
		_widthMax = widthMax;
	}

	private	int								_widthMax			= 1000;
	private List<GridCell>					_gridCellsList		= null;
	private	double							_maxBioSize			= 0;
	private	List<ShapeDesign>				_interCellsShapes	= null;
	private	List<Map<String, AnchorGen>>	_anchorsList		= null;
}
