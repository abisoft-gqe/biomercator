/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 01-Feb-2012
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.utils.Session;
import java.awt.GridBagConstraints;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;


public class WizardProgressDialog extends AbstractDefaultWizard{
	public WizardProgressDialog(String path, String dialogTitle, int nbSteps){
		super(path, dialogTitle, 1);
		_nbSteps = nbSteps;
	}

	/**
	 * Call this method as soon as a new step is processed.
	 * @param fileName The file name
	 */
	public	void	newStep(String stepName){
		_labelFileIt.setText(stepName + "(" + ++_curFileIdx + "/" + _nbSteps + ")");
	}

	public	void	end(){
		_progressBar.setIndeterminate(false);
		_progressBar.setValue(100);
	}

	/**
	 * Adds the different wizard's steps as different panels.
	 * @param panelFrame The frame JPanel where panels will be placed.
	 */
	@Override
	protected	final	void	addWizardPanels(JPanel panelFrame){
		JPanel					mainPanel	= new JPanel();
		int						cardId		= 1;

		addProgressBarOnPanel(mainPanel);
		panelFrame.add(mainPanel, String.valueOf(cardId++));
	}

	private	void			addProgressBarOnPanel(JPanel panelFrame){
		GridBagConstraints	c		= new GridBagConstraints();
		JLabel				label	= null;

		label = new JLabel(_bundle.getString("misc_exporting") + " : ");
		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.BOTH);
		c.weightx = 0;
		panelFrame.add(label, c);

		GridBagLayoutUtils.set_gbc(c, 1, 0, 1, 1, GridBagConstraints.BOTH);
		c.weightx = 0;
		panelFrame.add(_labelFileIt, c);

		GridBagLayoutUtils.set_gbc(c, 0, 1, 2, 1, GridBagConstraints.BOTH);
		c.weightx = 1;
		panelFrame.add(_progressBar, c);
		_progressBar.setIndeterminate(true);
	}

	private			int				_nbSteps		= -1;
	private			int				_curFileIdx		= 0;
	private			JProgressBar	_progressBar	= new JProgressBar();
	private			JLabel			_labelFileIt	= new JLabel();
	private	static	ResourceBundle	_bundle			= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
