/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 02-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.animations;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import org.jfree.chart.plot.PiePlot;


public class AnimationChartColor extends Animation{
	public	void		start(	int		delay,
								PiePlot	plot,
								int		sectionID,
								Color	color){
		ActionListener	actionListener = null;

		stop();
		_plot = plot;
		_sectionID = sectionID;
		_color = color;

		actionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (_alternator){
					_plot.setSectionPaint(_sectionID, new Color(42, 42, 42));
				}else{
					_plot.setSectionPaint(_sectionID, _color);
				}
				_alternator = !_alternator;
			}
		};
		_timer = new Timer(delay, actionListener);
		_timer.start();
	}

	@Override
	public	void	stop(){
		super.stop();
		if (null != _plot && null != _color){
			_plot.setSectionPaint(_sectionID, _color);
			_plot = null;
			_color = null;
			_sectionID = -1;
			_alternator = true;
		}
	}

	private	boolean	_alternator	= true;
	private	PiePlot	_plot		= null;
	private	Color	_color		= null;
	private	int		_sectionID	= 0;
}
