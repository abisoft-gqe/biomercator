/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 05-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.fileTools.tasks.TaskLoadingFile;
import fr.inra.moulon.starter.fileTools.tasks.TaskLoadingStructuralAnnotations;
import java.util.ArrayList;
import java.util.List;

public class WizardExploringStructuralAnnotations extends WizardExploringCSV_Multi{
	public WizardExploringStructuralAnnotations(WizardGenomeVersion wizardGenomeVersion){
		super("Explain columns", wizardGenomeVersion);
	}

	@Override
	protected	TaskLoadingFile	createTask(List<Field> fields){
		return new TaskLoadingStructuralAnnotations(_wizardGenomeVersion, _csvFile, fields, getFieldsSeparator());
	}

	@Override
	protected Field[]	generateFields() {
		List<Field>		fields	= new ArrayList<Field>();
		int				i		= 2;

		fields.add(new Field("seqname",			i++, Field.STRING));
		fields.add(new Field("source",			i++, Field.STRING));
		fields.add(new Field("feature_type",	i++, Field.STRING));
		fields.add(new Field("start",			i++, Field.INT));
		fields.add(new Field("end",				i++, Field.INT));
		fields.add(new Field("score",			i++, Field.STRING));
		fields.add(new Field("strand",			i++, Field.STRING));
		fields.add(new Field("frame",			i++, Field.STRING));
		fields.add(new Field("attributes",		i++, Field.STRING));
		fields.add(Field.DEFAULT);

		return fields.toArray(new Field[fields.size()]);
	}
}
