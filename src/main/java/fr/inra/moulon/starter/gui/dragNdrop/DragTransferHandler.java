/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.dragNdrop;

import fr.inra.moulon.starter.datamodel.container.Content;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.TreePath;

/**
 * This class is the dragging process handler; it's design to work with a JTree.
 * Getting a 'Content' for the JTtree, this class transforms it into a
 * transferableNode that will be handle by the 'DropTransferHandler'.
 * @author Sosnow
 */
public class DragTransferHandler extends TransferHandler {
	@Override
	public int getSourceActions(JComponent comp) {
		return COPY;
	}

	/**
	 * Creates a transferable as a List<Content> containing as object as the
	 * jTree drag offered (starting at the Project).
	 * @param comp The JTree
	 * @return The transferable
	 */
	@Override
	public Transferable createTransferable(JComponent comp) {
		Transferable	t = null;
		List<Content>	list = new ArrayList<Content>();
		TreePath		node = ((JTree)comp).getSelectionPath();

		if (null != node){
			for (int i = 1; i < node.getPathCount(); ++i){
				list.add((Content)node.getPathComponent(i));
			}

			t = new TransferableNode(list);
		}

		return t;
	}

	@Override
	public void exportDone(JComponent comp, Transferable trans, int action) {
		if (action != MOVE) {
			return;
		}
	}

}
