/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.JFileChooserTools;
import fr.inra.moulon.starter.utils.Session;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;


public class WizardGeneticFilesLoaderManual extends AbstractDefaultWizard{
	private	final	static	Dimension	WIZARD_SUMMARY		= new Dimension(600, 600);
	private	final	static	int			NB_CARDS			= 3;

	public WizardGeneticFilesLoaderManual(String path, String dialogTitle){
		super(path, dialogTitle, NB_CARDS);
	}

	@Override
	protected	void		launch(int panelId){
		WizardExploringCSV	wizard	= null;

		super.launch(panelId);
		if (1 == panelId){	// The second card has been validated
			wizard = new WizardExploringGeneticMap();
			wizard.setFile(_fileToLoad);
			wizard.show();
		}
	}

	/**
	 * Adds the different wizard's steps as different panels.
	 * @param panelFrame The frame JPanel where panels will be placed.
	 */
	@Override
	protected	void	addWizardPanels(JPanel panelFrame){
		JPanel			panelProjectChoice = new JPanel();
		JPanel			panelListFiles = new JPanel();
		JPanel			panelSummary = new JPanel();

		super.addProjectChoicePanelOnPanel(panelProjectChoice);
		panelFrame.add(panelProjectChoice, "1");

		addFileLoadingPanelOnPanel(panelListFiles);
		panelFrame.add(panelListFiles, "2");

		addSummaryLoadingPanelOnPanel(panelSummary);
		panelFrame.add(panelSummary, "3");
	}

	/**
	 * THE WIZARD'S SECOND STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	private	void			addFileLoadingPanelOnPanel(final JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();
		final JTextField	fileTextField	= new JTextField();
		JButton				browseButton	= new JButton("Browse");

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.weightx = 1;
		pane.add(browseButton, c);

		GridBagLayoutUtils.set_gbc(c, 0, 1, 1, 1, GridBagConstraints.BOTH);
		c.weighty = 1;
		c.insets = new Insets(10, 0, 0, 0);
		pane.add(fileTextField, c);

		//Actions performed
		browseButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void			actionPerformed(java.awt.event.ActionEvent evt) {
				JFileChooser	fileChooser = JFileChooserTools.createFileChooser("Choose file...", "Open", true, null);

				if (fileChooser.showOpenDialog(pane) == JFileChooser.APPROVE_OPTION){
					Session.instance().setValue(Session.CHOOSER_FILE_DIR, fileChooser.getCurrentDirectory());
					for (int i = 0; i < fileChooser.getSelectedFiles().length; i++) {
						File file = fileChooser.getSelectedFiles()[i];

						_fileToLoad = file;
						fileTextField.setText(_fileToLoad.getAbsolutePath());
					}
				}
            }
        });
	}

	/**
	 * THE WIZARD'S THIRD (and last) STEP
	 * Shows a summary of loaded files.
	 * @param panelFrame The panel where to place summary.
	 */
	private void			addSummaryLoadingPanelOnPanel(final JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();
		JLabel				label			= new JLabel("Summary");
		JPanel				summaryPanel	= new JPanel();
		JScrollPane			scrollPane		= new JScrollPane(summaryPanel);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.weightx = 1;
		pane.add(label, c);

		GridBagLayoutUtils.set_gbc(c, 0, 1, 1, 1, GridBagConstraints.BOTH);
		scrollPane.setSize(WIZARD_SUMMARY);
		c.weighty = 1;
		c.insets = new Insets(10, 0, 0, 0);
		pane.add(scrollPane, c);
	}

	private	File	_fileToLoad = null;
}
