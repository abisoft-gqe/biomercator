/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 06-Aug-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.tree;

import fr.inra.moulon.starter.controller.visitors.VisitorFindMetaAnalysis;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.Workspace;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.utils.Session;
import java.awt.Component;
import java.net.URL;
import java.util.HashMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;


public class TreeCellIconRenderer extends DefaultTreeCellRenderer {
	private final static	int	ICON_IMG		= 1;
	private final static	int	ICON_TXT		= 2;
	private final static	int	ICON_MAP		= 3;
	private final static	int	ICON_CHR		= 4;
	private final static	int	ICON_LKG		= 5;
	private final static	int	ICON_MAP_W_META	= 6;
	private final static	int	ICON_CHR_W_META	= 7;
	private final static	int	ICON_LKG_W_META	= 8;



	public TreeCellIconRenderer() {
		super();
		_icons = new HashMap<Integer, Icon>();
		_icons.put(ICON_IMG,		createImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_JPG)));
		_icons.put(ICON_TXT,		createImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_TXT)));
		_icons.put(ICON_MAP,		createImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_MAP)));
		_icons.put(ICON_CHR,		createImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_CHR)));
		_icons.put(ICON_LKG,		createImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_LKG)));
		_icons.put(ICON_MAP_W_META,	createImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_MAP_W_META)));
		_icons.put(ICON_CHR_W_META,	createImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_CHR_W_META)));
		_icons.put(ICON_LKG_W_META,	createImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_LKG_W_META)));
	}

	@Override
	public Component getTreeCellRendererComponent(	JTree tree,
													Object value,
													boolean sel,
													boolean expanded,
													boolean leaf,
													int row,
													boolean hasFocus) {
		VisitorFindMetaAnalysis	v				= new VisitorFindMetaAnalysis();
		boolean					hasMetaAnalysis	= false;

		try{
			((Container)value).accept(v);
			hasMetaAnalysis = v.hasMetaAnalysis();
		}catch(Exception e){
		}

		super.getTreeCellRendererComponent(
			tree, value, sel,
			expanded, leaf, row,
			hasFocus);

		if (value instanceof Workspace) {
			setIcon(null);
		} else if (value instanceof MapGene) {
			if (hasMetaAnalysis){
				setIcon(_icons.get(ICON_MAP_W_META));
			}else{
				setIcon(_icons.get(ICON_MAP));
			}
		} else if (value instanceof Chromosome) {
			if (hasMetaAnalysis){
				setIcon(_icons.get(ICON_CHR_W_META));
			}else{
				setIcon(_icons.get(ICON_CHR));
			}
		} else if (value instanceof LinkageGroup) {
			if (hasMetaAnalysis){
				setIcon(_icons.get(ICON_LKG_W_META));
			}else{
				setIcon(_icons.get(ICON_LKG));
			}
		} else if (value instanceof MetaModel) {
			setIcon(_icons.get(ICON_LKG_W_META));
		} else {
			setToolTipText(null); //no tool tip
		}

		return this;
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	public static ImageIcon createImageIcon(URL url) {
		ImageIcon			imageIcon = null;

		try{
			imageIcon = new ImageIcon(url);
		}catch(Exception e){}

		return imageIcon;
	}

	private HashMap<Integer, Icon> _icons = null;
}
