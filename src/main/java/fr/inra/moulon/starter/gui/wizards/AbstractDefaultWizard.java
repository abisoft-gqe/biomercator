/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 5, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.controller.ControllerGUI;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.wizards.utils.JComboBoxAutoTest;
import fr.inra.moulon.starter.gui.wizards.utils.JComponentAutoTest;
import fr.inra.moulon.starter.gui.wizards.utils.JTextFieldAutoTest;
import fr.inra.moulon.starter.gui.wizards.utils.JTreeAutoTest;
import fr.inra.moulon.starter.utils.Session;
import java.awt.CardLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * This class is an abstract default wizard class; is contains all common 
 * methods (such as the wizard main pane creation, the next, previous and cancel
 * buttons actions). The main method to make the wizard display its dialog is
 * the 'show(String)' method.
 * All subclasses must implements the following abstract methods :
 *  - void addWizardPanelsOnMainPanel(JPanel panelFrame) : for adding the panels
 * of the specific wizard
 *  - void launch(int panelId) : for associating an action per panelID
 * 
 * @author sosnowski
 */
public abstract class AbstractDefaultWizard extends AbstractWizard {
	
	private static ResourceBundle _bundle = (ResourceBundle) Session.instance()
			.getValue(Session.RESSOURCE_BUNDLE);
	
	private	final	static	String		NEW_PROJECT			= _bundle.getString("wizard_new_project");
	private	final	static	String		LABEL_NEXT			= _bundle.getString("wizard_next");
	private	final	static	String		LABEL_PREV			= _bundle.getString("wizard_prev");
	private	final	static	String		LABEL_CANCEL		= _bundle.getString("wizard_cancel");
	private	final	static	String		LABEL_NEXT_FINISH	= _bundle.getString("wizard_finish");
	private	final	static	Dimension	DEFAULT_DIALOG_SIZE	= new Dimension(400, 200);//new Dimension(444, 400);

	protected	final	static	int		FIELD_NOT_NULL		= 1;
	protected	final	static	int		FIELD_INTEGER		= 2;
	protected	final	static	int		FIELD_DOUBLE		= 3;
	protected	final	static	int		FIELD_MAP_RES		= 4;
	protected	final	static	int		FIELD_FILE_RES		= 5;
	protected	final	static	int		FIELD_META_RES		= 6;

	public	AbstractDefaultWizard(String path, String dialogTitle, int nbCards){
		_dialogTitle	= dialogTitle;
		_nbCards		= nbCards;
	}

	@Override
	public	String	getProjectName(){
		return _projectName;
	}

	public	void	setDialogTitle(String title){
		_dialogTitle = title;
	}

	/**
	 * Adds the different wizard's steps as different panels.
	 * @param panelFrame The frame JPanel where panels will be placed.
	 */
	protected	abstract	void	addWizardPanels(JPanel panelFrame);

	private	void	changeHasBeenDone(){
		boolean		valid		= true;
		boolean		fieldValid	= false;

		for (Iterator<JComponentAutoTest> itFields = _fields.keySet().iterator(); itFields.hasNext();) {
			JComponentAutoTest comp = itFields.next();

			switch (_fields.get(comp)){
				case FIELD_NOT_NULL:
					fieldValid = comp.isNotNull();
					comp.setValidity(fieldValid);
					valid &= fieldValid;
					break;
				case FIELD_INTEGER:
					try{
						Integer.valueOf(comp.getText());
					}catch(NumberFormatException e){
						valid = false;
					}
					break;
				case FIELD_DOUBLE:
					try{
						Double.valueOf(comp.getText());
					}catch(NumberFormatException e){
						valid = false;
					}
					break;
				case FIELD_MAP_RES:
					fieldValid = comp.isNotNull() && !FileManager.existsMap(_projectName, comp.getText());
					comp.setValidity(fieldValid);
					valid &= fieldValid;
					break;
				case FIELD_FILE_RES:
					fieldValid = comp.isNotNull() && !FileManager.existsFile(_projectName, comp.getText());
					comp.setValidity(fieldValid);
					valid &= fieldValid;
					break;
				case FIELD_META_RES:
					fieldValid = comp.isNotNull() && !FileManager.existsMap(_projectName, comp.getText());
					comp.setValidity(fieldValid);
					valid &= fieldValid;
					break;
			}
		}

		_nextButton.setEnabled(valid);
	}

	/**
	 * This method adds the given JComboBox as a "not null" field. A listener
	 * will also be added to the field to watch for any changes.
	 * @param field The JComboBox
	 */
	protected	void	addFieldVerification(JComboBox field){
		_fields.put(new JComboBoxAutoTest(field), FIELD_NOT_NULL);
		field.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				changeHasBeenDone();
			}
		});
	}

	/**
	 * This method adds the given JComboBox as a "not null" field. A listener
	 * will also be added to the field to watch for any changes.
	 * @param field The JComboBox
	 */
	protected	void	addFieldVerification(JTree tree){
		_fields.put(new JTreeAutoTest(tree), FIELD_NOT_NULL);
		tree.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				changeHasBeenDone();
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});

		tree.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				changeHasBeenDone();
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}
		});
	}

	/**
	 * This method sets a verification type to the given JTextField. A listener
	 * will also be added to the field to watch for any changes.
	 * @param field The JTextField
	 * @param field_verification_type The verification to set (usually FIELD_*)
	 */
	protected	void	addFieldVerification(JTextField field, int field_verification_type){
		_fields.put(new JTextFieldAutoTest(field), field_verification_type);
		field.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				changeHasBeenDone();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				changeHasBeenDone();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				changeHasBeenDone();
			}
		});
	}

	/**
	 * Launches the wizard pane's respective action.
	 * @param id The pane ID.
	 */
	protected	void	launch(int panelId){
		_nextButton.setEnabled(false);
		changeHasBeenDone();
		if (0 == panelId){

			if (null != _textField && _textField.isEnabled()){
				_projectName = _textField.getText();
			}
		}
	}

	/**
	 * Set the dialogs parameters (size, title) corresponding to the current
	 * card id.
	 */
	@Override
	protected	void	setDialogParameters(){
		Dimension	size = (_dialogSizes.containsKey(_cardId))
							?_dialogSizes.get(_cardId)
							:DEFAULT_DIALOG_SIZE;

		_dialog.setSize(size);
		_dialog.setTitle(_dialogTitle + " " + (_cardId+1) + "/" + _nbCards);
		_prevButton.setEnabled(_cardId != 0);
		if (_cardId == _nbCards-1){	// last card
			_nextButton.setText(null != _lastButtonNextLabel?_lastButtonNextLabel:LABEL_NEXT_FINISH);
		}else{
			_nextButton.setText(LABEL_NEXT);
		}
	}

	/**
	 * Adds a custom size to a dialog.
	 * @param id the card id
	 * @param size the corresponding dialog wanted size
	 */
	protected	void	addDialogSize(Integer id, Dimension size){
		_dialogSizes.put(id, size);
	}

	/**
	 * Sets the  next button's label when the last card is reached.
	 * @param lastButtonNextLabel the last button label wanted
	 */
	protected	void	setLastNextButtonLabel(String lastButtonNextLabel){
		_lastButtonNextLabel = lastButtonNextLabel;
	}

	@Override
	protected void resetWizardParameters() {
		_cardId = 0;
		_nextButton		= new JButton(LABEL_NEXT);
		_prevButton		= new JButton(LABEL_PREV);
		_cancelButton	= new JButton(LABEL_CANCEL);
		_nextButton.setEnabled(false);

		_fields	= new HashMap<JComponentAutoTest, Integer>();
	}

	/**
	 * Creates and add to the given Container all components belonging to the
	 * main GUI.
	 * @param pane the container where to add the components.
	 */
	@Override
	protected void			addComponentsToMainPane(Container pane) {
		GridBagConstraints	c				= new GridBagConstraints();
		final JPanel		panelFrame		= new JPanel();
		JSeparator			separator		= new JSeparator(JSeparator.HORIZONTAL);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		panelFrame.setLayout(new CardLayout());

		GridBagLayoutUtils.set_gbc(c, 0, 0, 3, 1, GridBagConstraints.BOTH);
		c.insets = new Insets(10, 10, 10, 10);
		c.weightx = 1;
		c.weighty = 1;
		pane.add(panelFrame, c);

		GridBagLayoutUtils.set_gbc(c, 0, 1, 3, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LAST_LINE_END;
		c.insets = new Insets(0, 0, 0, 0);
		c.weighty = 0;
		pane.add(separator, c);

		c.insets = new Insets(10,0,10,10);
		c.weightx = 0.9;
		c.weighty = 0;
		GridBagLayoutUtils.set_gbc(c, 0, 2, 1, 1, GridBagConstraints.NONE);
		pane.add(_prevButton, c);

		c.insets = new Insets(10,0,10,20);
		c.weightx = 0.05;
		GridBagLayoutUtils.set_gbc(c, 1, 2, 1, 1, GridBagConstraints.NONE);
		pane.add(_nextButton, c);

		if (_fields.isEmpty()){
			_nextButton.setEnabled(true);
		}

		c.insets = new Insets(10,0,10,10);
		c.weightx = 0.05;
		GridBagLayoutUtils.set_gbc(c, 2, 2, 1, 1, GridBagConstraints.NONE);
		pane.add(_cancelButton, c);

		// Actions performed
		_nextButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void	actionPerformed(java.awt.event.ActionEvent evt) {
				launch(_cardId);
				if (_cardId < _nbCards-1){// || 1 == _nbCards){
					((CardLayout)(panelFrame.getLayout())).next(panelFrame);
					++_cardId;
					setDialogParameters();
				}else{
					_dialog.setVisible(false);
				}
            }
        });

		_prevButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void	actionPerformed(java.awt.event.ActionEvent evt) {
				if (_cardId > 0){
					((CardLayout)(panelFrame.getLayout())).previous(panelFrame);
					--_cardId;
					setDialogParameters();
				}
            }
        });

		_cancelButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void		actionPerformed(java.awt.event.ActionEvent evt) {
				_dialog.setVisible(false);
            }
        });

		addWizardPanels(panelFrame);
	}

	/**
	 * Sets the 'enable' property of the 'Next', 'Previous' and 'Cancel' buttons.
	 * @param enableArray an array of 3 booleans indicating their 'enable'
	 * property : [is next button to enable,
	 *				is previous to enable,
	 *				is cancel to enable]
	 */
	protected void	refreshNavigationButtonsEnabling(Boolean[] enableArray){
		if (enableArray != null && enableArray.length == 3){
			_nextButton.setEnabled(enableArray[0]);
			_prevButton.setEnabled(enableArray[1]);
			_cancelButton.setEnabled(enableArray[2]);
		}
	}

	/**
	 * This function should be called if the wizard's first step is to select a
	 * project.
	 * Adds the project choice wizard components onto the given panel.
	 * @param panelFrame The panel where to place the components for the project
	 * choosing process.
	 */
	protected	void		addProjectChoicePanelOnPanel(JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();
		JComboBox			comboBox		= null;
		ResourceBundle		bundle			= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
		JLabel				label			= new JLabel(bundle.getString("wizard_choose_project"));
		List<String>		projectsName	= ControllerGUI.getListExistingProjects();

		projectsName.add(0, NEW_PROJECT);
		comboBox = new JComboBox(projectsName.toArray());

		_textField	= new JTextField("");
		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.weightx = 1;
		c.insets = new Insets(0, 0, 10, 0);
		pane.add(label, c);

		c.insets = new Insets(0, 0, 0, 0);
		c.anchor = GridBagConstraints.CENTER;
		c.weighty = 1;
		GridBagLayoutUtils.set_gbc(c, 0, 1, 1, 1, GridBagConstraints.NONE);
		pane.add(comboBox, c);

		GridBagLayoutUtils.set_gbc(c, 1, 1, 1, 1, GridBagConstraints.HORIZONTAL);
		pane.add(_textField, c);

		//Actions
		comboBox.addItemListener(new java.awt.event.ItemListener() {
			@Override
            public	void	itemStateChanged(java.awt.event.ItemEvent evt) {
                if (NEW_PROJECT.equals(evt.getItem())){
					_textField.setEnabled(true);
					if (!"".equals(_textField.getText())){
						_nextButton.setEnabled(true);
					}else{
						_nextButton.setEnabled(false);
					}
				}else{
					_textField.setEnabled(false);
					_nextButton.setEnabled(true);
					_projectName =  evt.getItem().toString();
				}
            }
        });

		_textField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				testValidity();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				testValidity();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				testValidity();
			}

			private	void	testValidity(){
				if (_textField.isEnabled()){
					if (null != _textField.getText()
							&& !"".equals(_textField.getText())
							&& !FileManager.existsProject(_textField.getText())){
						_nextButton.setEnabled(true);
					}else{
						_nextButton.setEnabled(false);
					}
				}
			}
		});
	}

	/**
	 * Overrides the validity button test and sets the next button's enabling.
	 * @param enable
	 */
	protected	void	enableNextButton(boolean enable){
		_nextButton.setEnabled(enable);
	}

	/**
	 * Overrides the validity button test and sets the prev button's enabling.
	 * @param enable
	 */
	protected	void	enablePrevButton(boolean enable){
		_prevButton.setEnabled(enable);
	}

	/**
	 * Overrides the validity button test and sets the cancel button's enabling.
	 * @param enable
	 */
	protected	void	enableCancelButton(boolean enable){
		_cancelButton.setEnabled(enable);
	}

	protected	String								_projectName	= null;	// The chosen project
	private		JButton								_nextButton		= null; // The wizard's next button
	private		JButton								_prevButton		= null; // The wizard's previous button
	private		JButton								_cancelButton	= null; // The wizard's cancel button
	private		String								_dialogTitle	= null;	// The wizard's dialog title
	private		int									_nbCards		= 0;	// The total number of cards
	private		int									_cardId			= 0;	// The wizard's next button
	private		JTextField							_textField		= null;	// The textField corresponding to the project name
	private		Map<JComponentAutoTest, Integer>	_fields			= null;	// The fields needed to be checked for "next" button enabling

	// A map associating a card id to the wanted dialog dimension
	private	Map<Integer, Dimension>	_dialogSizes			= new HashMap<Integer, Dimension>();
	private	String					_lastButtonNextLabel	= null;
}
