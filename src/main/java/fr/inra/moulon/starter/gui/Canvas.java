/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui;

import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.datamodel.entities.file.FileRes;
import fr.inra.moulon.starter.graphic.charts.Charts;
import fr.inra.moulon.starter.graphic.designers.GOStatisticsManager;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesigner;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesignerGenetic;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesignerPhysicalBlowUp;
import fr.inra.moulon.starter.graphic.designers.PhysicalManager;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import fr.inra.moulon.starter.gui.animations.AnimationZoom;
import fr.inra.moulon.starter.gui.dragNdrop.DropTransferHandlerCanvas;
import fr.inra.moulon.starter.gui.utils.ContextualMenu;
import fr.inra.moulon.starter.gui.utils.Grid;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.GridCell;
import fr.inra.moulon.starter.gui.utils.PointTools;
import fr.inra.moulon.starter.gui.wizards.WizardExportGFF;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter.utils.StringTools;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.Timer;
import javax.swing.TransferHandler;


public class	 Canvas extends JPanel {
	public	static	final	int	REDRAW_SIMPLE		= 0;
	public	static	final	int	REDRAW_LINKS		= 1;
	public	static	final	int	REDRAW_CELL_FRAME	= 2;
	public	static	final	int	REARRANGE_GRID		= 3;
	public	static	final	int	REDRAW_ALL			= 4;

	public		Canvas(){
		super();
		_grid = new Grid();
		_paintSections = new HashMap<Integer, Boolean>();
		initPaintSections();
		setTransferHandler(new DropTransferHandlerCanvas(TransferHandler.COPY, this));
		init();
	}

	/**
	 * Returns the physical extreme positions if applicable (null otherwise).
	 * @return The physical extreme positions if applicable (null otherwise).
	 */
	public	double[]	getPhysicalExtremePositions(){
		GridCell		cell		= null;
		double[]		res			= null;

		if (null != (cell = _grid.getFirstGridCell())){
			if (null != cell.lkgDesignerPhy){
				res = cell.lkgDesignerPhy.getExtremPositions();
			}
		}

		return res;
	}

	public	double[]	getGeneticScrollerPosition(){
		GridCell		cell		= null;
		double[]		res			= null;

		if (null != (cell = _grid.getFirstGridCell())){
			if (null != cell.lkgDesignerGen){
				res = cell.lkgDesignerGen.getGeneticScrollerPosition();
			}
		}

		return res;
	}

	public	LinkageGroupDesigner	getLkgDesignerGenetic(){
		GridCell					cell	= null;
		LinkageGroupDesigner		lkgDes	= null;

		if (null != (cell = _grid.getFirstGridCell())){
			lkgDes = cell.lkgDesignerGen;
		}

		return lkgDes;
	}

	/**
	 * Hides the informations pop up.
	 */
	public	void	hidePopup(){
		if (null != _popup){
			_popup.hide();
		}
	}

	private	void		init(){
		ActionListener	actionListener	= null;

		_animationZoom = new AnimationZoom(this);

		// TIMER FOR MOUSE STOP
		actionListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mouseStopped();
			}
		};
		_timerMouseStopped = new Timer(1000, actionListener);
		_timerMouseStopped.start();

	
		Charts.instance().setCanvas(this);
	}

	private	void	initPaintSections(){
		_paintSections.put(_GENETIC,		Boolean.TRUE);
		_paintSections.put(_PHYSIC,			Boolean.TRUE);
		_paintSections.put(_PHYSIC_BLOW,	Boolean.TRUE);
		_paintSections.put(_ANCHORS,		Boolean.TRUE);
	}

	/**
	 * Uses the given iterator to parse and draw on the given Graphics the
	 * shapes.
	 * @param g2d The Graphics object on which drawing must be performed
	 * @param it An iterator on a "ShapeDesign to draw" collection
	 */
	private	void	display(Graphics2D g2d, Iterator<ShapeDesign> it){
		if (null != it){
			while (it.hasNext()) {
				it.next().displayOnGraphics(g2d);
			}
		}
	}

	/**
	 * Display the cell edge
	 * @param g2d The Graphics where to display.
	 * @param cell The cell
	 */
	private	void	displayCellShapes(Graphics2D g2d, GridCell cell){
		ShapeDesign shapeDes	= cell.getShapeDesign();
		Stroke		savedStroke = g2d.getStroke();
		Stroke		stroke		= cell.getStroke();

		if (null != stroke){
			g2d.setColor(shapeDes.color);
			g2d.setStroke(stroke);
			g2d.drawRect(	(int)shapeDes.x,
							(int)shapeDes.y,
							(int)cell.getWidth(),
							(int)cell.getHeight());

			g2d.setStroke(savedStroke);
		}
	}

	@Override
	public void					paintComponent(Graphics g){
		LinkageGroupDesigner	lkgDes	= null;
		Graphics2D				g2d		= (Graphics2D)g;

		super.paintComponent(g);

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setFont((Font)Session.instance().getValue(Session.FONT));

		for (Iterator<GridCell> it = _grid.iterator(); it.hasNext();) {
			GridCell	cell		= it.next();
			Point		ptGen		= cell.getLkgDesignerGenPosition();
			Point		ptPhy		= cell.getLkgDesignerPhyPosition();
			Point		ptGO		= cell.getGOStatisticsPosition();
			Point		ptPhyBlow	= cell.getLkgDesignerPhyBlowUpPosition();
			g2d.translate(cell.xPos, cell.yPos);

			// LKG GENETIC
			if (null != (lkgDes = cell.lkgDesignerGen) && _paintSections.get(_GENETIC)){
				g2d.translate(ptGen.x, ptGen.y);
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.MISC));
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.MARKER));
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.ANCHOR));
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.QTL));
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.META_QTL));
				g2d.translate(lkgDes.getScrollerX(), 0);
				display(g2d, lkgDes.iteratorScrollerShapes());
				g2d.translate(-lkgDes.getScrollerX(), 0);
				g2d.translate(-ptGen.x, -ptGen.y);
			}
			// LKG PHYSIC
			if (null != (lkgDes = cell.lkgDesignerPhy) && _paintSections.get(_PHYSIC)){
				g2d.translate(ptPhy.x, ptPhy.y);
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.MISC));
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.ANCHOR));
				g2d.translate(lkgDes.getScrollerX(), 0);
				display(g2d, lkgDes.iteratorScrollerShapes());
				g2d.translate(-lkgDes.getScrollerX(), 0);
				g2d.translate(-ptPhy.x, -ptPhy.y);
			}
			// GO density
			if (PhysicalManager.instance().isLoaded() && null != GOStatisticsManager.instance().getSelected() && GOStatisticsManager.instance().getSelected().exists()){
				g2d.translate(ptGO.x, ptGO.y);
				display(g2d, GOStatisticsManager.instance().getSelected().iterator());
				g2d.translate(-ptGO.x, -ptGO.y);
			}
			// LKG PHYSIC SAMPLE
			if (null != (lkgDes = cell.lkgDesignerPhyBlowUp) && _paintSections.get(_PHYSIC_BLOW)){
				g2d.translate(ptPhyBlow.x, ptPhyBlow.y);
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.EMPHASISER));
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.GENE));
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.MISC));
				display(g2d, lkgDes.iterator(LinkageGroupDesigner.ANCHOR));
				g2d.translate(-ptPhyBlow.x, -ptPhyBlow.y);
			}
			// GRID CELL DRAWINGS (anchors links)
			if (_paintSections.get(_ANCHORS)){
				Stroke	savedStroke = g2d.getStroke();
				float[]	style		= {2,3};
				Stroke	stroke		= new BasicStroke(	0.4f,
														BasicStroke.CAP_ROUND,
														BasicStroke.JOIN_ROUND,
														10.0f,
														style,
														0);
				g2d.setStroke(stroke);
				display(g2d, cell.iterator());
				g2d.setStroke(savedStroke);
			}

			displayCellShapes(g2d, cell);
			g2d.translate(-cell.xPos, -cell.yPos);
		}
		// COMMON MARKERS LINKS
		if ((Boolean)Session.instance().getValue(Session.COMMON_MARKERS)){
			g2d.translate(0, LinkageGroupDesigner.TOP_MARGIN);
			display(g2d, _grid.iteratorInterCellShapes());
			g2d.translate(0, -LinkageGroupDesigner.TOP_MARGIN);
		}
	}

	/**
	 * Cleans the view of all displayed linkage group designers & files.
	 */
	public	void	cleanView(){
		_grid.clean();
		linkToPhysic();
		removeAll();
		repaint(REARRANGE_GRID);
	}

	/**
	 * Appends a linkage group to the grid.
	 * @param lkg the linkage group to add
	 */
	public	void	addLinkageGroup(final LinkageGroup lkg){
		this.addLinkageGroup(lkg, null, null);
	}

	/**
	 * Appends a linkage group to the grid.
	 * @param lkg the linkage group to add
	 */
	public	void	addLinkageGroup(final LinkageGroup	lkg,
									final MetaAnalysis	metaAnalysis,
									final MetaModel		metaModel){
		_grid.addLinkageGroup(lkg, metaAnalysis, metaModel);
		if (PhysicalManager.instance().isLoaded() && (Boolean)Session.instance().getValue(Session.DISPLAY_GENOME_VERSION) && _grid.size() == 1){
			linkToPhysic();
			PhysicalManager.instance().addCurrentMap(lkg.getBelongingMap());
		}
		repaint(REARRANGE_GRID);
	}

	/**
	 * Sets the genome version combo box enabling attribute to true if only one
	 * linkage group is present in the drawing area.
	 */
	public	void	linkToPhysic(){
		if (PhysicalManager.instance().isLoaded() && (Boolean)Session.instance().getValue(Session.DISPLAY_GENOME_VERSION) && _grid.size() == 1){
			initializePhysicalData();
		}
	}

	/**
	 * Adds a FileRes to the canvas.
	 * @param fileRes The FileRes to add
	 */
	public	void			addFile(FileRes fileRes) {
		GridBagConstraints	c				= null;
		Component			fileComponent	= null;

		cleanView();
		if (null != (fileComponent = fileRes.getComponentToAdd())){
			c = new GridBagConstraints();
			setLayout(new GridBagLayout());
			setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.BOTH);
			c.weightx = 1;
			c.weighty = 1;
			add(new JScrollPane(fileComponent), c);
		}
		repaint(REDRAW_SIMPLE);
	}

	/**
	 * Sets the cursor to a blocking one if given parameter is true. Sets to the
	 * default one if false.
	 * @param enable
	 */
	public	void	setEnableBlockingCursor(boolean enable){
		if (enable){
			setCursor(new Cursor(Cursor.WAIT_CURSOR));
		}else{
			setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
	}

	private	void	changeContext(int mousePosId){
		_paintSections.put(_ANCHORS, Boolean.FALSE);
		switch (mousePosId){
			case GridCell.MOUSE_ON_GEN:
				_paintSections.put(_ANCHORS, Boolean.TRUE);
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				break;
			case GridCell.MOUSE_ON_SCROLLER_GEN_TOP:
				setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
				break;
			case GridCell.MOUSE_ON_SCROLLER_GEN_BOTTOM:
				setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
				break;
			case GridCell.MOUSE_ON_SCROLLER_GEN:
				setCursor(new Cursor(Cursor.MOVE_CURSOR));
				break;
			case GridCell.MOUSE_ON_SCROLLER_PHY_TOP:
				setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
				break;
			case GridCell.MOUSE_ON_SCROLLER_PHY_BOTTOM:
				setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
				break;
			case GridCell.MOUSE_ON_SCROLLER_PHY:
				setCursor(new Cursor(Cursor.MOVE_CURSOR));
				break;
			case GridCell.MOUSE_ON_EDGE:
				setCursor(new Cursor(Cursor.MOVE_CURSOR));
				break;
			case GridCell.MOUSE_ON_ELT:
				setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
				break;
			default:
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				break;
		}
	}
	////////////////////////////// MOUSE EVENTS ////////////////////////////////
	/**
	 * Sets the 'mouseOn' under the mouse grid attribute to true and sets other
	 * cells 'mouseOn' attribute to false.
	 * @param evt
	 */
	public	void	mouseMovedCanvas(MouseEvent evt) {
		GridCell	gridCell		= null;
		boolean		mousePosChanged	= false;

		_mousePoint = evt.getPoint();
		_mouseRealPoint = evt.getLocationOnScreen();
		_timerMouseStopped.start();
		mousePosChanged = _grid.setMouseOn(evt.getPoint());
		if (null != (gridCell = _grid.getGridCell(evt.getPoint()))){
			if (mousePosChanged){
				changeContext(gridCell.getMousePosId());
				repaint(REDRAW_CELL_FRAME);
			}
		}
	}

	/**
	 * Suppress the frame around all grid cells.
	 * @param evt
	 */
	public	void	mouseExitedCanvas(MouseEvent evt) {
		if (_grid.setMouseOff()){
			setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			repaint(REDRAW_CELL_FRAME);
		}
	}

	/**
	 * When mouse stopped, display data information if applicable.
	 */
	public	void	mouseStopped(){
		GridCell	gridCell		= null;
		Point		pLkgDesPos		= null;
		Point		pTranslated		= null;
		String		infos			= null;

		_timerMouseStopped.stop();
		if (null != _mousePoint && null != (gridCell = _grid.getGridCell(_mousePoint))){
			switch (gridCell.getMousePosId()){
				case GridCell.MOUSE_ON_ELT:
					infos = gridCell.getCurrentShapeInfo();
					if (null != infos){
						JTextArea textArea = new JTextArea(infos);
						textArea.setEditable(false);
						textArea.setBackground(new Color(230, 230, 230));
						_popup = PopupFactory.getSharedInstance().getPopup(this, textArea, _mouseRealPoint.x, _mouseRealPoint.y);
						_popup.show();
					}
					break;
				case GridCell.MOUSE_ON_PHY_BLOW_UP:
					pLkgDesPos = gridCell.getLkgDesignerPhyBlowUpPosition();
					pTranslated = PointTools.copyNtranslate(_mousePoint, -pLkgDesPos.x, -pLkgDesPos.y);
					infos = ((LinkageGroupDesignerPhysicalBlowUp)gridCell.lkgDesignerPhyBlowUp).getGeneInfos(pTranslated);
					if (null != infos){
						Map<String,String> _dblinks = PhysicalManager.instance().getDBLinks();
						JPopupMenu popup = new JPopupMenu();
						popup.add(new JMenuItem(infos));
						popup.addSeparator();
						
						Iterator it = _dblinks.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry pairs = (Map.Entry)it.next();
							JMenuItem item = new JMenuItem((String)pairs.getKey());
							item.addActionListener(new LinkListener((String)pairs.getValue(),infos));
							popup.add(item);
						}
						popup.setLocation(_mouseRealPoint.x-10, _mouseRealPoint.y-10);
						popup.setVisible(true);
						popup.addMouseListener(new MouseListener() {
							
							@Override
							public void mouseReleased(MouseEvent e) {
							}
							
							@Override
							public void mousePressed(MouseEvent e) {
							}
							
							@Override
							public void mouseExited(MouseEvent e) {
								if(!mouseIsOverElement((JPopupMenu)e.getSource())){
									((JPopupMenu)e.getSource()).setVisible(false);
								}
							}
							
							@Override
							public void mouseEntered(MouseEvent e) {
							}
							
							@Override
							public void mouseClicked(MouseEvent e) {
							}
							
							private boolean mouseIsOverElement(Component compo){
								Point pt=MouseInfo.getPointerInfo().getLocation();
								Point ptCompo=compo.getLocationOnScreen();
								return (pt.x >= ptCompo.x
										&& pt.x <= ptCompo.x + compo.getWidth()
										&& pt.y >= ptCompo.y
										&& pt.y <= ptCompo.y + compo.getHeight());
							}
						});
						
					}
					break;
			}
		}
	}

	private	int	mouseLeftButtonPressed(MouseEvent evt, GridCell gridCell){
		Point	mousePoint	= null;
		Point	pTranslated	= null;
		int		repaintID	= REDRAW_SIMPLE;

		switch (gridCell.getMousePosId()){
			case GridCell.MOUSE_ON_GEN_LKG:
				mousePoint = gridCell.getLkgDesignerGenPosition();
				pTranslated = PointTools.copyNtranslate(_pointPressed, -mousePoint.x, -mousePoint.y);
				pTranslated = PointTools.copyNtranslate(pTranslated, -(int)gridCell.xPos, -(int)gridCell.yPos);
				if (gridCell.hasGenomeVersionEnabled()){
					gridCell.lkgDesignerGen.enableScroller(pTranslated.y, false);
				}else{
					_grid.manageCascadingZoom((LinkageGroupDesignerGenetic)gridCell.lkgDesignerGen, pTranslated.y, 20);
				}
				linkToPhysic();
				repaintID = REARRANGE_GRID;
				break;
			case GridCell.MOUSE_ON_PHY_BLOW_UP:
				if (!evt.isControlDown()){
					setCursor(new Cursor(Cursor.MOVE_CURSOR));
				}else{
					mousePoint = gridCell.getLkgDesignerPhyBlowUpPosition();
					pTranslated = PointTools.copyNtranslate(evt.getPoint(),
															-mousePoint.x,
															-mousePoint.y);
					ContextualMenu.showOnPhysicBlowUp(this, evt.getPoint());
					_memory = new CanvasSmallMemory(gridCell, gridCell.lkgDesignerPhyBlowUp, pTranslated, 200000, 20);
				}
				break;
			case GridCell.MOUSE_ON_ELT:
				_gridCellSelected.moveScrollerOnShape();
				break;
		}

		return repaintID;
	}

	private	int	mouseRightButtonPressed(MouseEvent evt, GridCell gridCell){
		Point	mousePoint	= null;
		Point	pTranslated	= null;
		int		repaintID	= REDRAW_SIMPLE;

		pTranslated = PointTools.copyNtranslate(_mousePoint,
												(int)-gridCell.xPos,
												(int)-gridCell.yPos);
		switch (gridCell.getMousePosId()){
			case GridCell.MOUSE_ON_GEN:
			case GridCell.MOUSE_ON_GEN_LKG:
				mousePoint = gridCell.getLkgDesignerGenPosition();
				pTranslated = PointTools.copyNtranslate(evt.getPoint(),
														-mousePoint.x,
														-mousePoint.y);
				pTranslated = PointTools.copyNtranslate(pTranslated, -(int)gridCell.xPos, -(int)gridCell.yPos);
				ContextualMenu.showOnGenetic(this, evt.getPoint());
				_memory = new CanvasSmallMemory(gridCell, gridCell.lkgDesignerGen, pTranslated, 5, 10);
				break;
			case GridCell.MOUSE_ON_PHY:
				mousePoint = gridCell.getLkgDesignerPhyPosition();
				pTranslated = PointTools.copyNtranslate(evt.getPoint(),
														-mousePoint.x,
														-mousePoint.y);
				ContextualMenu.showOnGenetic(this, evt.getPoint());
				_memory = new CanvasSmallMemory(gridCell, gridCell.lkgDesignerGen, pTranslated, 5, 10);
				break;
			case GridCell.MOUSE_ON_PHY_BLOW_UP:
				mousePoint = gridCell.getLkgDesignerPhyBlowUpPosition();
				pTranslated = PointTools.copyNtranslate(evt.getPoint(),
														-mousePoint.x,
														-mousePoint.y);
				ContextualMenu.showOnPhysicBlowUp(this, evt.getPoint());
				_memory = new CanvasSmallMemory(gridCell, gridCell.lkgDesignerPhyBlowUp, pTranslated, 200000, 20);
				break;
		}

		return repaintID;
	}

	/**
	 * Method called when the mouse is pressed on the Canvas
	 * @param evt The mouse event
	 */
	public	void	mousePressedCanvas(MouseEvent evt) {
		GridCell	gridCell	= null;
		int			repaintID	= REDRAW_SIMPLE;

		_pointPressed = evt.getPoint();
		if (null != (gridCell = _grid.getGridCell(evt.getPoint()))){
			_gridCellSelected = gridCell;
			_gridCellSelected.setSelected(true);
			switch (evt.getButton()){
				////////////// Left button //////////////
				case MouseEvent.BUTTON1:
					if (!evt.isControlDown()){
						repaintID = mouseLeftButtonPressed(evt, gridCell);
					}else{
						repaintID = mouseRightButtonPressed(evt, gridCell);
					}
					break;
				////////////// Middle button //////////////
				case MouseEvent.BUTTON2:
					repaintID = removeGridCell(gridCell);
					break;
				////////////// Right button //////////////
				case MouseEvent.BUTTON3:
					repaintID = mouseRightButtonPressed(evt, gridCell);
					break;
			}
			repaint(repaintID);
		}
	}

	/**
	 * Removes the given grid cell from the grid, and returns the needed repaint
	 * ID.
	 * @param gridCell The grid cell to remove.
	 * @return The needed repaint ID.
	 */
	private	int	removeGridCell(GridCell gridCell){
		int		repaintID = REDRAW_SIMPLE;

		if (null != gridCell){
			_grid.remove(gridCell);
			linkToPhysic();
			for (Iterator<GridCell> it = _grid.iterator(); it.hasNext();) {
				GridCell cell = it.next();
				cell.lkgDesignerGen.recreateShapes();
			}
			repaintID = REARRANGE_GRID;
		}

		return repaintID;
	}

	/**
	 * Sets the 'memory' value for the contextual menu.
	 * @param context
	 * @param value
	 */
	public	void	contextualMenuManage(ContextualMenu.Context context, ContextualMenu.Values value){
		int			repaintID = REDRAW_SIMPLE;

		if (null != _memory){
			switch (value){
				case ZOOM_OUT:
					_memory.m_zoomSteps *= -1;
				case ZOOM_IN:
					_animationZoom.start(50,
						_memory.m_zoomOcc,
						_memory.m_zoomSteps,
						_memory.m_mousePointLkg,
						_memory.m_lkgDes,
						null);
					break;
				case LKG_REMOVE:
					repaintID = removeGridCell(_memory.m_gridCell);
					break;
				case LKG_REVERSE:
					_memory.m_lkgDes.reverse();
					_grid.findAnchors();
					linkToPhysic();
					repaintID = REDRAW_ALL;
					break;
				case GFF_EXPORT:
					WizardExportGFF	wizard = new WizardExportGFF(_bundle.getString("contextual_export_gff"));
					wizard.setParams(PhysicalManager.instance().getStructuralAnnotationId(), _memory.m_lkgDes, null);
					wizard.show();
					break;
			}
			_memory = null;
			repaint(repaintID);
		}
	}

	/**
	 * Method called when the mouse is moving on the Canvas
	 * @param evt The mouse event
	 */
	public	void	mouseWheelMovedCanvas(MouseWheelEvent evt){
		GridCell	gridCell		= null;
		int			wheelRotation	= evt.getWheelRotation()*2;
		Point		pTranslated		= null;
		Point		pLkgDesPos		= null;
		int			repaintID		= REDRAW_SIMPLE;

		_animationZoom.stop();
		if (null != (gridCell = _grid.getGridCell(evt.getPoint()))){
			switch (gridCell.getMousePosId()){
				case GridCell.MOUSE_OFF:			// No action
					break;
				case GridCell.MOUSE_ON_GEN:			// Zooming inside lkg Gen
				case GridCell.MOUSE_ON_GEN_LKG:		// Zooming inside lkg Gen
					pLkgDesPos = gridCell.getLkgDesignerGenPosition();
					pTranslated = PointTools.copyNtranslate(evt.getPoint(), -pLkgDesPos.x, -pLkgDesPos.y);
					pTranslated = PointTools.copyNtranslate(pTranslated, -(int)gridCell.xPos, -(int)gridCell.yPos);

					if (_grid.isSingleZoomAvailable() &&
							gridCell.lkgDesignerGen.zoom(pTranslated, -wheelRotation*4)){
						gridCell.lkgDesignerGen.recreateShapes();
					}
					repaintID = REARRANGE_GRID;
					break;
				case GridCell.MOUSE_ON_PHY_BLOW_UP:	// Zooming inside lkg Phy blow up

					pLkgDesPos = gridCell.getLkgDesignerPhyBlowUpPosition();
					pTranslated = PointTools.copyNtranslate(evt.getPoint(), -pLkgDesPos.x, -pLkgDesPos.y);
					if (gridCell.lkgDesignerPhyBlowUp.zoom(pTranslated, -wheelRotation*50000)){
						gridCell.lkgDesignerPhyBlowUp.recreateShapes();
					}
					break;
			}
		}
		repaint(repaintID);
	}

	/**
	 * Launches the drawing of all grid cells.
	 */
	public	Dimension	redraw(){
		return _grid.rearrange();
	}

	/**
	 * Sets the maximum width the grid is allowed to use for drawing.
	 * @param widthMax
	 */
	public	void	resetWidthMax(){
		_grid.setWidthMax(getWidth());
	}

	/**
	 * Creates and returns a list of all marker' types present on the canvas.
	 * The list is ordered alphabetically.
	 * @return An alphabetically ordered list of marker' types.
	 */
	public List<String>			getMarkerTypesPresent(){
		List<String>			res		= new ArrayList<String>();
		Set<String>				traits	= new HashSet<String>();
		VisitorEltGet<Marker>	v		= new VisitorEltGet(Marker.class);

		for (Iterator<GridCell> it = _grid.iterator(); it.hasNext();) {
			GridCell gridCell = it.next();

			gridCell.lkgDesignerGen.getLinkageGroup().accept(v);
			for (Iterator<Content> itContent = v.getList().iterator(); itContent.hasNext();) {
				Marker marker = (Marker)itContent.next();

				if (!traits.contains(marker.getType())){
					traits.add(marker.getType());
				}
			}
		}

		for (Iterator<String> it = traits.iterator(); it.hasNext();) {
			res.add(it.next());
		}

		Collections.sort(res);

		return res;
	}

	/**
	 * Creates and returns a list of all qtls' and metaqtls' traits present on
	 * the canvas. The list is ordered alphabetically.
	 * @return An alphabetically ordered list of qtls' and metaqtls' traits.
	 */
	public List<String>			getTraitsPresent(){
		List<String>			res		= new ArrayList<String>();
		Set<String>				traits	= new HashSet<String>();
		VisitorEltGet<Qtl>		vQtl	= new VisitorEltGet(Qtl.class);
		VisitorEltGet<MetaQtl>	vMQtl	= new VisitorEltGet(MetaQtl.class);

		for (Iterator<GridCell> it = _grid.iterator(); it.hasNext();) {
			GridCell gridCell = it.next();

			gridCell.lkgDesignerGen.getLinkageGroup().accept(vQtl);
			for (Iterator<Content> itContent = vQtl.getList().iterator(); itContent.hasNext();) {
				Qtl qtl = (Qtl)itContent.next();

				if (!traits.contains(qtl.getTrait())){
					traits.add(qtl.getTrait());
				}
			}
			gridCell.lkgDesignerGen.getLinkageGroup().accept(vMQtl);
			for (Iterator<Content> itContent = vMQtl.getList().iterator(); itContent.hasNext();) {
				MetaQtl mqtl = (MetaQtl)itContent.next();

				if (!traits.contains(mqtl.getTrait())){
					traits.add(mqtl.getTrait());
				}
			}
		}

		for (Iterator<String> it = traits.iterator(); it.hasNext();) {
			res.add(it.next());
		}

		Collections.sort(res);

		return res;
	}

	/**
	 * Method called by the Charts class when the genes' colors should be
	 * changed.
	 */
	public	void							geneColorChange(boolean emphasize){
		LinkageGroupDesignerPhysicalBlowUp	lkgPhyBlowUp = null;
		GridCell							cell = _grid.getFirstGridCell();

		if (null != cell && null != cell.lkgDesignerPhyBlowUp){
			lkgPhyBlowUp = (LinkageGroupDesignerPhysicalBlowUp)cell.lkgDesignerPhyBlowUp;
			lkgPhyBlowUp.colorChange();
			lkgPhyBlowUp.setEmphasizeShapes(emphasize);
			repaint(REDRAW_SIMPLE);
		}
	}

	public	void	initializePhysicalData(){
		GridCell	cell = _grid.getFirstGridCell();

		if (null != cell){
			cell.initializePhysicalData();
		}
	}

	public	void	mouseDraggedCanvas(MouseEvent evt) {
		Point		point		= evt.getPoint();
		GridCell	gridCell	= null;
		int			repaintID	= REDRAW_SIMPLE;

		_animationZoom.stop();
		if (null != _gridCellSelected && null != _pointPressed){
			switch (_gridCellSelected.getMousePosId()){
				case GridCell.MOUSE_OFF:					// No action
					break;
				case GridCell.MOUSE_ON_GEN:
				case GridCell.MOUSE_ON_GEN_LKG:				// Scrolling on genetic
					if (_grid.isSingleZoomAvailable() && _gridCellSelected.lkgDesignerGen.scroll(point, _pointPressed)){
						_gridCellSelected.lkgDesignerGen.recreateShapes();
						repaintID = REDRAW_LINKS;
					}
					break;
				case GridCell.MOUSE_ON_SCROLLER_GEN:		// Moving scroller gen
					_gridCellSelected.lkgDesignerGen.moveScroller(_pointPressed, point);
					break;
				case GridCell.MOUSE_ON_SCROLLER_GEN_TOP:	// Resizing scroller gen
					_gridCellSelected.lkgDesignerGen.resizeScrollerTop(_pointPressed, point);
					break;
				case GridCell.MOUSE_ON_SCROLLER_GEN_BOTTOM:	// Resizing scroller gen
					_gridCellSelected.lkgDesignerGen.resizeScrollerBottom(_pointPressed, point);
					break;
				case GridCell.MOUSE_ON_EDGE:				// Moving gridCell
					_gridCellSelected.xPos += point.x - _pointPressed.x;
					_gridCellSelected.yPos += point.y - _pointPressed.y;
					if (null != (gridCell = _grid.getGridCell(evt.getPoint()))){
						_grid.switchCells(_gridCellSelected, gridCell);
						gridCell.lkgDesignerGen.recreateShapes();
						_gridCellSelected.lkgDesignerGen.recreateShapes();
					}
					repaintID = REDRAW_LINKS;
					break;
				case GridCell.MOUSE_ON_SCROLLER_PHY:		// Moving scroller phy
					if (_gridCellSelected.lkgDesignerPhy.moveScroller(_pointPressed, point)){
						_gridCellSelected.lkgDesignerPhyBlowUp.recreateShapes();
					}
					break;
				case GridCell.MOUSE_ON_SCROLLER_PHY_TOP:	// Resizing scroller phy
					if (_gridCellSelected.lkgDesignerPhy.resizeScrollerTop(_pointPressed, point)){
						_gridCellSelected.lkgDesignerPhyBlowUp.recreateShapes();
					}
					break;
				case GridCell.MOUSE_ON_SCROLLER_PHY_BOTTOM:	// Resizing scroller phy
					if (_gridCellSelected.lkgDesignerPhy.resizeScrollerBottom(_pointPressed, point)){
						_gridCellSelected.lkgDesignerPhyBlowUp.recreateShapes();
					}
					break;
				case GridCell.MOUSE_ON_PHY_BLOW_UP:			// Scrolling on physic blow up
					if (_gridCellSelected.lkgDesignerPhy.scrollSon(point, _pointPressed)){
						_gridCellSelected.lkgDesignerPhyBlowUp.recreateShapes();
					}
					break;
			}
			_pointPressed = point;
		}
		repaint(repaintID);
    }

	public	void	mouseReleasedCanvas(MouseEvent evt){
		if (null != _gridCellSelected){
			changeContext(_gridCellSelected.getMousePosId());

			_gridCellSelected.setSelected(false);
			_pointPressed = null;
			_gridCellSelected = null;
			repaint(REARRANGE_GRID);
		}
	}

	public	void	reProportionLkgs(){
		_grid.reProportion();
	}
	/**
	 * Repaints the canvas. The given parameter gives the repaint degree.
	 * @param redraw_id The repaint degree
	 * @return The canvas dimension.
	 */
	public	Dimension	repaint(int redraw_id){
		Dimension	dim		= null;

		switch (redraw_id){
			case REDRAW_ALL:
				_grid.redrawAllCells();
			case REARRANGE_GRID:
				dim = _grid.rearrange();
				_grid.createAnchorsShapes();
			case REDRAW_LINKS:
				_grid.createAnchorsShapes();
			case REDRAW_CELL_FRAME:
			case REDRAW_SIMPLE:
				super.repaint();
				break;
		}
		return dim;
	}

	/**
	 * Used for remembering key informations on canvas for contextual menus
	 * actions
	 */
	class CanvasSmallMemory{
		CanvasSmallMemory(	GridCell				gridCell,
							LinkageGroupDesigner	lkgDes,
							Point					mousePointLkg,
							int						zoomSteps,
							int						zoomOcc){
			m_gridCell = gridCell;
			m_lkgDes = lkgDes;
			m_mousePointLkg = mousePointLkg;
			m_zoomSteps = zoomSteps;
			m_zoomOcc = zoomOcc;
		}

		GridCell				m_gridCell		= null;
		LinkageGroupDesigner	m_lkgDes		= null;
		Point					m_mousePointLkg	= null;
		int						m_zoomSteps		= 0;
		int						m_zoomOcc		= 0;
	}

	private class LinkListener implements ActionListener {
		String _link = null;
		String _gene = null;

		public LinkListener(String link, String infos) {
			_link = link;
			_gene = infos;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String url = StringTools.replaceGeneTag(_link, _gene);
			if (Desktop.isDesktopSupported()) {
				Desktop desktop = Desktop.getDesktop();
				try {
					URI uri = new URI(url);
					desktop.browse(uri);
				} catch (IOException ex) {
					// do nothing
				} catch (URISyntaxException ex) {
					// do nothing
				}
			}
			((JPopupMenu) ((JMenuItem) e.getSource()).getParent())
					.setVisible(false);
		}

	}

	private CanvasSmallMemory		_memory				= null;
	private	AnimationZoom			_animationZoom		= null;
	private	Grid					_grid				= null;
	private	GridCell				_gridCellSelected	= null;
	private	Point					_pointPressed		= null;
	private	Timer					_timerMouseStopped	= null;
	private	Point					_mousePoint			= null;
	private	Point					_mouseRealPoint		= null;
	private	Map<Integer, Boolean>	_paintSections		= null;
	private	Popup					_popup				= null;

	private	static	Integer	_GENETIC			= 1;
	private	static	Integer	_PHYSIC				= 2;
	private	static	Integer	_PHYSIC_BLOW		= 3;
	private	static	Integer	_ANCHORS			= 4;

	private	static	ResourceBundle	_bundle = (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
