package fr.inra.moulon.starter.gui.wizards;

import java.util.ArrayList;
import java.util.List;

import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.fileTools.tasks.TaskLoadingDBLinks;
import fr.inra.moulon.starter.fileTools.tasks.TaskLoadingFile;

public class WizardExploringDBLinks extends WizardExploringCSV_Multi {

	public WizardExploringDBLinks(WizardGenomeVersion wizardGenomeVersion) {
		super("Explain columns", wizardGenomeVersion);
	}

	@Override
	protected TaskLoadingFile createTask(List<Field> fields) {

		return new TaskLoadingDBLinks(_wizardGenomeVersion, _csvFile, fields, getFieldsSeparator());
	}

	@Override
	protected Field[] generateFields() {
		List<Field>		fields	= new ArrayList<Field>();
		int				i		= 2;

		fields.add(new Field("dbname",		i++, Field.STRING));
		fields.add(new Field("link",	i++, Field.STRING));
		fields.add(Field.DEFAULT);

		return fields.toArray(new Field[fields.size()]);
	}

}
