/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 05-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.wizards.utils.TableModelPreview;
import fr.inra.moulon.starter.utils.Session;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


public abstract class WizardExploringCSV extends AbstractDefaultWizard{
	private	final	static	Dimension	WIZARD_MAIN	= new Dimension(700, 400);
	private	final	static	int			NB_CARDS	= 1;

	public WizardExploringCSV(String dialogTitle){
		super("", dialogTitle, NB_CARDS);
		ResourceBundle bundle = (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);

		super.addDialogSize(0, WIZARD_MAIN);
		_separatorChoices.get(TABULATION).radio.setSelected(true);
		_separatorChoices.get(TABULATION).radio.setText(bundle.getString("misc_tabulation"));
		_separatorChoices.get(COMMA).radio.setText(bundle.getString("misc_comma"));
		_separatorChoices.get(COLON).radio.setText(bundle.getString("misc_colon"));
		_separatorChoices.get(SEMI_COLON).radio.setText(bundle.getString("misc_semi_colon"));
		_separatorChoices.get(CUSTOM).radio.setText(bundle.getString("misc_custom"));
	}

	/**
	 * Sets the CSV file to load.
	 * @param csvFile The CSV file
	 */
	public	void	setFile(File csvFile){
		_csvFile = csvFile;
	}

	/**
	 * This methods returns the separator used in the CSV file.
	 * @return The separator used in the CSV file.
	 */
	protected	String	getFieldsSeparator(){
		return getSeparatorSelected().separator;
	}

	/**
	 * This method must return a fields array corresponding to the implemented
	 * wizard CSV file exploring.
	 * @return
	 */
	protected	abstract	Field[]			generateFields();

	@Override
	protected	void	launch(int panelId){
		if (0 == panelId){
			_dialog.setVisible(false);
		}
	}

	/**
	 * Adds the different wizard's steps as different panels.
	 * @param panelFrame The frame JPanel where panels will be placed.
	 */
	@Override
	protected	void	addWizardPanels(JPanel panelFrame){
		JPanel			panelColumnIdentification = new JPanel();

		addFilesListLoadingPanelOnPanel(panelColumnIdentification);
		panelFrame.add(panelColumnIdentification, "1");
	}

	/**
	 * THE WIZARD'S SECOND STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	protected	void		addFilesListLoadingPanelOnPanel(final JPanel pane){
		GridBagConstraints	c			= new GridBagConstraints();
		JLabel				label		= null;
		final Field[]		fields		= generateFields();
		final JTextField	sepCustom	= new JTextField();
		JSeparator			sepGraphic	= new JSeparator();
		JScrollPane			scrollPane	= null;
		int					iRow		= 0;

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		if (null != fields){
			// Separators
			label = new JLabel("Separator : ");
			GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
			c.anchor = GridBagConstraints.LINE_START;
			c.insets = new Insets(0, 0, 5, 0);
			pane.add(label, c);
			c.insets = new Insets(0, 0, 0, 0);

			for (int i = 0; i < SEPARATORS.length-1; i++) {
				final Separator separator = _separatorChoices.get(SEPARATORS[i]);
				GridBagLayoutUtils.set_gbc(c, i, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
				pane.add(separator.radio, c);
				separator.radio.addItemListener(new ItemListener() {

					@Override
					public void itemStateChanged(ItemEvent e) {
						if (e.getStateChange() == ItemEvent.SELECTED){
							sepCustom.setEnabled(false);
							loadPreviewInTable(_table, separator, fields);
							_combo.setEnabled(true);
						}
					}
				});
			}
			final Separator separator = _separatorChoices.get(CUSTOM);
			GridBagLayoutUtils.set_gbc(c, 0, ++iRow, 1, 1, GridBagConstraints.HORIZONTAL);
			pane.add(separator.radio, c);
			separator.radio.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED){
						sepCustom.setEnabled(true);
						if (!"".equalsIgnoreCase(sepCustom.getText())){
							loadPreviewInTable(_table, separator, fields);
							_combo.setEnabled(true);
						}
					}
				}
			});

			// Text field for custom CSV separator
			sepCustom.getDocument().addDocumentListener(new DocumentListener() {
				@Override
				public void insertUpdate(DocumentEvent e) {
					if (!"".equalsIgnoreCase(sepCustom.getText())){
						separator.separator = sepCustom.getText();
						loadPreviewInTable(_table, separator, fields);
						_combo.setEnabled(true);
					}
				}

				@Override
				public void removeUpdate(DocumentEvent e) {
					if (!"".equalsIgnoreCase(sepCustom.getText())){
						separator.separator = sepCustom.getText();
						loadPreviewInTable(_table, separator, fields);
						_combo.setEnabled(true);
					}
				}

				@Override
				public void changedUpdate(DocumentEvent e) {
					if (!"".equalsIgnoreCase(sepCustom.getText())){
						separator.separator = sepCustom.getText();
						loadPreviewInTable(_table, separator, fields);
						_combo.setEnabled(true);
					}
				}
			});

			sepCustom.setEnabled(false);
			GridBagLayoutUtils.set_gbc(c, 1, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
			c.anchor = GridBagConstraints.LINE_START;
			pane.add(sepCustom, c);

			// Field choose
			GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
			c.anchor = GridBagConstraints.LINE_START;
			c.insets = new Insets(10, 5, 10, 5);
			pane.add(sepGraphic, c);

			c.insets = new Insets(0, 0, 0, 0);

			label = new JLabel("Field : ");
			GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.NONE);
			c.anchor = GridBagConstraints.LINE_START;
			pane.add(label, c);

			_combo = new JComboBox(fields);
			_combo.setEnabled(false);
			setItemListener(_combo);
			GridBagLayoutUtils.set_gbc(c, 1, iRow++, 1, 1, GridBagConstraints.NONE);
			pane.add(_combo, c);

			c.anchor = GridBagConstraints.CENTER;
			_table = new JTable();
			loadPreviewInTable(_table, getSeparatorSelected(), fields);
			scrollPane = new JScrollPane(_table);
			GridBagLayoutUtils.set_gbc(c, 0, iRow++, SEPARATORS.length+2, 1, GridBagConstraints.BOTH);
			c.weightx = 1;
			c.weighty = 1;
			pane.add(scrollPane, c);
		}
	}

	private	void			loadPreviewInTable(	JTable		table,
												Separator	separator,
												Field[]		fields){
		DefaultTableModel	tableModel = TableModelPreview.getDataModel(_csvFile, separator.separator);

		table.setModel(tableModel);
		table.setCellSelectionEnabled(false);
		table.setRowSelectionAllowed(false);
		table.setColumnSelectionAllowed(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		SelectionListener listener = new SelectionListener();
		table.getSelectionModel().addListSelectionListener(listener);
		table.getColumnModel().getSelectionModel().addListSelectionListener(listener);
		_columnFields = new ArrayList<Field>();
		for (int i = 0; i < tableModel.getColumnCount(); ++i){
			Field field = (i < fields.length)?fields[i]:Field.DEFAULT;

			_columnFields.add(field);
			table.getColumnModel().getColumn(i).setHeaderValue(field);
		}
	}

	/**
	 * Sets the actionPerformed event to the given 'combo' so it sets the
	 * selected field to the 'fieldscolumn' array.
	 * @param combo The combobox
	 */
	private	void	setItemListener(JComboBox combo){
		combo.addItemListener(new java.awt.event.ItemListener() {
			@Override
            public	void	itemStateChanged(java.awt.event.ItemEvent evt) {
				Field		field = null;

				if (evt.getStateChange() == ItemEvent.SELECTED){
					field = (Field)evt.getItem();
					System.out.println(field);
					System.out.println(_columnSelected);
					_columnFields.set(_columnSelected, field);
					_table.getColumnModel().getColumn(_columnSelected).setHeaderValue(field);

					// Set to unused any other column using this field
					for (int i = 0; field != Field.DEFAULT && i < _columnFields.size(); ++i) {
						Field fieldTmp = _columnFields.get(i);

						if (field == fieldTmp && i != _columnSelected){
							_columnFields.set(i, Field.DEFAULT);
							_table.getColumnModel().getColumn(i).setHeaderValue(Field.DEFAULT);
						}
					}
					// Repaint the table after the changes
					_table.getTableHeader().resizeAndRepaint();
				}
            }
        });
	}

	public class SelectionListener implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (e.getSource() == _table.getColumnModel().getSelectionModel()
				   && _table.getColumnSelectionAllowed()
				   && _table.getSelectedColumn() != -1){

				_columnSelected = _table.getSelectedColumn();
				_combo.setEnabled(true);
				_combo.setSelectedItem(_columnFields.get(_columnSelected));
			}
		}
	}

	private	static	final	int		TABULATION	= 1;
	private	static	final	int		COMMA		= 2;
	private	static	final	int		SEMI_COLON	= 3;
	private	static	final	int		COLON		= 4;
	private	static	final	int		CUSTOM		= 5;
	private	static	final	int[]	SEPARATORS	= new int[]{
														TABULATION,
														COMMA,
														SEMI_COLON,
														COLON,
														CUSTOM
														};

	private	static	class Separator{
		Separator(String separator, ButtonGroup group){
			this.separator = separator;
			this.radio = new JRadioButton();
			group.add(this.radio);
		}
		String			separator;
		JRadioButton	radio;
	}

	private	static	Separator	getSeparatorSelected(){
		int						i = 0;
		Separator				separator = null;

		while (i < SEPARATORS.length){
			if (_separatorChoices.get(SEPARATORS[i]).radio.isSelected()){
				separator = _separatorChoices.get(SEPARATORS[i]);
				i = SEPARATORS.length;
			}
			++i;
		}

		return separator;
	}

	private	static	final	Map<Integer, Separator>	_separatorChoices;
	private	static	final	ButtonGroup				_buttonGroup = new ButtonGroup();
	static{
		_separatorChoices = new HashMap<Integer, Separator>();
		_separatorChoices.put(TABULATION,	new Separator("\t", _buttonGroup));
		_separatorChoices.put(COMMA,		new Separator(",", _buttonGroup));
		_separatorChoices.put(SEMI_COLON,	new Separator(";", _buttonGroup));
		_separatorChoices.put(COLON,		new Separator(":", _buttonGroup));
		_separatorChoices.put(COLON,		new Separator(":", _buttonGroup));
		_separatorChoices.put(CUSTOM,		new Separator("", _buttonGroup));
	}


	private		int						_columnSelected		= -1;
	protected	List<Field>				_columnFields		= null;
	private		JComboBox				_combo				= null;
	private		JTable					_table				= null;
	protected	File					_csvFile			= null;
}
