/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 5, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import java.awt.Container;
import javax.swing.JDialog;


/**
 * This class is the abstract wizard class; is contains the few methods (such
 * as the wizard dialog creation). The main method to make the wizard display
 * its dialog is the 'show(String)' method.
 * All subclasses must implements the following abstract methods :
 *  - void setDialogParameters() : for setting all dialog custom parameters
 *
 * @author sosnowski
 */
public	abstract	class	AbstractWizard {
	public	AbstractWizard(){
	}

	/**
	 * Launch the wizard display.
	 */
	public	void	show(){
		_dialog = new JDialog();
		resetWizardParameters();
		setDialogParameters();
		addComponentsToMainPane(_dialog.getContentPane());
		_dialog.setModal(true);
		_dialog.setVisible(true);
	}

	/**
	 * Returns the project's name in order to expand its node in the tree explorer
	 * when the Wizard is done. If no node is to be expanded, the method should
	 * return null.
	 * @return The project's name. Can be null.
	 */
	public	String	getProjectName(){
		return null;
	}

	protected	abstract	void	setDialogParameters();
	protected	abstract	void	addComponentsToMainPane(Container pane);

	/**
	 * Method used to reset all wizard's parameters for a new start.(no multiple
	 * instances)
	 */
	protected	abstract	void	resetWizardParameters();

	protected	JDialog	_dialog	= null;	// The wizard's dialog
}
