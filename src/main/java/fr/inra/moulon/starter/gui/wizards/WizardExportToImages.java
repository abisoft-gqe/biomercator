/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 5, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.gui.Canvas;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.JFileChooserTools;
import fr.inra.moulon.starter.gui.utils.JTabbedPaneCanvas;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter.utils.StringTools;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.svggen.SVGGraphics2DIOException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;


public class WizardExportToImages extends AbstractDefaultWizard {
	private	final	static	Dimension	WIZARD_PANEL_SIZE	= new Dimension(600, 400);
	private	final	static	String[]	SAVE_FORMATS		= new String[]{	"JPEG format (.jpeg)",
																			"PNG format (.png)",
																			"Vectorial format (.svg)"
																			};

	public	WizardExportToImages(String path, String dialogTitle, JTabbedPaneCanvas tabbedCanvas){
		super(path, dialogTitle, 1);
		super.addDialogSize(0, WIZARD_PANEL_SIZE);
		super.setLastNextButtonLabel("Export");
		_tabbedCanvas = tabbedCanvas;
	}

	@Override
	protected	void	addWizardPanels(JPanel panelFrame) {
		JPanel			panelSaveMaps = new JPanel();

		addSaveMapsPanelOnPanel(panelSaveMaps);
		panelFrame.add(panelSaveMaps, "1");
	}

	@Override
	protected	void	launch(int panelId) {
		String			path	= null;

		super.launch(panelId);
		if (0 == panelId){
			path = _fileName.getText();
	
			switch (_fileTypeBox.getSelectedIndex()){
				case 0:
					exportImage(path, "jpeg");
					break;
				case 1:
					exportImage(path, "png");
					break;
				case 2:
					exportSVG(path);
					break;
			}
		}
	}

	/**
	 * Adds the components for maps saving process onto the given panel.
	 * @param panelFrame The panel where to place the components.
	 */
	private	void			addSaveMapsPanelOnPanel(final JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();
		JLabel				fileTypeLabel	= new JLabel("Output type : ");
		JLabel				fileLabel		= new JLabel("Output file : ");
		JButton				browseButton	= new JButton("Browse");
		int					iRow			= 0;
		int					iCol			= 0;

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		c.anchor = GridBagConstraints.LINE_END;
		GridBagLayoutUtils.set_gbc(c, iCol, iRow, 1, 1, GridBagConstraints.NONE);
		c.weightx = 0;
		pane.add(fileLabel, c);

		addFieldVerification(_fileName, FIELD_NOT_NULL);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.weightx = 1;
		pane.add(_fileName, c);

		GridBagLayoutUtils.set_gbc(c, iCol, iRow, 1, 1, GridBagConstraints.NONE);
		c.weightx = 0;
		pane.add(fileTypeLabel, c);

		_fileTypeBox = new JComboBox(SAVE_FORMATS);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.weightx = 1;
		pane.add(_fileTypeBox, c);

		GridBagLayoutUtils.set_gbc(c, iCol+1, iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weighty = 1;
		pane.add(browseButton, c);

		//Actions performed
		browseButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void			actionPerformed(java.awt.event.ActionEvent evt) {
				JFileChooser	fileChooser = JFileChooserTools.createFileChooser("Export...", "Export", false, "BioMercator_screenshot");

				if (fileChooser.showOpenDialog(pane) == JFileChooser.APPROVE_OPTION){
					Session.instance().setValue(Session.CHOOSER_FILE_DIR, fileChooser.getCurrentDirectory());
					File file = fileChooser.getSelectedFile();
					_fileName.setText(file.getAbsolutePath());
				}
            }
        });
	}

	private	void		exportImage(String fileName, String extensionStr){
		Graphics2D		g2d				= null;
		Canvas			canvas			= _tabbedCanvas.getSelectedCanvas();
		BufferedImage	bufferedImage	= new BufferedImage(canvas.getWidth(),
															canvas.getHeight(),
															BufferedImage.TYPE_INT_RGB);
		g2d = bufferedImage.createGraphics();
		canvas.paintComponent(g2d);
		g2d.dispose();
		fileName = StringTools.addExtensionIfNeeded(fileName, extensionStr);
		try {
			ImageIO.write(bufferedImage, extensionStr, new File(fileName));
		} catch (IOException ex) {
			Logger.getLogger(WizardExportToImages.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private	void	exportSVG(String fileName){
		String		extensionStr = "svg";

		fileName = StringTools.addExtensionIfNeeded(fileName, extensionStr);
		// Get a DOMImplementation.
		DOMImplementation domImpl =
		GenericDOMImplementation.getDOMImplementation();

		// Create an instance of org.w3c.dom.Document.
		String svgNS = "http://www.w3.org/2000/svg";
		Document document = domImpl.createDocument(svgNS, extensionStr, null);

		// Create an instance of the SVG Generator.
		SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

		////////////////////////////////////////////////////////////////////////
		 // Ask the test to render into the SVG Graphics2D implementation.
		_tabbedCanvas.getSelectedCanvas().paintComponent(svgGenerator);

		////////////////////////////////////////////////////////////////////////
		// Finally, stream out SVG to the standard output using
		// UTF-8 encoding.
		boolean useCSS = true; // we want to use CSS style attributes
		Writer out;
		try {
			out = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8");
			svgGenerator.stream(out, useCSS);
		} catch (SVGGraphics2DIOException ex) {
			Logger.getLogger(WizardExportToImages.class.getName()).log(Level.SEVERE, null, ex);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(WizardExportToImages.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedEncodingException ex) {
			Logger.getLogger(WizardExportToImages.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}

	private	JTextField			_fileName		= new JTextField();
	private	JComboBox			_fileTypeBox	= null;
	private	JTabbedPaneCanvas	_tabbedCanvas = null;
}
