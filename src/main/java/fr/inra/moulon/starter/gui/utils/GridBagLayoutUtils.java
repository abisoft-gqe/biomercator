/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 5, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.utils;

import java.awt.GridBagConstraints;


public class GridBagLayoutUtils {
	/**
	 * Sets the given parameters to the given GridBagConstraints.
	 * @param gbc The GridBagConstraints to be modified; needs to be instanced
	 * @param row
	 * @param column
	 * @param width
	 * @param height
	 * @param fill
	 */
	public	static	void	set_gbc(GridBagConstraints gbc, int column, int row, int width, int height, int fill) {
		gbc.gridy = row;
		gbc.gridx = column;
		gbc.gridwidth = width;
		gbc.gridheight = height;
		gbc.fill = fill;  // GridBagConstraints.NONE .HORIZONTAL .VERTICAL .BOTH
	}
}
