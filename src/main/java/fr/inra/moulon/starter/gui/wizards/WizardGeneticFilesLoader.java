/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.FileManager.FileFormat;
import fr.inra.moulon.starter.fileTools.utils.Summary;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.JFileChooserTools;
import fr.inra.moulon.starter.utils.Session;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class WizardGeneticFilesLoader extends AbstractDefaultWizard{
	private	final	static	Dimension	WIZARD_PANEL_FILES	= new Dimension(600, 400);
	private	final	static	Dimension	WIZARD_SUMMARY		= new Dimension(600, 400);
	private	final	static	int			NB_CARDS			= 3;

	public WizardGeneticFilesLoader(String path, String dialogTitle){
		super(path, dialogTitle, NB_CARDS);
		_filesToLoad = new ArrayList<File>();
		_filesFormats = new ArrayList<FileFormat>();
		super.addDialogSize(1, WIZARD_PANEL_FILES);
		super.addDialogSize(2, WIZARD_SUMMARY);
	}

	@Override
	public	void	resetWizardParameters(){
		super.resetWizardParameters();
		_filesToLoad.clear();
		_filesFormats.clear();
	}

	@Override
	protected	void	launch(int panelId){
		GridLayout		experimentLayout	= null;
		File			file				= null;
		FileFormat		formatId			= null;

		super.launch(panelId);
		if (1 == panelId){	// The second card has been validated
			experimentLayout = new GridLayout(0,1);
			_summaryPanel.removeAll();
			_summaryPanel.setLayout(experimentLayout);
			for (int i = 0; i < _filesToLoad.size(); i++) {
				file = _filesToLoad.get(i);
				formatId = _filesFormats.get(i);
				FileManager.load(file, formatId, super._projectName, null);
				_summaryPanel.add(new JLabel("<html>File : <i>" + file.getAbsolutePath() + "</i></html>"));
				for (Iterator<String> it = Summary.instance().iterator(); it.hasNext();) {
					String infos = it.next();
					_summaryPanel.add(new JLabel(infos));
				}
			}
			_summaryPanel.revalidate();
		}
	}

	/**
	 * Adds the different wizard's steps as different panels.
	 * @param panelFrame The frame JPanel where panels will be placed.
	 */
	@Override
	protected	void	addWizardPanels(JPanel panelFrame){
		JPanel			panelProjectChoice = new JPanel();
		JPanel			panelListFiles = new JPanel();
		JPanel			panelSummary = new JPanel();

		super.addProjectChoicePanelOnPanel(panelProjectChoice);
		panelFrame.add(panelProjectChoice, "1");

		addFilesListLoadingPanelOnPanel(panelListFiles);
		panelFrame.add(panelListFiles, "2");

		addSummaryLoadingPanelOnPanel(panelSummary);
		panelFrame.add(panelSummary, "3");
	}

	/**
	 * THE WIZARD'S SECOND STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	private	void			addFilesListLoadingPanelOnPanel(final JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();
		final JPanel		filesPanel		= new JPanel();
		JScrollPane			scrollPane		= new JScrollPane(filesPanel);
		JButton				browseButton	= new JButton("Browse");

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.weightx = 1;
		pane.add(browseButton, c);

		GridBagLayoutUtils.set_gbc(c, 0, 1, 1, 1, GridBagConstraints.BOTH);
		scrollPane.setSize(WIZARD_PANEL_FILES);
		c.weighty = 1;
		c.insets = new Insets(10, 0, 0, 0);
		pane.add(scrollPane, c);

		GridLayout experimentLayout = new GridLayout(0,1);
		filesPanel.removeAll();
		filesPanel.setLayout(experimentLayout);

		//Actions performed
		browseButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
            public void			actionPerformed(java.awt.event.ActionEvent evt) {
				JFileChooser	fileChooser = JFileChooserTools.createFileChooser("Choose file...", "Open", true, null);
				GridLayout		experimentLayout = new GridLayout(0,2);

				filesPanel.removeAll();
				filesPanel.setLayout(experimentLayout);
				if (fileChooser.showOpenDialog(pane) == JFileChooser.APPROVE_OPTION){
					Session.instance().setValue(Session.CHOOSER_FILE_DIR, fileChooser.getCurrentDirectory());
					for (int i = 0; i < fileChooser.getSelectedFiles().length; i++) {
						File file = fileChooser.getSelectedFiles()[i];

						_filesToLoad.add(file);
						_filesFormats.add(FileManager.findFormat(file));
						filesPanel.add(new JLabel(_filesToLoad.get(i).getAbsolutePath()));
						filesPanel.add(new JLabel(FileManager.getFormatTxt(_filesFormats.get(i))));
					}
					filesPanel.setPreferredSize(new Dimension(100, 20*fileChooser.getSelectedFiles().length));
					filesPanel.revalidate();
				}
            }
        });
	}

	/**
	 * THE WIZARD'S THIRD (and last) STEP
	 * Shows a summary of loaded files.
	 * @param panelFrame The panel where to place summary.
	 */
	private void			addSummaryLoadingPanelOnPanel(final JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();
		JLabel				label			= new JLabel("Summary");
		JScrollPane			scrollPane		= null;

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.weightx = 1;
		pane.add(label, c);

		_summaryPanel = new JPanel();
		scrollPane = new JScrollPane(_summaryPanel);
		GridBagLayoutUtils.set_gbc(c, 0, 1, 1, 1, GridBagConstraints.BOTH);
		scrollPane.setSize(WIZARD_SUMMARY);
		c.weighty = 1;
		c.insets = new Insets(10, 0, 0, 0);
		pane.add(scrollPane, c);
	}

	private	JPanel				_summaryPanel	= null;
	private	List<File>			_filesToLoad	= null;
	private	List<FileFormat>	_filesFormats	= null;
}
