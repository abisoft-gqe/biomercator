/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 5, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;

import metaqtl.biomercatormetaqtltognpmapmetaqtl_0_3.BioMercatorMetaQTLToGnpMapMetaQTL;
import fr.inra.moulon.starter.controller.ControllerGUI;
import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.controller.visitors.VisitorSaveStandardXML;
import fr.inra.moulon.starter.controller.visitors.VisitorSaveStdBioM3;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.gui.MainFrame;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.JFileChooserTools;
import fr.inra.moulon.starter.utils.Session;

public class WizardSaveMaps extends AbstractDefaultWizard {
	
	private final static Dimension WIZARD_PANEL_SIZE = new Dimension(600, 400);
	private final static String[] SAVE_FORMATS = new String[] {
			"BioMercatorV3 standard file (.txt)",
			"BioMercatorV3 XML file (.xml)", "GnpMap submission file (.xls)" };

	public WizardSaveMaps(String path, String dialogTitle) {
		super(path, dialogTitle, 1);
		super.addDialogSize(0, WIZARD_PANEL_SIZE);
		super.setLastNextButtonLabel(_bundle.getString("wizard_save"));
	}

	@Override
	protected void addWizardPanels(JPanel panelFrame) {
		JPanel panelSaveMaps = new JPanel();

		addSaveMapsPanelOnPanel(panelSaveMaps);
		panelFrame.add(panelSaveMaps, "1");
	}

	@Override
	protected void launch(int panelId) {
		System.out.println("Launching export...");
		Visitor v = null;
		List<MapGene> maps = null;
		String path = null;
		// File urgiJob = getURGIJob();

		super.launch(panelId);
		if (0 == panelId) {
			maps = ControllerGUI.getCheckedMaps(_explorer);
			path = _fileName.getText();

			switch (_fileTypeBox.getSelectedIndex()) {
			case 0: // BioMercatorV3 standard file
				for (Iterator<MapGene> it = maps.iterator(); it.hasNext();) {
					MapGene mapGene = it.next();
					File fileMap = new File(path + "_" + mapGene.getName()
							+ "_map.txt");
					File fileQtl = new File(path + "_" + mapGene.getName()
							+ "_qtl.txt");

					v = new VisitorSaveStdBioM3(fileMap, fileQtl);
					mapGene.accept(v);
				}
				break;
			case 1: // BioMercatorV3 XML file
				for (Iterator<MapGene> it = maps.iterator(); it.hasNext();) {
					MapGene mapGene = it.next();
					String filePath = path + "_" + mapGene.getName() + ".xml";

					v = new VisitorSaveStandardXML(filePath);
					mapGene.accept(v);
				}
				break;
			case 2: // GnpMap submission file
				for (Iterator<MapGene> it = maps.iterator(); it.hasNext();) {
					MapGene mapGene = it.next();
					final String input;
					final String output;

					v = new VisitorSaveStandardXML(path);
					mapGene.accept(v);
					input = ((VisitorSaveStandardXML) v).getFinalFileName();
					output = input.replaceAll("\\.xml$", ".xls");

					try {
						Thread jobThread = new Thread() {
							@Override
							public synchronized void run() {
								try {
									runJob(input, output);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						};
						jobThread.start();
						showProgress();
					} catch (Exception e) {
						System.out.println("Unexpected error during export.");
					}
				}
				break;
			}
		}
	}

	private void showProgress() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				MainFrame.enableProgressBar();
			}
		});
	}

	private void hideProgress() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				MainFrame.disableProgressBar();
				JOptionPane.showMessageDialog(null, _bundle.getString("wizard_export_finished"));
			}
		});
	}

	private void runJob(String input, String output) throws Exception {

		
		System.out.println("Launching export to GnpMap xls format...");
		BioMercatorMetaQTLToGnpMapMetaQTL job = new BioMercatorMetaQTLToGnpMapMetaQTL();
		String[] args = new String[5];
		args[0] = "--context_param";
		args[1] = "FileInPut=" + input;
		args[2] = "--context_param";
		args[3] = "FileOutPut=" + output;
		args[4] = "--context=Default";
//		job.runJob(args);
		job.runJobInTOS(args);
		
		//

//		System.out.println("Launch separate jvm.");
//	    String separator = System.getProperty("file.separator");
//	    String classpath = System.getProperty("java.class.path");
//	    String path = System.getProperty("java.home")
//	            + separator + "bin" + separator + "java";
//	    ProcessBuilder processBuilder = 
//	            new ProcessBuilder(path, "-Xmx1024m", "-cp",
//	            classpath, 
//	            BioMercatorMetaQTLToGnpMapMetaQTL.class.getName(), args[0], args[1], args[2], args[3], args[4]);
//	    Process process = processBuilder.start();
//	    process.waitFor();
//	    System.out.println(process.exitValue());
//	    System.out.println(process.getOutputStream());
//	    System.out.println(process.getErrorStream());
//	    System.out.println(process.getInputStream());
	    
		System.out.println("Exporting done.");
		
		// clean temporary files
		File oldFile = new File(input);
		oldFile.delete();
		hideProgress();
	}

	/**
	 * Adds the components for maps saving process onto the given panel.
	 * 
	 * @param panelFrame
	 *            The panel where to place the components.
	 */
	private void addSaveMapsPanelOnPanel(final JPanel pane) {
		GridBagConstraints c = new GridBagConstraints();
		JLabel fileTypeLabel = new JLabel(
				_bundle.getString("wizard_save_output_type"));
		JLabel fileLabel = new JLabel(
				_bundle.getString("wizard_save_output_file"));
		JScrollPane scrollPane = null;
		JButton browseButton = new JButton(_bundle.getString("misc_browse"));
		final JLabel labelWarningLong = new JLabel();

		_explorer = ControllerGUI.createCheckTree(true);
		addFieldVerification(_explorer);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		scrollPane = new JScrollPane(_explorer);
		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 4, GridBagConstraints.BOTH);
		c.weightx = 1;
		pane.add(scrollPane, c);

		c.anchor = GridBagConstraints.LINE_END;
		GridBagLayoutUtils.set_gbc(c, 1, 0, 1, 1, GridBagConstraints.NONE);
		c.weightx = 0;
		pane.add(fileLabel, c);

		addFieldVerification(_fileName, FIELD_NOT_NULL);
		GridBagLayoutUtils
				.set_gbc(c, 2, 0, 1, 1, GridBagConstraints.HORIZONTAL);
		c.weightx = 1;
		pane.add(_fileName, c);

		GridBagLayoutUtils.set_gbc(c, 1, 1, 1, 1, GridBagConstraints.NONE);
		c.weightx = 0;
		pane.add(fileTypeLabel, c);

		_fileTypeBox = new JComboBox(SAVE_FORMATS);
		GridBagLayoutUtils
				.set_gbc(c, 2, 1, 1, 1, GridBagConstraints.HORIZONTAL);
		c.weightx = 1;
		pane.add(_fileTypeBox, c);

		GridBagLayoutUtils.set_gbc(c, 2, 2, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weighty = 1;
		pane.add(browseButton, c);

		GridBagLayoutUtils.set_gbc(c, 1, 3, 2, 1, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.CENTER;
		pane.add(labelWarningLong, c);

		// Actions performed
		browseButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				JFileChooser fileChooser = JFileChooserTools.createFileChooser(
						_bundle.getString("menu_file_save_as"),
						_bundle.getString("misc_save"), false, "map");

				if (fileChooser.showOpenDialog(pane) == JFileChooser.APPROVE_OPTION) {
					Session.instance().setValue(Session.CHOOSER_FILE_DIR,
							fileChooser.getCurrentDirectory());
					File file = fileChooser.getSelectedFile();
					_fileName.setText(file.getAbsolutePath());
				}
			}
		});

		/*_fileTypeBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					if (_fileTypeBox.getSelectedIndex() == 2) {
						labelWarningLong.setText(_bundle.getString("wizard_save_job_warning"));
					} else {
						labelWarningLong.setText("");
					}
				}
			}
		});*/
	}

	private JTextField _fileName = new JTextField();
	private JComboBox _fileTypeBox = null;
	private JTree _explorer = new JTree();
	private static ResourceBundle _bundle = (ResourceBundle) Session.instance()
			.getValue(Session.RESSOURCE_BUNDLE);
}
