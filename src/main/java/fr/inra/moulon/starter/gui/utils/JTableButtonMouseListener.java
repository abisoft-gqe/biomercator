/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.utils;

import fr.inra.moulon.starter.graphic.utils.ElementColorManager;
import fr.inra.moulon.starter.gui.MainFrame;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author sosnowski
 */
public class JTableButtonMouseListener implements MouseListener {
	public JTableButtonMouseListener(JTable table, ElementColorManager colorManager) {
		_table			= table;
		_colorManager	= colorManager;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		launchColorChoosing(e);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	private	void			launchColorChoosing(MouseEvent evt) {
		TableColumnModel	columnModel	= _table.getColumnModel();
		int					column		= columnModel.getColumnIndexAtX(evt.getX());
		int					row			= evt.getY() / _table.getRowHeight();
		Object				value		= null;
		JButton				button		= null;
		MouseEvent			buttonEvent	= null;
		Boolean				selection	= null;
		String				name		= null;

		if (row >= 0 && row < _table.getRowCount() && column >= 0 && column < _table.getColumnCount()){
			value = _table.getValueAt(row, column);
			name = (String)_table.getValueAt(row, 0);

			try{
				button = (JButton)value;

				Color c = JColorChooser.showDialog(null, "Color choice", button.getBackground());
				Color cPrev = _colorManager.getColor(name);
				button.setBackground(c);
				_colorManager.setColor(name, new Color(	c.getRed(),
														c.getGreen(),
														c.getBlue(),
														cPrev.getAlpha()));

				buttonEvent = (MouseEvent)SwingUtilities.convertMouseEvent(_table, evt, button);
				button.dispatchEvent(buttonEvent);
				_table.repaint();
				MainFrame.redraw();
			}catch (Exception e){}
			try{
				selection = (Boolean)value;
				_colorManager.setSelected(name, selection);
				MainFrame.redraw();
			}catch (Exception e){}
		}
	}

	private		JTable				_table			= null;
	protected	ElementColorManager	_colorManager	= null;
}
