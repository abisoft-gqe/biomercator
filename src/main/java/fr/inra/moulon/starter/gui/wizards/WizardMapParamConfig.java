/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-Sep-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class WizardMapParamConfig  extends AbstractDefaultWizard{
	private	final	static	Dimension	WIZARD_MAIN	= new Dimension(500, 400);
	private	final	static	int			NB_CARDS	= 1;

	public WizardMapParamConfig(String dialogTitle){
		super(null, dialogTitle, NB_CARDS);
		super.addDialogSize(0, WIZARD_MAIN);
	}

	public	void	setParams(String projectName, MapGene map){
		_map = map;
		_projectName = projectName;
	}

	@Override
	protected	void	addWizardPanels(JPanel panelFrame) {
		JPanel			panel = new JPanel();

		addMapParamConfigPanelOnPanel(panel);
		panelFrame.add(panel, "1");
	}


	@Override
	protected	void	launch(int panelId) {
		super.launch(panelId);
		if (0 == panelId){
			FileManager.deleteMap(_projectName, _map.getName());
			_map.setName(							FIELDS.get(MAP_NAME).textField.getText());
			_map.setGenus(							FIELDS.get(GENUS).textField.getText());
			_map.setSpecies(						FIELDS.get(SPECIES).textField.getText());
			_map.setCrossType(						FIELDS.get(CROSSTYPE).textField.getText());
			_map.setCrossSize(Integer.valueOf(		FIELDS.get(POPSIZE).textField.getText()));
			_map.setMappingCrossType(				FIELDS.get(MAPPING_CROSSTYPE).textField.getText());
			_map.setMappingFunction(				FIELDS.get(MAPPING_FUNCTION).textField.getText());
			_map.setMappingUnit(					FIELDS.get(MAP_UNIT).textField.getText());
			_map.setMapExpansion(Integer.valueOf(	FIELDS.get(MAP_EXPANSION).textField.getText()));
			_map.setMapQuality(	Integer.valueOf(	FIELDS.get(MAP_QUALITY).textField.getText()));
			FileManager.writeMap(_map, _projectName);
		}
	}

	private	void			addMapParamConfigPanelOnPanel(final JPanel pane){
		GridBagConstraints	c			= new GridBagConstraints();
		JLabel				label		= null;

		c.weightx = 1;
		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		addFieldVerifications();
		setDefaultTextFieldValues();
		for (int i = 0; i < MAP_PARAMS.length; i++) {
			Field field = FIELDS.get(MAP_PARAMS[i]);

			label = new JLabel(field.label);
			GridBagLayoutUtils.set_gbc(c, 0, i, 1, 1, GridBagConstraints.BOTH);
			pane.add(label, c);

			GridBagLayoutUtils.set_gbc(c, 1, i, 1, 1, GridBagConstraints.BOTH);
			pane.add(field.textField, c);
		}
	}

	private	void	setDefaultTextFieldValues(){
		FIELDS.get(MAP_NAME).textField.setText(			_map.getName());
		FIELDS.get(GENUS).textField.setText(			_map.getGenus());
		FIELDS.get(SPECIES).textField.setText(			_map.getSpecies());
		FIELDS.get(CROSSTYPE).textField.setText(		_map.getCrossType());
		FIELDS.get(POPSIZE).textField.setText(			String.valueOf(_map.getCrossSize()));
		FIELDS.get(MAPPING_CROSSTYPE).textField.setText(_map.getMappingCrossType());
		FIELDS.get(MAPPING_FUNCTION).textField.setText(	_map.getMappingFunction());
		FIELDS.get(MAP_UNIT).textField.setText(			_map.getMappingUnit());
		FIELDS.get(MAP_EXPANSION).textField.setText(	String.valueOf(_map.getMapExpansion()));
		FIELDS.get(MAP_QUALITY).textField.setText(		String.valueOf(_map.getMapQuality()));
	}

	private	void	addFieldVerifications(){
		super.addFieldVerification(FIELDS.get(MAP_NAME).textField,			FIELD_NOT_NULL);
		super.addFieldVerification(FIELDS.get(GENUS).textField,				FIELD_NOT_NULL);
		super.addFieldVerification(FIELDS.get(SPECIES).textField,			FIELD_NOT_NULL);
		super.addFieldVerification(FIELDS.get(CROSSTYPE).textField,			FIELD_NOT_NULL);
		super.addFieldVerification(FIELDS.get(POPSIZE).textField,			FIELD_INTEGER);
		super.addFieldVerification(FIELDS.get(MAPPING_CROSSTYPE).textField,	FIELD_NOT_NULL);
		super.addFieldVerification(FIELDS.get(MAPPING_FUNCTION).textField,	FIELD_NOT_NULL);
		super.addFieldVerification(FIELDS.get(MAP_UNIT).textField,			FIELD_NOT_NULL);
		super.addFieldVerification(FIELDS.get(MAP_EXPANSION).textField,		FIELD_NOT_NULL);
		super.addFieldVerification(FIELDS.get(MAP_QUALITY).textField,		FIELD_NOT_NULL);
	}

	private	MapGene	_map			= null;

	/* STATIC FIELDS */
	private	static	class Field{
		public	Field(String label){
			this.textField = new JTextField();
			this.label = label;
		}
		public	JTextField	textField;
		public	String		label;
	}

	private static final Integer		MAP_NAME			= 1;
	private static final Integer		GENUS				= 2;
	private static final Integer		SPECIES				= 3;
	private static final Integer		CROSSTYPE			= 4;
	private static final Integer		POPSIZE				= 5;
	private static final Integer		MAPPING_CROSSTYPE	= 6;
	private static final Integer		MAPPING_FUNCTION	= 7;
	private static final Integer		MAP_UNIT			= 8;
	private static final Integer		MAP_EXPANSION		= 9;
	private static final Integer		MAP_QUALITY			= 10;
	private static final Integer[]		MAP_PARAMS			= new Integer[]{
																MAP_NAME,
																GENUS,
																SPECIES,
																CROSSTYPE,
																POPSIZE,
																MAPPING_CROSSTYPE,
																MAPPING_FUNCTION,
																MAP_UNIT,
																MAP_EXPANSION,
																MAP_QUALITY
															};

	private	static final Map<Integer, Field>	FIELDS;

	static{
		FIELDS = new HashMap<Integer, Field>();
		FIELDS.put(MAP_NAME,			new Field("<html><b>Map name</b>"));
		FIELDS.put(GENUS,				new Field("<html><b>Genus</b>"));
		FIELDS.put(SPECIES,				new Field("<html><b>Species</b>"));
		FIELDS.put(CROSSTYPE,			new Field("<html><b>Cross type</b>"));
		FIELDS.put(POPSIZE,				new Field("<html><b>Population size</b>"));
		FIELDS.put(MAPPING_CROSSTYPE,	new Field("<html><b>Mapping cross type</b>"));
		FIELDS.put(MAPPING_FUNCTION,	new Field("<html><b>Mapping function</b>"));
		FIELDS.put(MAP_UNIT,			new Field("<html><b>Map unit</b>"));
		FIELDS.put(MAP_EXPANSION,		new Field("<html><b>Map expansion</b>"));
		FIELDS.put(MAP_QUALITY,			new Field("<html><b>Map quality</b>"));
	}
}
