/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 11-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.combomodels;

import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.utils.ContentNameComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * This model is a combo box model displaying children of the container given
 * in the constructor.
 * @author sosnowski
 */
public class ComboBoxLkgModel extends ComboBoxCustomModel {
	public	ComboBoxLkgModel(Container container, Class class2get){
		List<Content>	list = null;

		if (null != container){
			_v = new VisitorEltGet(class2get, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
			container.accept(_v);
			list = _v.getList();
			Collections.sort(list, new ContentNameComparator());
			_list = list;
		}else{
			_list = new ArrayList<Object>();
		}
	}
}
