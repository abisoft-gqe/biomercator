/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.dragNdrop;

import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.file.FileRes;
import fr.inra.moulon.starter.gui.MainFrame;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.Iterator;
import java.util.List;
import javax.swing.TransferHandler;

/**
 * This class is the dropping process handler; it's design to handle drops on
 * the 'Canvas' component.
 * @author Sosnow
 */
public abstract	class DropTransferHandler extends TransferHandler {
	public DropTransferHandler(int action) {
		_action = action;
	}

	@Override
	public	boolean		canImport(TransferHandler.TransferSupport support) {
		List<Content>	transferable	= getTransferable(support);
		boolean			res				= true;

		if (!support.isDrop() || !support.isDataFlavorSupported(new DataFlavorContents())) {
			res = false;
		}
		if (null == transferable){
			res = false;
		}else{
			switch (transferable.size()){
					case 0:	//	Can't import nothing
					case 1:	//	Can't import a project
					case 5:	//	Can't import a MetaAnalysis
						res = false;
						break;
				}
		}

		res &= (_action & support.getSourceDropActions()) == _action;
		
		return res;
	}

	public abstract	void addLinkageGroup(LinkageGroup lkg, MetaAnalysis metaAnalysis, MetaModel metaModel);

	/**
	 * Method for main frame's fields updating.
	 */
	protected	void	updateMainFrameFields(){
		MainFrame.updateFields();
	}

	public abstract	void addFile(FileRes fileRes);

	private	void	addChromosome(Chromosome chr){
		for (Iterator<LinkageGroup> itLkg = chr.iterator(); itLkg.hasNext();) {
			LinkageGroup lkg = itLkg.next();
			addLinkageGroup(lkg, null, null);
		}
	}

	@Override
	public boolean		importData(TransferHandler.TransferSupport support) {
		List<Content>	list = null;

		if (!canImport(support)) {
			return false;
		}

		if (null != (list = getTransferable(support))){
			try {
				switch (list.size()){
					case 0:	//	Nothing in the transferable
						break;
					case 1:	//	A project
						break;
					case 2:	//	...and a MapGene or a file
						try{
							addFile((FileRes)list.get(1));
						}catch(Exception e){
							MapGene map = (MapGene)list.get(1);
							for (Iterator<Chromosome> itChr = map.iterator(); itChr.hasNext();) {
							   addChromosome(itChr.next());
							}
						}
						break;
					case 3:	//	...and a Chromosome
						Chromosome chr = (Chromosome)list.get(2);
						addChromosome(chr);
						break;
					case 4:	//	...and a LinkageGroup
						addLinkageGroup((LinkageGroup)list.get(3), null, null);
						break;
					case 5:	//	...and a MetaAnalysis
						break;
					case 6:	//	...and a MetaModel
						try{
							addFile((FileRes)list.get(5));
						}catch(Exception e){
							addLinkageGroup((LinkageGroup)list.get(3),
											(MetaAnalysis)list.get(4),
											(MetaModel)list.get(5));
						}
						break;
				}
			} catch (Throwable t) {
				t.printStackTrace(System.out);
			}
		}

		return true;
	}


	/**
	 * Tries to get the Content from the dropped object.
	 * @param support
	 * @return The Content from the dropped object if possible; null otherwise.
	 */
	protected List<Content>	getTransferable(TransferHandler.TransferSupport support){
		List<Content>		t = null;

		try{
			t = (List<Content>)support.getTransferable().getTransferData(new DataFlavorContents());
		}
		catch (UnsupportedFlavorException e) {}
		catch (java.io.IOException e) {}

		return t;
	}

	private		int			_action	= 0;
}
