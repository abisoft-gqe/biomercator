/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 05-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.table.DefaultTableModel;


public class TableModelPreview {
	public	static	DefaultTableModel	getDataModel(File csvFile, String separator){
		DefaultTableModel				model	= new DefaultTableModel();
		BufferedReader					buf		= null;
		String							line	= null;
		int								nbCols	= 0;
		String[]						fields	= null;

		try {
			// Counting columns number
			buf = new BufferedReader(new FileReader(csvFile));
			for (int i = 0; i < 50 && null != (line = buf.readLine()); ++i){
				fields = line.split(separator);
				nbCols = Math.max(nbCols, fields.length);
			}
			buf.close();


			// Filling the nbCols colunms
			for (int i = 0; i < nbCols; ++i){
				model.addColumn("col " + i);
			}

			// Filling the n first lines
			buf = new BufferedReader(new FileReader(csvFile));
			for (int i = 0; i < 50 && null != (line = buf.readLine()); ++i){
				fields = line.split(separator);
				model.addRow(fields);
			}
			buf.close();
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		} catch (IOException ex){
			System.out.println(ex);
		}

		return model;
	}
}
