/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 16-Jun-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class WizardQTLChoice extends AbstractDefaultWizard {
	private	final	static	Dimension	WIZARD_PANEL_SIZE	= new Dimension(600, 400);

	public	WizardQTLChoice(String		dialogTitle,
							String		projectName,
							String		mapName,
							String		chrName,
							String		lkgName,
							List<Qtl>	dubiousQtls){
		super("", dialogTitle, 0);
		super.addDialogSize(0, WIZARD_PANEL_SIZE);
		super.setLastNextButtonLabel("Choose");
		_qtlProjectName	= projectName;
		_qtlMapName		= mapName;
		_qtlChrName		= chrName;
		_qtlLkgName		= lkgName;
		_dubiousQtls	= dubiousQtls;
	}

	@Override
	protected	void	addWizardPanels(JPanel panelFrame) {
		JPanel			panelQTLChoose = new JPanel();

		addQTLChoosePanelOnPanel(panelQTLChoose);
		panelFrame.add(panelQTLChoose, "1");
	}

	@Override
	protected	void	launch(int panelId) {
		if (0 == panelId){
			setDubiousQtls();
		}
	}

	/**
	 * Adds the components for maps saving process onto the given panel.
	 * @param panelFrame The panel where to place the components.
	 */
	private	void			addQTLChoosePanelOnPanel(final JPanel pane){
		GridBagConstraints	c				= new GridBagConstraints();

		_qtlChooserTable = new JTable();
		_qtlChooserTable.setAutoCreateRowSorter(true);
		setTableModel(_qtlChooserTable);
		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 1;
		c.weighty = 1;

		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.BOTH);
		pane.add(new JScrollPane(_qtlChooserTable), c);
	}

	/**
	 * Set the model for QTL listing and selection to the given JTable.
	 * @param table The Jtable
	 */
	private void			setTableModel(JTable table){
		DefaultTableModel	model = null;

		table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            },
            new String[] {
				"Qtl",
				"Trait",
				"Position",
				"Start",
				"End",
				"Selection"
			}
        ) {
            Class[] types = new Class [] {
                String.class,
				String.class,
				Double.class,
				Double.class,
				Double.class,
				Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false,
				false,
				false,
				false,
				false,
				true
            };

			@Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

			@Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
		model = (DefaultTableModel)table.getModel();

		Content		content = Controller.getContent(new String[]{	_qtlProjectName,
																	_qtlMapName,
																	_qtlChrName,
																	_qtlLkgName
																});
		Container	container = null;
		List<Qtl>	qtls = null;

		if (null != content){
			try{
				container = (Container)content;
				qtls = Controller.getElements(container, Qtl.class);
			}catch (Exception e){
				System.out.println("Error in WizardQTLChoice.");
			}
		}

		if (null != qtls){
			for (int i = 0; i < qtls.size(); ++i){
				Qtl qtl = qtls.get(i);
				Boolean	selected = Boolean.TRUE;
				String qtlName = qtl.getName();
				String qtlTraitName = qtl.getTrait();
				DecimalFormat f = new DecimalFormat();
				f.setMaximumFractionDigits(1);
								
				if (null != _dubiousQtls && _dubiousQtls.contains(qtl)){
					selected = Boolean.FALSE;
				}
				model.addRow(new Object[] {	qtlName,
											qtlTraitName,
											qtl.getPosition(),
											qtl.getPositionStart(),
											qtl.getPositionEnd(),
											selected});
			}
		}
	}

	private void		setDubiousQtls(){
		LinkageGroup	lkg = null;

		_dubiousQtls = new ArrayList<Qtl>();
		lkg = (LinkageGroup)Controller.getContent(new String[]{	_qtlProjectName,
																_qtlMapName,
																_qtlChrName,
																_qtlLkgName});
		for (int i = 0; i < _qtlChooserTable.getRowCount(); ++i){
			Boolean selected = (Boolean)_qtlChooserTable.getValueAt(i, 5);
			if (!selected){
				String qtlName = (String)_qtlChooserTable.getValueAt(i, 0);
				Qtl qtl = (Qtl)lkg.get(qtlName);
				_dubiousQtls.add(qtl);
			}
		}
	}

	public	List<Qtl>	getDubiousQtls(){
		return _dubiousQtls;
	}

	private	JTable		_qtlChooserTable = new JTable();
	private	String		_qtlProjectName	= null;
	private	String		_qtlMapName		= null;
	private	String		_qtlChrName		= null;
	private	String		_qtlLkgName		= null;
	private List<Qtl>	_dubiousQtls	= null;
}
