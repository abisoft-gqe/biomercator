/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.tree;

import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.file.ImageFile;
import fr.inra.moulon.starter.datamodel.entities.file.SeparatedTextFile;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * This class is used to show the data tree explorer in the left of the main GUI.
 * Although is has access to the data directly, we prefer here to use the
 * Controller class to operate in the methods.
 * A way to prevent this data use possibility could be done.
 * @author sosnowski
 */
public class DataExplorerTreeModel implements TreeModel {
    public  DataExplorerTreeModel(){
        super();
    }

	@Override
    public Object getRoot()  {
        return Controller.getWorkspace();
    }

	@Override
    public int getChildCount(Object parent)  {
		return Controller.getNbContained((Container)parent);
    }

	@Override
    public Object getChild(Object parent, int index) {
		return  Controller.getContained((Container)parent, index);
    }

	@Override
    public int getIndexOfChild(Object parent, Object child) {
        return 0;
    }

	@Override
    public boolean  isLeaf(Object node) {
        boolean     res = false;

//		System.out.println("node : " + node.toString() + " : " + node.getClass());
//		if (node.getClass() == Chromosome.class){
//			Chromosome chr = (Chromosome)node;
//			res = (chr.size() == 1);
//		}else
			if (	node.getClass() == MetaModel.class ||
					node.getClass() == SeparatedTextFile.class ||
					node.getClass() == ImageFile.class){
			res = true;
		}

        return res;
    }

	@Override
    public void addTreeModelListener(javax.swing.event.TreeModelListener l) {}
	@Override
    public void removeTreeModelListener(javax.swing.event.TreeModelListener l) {}
	@Override
    public void valueForPathChanged(TreePath path, Object newValue) {}
}
