/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 05-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.fileTools.converters.FileConverter;
import fr.inra.moulon.starter.fileTools.converters.FromCSV;
import java.util.ArrayList;
import java.util.List;

public class WizardExploringGeneticMap extends WizardExploringCSV{
	public WizardExploringGeneticMap(){
		super("Explain columns");
	}

	@Override
	protected	void	launch(int panelId){
		FileConverter	converter	= null;

		if (0 == panelId){
			converter = new FromCSV(_csvFile, "OUTPUT.MAP.TXT", "MAPMANUAL", _columnFields);
			converter.execute();
			_dialog.setVisible(false);
		}
	}


	@Override
	protected Field[]	generateFields() {
		List<Field>		fields	= new ArrayList<Field>();
		int				i		= 2;

		fields.add(new Field("chr name",		i++, Field.STRING));
		fields.add(new Field("locus name",		i++, Field.STRING));
		fields.add(new Field("locus type",		i++, Field.STRING));
		fields.add(new Field("position",		i++, Field.DOUBLE));
		fields.add(Field.DEFAULT);

		return fields.toArray(new Field[fields.size()]);
	}
}
