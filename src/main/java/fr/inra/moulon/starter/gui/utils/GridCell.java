/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.utils;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.graphic.designers.GOStatistics;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesigner;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesignerGenetic;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesignerPhysical;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesignerPhysicalBlowUp;
import fr.inra.moulon.starter.graphic.designers.PhysicalManager;
import fr.inra.moulon.starter.graphic.utils.Anchor;
import fr.inra.moulon.starter.graphic.utils.Scroller;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Point;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author sosnowski
 */
public class GridCell {
	public	static final	int	MOUSE_OFF						= 0;
	public	static final	int	MOUSE_ON						= 1;
	public	static final	int	MOUSE_ON_EDGE					= 2;
	public	static final	int	MOUSE_ON_GEN					= 3;
	public	static final	int	MOUSE_ON_GEN_LKG				= 4;
	public	static final	int	MOUSE_ON_SCROLLER_GEN			= 5;
	public	static final	int	MOUSE_ON_SCROLLER_GEN_TOP		= 6;
	public	static final	int	MOUSE_ON_SCROLLER_GEN_BOTTOM	= 7;
	public	static final	int	MOUSE_ON_PHY					= 8;
	public	static final	int	MOUSE_ON_SCROLLER_PHY			= 9;
	public	static final	int	MOUSE_ON_SCROLLER_PHY_TOP		= 10;
	public	static final	int	MOUSE_ON_SCROLLER_PHY_BOTTOM	= 11;
	public	static final	int	MOUSE_ON_PHY_BLOW_UP			= 12;
	public	static final	int	MOUSE_ON_ELT					= 13;

	public GridCell(LinkageGroupDesigner	lkgDesignerGen,
					LinkageGroupDesigner	lkgDesignerPhy,
					LinkageGroupDesigner	lkgDesignerPhyBlowUp,
					double					xPos,
					double					yPos){
		this.lkgDesignerGen = lkgDesignerGen;
		this.lkgDesignerPhy = lkgDesignerPhy;
		this.lkgDesignerPhyBlowUp = lkgDesignerPhyBlowUp;
		this.xPos = xPos;
		this.yPos = yPos;
		_frame = new ShapeDesign(ShapeDesign.NONE,xPos, yPos, getWidth(), getHeight(), Color.GRAY);
	}

	/**
	 * Returns if the cell contains the given point.
	 * @param point The point
	 * @return if the cell contains the given point
	 */
	public	boolean	contains(Point point){
		return	point.x >= 0			&&
				point.x <= getWidth()	&&
				point.y >= 0			&&
				point.y <= getHeight();
	}

	/**
	 * Returns if the cell's edge contains the given point.
	 * @param point The point
	 * @return if the cell's edge contains the given point
	 */
	private	boolean	edge_contains(Point point){
		return 	point.x <= _EDGE_THICKNESS			||
				point.x >= getWidth() - _EDGE_THICKNESS	||
				point.y <= _EDGE_THICKNESS			||
				point.y >= getHeight() - _EDGE_THICKNESS;
	}

	/**
	 * Sets the gridCell's "selected" attribute and changes its display in
	 * consequence.
	 * @param selected If the scroller is to be selected
	 * @return if the scroller's display has been changed
	 */
	public	boolean	setSelected(boolean selected){
		boolean		res = false;

		if (_selected != selected){
			_selected = selected;
			res = true;
		}
		return res;
	}

	/**
	 * Sets the gridCell's "selected" attribute and changes its display in
	 * consequence.
	 * @param selected If the scroller is to be selected
	 * @return if the scroller's display has been changed
	 */
	public	void	darken(){
		lkgDesignerGen.darkenShapes(true);
	}

	/**
	 * Sets the cells MouseON attribute for frame drawing. Returns if the mouse
	 * area changed.
	 * @param point The mouse point translated to the correct grid cell (x/yPos)
	 * @return If the mouse area changed.
	 */
	public	boolean	setMousePosition(Point point){
		boolean		res		= false;
		int			area	= containingArea(point);

		if (area != _mouseAreaId){
			res = true;
			_mouseAreaId = area;
			setMousePosition();
		}

		return res;
	}

	/**
	 * Sets the cells MouseON attribute to MOUSE_OFF.
	 */
	public	void	setMouseOff(){
		_mouseAreaId = MOUSE_OFF;
		setMousePosition();
	}

	/**
	 * Returns the area containing the given point. (values can be MOUSE_OFF,
	 * MOUSE_ON, MOUSE_ON_EDGE...)
	 * @param p The point
	 * @return the area containing the given point.
	 */
	private	int	containingArea(Point point){
		int		mousePosId			= MOUSE_OFF;
		Point	pTranslated			= null;
		Point	pLkg				= null;

		if (contains(point)){
			mousePosId = MOUSE_ON;
			if (edge_contains(point)){
				mousePosId = MOUSE_ON_EDGE;
			}else {
				//	LINKAGE GROUP GENETIC
				if (null != lkgDesignerGen){
					pLkg = getLkgDesignerGenPosition();
					pTranslated = PointTools.copyNtranslate(point, -pLkg.x, -pLkg.y);
					if (lkgDesignerGen.contains(pTranslated)){	// INSIDE LKG DESIGNER GENETIC
						switch (lkgDesignerGen.scrollerArea(pTranslated)){
							case Scroller.OUSIDE:
								mousePosId = MOUSE_ON_GEN;
								break;
							case Scroller.TOP:
								mousePosId = MOUSE_ON_SCROLLER_GEN_TOP;
								break;
							case Scroller.BOTTOM:
								mousePosId = MOUSE_ON_SCROLLER_GEN_BOTTOM;
								break;
							case Scroller.INSIDE:
								mousePosId = MOUSE_ON_SCROLLER_GEN;
								break;
						}
						if (MOUSE_ON_GEN == mousePosId){
							if (lkgDesignerGen.containsCloseToLkg(pTranslated, 30)){
								mousePosId = MOUSE_ON_GEN_LKG;
							}
							if (null != (_shapeUnderMouse = lkgDesignerGen.onWhichElt(pTranslated))){
								mousePosId = MOUSE_ON_ELT;
							}
						}
					}
				}
				//	LINKAGE GROUP PHYSIC
				if (null != lkgDesignerPhy){
					pLkg = getLkgDesignerPhyPosition();
					pTranslated = PointTools.copyNtranslate(point, -pLkg.x, -pLkg.y);
					if (lkgDesignerPhy.contains(pTranslated)){	// INSIDE LKG DESIGNER PHYSIC
						switch (lkgDesignerPhy.scrollerArea(pTranslated)){
							case Scroller.OUSIDE:
								mousePosId = MOUSE_ON_PHY;
								break;
							case Scroller.TOP:
								mousePosId = MOUSE_ON_SCROLLER_PHY_TOP;
								break;
							case Scroller.BOTTOM:
								mousePosId = MOUSE_ON_SCROLLER_PHY_BOTTOM;
								break;
							case Scroller.INSIDE:
								mousePosId = MOUSE_ON_SCROLLER_PHY;
								break;
						}
					}
				}
				//	LINKAGE GROUP PHYSIC BLOW UP
				if (null != lkgDesignerPhyBlowUp){
					pLkg = getLkgDesignerPhyBlowUpPosition();
					pTranslated = PointTools.copyNtranslate(point, -pLkg.x, -pLkg.y);
					if (lkgDesignerPhyBlowUp.contains(pTranslated)){
						mousePosId = MOUSE_ON_PHY_BLOW_UP;
					}
				}
			}
		}

		return mousePosId;
	}

	public	String	getCurrentShapeInfo(){
		String		info = null;

		if (null != _shapeUnderMouse){
			info = _shapeUnderMouse.info;
		}

		return info;
	}

	/**
	 * Moves the scroller on the previously selected shape.
	 * (through the setMousePosition(Point) function)
	 */
	public	void	moveScrollerOnShape(){
		int			y = 0;
		int			h = 0;

		if (null != lkgDesignerGen && null != lkgDesignerPhy && null != lkgDesignerPhyBlowUp){
			y = (int)(_shapeUnderMouse.y+_shapeUnderMouse.h/2);
			h = (int)_shapeUnderMouse.h;
			lkgDesignerGen.enableScroller(y, h, false);
			lkgDesignerPhyBlowUp.recreateShapes();
		}
	}

	/**
	 * Sets the corresponding stroke to the mouse position
	 */
	private	void	setMousePosition(){
		float[]		style	= {10,5};

		switch (_mouseAreaId){
			case MOUSE_OFF:
				_stroke = null;
				break;
			case MOUSE_ON_EDGE:
				_stroke = new BasicStroke(	2,
											BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_ROUND
											);
				break;
			case MOUSE_ON:
			case MOUSE_ON_GEN:
			case MOUSE_ON_PHY:
			case MOUSE_ON_SCROLLER_GEN:
			case MOUSE_ON_SCROLLER_PHY:
				_stroke = new BasicStroke(	1,
											BasicStroke.CAP_ROUND,
											BasicStroke.JOIN_ROUND,
											10.0f,
											style,
											0
											);
				break;
		}
	}

	/**
	 * Returns the ShapeDesign.
	 * @return the ShapeDesign
	 */
	public	ShapeDesign	getShapeDesign(){
		return _frame;
	}

	/**
	 * Returns if the cell is selected (mouse has clicked on it previously).
	 * @return if the cell is selected
	 */
	public	boolean	getSelected(){
		return _selected;
	}

	/**
	 * Returns the mouse position Id (values can be MOUSE_ON, MOUSE_ON_EDGE,
	 * MOUSE_OFF...)
	 * @return the mouse position Id
	 */
	public	int	getMousePosId(){
		return _mouseAreaId;
	}

	/**
	 * Return the corresponding stroke to the Mouse position. If null, no shape
	 * should be drawn. (mouse off the cell)
	 * @return the corresponding stroke to the Mouse position
	 */
	public	Stroke	getStroke(){
		return _stroke;
	}

	/**
	 * Returns the position of the genetic linkage group designer in pixels.
	 * @return the position of the genetic linkage group designer in pixels
	 */
	public	Point	getLkgDesignerGenPosition(){
		return new Point(	(int)LinkageGroupDesigner.LEFT_MARGIN,
							(int)LinkageGroupDesigner.TOP_MARGIN);
	}

	/**
	 * Returns the position of the physic linkage group designer in pixels.
	 * @return the position of the physic linkage group designer in pixels
	 */
	public	Point	getLkgDesignerPhyPosition(){
		return new Point(	(int)LinkageGroupDesigner.LEFT_MARGIN
						+	(int)getlkgDesignerGenWidth(),
							(int)LinkageGroupDesigner.TOP_MARGIN);
	}

	public	Point	getGOStatisticsPosition(){
		return new Point(	(int)getlkgDesignerGenWidth()
						+	(int)getlkgDesignerPhyWidth()
						+	(int)LinkageGroupDesigner.LEFT_MARGIN,
							(int)LinkageGroupDesigner.TOP_MARGIN);
	}

	/**
	 * Returns the position of the physic BlowUp linkage group designer in
	 * pixels.
	 * @return the position of the physic BlowUplinkage group designer in pixels
	 */
	public	Point	getLkgDesignerPhyBlowUpPosition(){
		return new Point(	(int)getlkgDesignerGenWidth()
						+	(int)getlkgDesignerPhyWidth()
						+	GOStatistics.WIDTH
						+	(int)LinkageGroupDesigner.LEFT_MARGIN,
							(int)LinkageGroupDesigner.TOP_MARGIN);
	}

	/**
	 * Returns the cell's width.
	 * @return the cell's width
	 */
	public	final	double	getWidth(){
		return	getlkgDesignerGenWidth()
			+	getlkgDesignerPhyWidth()
			+	((null != lkgDesignerPhy)?GOStatistics.WIDTH:0)
			+	getlkgDesignerPhyBlowUpWidth();
	}

	/**
	 * Returns the cell's height.
	 * @return the cell's height
	 */
	public	final	double	getHeight(){
		return	getlkgDesignerGenHeight() +
				getlkgDesignerPhyHeight() +
				getlkgDesignerPhyBlowUpHeight();
	}

	/**
	 * Launches the drawing of all linkage group designers.
	 */
	public	void	reDrawShapes(){
		if (null != lkgDesignerGen){
			lkgDesignerGen.recreateShapes();
		}
		if (null != lkgDesignerPhy){
			lkgDesignerPhy.recreateShapes();
		}
		if (null != lkgDesignerPhyBlowUp){
			lkgDesignerPhyBlowUp.recreateShapes();
		}
	}

	/**
	 * Uses the loaded physical data to display the linkage group
	 */
	public	void				initializePhysicalData(){
		final	LinkageGroup	lkg	= lkgDesignerGen.getLinkageGroup();

		try{
			lkgDesignerPhy = new LinkageGroupDesignerPhysical(lkg, lkgDesignerGen.getMetaModel());
			lkgDesignerPhyBlowUp = new LinkageGroupDesignerPhysicalBlowUp(lkg, lkgDesignerGen.getMetaModel());

			// Scrollers init
			lkgDesignerGen.initScroller(null, lkgDesignerPhy.getScroller(), Anchor.CM_TO_PB);
			lkgDesignerPhy.initScroller(lkgDesignerPhyBlowUp, lkgDesignerGen.getScroller(), Anchor.PB_TO_CM);

			findAnchors(lkg);

	//		lkgDesignerGen.enableScroller(10, 20, false);
			lkgDesignerGen.enableScroller(40, 20, false);

			// Shapes recreation
			lkgDesignerGen.recreateShapes();
			lkgDesignerPhy.recreateShapes();
			lkgDesignerPhyBlowUp.recreateShapes();

			_frame = new ShapeDesign(ShapeDesign.NONE, xPos, yPos, getWidth(), getHeight(), Color.GRAY);
		}catch(Exception e){
			System.out.println("Linkage group unknown in the chosen genome version");
		}
	}

	public	void		findAnchors(final LinkageGroup lkg){
		List<Anchor>	anchors = PhysicalManager.instance().getAnchors(lkg);

		_anchorsRemoved = PhysicalManager.instance().getAnchorsRemoved(lkg);
		lkgDesignerGen.setAnchors(anchors);
		lkgDesignerPhy.setAnchors(anchors);
		lkgDesignerPhyBlowUp.setAnchors(anchors);
	}

	/**
	 * Returns an iterator on the shapeDesigns corresponding to grid cell; if
	 * no shape designs are found, the method returns an iterator on an empty
	 * collection. (The iterator return is never null)
	 * @return an iterator on the shapeDesigns corresponding to the grid cell.
	 */
	public	Iterator<ShapeDesign>	iterator(){
		List<ShapeDesign>			shapes	= new ArrayList<ShapeDesign>();
		List<Anchor>				anchors	= null;
		Point						ptGen	= null;
		Point						ptPhy	= null;
		double						xMargin	= lkgDesignerGen.getLeftAreaWidth() + LinkageGroupDesignerGenetic.WIDTH_LINKAGE_GROUP;

		if (null != lkgDesignerGen && null != lkgDesignerPhy &&	null != (anchors = lkgDesignerGen.getAnchors())){
			ptGen = getLkgDesignerGenPosition();
			ptPhy = getLkgDesignerPhyPosition();
			for (Iterator<Anchor> itAnchors = anchors.iterator(); itAnchors.hasNext();) {
				Anchor	anchor = itAnchors.next();
				double	pos_x = ptGen.x + xMargin;
				double	pos_y = ptGen.y + lkgDesignerGen.position_data_to_pxl(anchor.getPosition(Anchor.CM)).getY();

				shapes.add(new ShapeDesign(	ShapeDesign.LINE,
											pos_x,
											pos_y,
											ptPhy.x - pos_x,
											ptPhy.y + lkgDesignerPhy.position_data_to_pxl(anchor.getPosition(Anchor.PB)).getY() - pos_y,
											new Color(0, 0, 255, 120)));
			}
			for (Iterator<Anchor> itAnchors = _anchorsRemoved.iterator(); itAnchors.hasNext();) {
				Anchor	anchor = itAnchors.next();
				double	pos_x = ptGen.x + xMargin;
				double	pos_y = ptGen.y + lkgDesignerGen.position_data_to_pxl(anchor.getPosition(Anchor.CM)).getY();

				shapes.add(new ShapeDesign(	ShapeDesign.LINE,
											pos_x,
											pos_y,
											ptPhy.x - pos_x,
											ptPhy.y + lkgDesignerPhy.position_data_to_pxl(anchor.getPosition(Anchor.PB)).getY() - pos_y,
											new Color(255, 0, 0, 120)));
			}
		}

		return shapes.iterator();
	}

	/**
	 * Returns the linkage group designer width; 0 if lkg designer is null.
	 * @return
	 */
	private double	getlkgDesignerGenWidth(){
		return (null != lkgDesignerGen)?lkgDesignerGen.getWidth():0;
	}

	/**
	 * Returns the linkage group designer width; 0 if lkg designer is null.
	 * @return
	 */
	private double	getlkgDesignerPhyWidth(){
		return (null != lkgDesignerPhy)?lkgDesignerPhy.getWidth():0;
	}

	/**
	 * Returns the linkage group designer width; 0 if lkg designer is null.
	 * @return
	 */
	private double	getlkgDesignerPhyBlowUpWidth(){
		return (null != lkgDesignerPhyBlowUp)?lkgDesignerPhyBlowUp.getWidth():0;
	}

	/**
	 * Returns the linkage group designer width; 0 if lkg designer is null.
	 * @return
	 */
	private double	getlkgDesignerGenHeight(){
		return (null != lkgDesignerGen)?lkgDesignerGen.getHeight():0;
	}

	/**
	 * Returns the linkage group designer width; 0 if lkg designer is null.
	 * @return
	 */
	private double	getlkgDesignerPhyHeight(){
		return (null != lkgDesignerPhy)?lkgDesignerPhy.getHeight():0;
	}

	/**
	 * Returns the linkage group designer width; 0 if lkg designer is null.
	 * @return
	 */
	private double	getlkgDesignerPhyBlowUpHeight(){
		return (null != lkgDesignerPhyBlowUp)?lkgDesignerPhyBlowUp.getHeight():0;
	}

	/**
	 * Returns if the grid cell contains a genome version.
	 * @return If the grid cell contains a genome version.
	 */
	public	boolean	hasGenomeVersionEnabled(){
		return (null != lkgDesignerPhyBlowUp && null != lkgDesignerPhy);
	}

	public					LinkageGroupDesigner	lkgDesignerGen				= null;
	public					LinkageGroupDesigner	lkgDesignerPhy				= null;
	public					LinkageGroupDesigner	lkgDesignerPhyBlowUp		= null;
	public					double					xPos						= 0;
	public					double					yPos						= 0;
	private					ShapeDesign				_frame						= null;
	private					ShapeDesign				_shapeUnderMouse			= null;
	private					boolean					_selected					= false;
	private					int						_mouseAreaId				= MOUSE_OFF;
	private					Stroke					_stroke						= null;
	private					List<Anchor>			_anchorsRemoved				= null;
	private	static	final	int						_EDGE_THICKNESS				= 20;
}
