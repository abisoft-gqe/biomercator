/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 11-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.combomodels;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This model is a combo box model displaying unique traits of the QTLs in the
 * given linkage group.
 * @author sosnowski
 */
public class ComboBoxTraitModel extends ComboBoxCustomModel {
	public	ComboBoxTraitModel(String filePath){
		File	file = new File(filePath);

		_list = new ArrayList<Object>();
		if (file.exists() && file.isFile() && file.canRead()){
			readResFile(file);
		}
	}

	/**
	 * Parses the model file created by ClustQTL MetaQTL's method to get all the
	 * meta traits.
	 * @param filePath The path to the "model" file
	 */
	private	void		readResFile(File file){
		BufferedReader	buf		= null;
		String			line	= null;
		Set<String>		traits	= new HashSet<String>();
		String			sep		= "\t";
		Pattern			pattern	= Pattern.compile("(.+)" + sep
												+ "(.+)" + sep
												+ "(.+)" + sep
												+ "(.+)");
		Matcher			matcher	= null;

		try{
			buf = new BufferedReader(new FileReader(file));
			while (null != (line = buf.readLine())){
				if (!line.startsWith("Criterion")){
					matcher = pattern.matcher(line);
					if (matcher.find()){
						traits.add(matcher.group(3));
					}
				}
			}
			for (Iterator<String> it = traits.iterator(); it.hasNext();) {
				_list.add(it.next());
			}
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
}
