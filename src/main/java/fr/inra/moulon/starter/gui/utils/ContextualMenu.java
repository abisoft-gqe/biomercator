/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 30-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.utils;

import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Workspace;
import fr.inra.moulon.starter.datamodel.entities.file.FileRes;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.gui.Canvas;
import fr.inra.moulon.starter.gui.tree.DataExplorerTreeModel;
import fr.inra.moulon.starter.gui.wizards.WizardMapParamConfig;
import fr.inra.moulon.starter.utils.Session;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.tree.TreePath;


public class ContextualMenu {
	/**
	 *
	 * @param comp
	 * @param projectName
	 * @param mapGene
	 * @param mousePointReal
	 */
	public	static	void	showMapMenu(Component	comp,
										final	Project		project,
										final	MapGene		mapGene,
										final	JTree		explorer,
										final	Point		mousePointReal){
		final	JPopupMenu	popUpMenu	= new JPopupMenu();
		final	String		projectName	= project.getName();
		final	String		mapName		= mapGene.getName();
		JMenuItem			item		= null;

		item = new JMenuItem(_bundle.getString("contextual_parameter_set"));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				popUpMenu.setVisible(false);
				WizardMapParamConfig	wizard = new WizardMapParamConfig(_bundle.getString("contextual_parameter_set"));
				wizard.setParams(projectName, mapGene);
				wizard.show();
			}
		});
		popUpMenu.add(item);

		item = new JMenuItem(_bundle.getString("misc_suppress"));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				popUpMenu.setVisible(false);
				Object[] options = {_bundle.getString("misc_yes"),
									_bundle.getString("misc_no")};
				int n = JOptionPane.showOptionDialog(null,
					_bundle.getString("misc_suppression_confirm_1") + " : " + mapName + " ?",
					_bundle.getString("misc_suppression"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[0]);

				// File suppression if user selected "yes"
				if (0 == n){
					project.remove(mapName);
					FileManager.deleteMap(projectName, mapName);
					explorer.setModel(new DataExplorerTreeModel());
					TreePath treePath = new TreePath(new Object[]{Workspace.instance(), Controller.getProject(projectName)});
					explorer.expandPath(treePath);
					explorer.scrollPathToVisible(treePath);
				}
			}
		});
		popUpMenu.add(item);

		popUpMenu.show(comp, mousePointReal.x, mousePointReal.y);
	}

	public	static	void	showDefaultEditions(	Component	comp,
											final	FileRes		fileRes,
											final	Project		project,
											final	JTree		explorer,
											final	Point		mousePointReal){
		final	JPopupMenu	popUpMenu	= new JPopupMenu();
		JMenuItem			item		= new JMenuItem(_bundle.getString("misc_suppress"));

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				popUpMenu.setVisible(false);
				Object[] options = {_bundle.getString("misc_yes"),
									_bundle.getString("misc_no")};
				int n = JOptionPane.showOptionDialog(null,
					_bundle.getString("misc_suppression_confirm_1") + " : " + fileRes.getName() + " ?",
					_bundle.getString("misc_suppression"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[0]);

				// File suppression if user selected "yes"
				if (0 == n){
					project.removeFile(fileRes);
					explorer.setModel(new DataExplorerTreeModel());
					TreePath treePath = new TreePath(new Object[]{Workspace.instance(), Controller.getProject(project.getName())});
					explorer.expandPath(treePath);
					explorer.scrollPathToVisible(treePath);
				}
			}
		});

		popUpMenu.add(item);

		popUpMenu.show(comp, mousePointReal.x, mousePointReal.y);
	}
	
	/**
	 * Creates and shows a contextual pop up menu for a project node.
	 * @param comp The component calling this popupMenu
	 * @param project the object concerned by the event
	 * @param explorer the tree on which we call the popupMenu
	 * @param mousePointReal position of the mouse
	 */
	public static void showProjectSupp(Component	comp,
											final	Project		project,
											final	JTree		explorer,
											final	Point		mousePointReal) {
		
		final	JPopupMenu	popUpMenu	= new JPopupMenu();
		JMenuItem			item		= new JMenuItem(_bundle.getString("misc_suppress"));

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				popUpMenu.setVisible(false);
				Object[] options = {_bundle.getString("misc_yes"),
									_bundle.getString("misc_no")};
				int n = JOptionPane.showOptionDialog(null,
					_bundle.getString("misc_suppression_confirm_1") + " : " + project.getName() + " ?",
					_bundle.getString("misc_suppression"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[0]);

				// File suppression if user selected "yes"
				if (0 == n){
					project.delete();
					explorer.setModel(new DataExplorerTreeModel());
					//TreePath treePath = new TreePath(new Object[]{Workspace.instance(), Controller.getProject(project.getName())});
					//explorer.expandPath(treePath);
					//explorer.scrollPathToVisible(treePath);
				}
			}
		});

		popUpMenu.add(item);

		popUpMenu.show(comp, mousePointReal.x, mousePointReal.y);
	}
	
	/**
	 * Creates and shows a contextual pop up menu for a genetic designer.
	 * @param canvas The canvas calling this function
	 * @param position The position where to create the pop up menu
	 */
	public	static	void	showOnGenetic(Canvas canvas, Point position){
		final	JPopupMenu	popUpMenu	= new JPopupMenu();

		popUpMenu.add(createMenuItem(canvas, popUpMenu, _bundle.getString("contextual_zoom_in"), Context.GENETIC, Values.ZOOM_IN));
		popUpMenu.add(createMenuItem(canvas, popUpMenu, _bundle.getString("contextual_zoom_out"), Context.GENETIC, Values.ZOOM_OUT));
		popUpMenu.add(createMenuItem(canvas, popUpMenu, _bundle.getString("misc_suppress_from_view"), Context.GENETIC, Values.LKG_REMOVE));
		popUpMenu.add(createMenuItem(canvas, popUpMenu, _bundle.getString("contextual_lkg_reverse"), Context.GENETIC, Values.LKG_REVERSE));

		popUpMenu.show(canvas, position.x, position.y);
	}

	/**
	 * Creates and returns a menu item that launches a calling to the canvas
	 * actions manager with the correct ID.
	 * clicked.
	 * @param canvas The canvas
	 * @param popUpMenu The pop up that will contain this menu item.
	 * @param menuTitle the menu name
	 * @param value The action ID
	 * @return A menu item that calls the canvas for an action when clicked.
	 */
	private	static	JMenuItem	createMenuItem(	final	Canvas		canvas,
												final	JPopupMenu	popUpMenu,
														String		menuTitle,
												final	Context		context,
												final	Values		value){
		JMenuItem	item		= new JMenuItem(menuTitle);

		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				popUpMenu.setVisible(false);
				canvas.contextualMenuManage(context, value);
			}
		});

		return item;
	}

	public	static	void	showOnPhysicBlowUp(final Canvas canvas, Point position){
		final	JPopupMenu	popUpMenu	= new JPopupMenu();
		JMenuItem			item		= new JMenuItem(_bundle.getString("contextual_export_gff"));

		popUpMenu.add(createMenuItem(canvas, popUpMenu, _bundle.getString("contextual_zoom_in"), Context.PHYSIC_BLOW_UP, Values.ZOOM_IN));
		popUpMenu.add(createMenuItem(canvas, popUpMenu, _bundle.getString("contextual_zoom_out"), Context.PHYSIC_BLOW_UP, Values.ZOOM_OUT));

		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				popUpMenu.setVisible(false);
				canvas.contextualMenuManage(Context.PHYSIC_BLOW_UP, Values.GFF_EXPORT);
			}
		});

		popUpMenu.add(item);

		popUpMenu.show(canvas, position.x, position.y);
	}

	public	enum	Values {ZOOM_IN, ZOOM_OUT, LKG_REMOVE, LKG_REVERSE, GFF_EXPORT};
	public	enum	Context {GENETIC, PHYSIC, PHYSIC_BLOW_UP};


	private	static	ResourceBundle	_bundle = (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
