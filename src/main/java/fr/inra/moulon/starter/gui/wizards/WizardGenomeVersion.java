/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.database.Pair;
import fr.inra.moulon.starter.fileTools.tasks.TaskLoadingFile;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.JFileChooserTools;
import fr.inra.moulon.starter.utils.PhysicDataTypes;
import fr.inra.moulon.starter.utils.PhysicDataTypesTools;
import fr.inra.moulon.starter.utils.Session;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.ArrayDeque;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.ResourceBundle;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.JTextField;


public class WizardGenomeVersion extends AbstractDefaultWizard{
	private	final	static	Dimension	WIZARD_MAIN	= new Dimension(550, 250);
	private	final	static	int			NB_CARDS	= 2;

	public WizardGenomeVersion(String path, String dialogTitle){
		super(path, dialogTitle, NB_CARDS);
		_tasks = new ArrayDeque<Task>();
		_ids = new EnumMap<PhysicDataTypes, Integer>(PhysicDataTypes.class);
		super.addDialogSize(1, WIZARD_MAIN);
	}

	@Override
	protected	void	launch(int panelId){
		super.launch(panelId);
		if (1 == panelId){	// The second card has been validated
			//	CREATE GENOME VERSION IN DATABASE
			DatabaseManager.instance().
					insertNewGenomeVersion(_genomeVersionField.getText(),
											_ids.get(PhysicDataTypes.STRUCTURAL_ANNOTATIONS),
											_ids.get(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS),
											_ids.get(PhysicDataTypes.ANCHORS),
											_ids.get(PhysicDataTypes.DBLINKS),
											"");
		}
	}

	/**
	 * Adds the different wizard's steps as different panels.
	 * @param panelFrame The frame JPanel where panels will be placed.
	 */
	@Override
	protected	void	addWizardPanels(JPanel panelFrame){
		JPanel			panelGenomeVersionChoice = new JPanel();
		JPanel			panelGenomeVersionFiles = new JPanel();

		addGenomeVersionChoicePanelOnPanel(panelGenomeVersionChoice);
		panelFrame.add(panelGenomeVersionChoice, "1");

		addGenomeVersionFilesPanelOnPanel(panelGenomeVersionFiles);
		panelFrame.add(panelGenomeVersionFiles, "2");
	}

	/**
	 * THE WIZARD'S FIRST STEP
	 * Adds the genome version choice wizard components onto the given panel.
	 * @param panelFrame The panel where to place the components for the project
	 * choosing process.
	 */
	protected	void		addGenomeVersionChoicePanelOnPanel(JPanel pane){
		GridBagConstraints	c		= new GridBagConstraints();
		JLabel				label	= new JLabel("New genome version");

		_genomeVersionField	= new JTextField("");
		addFieldVerification(_genomeVersionField, FIELD_NOT_NULL);

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 0.5;
		c.insets = new Insets(0, 0, 10, 0);
		pane.add(label, c);

		c.insets = new Insets(0, 0, 0, 0);
		c.weightx = 0.6;
		c.anchor = GridBagConstraints.LINE_START;
		GridBagLayoutUtils.set_gbc(c, 1, 0, 1, 1, GridBagConstraints.HORIZONTAL);
		pane.add(_genomeVersionField, c);
	}

	/**
	 * THE WIZARD'S SECOND STEP
	 * Adds the files browsing onto the given panel.
	 * @param panelFrame The panel where to place the components for the files
	 * browsing process.
	 */
	private	void			addGenomeVersionFilesPanelOnPanel(final JPanel pane){
		GridBagConstraints	c			= new GridBagConstraints();
		JComboBox			combo		= null;
		JLabel				label		= null;
		JProgressBar		progress	= null;
		JSeparator			separator	= null;
		int					iLine		= 0;
		List<Pair>			pairs		= null;
		Pair				pair_noFile	= new Pair(-1, _bundle.getString("misc_none"));
		Pair				pair_browse	= new Pair(0,  _bundle.getString("misc_browse") + "...");

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		label = new JLabel(PhysicDataTypesTools.typeToString(PhysicDataTypes.STRUCTURAL_ANNOTATIONS));
		c.weightx = 1;
		GridBagLayoutUtils.set_gbc(c, 0, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		pairs = DatabaseManager.instance().getStructuralAnnotationPairs();
		pairs.add(0, pair_noFile);
		pairs.add(1, pair_browse);
		combo = new JComboBox(pairs.toArray());
		progress = new JProgressBar(JProgressBar.HORIZONTAL);
		setItemListener(new WizardExploringStructuralAnnotations(this),
						PhysicDataTypes.STRUCTURAL_ANNOTATIONS,
						pane,
						combo,
						progress);
		GridBagLayoutUtils.set_gbc(c, 1, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(combo, c);

		GridBagLayoutUtils.set_gbc(c, 2, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(progress, c);

		separator = new JSeparator();
		c.anchor = GridBagConstraints.CENTER;
		GridBagLayoutUtils.set_gbc(c, 0, ++iLine, 3, 1, GridBagConstraints.BOTH);
		pane.add(separator, c);

		////////////////////////////////////////////////////////////////////////
		++iLine;
		label = new JLabel(PhysicDataTypesTools.typeToString(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS));
		GridBagLayoutUtils.set_gbc(c, 0, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		pairs = DatabaseManager.instance().getFunctionalAnnotationPairs();
		pairs.add(0, pair_noFile);
		pairs.add(1, pair_browse);
		combo = new JComboBox(pairs.toArray());
		progress = new JProgressBar(JProgressBar.HORIZONTAL);
		setItemListener(new WizardExploringFunctionalAnnotations(this),
						PhysicDataTypes.FUNCTIONAL_ANNOTATIONS,
						pane,
						combo,
						progress);
		GridBagLayoutUtils.set_gbc(c, 1, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(combo, c);

		GridBagLayoutUtils.set_gbc(c, 2, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(progress, c);

		separator = new JSeparator();
		c.anchor = GridBagConstraints.CENTER;
		GridBagLayoutUtils.set_gbc(c, 0, ++iLine, 3, 1, GridBagConstraints.BOTH);
		pane.add(separator, c);

		////////////////////////////////////////////////////////////////////////
		label = new JLabel(PhysicDataTypesTools.typeToString(PhysicDataTypes.ANCHORS));
		++iLine;
		GridBagLayoutUtils.set_gbc(c, 0, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		pairs = DatabaseManager.instance().getAnchorsPairs();
		pairs.add(0, pair_noFile);
		pairs.add(1, pair_browse);
		combo = new JComboBox(pairs.toArray());
		progress = new JProgressBar(JProgressBar.HORIZONTAL);
		setItemListener(new WizardExploringAnchors(this),
						PhysicDataTypes.ANCHORS,
						pane,
						combo,
						progress);
		GridBagLayoutUtils.set_gbc(c, 1, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(combo, c);

		GridBagLayoutUtils.set_gbc(c, 2, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(progress, c);

		separator = new JSeparator();
		c.anchor = GridBagConstraints.CENTER;
		GridBagLayoutUtils.set_gbc(c, 0, ++iLine, 3, 1, GridBagConstraints.BOTH);
		pane.add(separator, c);
		
		////////////////////////////////////////////////////////////////////////
		label = new JLabel(PhysicDataTypesTools.typeToString(PhysicDataTypes.DBLINKS));
		++iLine;
		GridBagLayoutUtils.set_gbc(c, 0, iLine, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		pairs = DatabaseManager.instance().getDBLinksPairs();
		pairs.add(0, pair_noFile);
		pairs.add(1, pair_browse);
		combo = new JComboBox(pairs.toArray());
		progress = new JProgressBar(JProgressBar.HORIZONTAL);
		setItemListener(new WizardExploringDBLinks(this),
						PhysicDataTypes.DBLINKS,
						pane,
						combo,
						progress);
		GridBagLayoutUtils.set_gbc(c, 1, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(combo, c);

		GridBagLayoutUtils.set_gbc(c, 2, iLine, 1, 1, GridBagConstraints.NONE);
		pane.add(progress, c);

		separator = new JSeparator();
		c.anchor = GridBagConstraints.CENTER;
		GridBagLayoutUtils.set_gbc(c, 0, ++iLine, 3, 1, GridBagConstraints.BOTH);
		pane.add(separator, c);
	}

	/**
	 * Sets the actionPerformed event to the given 'menuItem'.
	 * @param menuItem The menuItem to set the action.
	 * @param wizard The wizard to call when the menuItem is selected.
	 */
	private	void	setItemListener(final	WizardExploringCSV_Multi	wizardExploringCSV,
									final	PhysicDataTypes		phyDataType,
									final	JPanel				pane,
									final	JComboBox			combo,
									final	JProgressBar		progress){
		combo.addItemListener(new java.awt.event.ItemListener() {
			@Override
            public	void		itemStateChanged(ItemEvent evt) {
				JFileChooser	fileChooser		= null;
				Pair			selectedPair	= (Pair)evt.getItem();

				if (evt.getStateChange() == ItemEvent.SELECTED){
					switch (selectedPair.id){
						case -1:// No file chosen
							break;
						case 0:// Browse files chosen
							fileChooser = JFileChooserTools.createFileChooser(	_bundle.getString("wizard_choose_file"),
																				_bundle.getString("misc_open"),
																				false,
																				null);
							if (fileChooser.showOpenDialog(pane) == JFileChooser.APPROVE_OPTION){
								Session.instance().setValue(Session.CHOOSER_FILE_DIR, fileChooser.getCurrentDirectory());
								File file = fileChooser.getSelectedFile();
								_progressSelected = progress;
								_comboSelected = combo;
								wizardExploringCSV.setFile(file);
								wizardExploringCSV.show();
							}else{
								combo.setSelectedIndex(0);
							}
							break;
						default:// Existing ID chosen
							_ids.put(phyDataType, selectedPair.id);
							progress.setValue(100);
							break;
					}
				}
            }
        });
	}

	/**
	 * Method to call by a task when it reaches the end.
	 * The given parameters allow to set the map for storing the annotations and
	 * anchors identifiers.(inserted into the database)
	 */
	public	void	taskIsDone(PhysicDataTypes physicDataType, int dtbId){
		_ids.put(physicDataType, dtbId);
		executeNextTask();
	}

	/**
	 * Method to add a task to the queue.
	 * @param task The task to add
	 */
	public	void	addTask(TaskLoadingFile task){
		if (null != task){
			_tasks.add(new Task(task, _progressSelected, _comboSelected));
			_progressSelected.setIndeterminate(true);
			_comboSelected.setEnabled(false);
			executeNextTask();
		}
	}

	/**
	 * Checks the current task state and decides to
	 *  - start the task if its state is pending
	 *  - remove the task and start the next one if the state is finished
	 */
	private	void	executeNextTask(){
		Task		taskCur = null;

		if (null != (taskCur = _tasks.peek())){
			switch (taskCur.task.getState()){
				case DONE:
					super.enableNextButton(true);
					super.enableCancelButton(true);
					_tasks.poll();
					taskCur.combo.setEnabled(true);
					executeNextTask();
					break;
				case PENDING:
					super.enableNextButton(false);
					super.enablePrevButton(false);
					super.enableCancelButton(false);
					taskCur.combo.setEnabled(false);
					taskCur.progress.setIndeterminate(false);
					taskCur.task.setBoundedModel(taskCur.progress.getModel());
					taskCur.task.execute();
					break;
				case STARTED:
					break;
			}
		}
	}

	/**
	 * This class allow to store a task with its combo box and progress bar for
	 * interactions.
	 */
	private class Task{
		public	Task(	TaskLoadingFile task,
						JProgressBar	progress,
						JComboBox		combo){
			this.task = task;
			this.progress = progress;
			this.combo = combo;
		}

		public	TaskLoadingFile	task		= null;
		public	JProgressBar	progress	= null;
		public	JComboBox		combo		= null;
	}

	private	JProgressBar					_progressSelected	= null;	// The progress bar associated to the task
	private	JComboBox						_comboSelected		= null;	// The combo box associated to the task
	private	JTextField						_genomeVersionField	= null;
	private	Queue<Task>						_tasks				= null;
	private	Map<PhysicDataTypes, Integer>	_ids				= null;

	private	static	ResourceBundle			_bundle				= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
