/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.utils;

import fr.inra.moulon.starter.utils.Session;
import java.io.File;
import javax.swing.JFileChooser;


public class JFileChooserTools {
//	/**
//	 * Creates an returns a new file chooser.
//	 * @param title The file chooser's title
//	 * @param buttonText The text that should appear on the validation button
//	 * @param multiSelection True if the user can select more than one file
//	 * @return The new file chooser.
//	 */
//	public static	JFileChooser	createFileChooser(	String	title,
//														String	buttonText,
//														Boolean	multiSelection){
//		return createFileChooser(title, buttonText, multiSelection, null, FileType.ALL_FILES);
//	}

	/**
	 * Creates an returns a new file chooser.
	 * @param title The file chooser's title
	 * @param buttonText The text that should appear on the validation button
	 * @param multiSelection True if the user can select more than one file
	 * @param defaultFileName The default file's name
	 * @param extensionFilter If not null, a filter on the files' extension
	 * @return The new file chooser.
	 */
	public static	JFileChooser	createFileChooser(	String		title,
														String		buttonText,
														Boolean		multiSelection,
														String		defaultFileName){
		JFileChooser				fileChooser = new JFileChooser((File)Session.instance().getValue(Session.CHOOSER_FILE_DIR));

		fileChooser.setDialogTitle(title);
		fileChooser.setMultiSelectionEnabled(multiSelection);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if (null != defaultFileName){
			fileChooser.setSelectedFile(new File(defaultFileName));
		}
		if (null != buttonText){
			fileChooser.setApproveButtonText(buttonText);
		}

		return fileChooser;
	}
}
