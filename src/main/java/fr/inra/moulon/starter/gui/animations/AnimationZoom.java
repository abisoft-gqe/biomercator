/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 02-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.animations;

import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesigner;
import fr.inra.moulon.starter.gui.Canvas;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;


public class AnimationZoom extends Animation{
	public	AnimationZoom(Canvas canvas){
		_canvas = canvas;
	}

	public	void		start(	int						delay,
								int						nbOcc,
								long					step,
								Point					pointOrigin,
								LinkageGroupDesigner	lkgDesigner,
								LinkageGroupDesigner	lkgDesignerSon){
		ActionListener	actionListener = null;

		_nbOcc			= nbOcc;
		_step			= step;
		_pointOrigin	= pointOrigin;
		_lkgDesigner	= lkgDesigner;
		_lkgDesignerSon	= lkgDesignerSon;

		actionListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (_nbOcc-- > 0){
					if (null == _lkgDesignerSon){
						if (_lkgDesigner.zoom(_pointOrigin, _step)){
							_lkgDesigner.recreateShapes();
							_canvas.repaint(Canvas.REARRANGE_GRID);
						}
					}else if (_lkgDesigner.zoomSon(_pointOrigin, _step)){
						_lkgDesignerSon.recreateShapes();
						_canvas.repaint(Canvas.REARRANGE_GRID);
					}
				}else{
					_timer.stop();
				}
			}
		};
		_timer = new Timer(delay, actionListener);
		_timer.start();
	}

	private	Canvas					_canvas			= null;
	private	int						_nbOcc			= 0;
	private	long					_step			= 0L;
	private	Point					_pointOrigin	= null;
	private	LinkageGroupDesigner	_lkgDesigner	= null;
	private	LinkageGroupDesigner	_lkgDesignerSon	= null;
}
