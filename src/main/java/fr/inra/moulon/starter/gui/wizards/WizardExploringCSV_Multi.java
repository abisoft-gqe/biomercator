/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 05-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.fileTools.tasks.TaskLoadingFile;
import java.awt.Dimension;
import java.util.List;
import javax.swing.JPanel;


public abstract class WizardExploringCSV_Multi extends WizardExploringCSV{
	private	final	static	Dimension	WIZARD_MAIN	= new Dimension(700, 400);

	public WizardExploringCSV_Multi(	String				dialogTitle,
										WizardGenomeVersion	wizardGenomeVersion){
		super(dialogTitle);
		_wizardGenomeVersion = wizardGenomeVersion;
		super.addDialogSize(0, WIZARD_MAIN);
	}

	/**
	 * This methods must return the task to launch when the csv file columns
	 * have been identified.
	 * @return The created task
	 */
	protected	abstract	TaskLoadingFile	createTask(List<Field> fields);

	@Override
	protected	void	launch(int panelId){
		if (0 == panelId){
			_task = createTask(_columnFields);
			_dialog.setVisible(false);
			_wizardGenomeVersion.addTask(_task);
		}
	}

	/**
	 * Adds the different wizard's steps as different panels.
	 * @param panelFrame The frame JPanel where panels will be placed.
	 */
	@Override
	protected	void	addWizardPanels(JPanel panelFrame){
		JPanel			panelColumnIdentification = new JPanel();

		super.addFilesListLoadingPanelOnPanel(panelColumnIdentification);
		panelFrame.add(panelColumnIdentification, "1");
	}

	private		TaskLoadingFile			_task					= null;
	protected	WizardGenomeVersion		_wizardGenomeVersion	= null;
}
