/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 4, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui;

import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.database.Pair;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.datamodel.entities.Workspace;
import fr.inra.moulon.starter.datamodel.entities.file.FileRes;
import fr.inra.moulon.starter.datamodel.gontology.GOTerm;
import fr.inra.moulon.starter.datamodel.gontology.GOntologies;
import fr.inra.moulon.starter.datamodel.gontology.GOntology;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.config.ConfigWriter;
import fr.inra.moulon.starter.graphic.charts.Charts;
import fr.inra.moulon.starter.graphic.designers.GOStatistics;
import fr.inra.moulon.starter.graphic.designers.GOStatisticsManager;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesigner;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesignerGenetic;
import fr.inra.moulon.starter.graphic.designers.PhysicalManager;
import fr.inra.moulon.starter.graphic.utils.ElementColorManager;
import fr.inra.moulon.starter.graphic.utils.MarkerColorManager;
import fr.inra.moulon.starter.graphic.utils.QTLColorManager;
import fr.inra.moulon.starter.gui.dragNdrop.DragTransferHandler;
import fr.inra.moulon.starter.gui.dragNdrop.DropTransferHandlerTabbedCanvas;
import fr.inra.moulon.starter.gui.tree.DataExplorerTreeModel;
import fr.inra.moulon.starter.gui.tree.TreeCellIconRenderer;
import fr.inra.moulon.starter.gui.utils.ContextualMenu;
import fr.inra.moulon.starter.gui.utils.comboboxWrapper.ComboBoxRendererOntologyWrapper;
import fr.inra.moulon.starter.gui.utils.comboboxWrapper.ComboBoxRendererStatisticsWrapper;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.gui.utils.JTabbedPaneCanvas;
import fr.inra.moulon.starter.gui.utils.JTableButtonMouseListener;
import fr.inra.moulon.starter.gui.utils.JTableButtonRenderer;
import fr.inra.moulon.starter.gui.utils.Sizes;
import fr.inra.moulon.starter.gui.wizards.AbstractWizard;
import fr.inra.moulon.starter.gui.wizards.WizardAbout;
import fr.inra.moulon.starter.gui.wizards.WizardExportToImages;
import fr.inra.moulon.starter.gui.wizards.WizardGeneticFilesLoader;
import fr.inra.moulon.starter.gui.wizards.WizardGenomeVersion;
import fr.inra.moulon.starter.gui.wizards.WizardGenomeVersionManagement;
import fr.inra.moulon.starter.gui.wizards.WizardSaveMaps;
import fr.inra.moulon.starter.gui.wizards.Wizard_Analysis;
import fr.inra.moulon.starter.utils.FractionString;
import fr.inra.moulon.starter.utils.HTMLTools;
import fr.inra.moulon.starter.utils.NumericalUtilities;
import fr.inra.moulon.starter.utils.PhysicDataTypes;
import fr.inra.moulon.starter.utils.Session;
import fr.inra.moulon.starter_plugins.wizards.Wizard_BioM_MetaAnalysis;
import fr.inra.moulon.starter_plugins.wizards.Wizard_BioM_QTLProjection;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_Cons;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_InfoMap;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_MMapView;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_QTLClust;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_QTLClustInfo;
import fr.inra.moulon.starter_plugins.wizards.Wizard_MetaQTL_QtlProj;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.tree.TreePath;



public	class MainFrame {
	/**
	 * Creates a new GUI
	 */
	public	static void	createAndShowGUI() {
		JFrame			frame	= new JFrame("BioMercator V" + Session.instance().getValue(Session.VERSION));

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(Sizes.MAIN_FRAME);

		addComponentsToPane(frame.getContentPane());
		addMenuBar(frame);

        //Display the window.
        frame.pack();
        frame.setVisible(true);

        frame.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {}

			@Override
			public void windowClosing(WindowEvent e) {
				FileManager.cleanEmptyAndTmpDirectories();
			}
			
			@Override
			public void windowClosed(WindowEvent e) {}
			@Override
			public void windowIconified(WindowEvent e) {}
			@Override
			public void windowDeiconified(WindowEvent e) {}
			@Override
			public void windowActivated(WindowEvent e) {}
			@Override
			public void windowDeactivated(WindowEvent e) {}
        	
        });
		// Set ths SplitPanes divider location
		_splitPaneMain.setDividerLocation(0.2);
		_splitPaneCenter.setDividerLocation(0.8);
		_splitPaneLeft.setDividerLocation(0.5);
	}

	/**
	 * Refreshes the tree explorer. (after new maps are added for instance)
	 */
	public	static	void	refreshExplorer(){
		_explorer.setModel(new DataExplorerTreeModel());
	}

	/**
	 * Adds the menu bar to the given JFrame.
	 * @param frame the JFrame where to add the menu bar.
	 */
	private	static	void	addMenuBar(final JFrame frame){
		Wizard_Analysis		wizard		= null;
		JMenuBar			menuBar		= new JMenuBar();
		JMenu				menu		= null;
		JMenu				subMenu		= null;
		JMenuItem			menuItem	= null;
		JButton				button		= null;
		JCheckBoxMenuItem	cbMenuItem	= null;

		frame.setJMenuBar(menuBar);

		//********************************************************************//
		// File menu
		menu = new JMenu(_bundle.getString("menu_file"));
		menu.setMnemonic(KeyEvent.VK_F);
		
		// Genetic files load
		subMenu = new JMenu(_bundle.getString("menu_file_wizard_genetic"));
		//	Genetic load automatic
		menuItem = new JMenuItem(_bundle.getString("menu_file_wizard_genetic_auto"));
		setActionActionPerformed(menuItem, new WizardGeneticFilesLoader("", _bundle.getString("menu_file_wizard_genetic_auto")));
		subMenu.add(menuItem);
		menu.add(subMenu);

		menuItem = new JMenuItem(_bundle.getString("menu_file_wizard_genome"));
		menu.add(menuItem);
		setActionActionPerformed(menuItem, new WizardGenomeVersion("", _bundle.getString("menu_file_wizard_genome")));

		// save as
		menuItem = new JMenuItem(_bundle.getString("menu_file_save_as"));
		menu.add(menuItem);
		setActionActionPerformed(menuItem, new WizardSaveMaps("", _bundle.getString("menu_file_save_as")));
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				KeyEvent.CTRL_DOWN_MASK));

		menuItem = new JMenuItem(_bundle.getString("menu_file_export"));
		menu.add(menuItem);
		setActionActionPerformed(menuItem, new WizardExportToImages("", _bundle.getString("menu_file_export"), _panelsDrawing));

		// quit
		menuItem = new JMenuItem(_bundle.getString("menu_file_quit"));
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FileManager.cleanEmptyAndTmpDirectories();
				System.exit(0);
			}
		});
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				KeyEvent.CTRL_DOWN_MASK));
		menu.add(menuItem);
		
		menuBar.add(menu);

		//********************************************************************//
		// Analysis menu
		menu = new JMenu(_bundle.getString("menu_analysis"));

		// Analysis Statistics
		subMenu = new JMenu(_bundle.getString("menu_analysis_statistics"));

		wizard = new Wizard_MetaQTL_InfoMap("", "InfoMap");
		menuItem = new JMenuItem(wizard.getAnalysisName());
		subMenu.add(menuItem);
		setActionActionPerformed(menuItem, wizard);

		wizard = new Wizard_MetaQTL_MMapView("", "MetaQTL_MMapView");
		menuItem = new JMenuItem(wizard.getAnalysisName());
		subMenu.add(menuItem);
		setActionActionPerformed(menuItem, wizard);

		menu.add(subMenu);

		// Analysis Compilations
		subMenu = new JMenu(_bundle.getString("menu_analysis_compilation"));

		wizard = new Wizard_BioM_QTLProjection("", "Bio_Proj");
		menuItem = new JMenuItem(wizard.getAnalysisName());
		subMenu.add(menuItem);
		setActionActionPerformed(menuItem, wizard);

		wizard = new Wizard_MetaQTL_Cons("", "MetaQTL_Cons");
		menuItem = new JMenuItem(wizard.getAnalysisName());
		subMenu.add(menuItem);
		setActionActionPerformed(menuItem, wizard);

		wizard = new Wizard_MetaQTL_QtlProj("", "MetaQTL_QtlProj");
		menuItem = new JMenuItem(wizard.getAnalysisName());
		subMenu.add(menuItem);
		setActionActionPerformed(menuItem, wizard);

		menu.add(subMenu);

		// Analysis Meta-analyses
		subMenu = new JMenu(_bundle.getString("menu_analysis_metaanalysis"));

		wizard = new Wizard_BioM_MetaAnalysis("", "Bio_Meta_Analysis");
		menuItem = new JMenuItem(wizard.getAnalysisName());
		subMenu.add(menuItem);
		setActionActionPerformed(menuItem, wizard);

		wizard = new Wizard_MetaQTL_QTLClust("", "MetaQTL_QTLClust");
		menuItem = new JMenuItem(wizard.getAnalysisName());
		subMenu.add(menuItem);
		setActionActionPerformed(menuItem, wizard);

		wizard = new Wizard_MetaQTL_QTLClustInfo("", "MetaQTL_QTLClustInfo");
		menuItem = new JMenuItem(wizard.getAnalysisName());
		subMenu.add(menuItem);
		setActionActionPerformed(menuItem, wizard);
		
		menu.add(subMenu);

		menuBar.add(menu);

		//********************************************************************//
		// View menu
		menu = new JMenu(_bundle.getString("menu_view"));
		//  GO tab show
		cbMenuItem = new JCheckBoxMenuItem(_bundle.getString("menu_view_show_go_tab"), false);
		_chartPanel.setVisible(false);
		cbMenuItem.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				boolean	selected = e.getStateChange() == ItemEvent.SELECTED;
				_chartPanel.setVisible(selected);
				if (selected){
					_splitPaneCenter.setDividerLocation(0.7);
				}
			}
		});
		menu.add(cbMenuItem);

		menuBar.add(menu);

		//********************************************************************//
		// Help menu
		menu = new JMenu(_bundle.getString("menu_help"));
		menuItem = new JMenuItem(_bundle.getString("menu_help_about"));
		menu.add(menuItem);
		setActionActionPerformed(menuItem, new WizardAbout());

		subMenu = new JMenu(_bundle.getString("menu_help_language"));
		menuItem = new JMenuItem(_bundle.getString("menu_help_language_fr"));
		menuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Session.instance().setValue(Session.RESSOURCE_BUNDLE, ResourceBundle.getBundle("bundle/MessagesBundle", new Locale("fr", "FR")));
				ConfigWriter.writeConfigFile(FileManager.CONFIG_FILE_LOCAL);
				JOptionPane.showMessageDialog(frame, "Relancez le logiciel changer de langue.");
            }
        });
		subMenu.add(menuItem);

		menuItem = new JMenuItem(_bundle.getString("menu_help_language_en"));
		menuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Session.instance().setValue(Session.RESSOURCE_BUNDLE, ResourceBundle.getBundle("bundle/MessagesBundle", new Locale("en", "US")));
				ConfigWriter.writeConfigFile(FileManager.CONFIG_FILE_LOCAL);
				JOptionPane.showMessageDialog(frame, "You need to reboot to apply the language change.");
            }
        });
		subMenu.add(menuItem);

		menu.add(subMenu);

		menuItem = new JMenuItem(_bundle.getString("menu_help_doc"));
		menuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				
            }
        });

		menuBar.add(menu);

		button = new JButton(_bundle.getString("misc_clean"));
		button.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				_panelsDrawing.getSelectedCanvas().cleanView();
				if (GOStatisticsManager.instance().hasSelected()){
					GOStatisticsManager.instance().getSelected().clear();
				}
			}
		});
		menuBar.add(button);

		_progress = new JProgressBar();
		menuBar.add(_progress);
	}

	/**
	 * Sets the actionPerformed event to the given 'menuItem' so it calls the
	 * given wizard when selected.
	 * @param menuItem The menuItem to set the action.
	 * @param wizard The wizard to call when the menuItem is selected.
	 */
	private	static	void	setActionActionPerformed(JMenuItem menuItem, final AbstractWizard wizard){
		menuItem.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				TreePath	treePath = null;
				String		projectName = null;

				wizard.show();
				_explorer.setModel(new DataExplorerTreeModel());
				if (null != (projectName = wizard.getProjectName())){
					treePath = new TreePath(new Object[]{Workspace.instance(), Controller.getProject(projectName)});
					_explorer.expandPath(treePath);
					_explorer.scrollPathToVisible(treePath);
				}
				fillComboGenomeVersion();
			}
		});
	}

	/**
	 * Creates a canvas and insert it into a new tab.
	 * @return The created canvas.
	 */
	public	static	Canvas	createCanvas(){
		Canvas				canvas	= new Canvas();
		Dimension			dim		= null;

		addMouseListenerToCanvas(canvas);
		canvas.setBackground(new java.awt.Color(255, 255, 255));
		dim = canvas.redraw();
		canvas.setPreferredSize(dim);
		canvas.revalidate();
		_panelsDrawing.addTab(canvas);

		return canvas;
	}

	private	static	void	setICFields(final Canvas canvas){
		double[]			positions = canvas.getGeneticScrollerPosition();

		if (null != positions){
			_ciPositions[0].setText(String.valueOf(NumericalUtilities.doubleToStr(positions[0])));
			_ciPositions[1].setText(String.valueOf(NumericalUtilities.doubleToStr(positions[1])));
		}
	}

	private	static	void	addMouseListenerToCanvas(final Canvas canvas){
		canvas.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
            public	void	mouseExited(MouseEvent evt) {
				Boolean		block = (Boolean)Session.instance().getValue(Session.LOADING_BLOCKING);

				if (!block){
					canvas.mouseExitedCanvas(evt);
				}
            }

			@Override
            public void mousePressed(MouseEvent evt) {
				Boolean		block = (Boolean)Session.instance().getValue(Session.LOADING_BLOCKING);

				if (!block){
					canvas.mousePressedCanvas(evt);
				}
            }
			@Override
            public	void	mouseReleased(MouseEvent evt) {
				canvas.mouseReleasedCanvas(evt);
				setICFields(canvas);
            }
        });

		canvas.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
			@Override
            public void mouseMoved(MouseEvent evt) {
				Boolean		block = (Boolean)Session.instance().getValue(Session.LOADING_BLOCKING);

				canvas.hidePopup();
				if (!block){
					canvas.mouseMovedCanvas(evt);
				}
            }
			@Override
			public void mouseDragged(java.awt.event.MouseEvent evt) {
				Boolean		block = (Boolean)Session.instance().getValue(Session.LOADING_BLOCKING);

				if (!block){
					canvas.hidePopup();
					canvas.mouseDraggedCanvas(evt);
					setICFields(canvas);
				}
			}
		});

		canvas.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
			@Override
			public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
				Boolean		block = (Boolean)Session.instance().getValue(Session.LOADING_BLOCKING);

				if (!block){
					canvas.hidePopup();
					canvas.mouseWheelMovedCanvas(evt);
					setICFields(canvas);
				}
			}
		});
	}

	/**
	 * Creates and add to the given Container all components belonging to the
	 * main GUI.
	 * @param pane the container where to add the components.
	 */
	private	static	void	addComponentsToPane(Container pane) {
		JTabbedPane			options				= new JTabbedPane();
		JPanel				generalOpts			= new JPanel();
		JPanel				expertOpts			= new JPanel();
		JPanel				genomeVersionOpts	= new JPanel();
		JPanel				locusOpts			= new JPanel();
		JPanel				qtlOpts				= new JPanel();
		
		//	PANEL ON WHICH ELEMENTS ARE DRAWN
		_panelsDrawing = new JTabbedPaneCanvas();
		createCanvas();
		_panelsDrawing.setTransferHandler(new DropTransferHandlerTabbedCanvas(TransferHandler.COPY));
		_panelsDrawing.addComponentListener(new java.awt.event.ComponentAdapter() {

			@Override
            public	void	componentResized(java.awt.event.ComponentEvent evt) {
				_panelsDrawing.getSelectedCanvas().resetWidthMax();
				redraw();
            }
        });

		//	CHARTS PANEL
		_chartPanel = new JPanel();
		addComponentsToChartPane(_chartPanel);

		//	TREE PROJECTS EXPLORER
		_explorer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		_explorer.setDragEnabled(true);
		_explorer.setTransferHandler(new DragTransferHandler());
		_explorer.setModel(new DataExplorerTreeModel());
		_explorer.setCellRenderer(new TreeCellIconRenderer());
		_explorer.addMouseListener(new MouseListener() {

			@Override
			public	void	mouseClicked(MouseEvent e) {
				TreePath	path	= null;
				Project		project	= null;
				MapGene		map		= null;
				FileRes		fileRes	= null;

				if (e.getButton() == MouseEvent.BUTTON3){
					path = _explorer.getPathForLocation(e.getX(), e.getY());
					try{
						project = (Project)path.getPathComponent(1);
						map = (MapGene)path.getLastPathComponent();
						ContextualMenu.showMapMenu(_explorer, project, map, _explorer, e.getPoint());
					} catch(Exception ex){
						System.out.println("Not a MapGene node ...");
					}
					try{
						project = (Project)path.getPathComponent(1);
						fileRes = (FileRes)path.getLastPathComponent();
						ContextualMenu.showDefaultEditions(_explorer, fileRes, project, _explorer, e.getPoint());
					} catch(Exception ex){
						System.out.println("Not a FileRes node ...");
					}
					try {
						project = (Project)path.getLastPathComponent();
						ContextualMenu.showProjectSupp(_explorer, project, _explorer, e.getPoint());
					} catch(Exception ex){
						System.out.println("Not a Project node ...");
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});

		//	SETTING ELEMENTS INTO SPLITPANES
		_splitPaneMain.setLeftComponent(_splitPaneLeft);
		_splitPaneMain.setRightComponent(_splitPaneCenter);
		_splitPaneLeft.setRightComponent(options);
		_splitPaneLeft.setLeftComponent(new JScrollPane(_explorer));
		_splitPaneCenter.setLeftComponent(_panelsDrawing);
		_splitPaneCenter.setRightComponent(_chartPanel);
		_chartPanel.setVisible(true);

		//	GIVEN PANEL SETTINGS
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		pane.add(_splitPaneMain, BorderLayout.CENTER);

		//	OPTIONS TABBED PANE
		addComponentsToGeneralPane(generalOpts);
		addComponentsToExpertTabPane(expertOpts);
		addComponentsToLocusTabPane(locusOpts);
		addComponentsToQtlTabPane(qtlOpts);
		addComponentsToGenomeVersionTabPane(genomeVersionOpts);
		options.addTab(_bundle.getString("tab_general"), generalOpts);
		options.addTab(_bundle.getString("tab_expert"), expertOpts);
		options.addTab(_bundle.getString("tab_locus"), locusOpts);
		options.addTab(_bundle.getString("tab_qtl"), qtlOpts);
		options.addTab(_bundle.getString("tab_genome_version"), genomeVersionOpts);
	}

	private	static	JPanel	_chartPanel		= null;

	private	static	void	addComponentsToChartPane(Container pane){
		JTabbedPane			tabs		= new JTabbedPane();
		JSplitPane			tabGO		= new JSplitPane();
		JPanel				tabGOTerms	= new JPanel();
		JPanel				tabGOChart	= new JPanel();
		JPanel				tabAnalysis	= new JPanel();

		pane.setLayout(new GridLayout(1, 1));
		addComponentsToTabGOTerms(tabGOTerms);
		addComponentsToTabGOChart(tabGOChart);
		tabGO.setLeftComponent(tabGOTerms);
		tabGO.setRightComponent(tabGOChart);
		tabGO.setDividerLocation(0.4);
		addComponentsToTabAnalysis(tabAnalysis);
		tabs.addTab(_bundle.getString("go_tab_goterms_goterms"), tabGO);
		tabs.addTab(_bundle.getString("go_tab_goterms_analysis"), tabAnalysis);
		
		pane.add(tabs);
	}

	/**
	 * Fills the given pane with the general tab components.
	 * @param pane The pane to fill
	 */
	private	static	void	addComponentsToTabGOTerms(Container pane){
		GridBagConstraints	c				= new GridBagConstraints();
		JButton				buttonFind		= null;
		JButton				buttonClear		= null;
		JLabel				label			= null;
		int					iRow			= 0;
		int					iCol			= 0;

		setGridBagLayout(pane);

		c.weightx = 0.2;
		GridBagLayoutUtils.set_gbc(c, iCol, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		pane.add(_goTerm, c);

		buttonFind = new JButton(_bundle.getString("go_tab_goterms_find"));
		c.weightx = 0.2;
		GridBagLayoutUtils.set_gbc(c, iCol+1, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		pane.add(buttonFind, c);

		buttonClear = new JButton(_bundle.getString("go_tab_goterms_clear"));
		c.weightx = 0.2;
		GridBagLayoutUtils.set_gbc(c, iCol+2, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
		pane.add(buttonClear, c);

		c.weightx = 0;
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_combosGO[0], c);
		fillComboOntologies(_combosGO[0]);

		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
		pane.add(_combosGO[1], c);
		fillComboStats(_combosGO[1]);

		label = new JLabel();
		GOStatisticsManager.instance().addLabel(GOStatistics.INFO_GOTERM_ID, label);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		label = new JLabel();
		GOStatisticsManager.instance().addLabel(GOStatistics.INFO_GOTERM_NAME, label);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		label = new JLabel();
		GOStatisticsManager.instance().addLabel(GOStatistics.INFO_CHART_LEVEL, label);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		label = new JLabel();
		GOStatisticsManager.instance().addLabel(GOStatistics.INFO_GENES_NB, label);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		//	Vertically align all above components
		addDesignSep(pane, iRow, 3);


		buttonFind.addActionListener(new ActionListener() {
			@Override
			public	void	actionPerformed(ActionEvent e) {
				GOTerm		goTerm = GOntologies.instance().getTerm(_goTerm.getText());

				if (null != goTerm){
					_combosGO[0].setSelectedItem(goTerm.getNamespace());
					Charts.instance().goTo(Charts.STATS_LOCAL, goTerm);
					GOStatisticsManager.instance().setInfos(goTerm.getID());
				}
			}
		});

		buttonClear.addActionListener(new ActionListener() {
			@Override
			public	void	actionPerformed(ActionEvent e) {
				
			}
		});
	}


	/**
	 * Fills the given pane with the general tab components.
	 * @param pane The pane to fill
	 */
	private	static	void	addComponentsToTabGOChart(JPanel pane){
		Integer				ontologyID		= (Integer)_combosGO[0].getSelectedItem();
		Integer				statisticsID	= (Integer)_combosGO[1].getSelectedItem();

		pane.setLayout(new GridLayout(1, 1));
		GOStatisticsManager.instance().addPanel(pane);
		Charts.instance().setSelectedOntology(ontologyID);
		Charts.instance().setSelectedStats(statisticsID);
		GOStatisticsManager.instance().setChartPanel(Charts.instance().getChartPanel());
	}

	 private enum GoBias {REGION, TRAIT};

	 private enum GoReference {CHR, GENOME, TRAIT};

	/**
	 * Adds an item listener to the given JCheckBox and sets the checkbox's
	 * selection as the given field session ID value.
	 * @param checkbox The JCheckBox
	 * @param sessionFieldID The Session field identifier (such as
	 *  Section.LOCUS_DISPLAY)
	 */
	private	static	void	setRadioGoListener(JRadioButton radio, final Integer id, final Enum goEnum){
		radio.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					Session.instance().setValue(id, goEnum);
					redraw();
					try{
						GoBias p = (GoBias)goEnum;
						goBiasEnabling(p);
					}catch (Exception ex){
						GoReference a = (GoReference)goEnum;
						goReferencesEnabling(a);
					}
				}
			}
		});
	}

	/**
	 * Method for main frame's fields updating.
	 */
	public	static	void	updateFields(){
		fillComboTraits(_comboTraits);
		fillComboTraits(_goReferenceTraitsCombo);
	}

	/**
	 * Adds list of traits to the given combo box.
	 * @param combo The combo box.
	 */
	private	static	void		fillComboTraits(JComboBox combo){
		DefaultComboBoxModel	model	= (DefaultComboBoxModel)combo.getModel();
		VisitorEltGet<Qtl>		vQtls	= new VisitorEltGet<Qtl>(Qtl.class);
		Canvas					canvas	= _panelsDrawing.getSelectedCanvas();
		LinkageGroupDesigner	lkgDes	= null;
		Set<String>				traits	= new HashSet<String>();

		if (null != canvas && null != (lkgDes = canvas.getLkgDesignerGenetic())){
			lkgDes.getLinkageGroup().getBelongingMap().accept(vQtls);
			for (int i = 0; i < vQtls.getList().size(); i++) {
				Qtl qtl = (Qtl)vQtls.getList().get(i);

				traits.add(qtl.getTrait());
			}
		}
		
		model.removeAllElements();
		for (Iterator<String> it = traits.iterator(); it.hasNext();) {
			model.addElement(it.next());
		}
		if (traits.size() > 0){
			combo.setSelectedIndex(0);
		}
	}

	private	static	void	goBiasEnabling(GoBias goBias){
		switch (goBias){
			case REGION:
				_ciPositions[0].setEnabled(true);
				_ciPositions[1].setEnabled(true);
				_comboTraits.setEnabled(false);
				_goReferenceRadios.get(2).setEnabled(true);
				break;
			case TRAIT:
				_ciPositions[0].setEnabled(false);
				_ciPositions[1].setEnabled(false);
				_comboTraits.setEnabled(true);
				if (_goReferenceRadios.get(2).isSelected()){
					_goReferenceRadios.get(0).setSelected(true);
				}
				_goReferenceRadios.get(2).setEnabled(false);
				_goReferenceTraitsCombo.setEnabled(false);
				break;
		}
	}

	private	static	void	goReferencesEnabling(GoReference reference){
		switch (reference){
			case CHR:
				_goReferenceTraitsCombo.setEnabled(false);
				break;
			case GENOME:
				_goReferenceTraitsCombo.setEnabled(false);
				break;
			case TRAIT:
				_goReferenceTraitsCombo.setEnabled(true);
				break;
		}
	}

	/**
	 * Fills the given pane with the general tab components.
	 * @param pane The pane to fill
	 */
	private	static	void		addComponentsToTabAnalysis(Container pane){
		GridBagConstraints		c				= new GridBagConstraints();
		JSeparator				sep				= null;
		JButton					buttonAnalyse	= null;
		int						iRow			= 0;
		int						iCol			= 0;
		final	JTable			tableResults	= new JTable();
		final	JProgressBar	progress		= new JProgressBar();
		JRadioButton			radio			= null;
		ButtonGroup				radioGrpBias	= new ButtonGroup();
		ButtonGroup				radioGrpGoRef	= new ButtonGroup();

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.weightx = 0.2;
		pane.add(new JLabel("<html><b>" + _bundle.getString("tab_genome_version_go_bias") + "</b>"), c);

		radio = new JRadioButton(_bundle.getString("tab_genome_version_go_bias_region"));
		radio.setSelected(true);
		Session.instance().setValue(Session.GO_ANALYSIS_BIAS, GoBias.REGION);
		setRadioGoListener(radio, Session.GO_ANALYSIS_BIAS, GoBias.REGION);
		c.weightx = 0;
		radioGrpBias.add(radio);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(5, 10, 0, 0);
		pane.add(radio, c);

		GridBagLayoutUtils.set_gbc(c, iCol, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(5, 20, 0, 0);
		pane.add(new JLabel(_bundle.getString("go_tab_goterms_from")), c);

		GridBagLayoutUtils.set_gbc(c, iCol+1, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(0, 0, 0, 0);
		pane.add(_ciPositions[0], c);

		GridBagLayoutUtils.set_gbc(c, iCol, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(5, 20, 0, 0);
		pane.add(new JLabel(_bundle.getString("go_tab_goterms_to")), c);

		GridBagLayoutUtils.set_gbc(c, iCol+1, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(0, 0, 0, 0);
		pane.add(_ciPositions[1], c);

		radio = new JRadioButton(_bundle.getString("tab_genome_version_go_bias_trait"));
		setRadioGoListener(radio, Session.GO_ANALYSIS_BIAS, GoBias.TRAIT);
		radioGrpBias.add(radio);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(5, 10, 0, 0);
		pane.add(radio, c);

		_comboTraits.setEnabled(false);
		fillComboTraits(_comboTraits);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(5, 20, 0, 0);
		pane.add(_comboTraits, c);

		iCol = 2;
		iRow = 0;

		GridBagLayoutUtils.set_gbc(c, iCol++, iRow, 1, 6, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(0, 5, 0, 5);
		pane.add(new JSeparator(JSeparator.VERTICAL), c);

		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(0, 0, 0, 0);
		c.weightx = 0.1;
		pane.add(new JLabel("<html><b>" + _bundle.getString("tab_genome_version_go_ref") + "</b>"), c);

		//	Vertically align all above components
		addDesignSep(pane, 7, 3);

		buttonAnalyse = new JButton(_bundle.getString("go_tab_goterms_analyse"));
		GridBagLayoutUtils.set_gbc(c, 0, 7, 4, 1, GridBagConstraints.HORIZONTAL);
		c.weightx = 0;
		pane.add(buttonAnalyse, c);

		c.insets = new Insets(5, 10, 0, 0);

		radio = new JRadioButton(_bundle.getString("tab_genome_version_go_ref_chr"));
		_goReferenceRadios.add(radio);
		radio.setSelected(true);
		Session.instance().setValue(Session.GO_ANALYSIS_REFERENCE, GoReference.CHR);
		setRadioGoListener(radio, Session.GO_ANALYSIS_REFERENCE, GoReference.CHR);
		radioGrpGoRef.add(radio);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		pane.add(radio, c);

		radio = new JRadioButton(_bundle.getString("tab_genome_version_go_ref_genome"));
		_goReferenceRadios.add(radio);
		setRadioGoListener(radio, Session.GO_ANALYSIS_REFERENCE, GoReference.GENOME);
		radioGrpGoRef.add(radio);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		pane.add(radio, c);

		radio = new JRadioButton(_bundle.getString("tab_genome_version_go_ref_qtls") + ":");
		_goReferenceRadios.add(radio);
		setRadioGoListener(radio, Session.GO_ANALYSIS_REFERENCE, GoReference.TRAIT);
		radioGrpGoRef.add(radio);
		GridBagLayoutUtils.set_gbc(c, iCol, iRow, 1, 1, GridBagConstraints.HORIZONTAL);
		pane.add(radio, c);

		_goReferenceTraitsCombo.setEnabled(false);
		fillComboTraits(_goReferenceTraitsCombo);
		GridBagLayoutUtils.set_gbc(c, iCol+1, iRow++, 2, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(5, 0, 0, 0);
		pane.add(_goReferenceTraitsCombo, c);

		buttonAnalyse.addActionListener(new ActionListener() {

			@Override
			public	void				actionPerformed(ActionEvent e) {
				DefaultTableModel		model			= (DefaultTableModel)tableResults.getModel();
				Canvas					canvas			= _panelsDrawing.getSelectedCanvas();

				if (null != canvas && PhysicalManager.instance().isLoaded()){
					// Launching
					new TaskGOAnalysis(model, progress).execute();
				}
			}
		});

		iCol += 3;
		// New column
		sep = new JSeparator(JSeparator.VERTICAL);
		GridBagLayoutUtils.set_gbc(c, iCol++, 0, 1, 8, GridBagConstraints.VERTICAL);
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 0;
		pane.add(sep, c);

		// New column
		setTableModel(tableResults);
		GridBagLayoutUtils.set_gbc(c, iCol, 0, 1, 7, GridBagConstraints.BOTH);
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.weighty = 1;
		pane.add(new JScrollPane(tableResults), c);

		GridBagLayoutUtils.set_gbc(c, iCol, 7, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		c.weighty = 0;
		pane.add(progress, c);

		// Table result mouse listener
		tableResults.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent evt) {
				TableColumnModel	columnModel	= tableResults.getColumnModel();
				int					column		= columnModel.getColumnIndexAtX(evt.getX());
				int					row			= evt.getY() / tableResults.getRowHeight();
				String				termId		= null;
				GOTerm				goTerm		= null;

				if (row >= 0 && row < tableResults.getRowCount() && column >= 0 && column < tableResults.getColumnCount()){
					//	Get the termID
					termId = (String)tableResults.getValueAt(row, 3);
					goTerm = GOntologies.instance().getTerm(termId);
					if (null != goTerm){
						_combosGO[0].setSelectedItem(goTerm.getNamespace());
						Charts.instance().goTo(Charts.STATS_LOCAL, goTerm);
						Charts.instance().startFlashing(Charts.STATS_LOCAL, goTerm);
						GOStatisticsManager.instance().getSelected().computeDensity(termId);
						GOStatisticsManager.instance().setInfos(termId);
					}
					repaintCanvas();
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});
	}

	public static class TaskGOAnalysis extends SwingWorker<Void, Void>{
		public	TaskGOAnalysis(DefaultTableModel	model,
								JProgressBar		progressBar){
			_model			= model;
			_progressBar	= progressBar;
		}

		@Override
		protected	Void				doInBackground() throws Exception {
			GoBias						goBias		= (GoBias)Session.instance().getValue(Session.GO_ANALYSIS_BIAS);
			GoReference					goRef		= (GoReference)Session.instance().getValue(Session.GO_ANALYSIS_REFERENCE);
			Canvas						canvas		= _panelsDrawing.getSelectedCanvas();
			LinkageGroupDesigner		lkgDes		= (null != canvas)?canvas.getLkgDesignerGenetic():null;
			List<double[]>				icsList		= null;
			Map<String, List<double[]>>	ics			= null;
			Map<String, List<double[]>>	icsRef		= null;

			_timeStart = System.currentTimeMillis();

			switch (goBias){
				case REGION:
					if (null != lkgDes){
						_icStartPb = lkgDes.getScroller().data_to_data(Double.valueOf(_ciPositions[0].getText()));
						_icEndPb = lkgDes.getScroller().data_to_data(Double.valueOf(_ciPositions[1].getText()));

						icsList = new ArrayList<double[]>();
						icsList.add(new double[]{_icStartPb, _icEndPb});
						ics = new HashMap<String, List<double[]>>();
						ics.put(GOStatisticsManager.instance().getSelectedLkgName(), icsList);

						System.out.println("Start Analysis");
						long start = System.currentTimeMillis();
						if (GoReference.TRAIT == goRef){
							icsRef = setIndexes((String)_goReferenceTraitsCombo.getSelectedItem());
							_results = GOStatisticsManager.instance().analyseBoyle(ics, icsRef, Boolean.TRUE, _progressBar);
						}else{
							_results = GOStatisticsManager.instance().analyseBoyle(ics, null, goRef == GoReference.GENOME, _progressBar);
						}
						System.out.println("End Analysis : " + (System.currentTimeMillis() - start) + " ms");
					}
					break;
				case TRAIT:
					ics = setIndexes((String)_comboTraits.getSelectedItem());
					_results = GOStatisticsManager.instance().analyseBoyle(ics, null, goRef == GoReference.GENOME, _progressBar);
					break;
			}

			return null;
		}

		@Override
		public	void		done(){
			DecimalFormat	df			= new DecimalFormat("0.###E0");
			GOTerm			goTerm		= null;
			String			name		= null;
			String			ontology	= null;

			System.out.println("Time elapsed : " + (System.currentTimeMillis()-_timeStart) + "ms");

			_progressBar.setValue(0);
			if (null != _results){
				removeRows();
				// Filling the table through its model
				for (Iterator<GOStatisticsManager.Distribution> it = _results.iterator(); it.hasNext();) {
					GOStatisticsManager.Distribution	distribution = it.next();

					goTerm = GOntologies.instance().getTerm(distribution.goTermID);
					if (null != goTerm){
						name = goTerm.getName();
						ontology = GOntology.idToString(goTerm.getNamespace());
					}else{
						name = "Unknown term!";
						ontology = "---";
					}

					_model.addRow(new Object[]{
												df.format(distribution.pValue),
												new FractionString(distribution.nbCountWin, distribution.nbCountTotal),
												String.valueOf(goTerm.getDepth()) + "(" + distribution.nbGenesSameDepth + ")",
												distribution.goTermID,
												name,
												ontology,
												});
				}
			}
		}

		/**
		 * Browse through linkage groups and fills the indexes with QTLs CI
		 * corresponding to the given trait
		 */
		private	Map<String, List<double[]>>	setIndexes(String trait){
			Map<String, List<double[]>>		ics		= new HashMap<String, List<double[]>>();
			List<double[]>					icsList = new ArrayList<double[]>();
			VisitorEltGet<LinkageGroup>		vLkgs	= new VisitorEltGet<LinkageGroup>(LinkageGroup.class);
			VisitorEltGet<Qtl>				vQtls	= new VisitorEltGet<Qtl>(Qtl.class);
			Canvas							canvas	= _panelsDrawing.getSelectedCanvas();
			LinkageGroupDesigner			lkgDes	= null;

			if (null != canvas && null != (lkgDes = canvas.getLkgDesignerGenetic())){
				lkgDes.getLinkageGroup().getBelongingMap().accept(vLkgs);

				for (int i = 0; i < vLkgs.getList().size(); i++) {
					LinkageGroup lkg = (LinkageGroup)vLkgs.getList().get(i);

					vQtls.resetList();
					lkg.accept(vQtls);

					icsList = new ArrayList<double[]>();
					for (int iQtls = 0; iQtls < vQtls.getList().size(); iQtls++) {
						Qtl qtl = (Qtl)vQtls.getList().get(iQtls);

						if (trait.equalsIgnoreCase(qtl.getTrait())){
							double start = PhysicalManager.instance().cM_to_pb(lkg, qtl.getPositionStart());
							double end = PhysicalManager.instance().cM_to_pb(lkg, qtl.getPositionEnd());

//							System.out.println(qtl.getPositionStart() + " cM <=> " + start + " pb");
//							System.out.println(qtl.getPositionEnd() + " cM <=> " + end + " pb");

							icsList.add(new double[]{start, end});
						}
					}
					if (!icsList.isEmpty()){
						ics.put(lkg.getName(), icsList);
					}
				}
			}
			return ics;
		}
		/**
		 * Removes all rows from model.
		 */
		private	void	removeRows(){
			while (_model.getRowCount() > 0){
				_model.removeRow(0);
			}
		}

		private	List<GOStatisticsManager.Distribution>	_results		= null;
		private	DefaultTableModel						_model			= null;
		private	JProgressBar							_progressBar	= null;
		private	double									_icStartPb		= 0;
		private	double									_icEndPb		= 0;
		private	long									_timeStart		= 0L;
	}

	/**
	 * Set the model for QTL listing and selection to the given JTable.
	 * @param table The Jtable
	 */
	private	static	void	setTableModel(JTable table){
		table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            },
            new String[] {
				_bundle.getString("go_tab_goterms_pvalue"),
				_bundle.getString("go_tab_goterms_go_term_density"),
				"depth",
				_bundle.getString("go_tab_goterms_go_term_id"),
				_bundle.getString("go_tab_goterms_go_term_name"),
				_bundle.getString("go_tab_goterms_ontology"),
			}
        ) {
            Class[] types = new Class [] {
                String.class,
                String.class,
				String.class,
				String.class,
				String.class,
				String.class,
            };
            boolean[] canEdit = new boolean [] {
                false,
                false,
				false,
				false,
				false,
				false,
            };

			@Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

			@Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

		table.setAutoCreateRowSorter(true);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());

		// Sorting first column
		sorter.setComparator(0, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				Double	d1 = null;
				Double	d2 = null;
				int		res	= 0;

				try{
					d1 = Double.valueOf(o1.replaceAll(",", "."));
					d2 = Double.valueOf(o2.replaceAll(",", "."));
					res = d1.compareTo(d2);
				}catch (Exception e){
					System.out.println(e.getMessage());
				}

				return res;
			}
		});

		// Sorting second column (go density), the greater the better.
		sorter.setComparator(1, new Comparator<FractionString>() {
			@Override
			public int compare(FractionString fs1, FractionString fs2) {
				Double	d1 = fs1.getRes();
				Double	d2 = fs2.getRes();
				int		res	= 0;

				try{
					res = d1.compareTo(d2);
				}catch (Exception e){
					System.out.println(e.getMessage());
				}

				return res;
			}
		});

		table.setRowSorter(sorter);
	}

	private	static	void		fillComboOntologies(JComboBox combo){
		DefaultComboBoxModel	model	= (DefaultComboBoxModel)combo.getModel();

		combo.setRenderer(new ComboBoxRendererOntologyWrapper(combo.getRenderer()));
		model.removeAllElements();
		for (Iterator<Integer> it = GOntologies.instance().iterator(); it.hasNext();) {
			Integer ontologyID = (Integer)it.next();
			model.addElement(ontologyID);
		}
		combo.setSelectedIndex(0);

		combo.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					Charts.instance().setSelectedOntology((Integer)e.getItem());
					Charts.instance().changeCanvasObjColor();
					GOStatisticsManager.instance().setChartPanel(Charts.instance().getChartPanel());
				}
			}
		});
	}

	private	static	void		fillComboStats(JComboBox combo){
		DefaultComboBoxModel	model	= (DefaultComboBoxModel)combo.getModel();

		combo.setRenderer(new ComboBoxRendererStatisticsWrapper(combo.getRenderer()));
		model.removeAllElements();
		for (Iterator itStats = Charts.instance().iteratorStatisticsID(); itStats.hasNext();) {
			Integer statsID = (Integer)itStats.next();
			model.addElement(statsID);
		}
		combo.setSelectedIndex(0);

		combo.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					Charts.instance().setSelectedStats((Integer)e.getItem());
					Charts.instance().changeCanvasObjColor();
					GOStatisticsManager.instance().setChartPanel(Charts.instance().getChartPanel());

				}
			}
		});
	}

	/**
	 * Fills the given pane with the general tab components.
	 * @param pane The pane to fill
	 */
	private	static	void	addComponentsToGeneralPane(Container pane){
		GridBagConstraints	c			= new GridBagConstraints();
		JCheckBox			checkbox	= null;
		JLabel				label		= null;
		int					iRow		= 0;

		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		//	ZOOM
		label = new JLabel("Zoom");
		GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		c.weighty = 0;
		c.insets = new Insets(10, 10, 0, 0);
		pane.add(label, c);

		_slider = new JSlider(1, 100, 10);
		GridBagLayoutUtils.set_gbc(c, 1, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_slider, c);

		Session.instance().setValue(Session.LKGDES_DEFAULT_SIZE, new Double(_slider.getValue()*60));
		Session.instance().setValue(Session.LKGDES_SLIDER_DEFAULT_POS, new Integer(_slider.getValue()));
		_slider.addChangeListener(new javax.swing.event.ChangeListener() {
			@Override
			public void stateChanged(javax.swing.event.ChangeEvent evt) {
				Session.instance().setValue(Session.LKGDES_SIZE, new Double(_slider.getValue()*60));
				redraw();
			}
		});

		//	Show all loci names
		checkbox = new JCheckBox(_bundle.getString("tab_general_map_common_markers"), true);
		initCheckBox(checkbox, Session.COMMON_MARKERS);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Show all loci names
		checkbox = new JCheckBox(_bundle.getString("tab_general_map_name_display"), true);
		initCheckBox(checkbox, Session.DISPLAY_MAP_NAME);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Show all loci names
		checkbox = new JCheckBox(_bundle.getString("tab_general_chr_name_display"), true);
		initCheckBox(checkbox, Session.DISPLAY_CHR_NAME);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Show all loci names
		checkbox = new JCheckBox(_bundle.getString("tab_general_lkg_name_display"), true);
		initCheckBox(checkbox, Session.DISPLAY_LKG_NAME);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Show all loci names
		checkbox = new JCheckBox(_bundle.getString("tab_general_mini_lkg_display"), true);
		initCheckBox(checkbox, Session.DISPLAY_MINI_LKG);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Vertically align all above components
		addDesignSep(pane, iRow, 2);
	}

	/**
	 * Call this method if you want to position above components vertically
	 * aligned on top.
	 */
	private	static	void	addDesignSep(Container pane, int iRow, int width){
		GridBagConstraints	c = new GridBagConstraints();

		GridBagLayoutUtils.set_gbc(c, 0, iRow, width, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.CENTER;
		c.weighty = 1;
		pane.add(new JSeparator(), c);
	}

	/**
	 * Fills the given pane with the expert tab components.
	 * @param pane The pane to fill
	 */
	private	static	void	addComponentsToExpertTabPane(Container pane){
		GridBagConstraints	c			= new GridBagConstraints();
		JCheckBox			checkbox	= null;
		final	JSlider		sliderFont	= new JSlider(10, 140);
		int					iRow		= 0;

		setGridBagLayout(pane);

		//	Show all loci names
		checkbox = new JCheckBox(_bundle.getString("tab_expert_display_all_loci"), false);
		initCheckBox(checkbox, Session.LOCUS_SHOW_ALL);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(10, 0, 0, 0);
		pane.add(checkbox, c);

		//	Automatic font size...
		//	 ...Checkbox
		checkbox = new JCheckBox(_bundle.getString("tab_expert_automatic_font_size"), true);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);
		//	 ...Slider
		sliderFont.setEnabled(false);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(sliderFont, c);

		//	Vertically align all above components
		addDesignSep(pane, iRow, 1);

		//	 Checkbox action
		checkbox.addItemListener(new ItemListener() {
			@Override
			public	void	itemStateChanged(ItemEvent e) {
				Canvas		canvas = _panelsDrawing.getSelectedCanvas();
				Font		font = null;

				if (e.getStateChange() == ItemEvent.SELECTED){
					//font = new Font("biomercator_manual", Font.PLAIN, sliderFont.getValue()/10);
					font = new Font("Courier", Font.PLAIN, 11);
					sliderFont.setEnabled(false);
				}else{
					//font = new Font("biomercator_auto", Font.PLAIN, 11);
					font = new Font("Courier", Font.PLAIN, sliderFont.getValue() / 10);
					sliderFont.setEnabled(true);
				}
				Session.instance().setValue(Session.FONT, font);
				Session.instance().setValue(Session.FONT_METRICS, canvas.getFontMetrics((Font)Session.instance().getValue(Session.FONT)));
				redraw();
			}
		});
		//	 Slider action
		sliderFont.addChangeListener(new javax.swing.event.ChangeListener() {
			@Override
			public void	stateChanged(javax.swing.event.ChangeEvent evt) {
				Canvas	canvas = _panelsDrawing.getSelectedCanvas();

				Session.instance().setValue(Session.FONT, new Font("biomercator", Font.PLAIN, sliderFont.getValue()/10));
				Session.instance().setValue(Session.FONT_METRICS, canvas.getFontMetrics((Font)Session.instance().getValue(Session.FONT)));
				redraw();
			}
		});
	}

	/**
	 * Fills the given pane with the locus tab components.
	 * @param pane The pane to fill
	 */
	private	static	void	addComponentsToLocusTabPane(Container pane){
		GridBagConstraints	c			= new GridBagConstraints();
		JCheckBox			checkbox	= null;
		int					iRow		= 0;

		setGridBagLayout(pane);

		//	Locus display
		checkbox = new JCheckBox(_bundle.getString("tab_locus_display"));
		initCheckBox(checkbox, Session.LOCUS_DISPLAY);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Locus name display
		checkbox = new JCheckBox(_bundle.getString("tab_locus_name_display"));
		initCheckBox(checkbox, Session.LOCUS_NAME_DISPLAY);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Locus position display
		checkbox = new JCheckBox(_bundle.getString("tab_locus_position_display"));
		initCheckBox(checkbox, Session.LOCUS_POSITION_DISPLAY);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Legend
		c.insets = new Insets(0, 0, 0, 0);
		initDisplaySettingsTable(_legendMarker, "Type", MarkerColorManager.instance());
		setDisplaySettingsTable(_legendMarker, Marker.class);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.BOTH);
		c.weighty = 1;
		pane.add(new JScrollPane(_legendMarker), c);
	}

	/**
	 * Fills the given pane with the qtl tab components.
	 * @param pane The pane to fill
	 */
	private	static	void	addComponentsToQtlTabPane(Container pane){
		GridBagConstraints	c			= new GridBagConstraints();
		JCheckBox			checkbox	= null;
		JRadioButton		radio		= null;
		ButtonGroup			group		= null;
		JLabel				label		= null;
		int					iRow		= 0;

		setGridBagLayout(pane);

		//	Qtl display
		checkbox = new JCheckBox(_bundle.getString("tab_qtl_display"));
		initCheckBox(checkbox, Session.QTL_DISPLAY);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Qtl name display
		checkbox = new JCheckBox(_bundle.getString("tab_qtl_name_display"));
		initCheckBox(checkbox, Session.QTL_NAME_DISPLAY);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(checkbox, c);

		//	Qtl density label
		label = new JLabel(_bundle.getString("tab_qtl_density"));
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		//	Qtl densities
		group = new ButtonGroup();
		c.insets = new Insets(0, 20, 0, 0);
		//	 standard
		radio = new JRadioButton(_bundle.getString("tab_qtl_density_none"), true);
		setRadioQTLDensityActionListener(radio, LinkageGroupDesignerGenetic.MODE_QTL_STANDARD);
		group.add(radio);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(radio, c);
		//	 condensed
		radio = new JRadioButton(_bundle.getString("tab_qtl_density_condensed"));
		setRadioQTLDensityActionListener(radio, LinkageGroupDesignerGenetic.MODE_QTL_CONDENSED);
		group.add(radio);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(radio, c);
		//	 lined
		radio = new JRadioButton(_bundle.getString("tab_qtl_density_lined"));
		setRadioQTLDensityActionListener(radio, LinkageGroupDesignerGenetic.MODE_QTL_CURVED);
		group.add(radio);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(radio, c);
		//	 overview
		radio = new JRadioButton(_bundle.getString("tab_qtl_density_overview"));
		setRadioQTLDensityActionListener(radio, LinkageGroupDesignerGenetic.MODE_QTL_OVERVIEW);
		group.add(radio);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(radio, c);
		//	 lined r2 weighted
		radio = new JRadioButton(_bundle.getString("tab_qtl_density_lined_r2"));
		setRadioQTLDensityActionListener(radio, LinkageGroupDesignerGenetic.MODE_QTL_CURVED_R2);
		group.add(radio);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.NONE);
		pane.add(radio, c);

		//	Legend
		c.insets = new Insets(0, 0, 0, 0);
		initDisplaySettingsTable(_legendQtl, "Trait", QTLColorManager.instance());
		setDisplaySettingsTable(_legendQtl, Qtl.class);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 1, 1, GridBagConstraints.BOTH);
		c.weighty = 1;
		pane.add(new JScrollPane(_legendQtl), c);
	}

	private static	void	initDisplaySettingsTable(	JTable				table,
														String				attributeName,
														ElementColorManager colorManager){
		table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            },
            new String [] {
                attributeName, "", ""
            }
        ) {
            Class[] types = new Class [] {
                String.class, JButton.class, Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

			@Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

			@Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
		TableColumn col = table.getColumnModel().getColumn(0);
		col.setPreferredWidth(100);
		col = table.getColumnModel().getColumn(1);
		col.setPreferredWidth(10);
		col = table.getColumnModel().getColumn(2);
		col.setPreferredWidth(10);
		TableCellRenderer defaultRenderer = table.getDefaultRenderer(JButton.class);
		table.setDefaultRenderer(JButton.class,
				new JTableButtonRenderer(defaultRenderer));
		table.addMouseListener(
				new JTableButtonMouseListener(table, colorManager));
	}

	public	static	void	redraw(){
		Dimension	dim		= null;
		Canvas		canvas	= _panelsDrawing.getSelectedCanvas();

//		dim = canvas.redraw();
		canvas.reProportionLkgs();
		dim = canvas.repaint(Canvas.REDRAW_ALL);
		resizePane(canvas, dim);
	}

	/**
	 * Creates the Display legend tables. (for QTLs and locus)
	 */
	public static	void	setDisplaySettingsTables(){
		setDisplaySettingsTable(_legendMarker, Marker.class);
		setDisplaySettingsTable(_legendQtl, Qtl.class);
	}

	/**
	 * Creates the Display table of the Class
	 */
	private static	void	setDisplaySettingsTable(JTable table, Class c){
		ElementColorManager	colorManager	= null;
		DefaultTableModel	model			= null;
		List<String>		names			= new ArrayList<String>();
		Object[]			row				= null;
		String				name			= null;
		JButton				button			= null;
		Canvas				canvas			= _panelsDrawing.getSelectedCanvas();

		if (null != canvas){
			if (Qtl.class == c){
				names = canvas.getTraitsPresent();
				colorManager = QTLColorManager.instance();
			}else if (Marker.class == c){
				names = canvas.getMarkerTypesPresent();
				colorManager = MarkerColorManager.instance();
			}
		}
		model = (DefaultTableModel)table.getModel();
		if (null != model && null != names){
			while (model.getRowCount() > 0){
				model.removeRow(0);
			}
			for (int i = 0; i < names.size(); i++) {
				name = names.get(i);

				row = new Object[3];
				button = new JButton();
				button.setBackground(colorManager.getColor(name));
				row[0] = name;
				row[1] = button;
				row[2] = true;
				model.addRow(row);
			}
			table.setModel(model);
		}
	}

	/**
	 * Fills the given pane with the genome version tab components.
	 * @param pane The pane to fill
	 */
	private	static	void	addComponentsToGenomeVersionTabPane(Container pane){
		GridBagConstraints	c							= new GridBagConstraints();
		JCheckBox			checkbox					= null;
		JButton				button						= null;
		JLabel				label						= null;
		JLabel				labelStruturalAnnotations	= null;
		JLabel				labelFunctionalAnnotations	= null;
		JLabel				labelAnchors				= null;
		int					iRow						= 0;

		setGridBagLayout(pane);

		//	GENOME VERSION
		c.insets = new Insets(10, 0, 0, 0);
		label = new JLabel(HTMLTools.setFontBold(_bundle.getString("tab_genome_version")));
		GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(label, c);

		GridBagLayoutUtils.set_gbc(c, 1, iRow++, 1, 1, GridBagConstraints.HORIZONTAL);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(_comboGenomeVersion, c);
		fillComboGenomeVersion();

		//	STRUCTURAL ANNOTATIONS
		label = new JLabel(_bundle.getString("tab_genome_version_annot_struct"));
		GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(10, 10, 0, 0);
		pane.add(label, c);

		labelStruturalAnnotations = new JLabel("");
		GridBagLayoutUtils.set_gbc(c, 1, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(10, 5, 0, 0);
		pane.add(labelStruturalAnnotations, c);

		//	FUNCTIONAL ANNOTATIONS
		label = new JLabel(_bundle.getString("tab_genome_version_annot_funct"));
		GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(10, 10, 0, 0);
		pane.add(label, c);

		labelFunctionalAnnotations = new JLabel("");
		GridBagLayoutUtils.set_gbc(c, 1, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(10, 5, 0, 0);
		pane.add(labelFunctionalAnnotations, c);

		//	ANCHORS
		label = new JLabel(_bundle.getString("tab_genome_version_anchors"));
		GridBagLayoutUtils.set_gbc(c, 0, iRow, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(10, 10, 0, 0);
		pane.add(label, c);

		labelAnchors = new JLabel("");
		GridBagLayoutUtils.set_gbc(c, 1, iRow++, 1, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(10, 5, 0, 0);
		pane.add(labelAnchors, c);

		//	SHOW GENOME VERSION IN PANEL
		checkbox = new JCheckBox(_bundle.getString("tab_genome_version_display"), true);
		initCheckBox(checkbox, Session.DISPLAY_GENOME_VERSION);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);
		checkbox.addItemListener(new ItemListener() {
			@Override
			public	void	itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					Canvas canvas = _panelsDrawing.getSelectedCanvas();
					canvas.linkToPhysic();
				}
			}
		});

		//	Show anchors
		checkbox = new JCheckBox(_bundle.getString("tab_genome_version_display_anchors"), true);
		initCheckBox(checkbox, Session.DISPLAY_PHY_BLOW_ANCHORS);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		//	Show Genes even without GO
		checkbox = new JCheckBox(_bundle.getString("tab_genome_version_display_gene_no_go"), true);
		initCheckBox(checkbox, Session.DISPLAY_PHY_BLOW_GENES_NO_GO);
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.LINE_START;
		pane.add(checkbox, c);

		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.CENTER;
		c.weighty = 1;
		pane.add(new JSeparator(), c);

		// Button for genome version management.
		button = new JButton(_bundle.getString("wizard_genome_version_management_button"));
		GridBagLayoutUtils.set_gbc(c, 0, iRow++, 2, 1, GridBagConstraints.NONE);
		c.anchor = GridBagConstraints.CENTER;
		c.weighty = 1;
		pane.add(button, c);

		//	LISTENER
		setComboListener(	_comboGenomeVersion,
							labelStruturalAnnotations,
							labelFunctionalAnnotations,
							labelAnchors);

		button.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				WizardGenomeVersionManagement w = new WizardGenomeVersionManagement();

				w.show();
				fillComboGenomeVersion();
			}
		});
	}

	/**
	 * Resizes and redraw the given pane.
	 * @param pane The pane to resize
	 * @param dim The wanted pane dimension
	 */
	private static	void	resizePane(JPanel pane, Dimension dim){
		if (null != dim){
			pane.setPreferredSize(dim);
			pane.revalidate();
		}
		pane.repaint();
	}

	/**
	 * Repaints the selected canvas.
	 */
	private	static	void	repaintCanvas(){
		Canvas				canvas = _panelsDrawing.getSelectedCanvas();

		if (null != canvas){
			canvas.repaint();
		}
	}

	/**
	 * Sets the GridBagLayout for the given pane.
	 * @param pane The pane
	 */
	private	static	void	setGridBagLayout(Container pane){
		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
	}

	/**
	 * Sets the selection attribute to the setted value in the Session class.
	 * Adds an item listener to the given JCheckBox and sets the checkbox's
	 * selection as the given field session ID value.
	 * @param checkbox The JCheckBox
	 * @param sessionFieldID The Session field identifier (such as
	 *  Section.LOCUS_DISPLAY)
	 */
	private	static	void	initCheckBox(JCheckBox checkbox, final Integer sessionFieldID){
		Boolean				selected = (Boolean)Session.instance().getValue(sessionFieldID);

		if (null != selected){
			checkbox.setSelected(selected);
		}
		checkbox.addItemListener(new ItemListener() {
			@Override
			public	void	itemStateChanged(ItemEvent e) {
				Session.instance().setValue(sessionFieldID, e.getStateChange() == ItemEvent.SELECTED);
				redraw();
			}
		});
	}

	/**
	 * Adds an item listener to the given JCheckBox and sets the checkbox's
	 * selection as the given field session ID value.
	 * @param checkbox The JCheckBox
	 * @param sessionFieldID The Session field identifier (such as
	 *  Section.LOCUS_DISPLAY)
	 */
	private	static	void	setRadioQTLDensityActionListener(JRadioButton radio, final Integer densityId){
		radio.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Session.instance().setValue(Session.QTL_DENSITY, (int)densityId);
				redraw();
			}
		});
	}

	private	static	void		fillComboGenomeVersion(){
		DefaultComboBoxModel	model	= (DefaultComboBoxModel)_comboGenomeVersion.getModel();
		List<Pair>				pairs	= DatabaseManager.instance().getGenomeVersionsPairs();

		model.removeAllElements();
		if (null != pairs){
			model.addElement(new Pair(null, _bundle.getString("misc_select")));
			for (int i = 0; i < pairs.size(); i++) {
				model.addElement(pairs.get(i));
			}
		}
	}

	/**
	 * Sets the actionPerformed event to the given combo box so it gets the
	 * physic annotations and anchors id.
	 * @param menuItem The menuItem to set the action.
	 * @param wizard The wizard to call when the menuItem is selected.
	 */
	private	static	void	setComboListener(	final	JComboBox	combo,
												final	JLabel		labelStruturalAnnotations,
												final	JLabel		labelFunctionalAnnotations,
												final	JLabel		labelAnchors){
		combo.addItemListener(new java.awt.event.ItemListener() {
			@Override
            public	void					itemStateChanged(java.awt.event.ItemEvent evt) {
				Map<PhysicDataTypes, Pair>	ids = null;
				Pair						pair = null;

                if (evt.getStateChange() == ItemEvent.SELECTED){
					pair = (Pair)evt.getItem();
					if (null != pair.id){
						// Zoom slider set to default
						_slider.setValue((Integer)Session.instance().getValue(Session.LKGDES_SLIDER_DEFAULT_POS));
						_slider.setEnabled(false);
						Session.instance().setValue(Session.LKGDES_SIZE, (Double)Session.instance().getValue(Session.LKGDES_DEFAULT_SIZE));
						redraw();
						// Physical data loading...
						//  Reseting for new loading
						PhysicalManager.instance().clean();
						GOStatisticsManager.instance().clean();
						_panelsDrawing.getSelectedCanvas().cleanView();
						//  Loading....
						ids = DatabaseManager.instance().getGenomeVersionPhysicDataPairs(pair.id);
						labelStruturalAnnotations.setText(HTMLTools.setFont(ids.get(PhysicDataTypes.STRUCTURAL_ANNOTATIONS).comments, HTMLTools.Style.ITALIC));
						labelFunctionalAnnotations.setText(HTMLTools.setFont(ids.get(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS).comments, HTMLTools.Style.ITALIC));
						labelAnchors.setText(HTMLTools.setFont(ids.get(PhysicDataTypes.ANCHORS).comments, HTMLTools.Style.ITALIC));
						//add a label links ?
						new TaskLoadingPhysic(ids).execute();
					}else{
						_slider.setEnabled(true);
						labelStruturalAnnotations.setText("");
						labelFunctionalAnnotations.setText("");
						labelAnchors.setText("");
						PhysicalManager.instance().clean();
						_panelsDrawing.getSelectedCanvas().cleanView();
						GOStatisticsManager.instance().clean();
					}
				}
            }
		});
	}

	public static class TaskLoadingPhysic extends SwingWorker<Void, Void>{
		public	TaskLoadingPhysic(Map<PhysicDataTypes, Pair> ids){
			_ids = ids;
		}

		@Override
		protected	Void				doInBackground() throws Exception {
			Canvas						canvas	= _panelsDrawing.getSelectedCanvas();

			System.out.println("START \"TaskLoadingPhysic\"");
			_start = System.currentTimeMillis();

			try{
				Session.instance().setValue(Session.LOADING_BLOCKING, Boolean.TRUE);
				System.out.println("Blocking ON");
				_progress.setIndeterminate(false);
				_progress.getModel().setValue(0);
				canvas.setEnableBlockingCursor(true);
				PhysicalManager.instance().initialize(
						_ids.get(PhysicDataTypes.STRUCTURAL_ANNOTATIONS).id,
						_ids.get(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS).id,
						_ids.get(PhysicDataTypes.ANCHORS).id,
						_ids.get(PhysicDataTypes.DBLINKS).id,
						_progress);
			}catch (Throwable t){
				t.printStackTrace(System.out);
			}
			return null;
		}

		@Override
		public	void	done(){
			Canvas		canvas	= _panelsDrawing.getSelectedCanvas();

			System.out.println("END \"TaskLoadingPhysic\" : " + (System.currentTimeMillis()-_start) + " ms");

			Session.instance().setValue(Session.LOADING_BLOCKING, Boolean.FALSE);
			Session.instance().setValue(Session.BACKGROUND_NEED_UPDATE, Boolean.TRUE);
			System.out.println("Blocking OFF");
			canvas.setEnableBlockingCursor(false);
			_progress.getModel().setValue(0);

			// Drawing
			if (null != canvas){
				canvas.initializePhysicalData();
			}
		}

		private Map<PhysicDataTypes, Pair>	_ids	= null;
		private	long						_start	= 0L;
	}

	//Sets if possible a look and feel for a more attractive design.
	static {
		for (UIManager.LookAndFeelInfo laf : UIManager
				.getInstalledLookAndFeels()) {
			System.out.println("Look and feel : " + laf.getName());
			if ("Nimbus".equals(laf.getName())) {
				try {
					UIManager.setLookAndFeel(laf.getClassName());
				} catch (Exception e) {
					System.out.println("No Nimbus Look and Feel : "
							+ e.getMessage());
				}
			}
		}
		_goTerm = new JTextField("GO:");
		_combosGO = new JComboBox[] { new JComboBox(), new JComboBox() };
		_ciPositions = new JTextField[] { new JTextField(), new JTextField(),
				new JTextField("0.05") };
	}
	
	public static void enableProgressBar() {
		_progress.setIndeterminate(true);
	}
	
	public static void disableProgressBar() {
		_progress.setIndeterminate(false);
	}

	private	static	JTextField			_goTerm;
	private	static	JComboBox[]			_combosGO;
	private	static	JTextField[]		_ciPositions;
	private	static	JSlider				_slider					= null;
	private	static	JTree				_explorer				= new JTree();
	private	static	JTabbedPaneCanvas	_panelsDrawing			= null;
	private	static	JSplitPane			_splitPaneMain			= new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	private	static	JSplitPane			_splitPaneCenter		= new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	private	static	JSplitPane			_splitPaneLeft			= new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	private	static	JComboBox			_comboGenomeVersion		= new JComboBox();
	private	static	JComboBox			_comboTraits			= new JComboBox();
	private	static	JComboBox			_goReferenceTraitsCombo	= new JComboBox();
	private	static	List<JRadioButton>	_goReferenceRadios		= new ArrayList<JRadioButton>();
	private	static	JTable				_legendQtl				= new JTable();
	private	static	JTable				_legendMarker			= new JTable();
	private	static	JProgressBar		_progress				= null;
	private	static	ResourceBundle		_bundle					= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
