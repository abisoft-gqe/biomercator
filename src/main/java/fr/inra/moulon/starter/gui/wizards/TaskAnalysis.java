/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 8, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.wizards;

import fr.inra.moulon.starter.analysis.AnalysisResult;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.logging.Parameters;
import java.security.Permission;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingWorker;


public abstract class TaskAnalysis extends SwingWorker<Void, Void> {
	public	TaskAnalysis(Wizard_Analysis wizard){
		super();
		_wizard = wizard;
		_results = new ArrayList<AnalysisResult>();
	}

	/**
	 * Launch the analysis. This method should implements the _project attribute
	 * with maps and files created by the analysis.
	 * @throws Exception
	 */
	public	abstract	void	executeAnalysis() throws Exception;

	/**
	 * Adds all file and maps to the analysis project.
	 */
	public	abstract	void	fillAnalysisResultList();

	/**
	 * This methods should return a Parameters that will be contain the fields
	 * and their associated values resume the analysis input.
	 */
	protected	abstract	Parameters	getInputsPrettyPrint();

	/**
	 * This methods should return a Parameters that will be contain the fields
	 * and their associated values resume the analysis output.
	 */
	protected	abstract	Parameters	getOutputsPrettyPrint();

	/**
	 * This methods should return the analysis name.
	 */
	public	abstract	String	getAnalysisName();

	/*
	 * New thread for analysis
	 */
	@Override
	public Void			doInBackground() {
		SecurityManager	securityManager = System.getSecurityManager();

		setProgress(0);
		try{
			System.setSecurityManager(new SecurityManager() {
				@Override
					public void checkPermission( Permission permission ){
						String	str = permission.getName();
						if (null != str && str.startsWith("exitVM")) {
							throw new SecurityException(str.replaceFirst("exitVM.", ""));
						}
					}
				});

			loggingInput();
			executeAnalysis();
			// NOTHING SHOULD COME AFTER THIE LINE ; the executeAnalysis method
			// always throws an exception
		} catch (SecurityException e) {
			if (!e.getMessage().equals("0")){
				_throwable = e;
			}
		} catch (Throwable thr) {
			_throwable = thr;
		} finally {
			System.setSecurityManager(securityManager);
		}

		if (null == _throwable){
			fillAnalysisResultList();
		}
		loggingOutput();

		return null;
	}

	/**
	 * Method called at the end of an analysis; it notifies to the wizard it 
	 * ended and supplies the results of the analysis as an exception an a
	 * project. If the analysis ran correctly, the exception will be null.
	 * The project should not be added the the data as it is, but browsed to add
	 * its contents.
	 */
	@Override
	public void done() {
		_wizard.endProgressDialog(_throwable);
	}

	/**
	 * Returns the analysis results.
	 * @return the analysis results
	 */
	public	List<AnalysisResult>	getResults(){
		return _results;
	}

	private	void	loggingInput(){
		Parameters	inputs = getInputsPrettyPrint();

		if (null != inputs){
			FileManager.analysisLoggingStart(_wizard.getProjectName());
			FileManager.analysisLoggingNew(getAnalysisName());
			FileManager.logAttributes("INPUT", inputs);
			FileManager.analysisLoggingEnd();
		}
	}

	private	void	loggingOutput(){
		Parameters	outputs = new Parameters();

		if (null != _throwable){
			outputs.addParam("Analysis result", _throwable.getMessage());
		}else{
			outputs.addParam("Analysis result", "Success");
		}

		FileManager.analysisLoggingStart(_wizard.getProjectName());
		FileManager.logAttributes("OUTPUT", outputs);
		FileManager.analysisLoggingEnd();
	}

	protected	List<AnalysisResult>	_results	= null;
	private		Throwable				_throwable	= null;
	private		Wizard_Analysis			_wizard		= null;
}
