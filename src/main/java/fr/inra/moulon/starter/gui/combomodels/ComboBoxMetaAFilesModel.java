/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 11-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.combomodels;

import fr.inra.moulon.starter.fileTools.FileManager;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * This model is a combo box model displaying children of the container given
 * in the constructor.
 * @author sosnowski
 */
public class ComboBoxMetaAFilesModel extends ComboBoxCustomModel {
	public			ComboBoxMetaAFilesModel(String	projectName){
		Set<String>	metaAnalyses	= new HashSet<String>();
		File		curDir			= new File(FileManager.DIR_SOFT_MAP + projectName);
		File[]		files			= null;
		String		fileName		= null;

		if (curDir.exists() && curDir.isDirectory()){
			files = curDir.listFiles();
			for (int i = 0; i < files.length; ++i){
				fileName = files[i].getName();

				if (fileName.endsWith("_model.txt")){
					metaAnalyses.add(fileName.substring(0, fileName.indexOf("_")));
				}
			}
		}
		_list = new ArrayList<Object>();
		for (Iterator<String> it = metaAnalyses.iterator(); it.hasNext();) {
			_list.add(it.next());
		}
	}
}
