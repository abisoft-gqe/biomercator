/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.utils;

import java.awt.Dimension;

/**
 *
 * @author sosnowski
 */
public final strictfp class Sizes {
	public static Dimension	MAIN_FRAME						= new Dimension(1400, 800);

	public static Dimension	DIM_PROJECT_DIALOG				= new Dimension(400, 200);
	public static Dimension	DIM_PROJECT_CHOOSER_DIALOG		= new Dimension(456, 300);
	public static Dimension	DIM_WIZARD_LOAD					= new Dimension(400, 200);
	public static Dimension	DIM_OPTIONS_DIALOG				= new Dimension(460, 360);
	public static Dimension	DIM_BIOM_PROJ_DIALOG			= new Dimension(866, 400);
	public static Dimension	DIM_BIOM_META_A_DIALOG			= new Dimension(620, 345);
	public static Dimension	DIM_BIOM_RES_DIALOG				= new Dimension(757, 167);
	public static Dimension	DIM_METAQTL_INFOMAP_DIALOG		= new Dimension(650, 500);
	public static Dimension	DIM_METAQTL_MMAPVIEW_DIALOG		= new Dimension(940, 400);
	public static Dimension	DIM_METAQTL_CONS_DIALOG			= new Dimension(800, 506);
	public static Dimension	DIM_METAQTL_COMP_DIALOG			= new Dimension(900, 506);
	public static Dimension	DIM_METAQTL_META_A_DIALOG		= new Dimension(720, 470);
	public static Dimension	DIM_METAQTL_META_A_INFO_DIALOG	= new Dimension(715, 600);
	public static Dimension	DIM_ANALYSIS_OK_DIALOG			= new Dimension(490, 262);
	public static Dimension	DIM_ANALYSIS_KO_DIALOG			= new Dimension(500, 262);
	public static Dimension	DIM_MAP_FUNCTIONS_DIALOG		= new Dimension(620, 450);
	public static Dimension	DIM_LOCI_FUNCTIONS_DIALOG		= new Dimension(620, 450);
	public static Dimension	DIM_SAVE_DIALOG					= new Dimension(850, 600);
	public static Dimension	DIM_QTL_CHOOSER_DIALOG			= new Dimension(650, 400);
	public static Dimension	DIM_PLUGINS_DIALOG				= new Dimension(300, 200);
	public static Dimension	DIM_PLUGINS_BROWSE_DIALOG		= new Dimension(650, 300);
	public static Dimension	DIM_TEXT_DISPLAYER				= new Dimension(600, 600);
	public static Dimension	DIM_IMAGE_DISPLAYER				= new Dimension(600, 600);
	public static Dimension	DIM_ABOUT_DIALOG				= new Dimension(600, 280);
	public static Dimension	DIM_QTL_DISPLAY_DIALOG			= new Dimension(200, 300);
	public static Dimension	DIM_COLOR_CHOOSER_DIALOG		= new Dimension(500, 400);
	public static Dimension	DIM_SUPPRESS_VALID_DIALOG		= new Dimension(233, 236);
	public static Dimension	DIM_LEGEND_DIALOG				= new Dimension(120, 130);
	public static Dimension	DIM_PROGRESS_DIALOG				= new Dimension(500, 380);
	public static Dimension	DIM_CHART_PANEL					= new Dimension(200, 200);
}
