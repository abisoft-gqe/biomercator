/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 11-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.combomodels;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * This model is a combo box model displaying unique traits of the QTLs in the
 * given linkage group.
 * @author sosnowski
 */
public class ComboBoxTraitFromLKGModel extends ComboBoxLkgModel {
	public	ComboBoxTraitFromLKGModel(LinkageGroup lkg){
		super(lkg, Qtl.class);

		if (null != _list){
			Set<String>		traits = new HashSet<String>();

			for (Iterator<Object> it = _list.iterator(); it.hasNext();) {
				Qtl qtl = (Qtl)it.next();

				if (!traits.contains(qtl.getTrait())){
					traits.add(qtl.getTrait());
				}
			}
			_list.clear();
			for (Iterator<String> it = traits.iterator(); it.hasNext();) {
				_list.add(it.next());
			}
		}
	}
}
