/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.dragNdrop;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * This class is the object used in dragNdrop transfers. It's designed to work
 * with a JTree and a Canvas.
 * @author Sosnow
 */
public class TransferableNode implements Transferable {
	public	TransferableNode(Object c){
		_transferNode = c;
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[]{_flavor};
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor df) {
		System.out.println("MIME : " + df.getMimeType());
		System.out.println("DataFavor" + df);

		return true;
	}


	@Override
	public Object getTransferData(DataFlavor df) throws UnsupportedFlavorException, IOException {
		return _transferNode;
	}

	private static final DataFlavor _flavor			= new DataFlavorContents();
	private Object					_transferNode	= null;
}
