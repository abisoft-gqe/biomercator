/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 11-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.gui.combomodels;

import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;


/**
 * This model is a combo box model displaying children of the container given
 * in the constructor.
 * @author sosnowski
 */
public abstract class ComboBoxCustomModel implements ComboBoxModel {
	@Override
	public void setSelectedItem(Object anItem) {
		_selection = (Object) anItem;
	}

	@Override
	public Object getSelectedItem() {
		return _selection;
	}

	@Override
	public int getSize() {
		return _list.size();
	}

	@Override
	public Object getElementAt(int index) {
		return _list.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
//		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
//		throw new UnsupportedOperationException("Not supported yet.");
	}

	protected	VisitorEltGet	_v			= null;
	protected	Object			_selection	= null;
	protected	List			_list		= null;
}
