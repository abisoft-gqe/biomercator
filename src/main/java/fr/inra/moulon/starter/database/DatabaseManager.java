/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 26-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.database;

import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.utils.PhysicDataTypes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * This class is used to create and get the connection to the sqlite database,
 * and create tables at the first program use.
 * @author sosnowski
 */
public class DatabaseManager {
	private	final	static	String	STRUCTURAL_ANNOTATIONS_LIST_TABLE	= "structural_annotations_list";
	private	final	static	String	STRUCTURAL_ANNOTATIONS_TABLE		= "structural_annotations";
	private	final	static	String	FUNCTIONAL_ANNOTATIONS_LIST_TABLE	= "functional_annotations_list";
	private	final	static	String	FUNCTIONAL_ANNOTATIONS_TABLE		= "functional_annotations";
	private	final	static	String	ANCHORS_LIST_TABLE					= "anchors_list";
	private	final	static	String	ANCHORS_TABLE						= "anchors";
	private	final	static	String	DBLINKS_TABLE						= "dblinks";
	private	final	static	String	DBLINKS_LIST_TABLE					= "dblinks_list";
	private	final	static	String	GENOME_VERSION_TABLE				= "genome_versions";

	private DatabaseManager(){
		String _connection_url = "jdbc:sqlite:"+FileManager.DIR_SOFT+"BioMercator_Physic.db";
		try{
			Class.forName("org.sqlite.JDBC");
			_connection = DriverManager.getConnection(_connection_url);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static DatabaseManager instance(){
		if (null == _instance){
			synchronized (DatabaseManager.class){
				if (null == _instance){
					_instance = new DatabaseManager();
				}
			}
		}
		return _instance;
	}

	public	Connection	getConnection(){
		return _connection;
	}

	/**
	 * Initialise the database.
	 */
	public	void	init(){
		createTablesIfNotExists();
	}

	private	void	createTablesIfNotExists(){
		Statement	stat	= null;
		ResultSet	rs		= null;

		try{
			stat = _connection.createStatement();

			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + STRUCTURAL_ANNOTATIONS_LIST_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + STRUCTURAL_ANNOTATIONS_LIST_TABLE + "(" +
									"id			INTEGER	PRIMARY KEY," +
									"comments	TEXT				" +
									");");
			}
			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + STRUCTURAL_ANNOTATIONS_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + STRUCTURAL_ANNOTATIONS_TABLE + "("
						+ "structural_annotations_id	INTEGER,"
						+ "seqname						TEXT,	"
						+ "source						TEXT,	"
						+ "feature_type					TEXT,	"
						+ "start						INTEGER,"
						+ "end							INTEGER,"
						+ "score						TEXT,	"
						+ "strand						TEXT,	"
						+ "frame						TEXT,	"
						+ "attributes					TEXT	"
						+ ");");
				stat.executeUpdate(	"CREATE INDEX idx_structural_annotations_id "
									+ "ON " + STRUCTURAL_ANNOTATIONS_TABLE + "(structural_annotations_id,seqname,feature_type,start);");
			}

			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + FUNCTIONAL_ANNOTATIONS_LIST_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + FUNCTIONAL_ANNOTATIONS_LIST_TABLE + "(" +
									"id			INTEGER	PRIMARY KEY," +
									"comments	TEXT				" +
									");");
			}
			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + FUNCTIONAL_ANNOTATIONS_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + FUNCTIONAL_ANNOTATIONS_TABLE + "("
									+ "functional_annotations_id	INTEGER,"
									+ "transcript					TEXT,	"
									+ "db							TEXT,	"
									+ "accession					TEXT	"
									+ ");");
				stat.executeUpdate(	"CREATE INDEX idx_functional_annotations_id "
									+ "ON " + FUNCTIONAL_ANNOTATIONS_TABLE + "(functional_annotations_id, db);");
			}

			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + ANCHORS_LIST_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + ANCHORS_LIST_TABLE + "(" +
									"id			INTEGER	PRIMARY KEY," +
									"comments	TEXT				" +
									");");
			}

			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + ANCHORS_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + ANCHORS_TABLE + "("
									+ "anchors_id	INTEGER,"
									+ "locus		TEXT,	"
									+ "chromosome	TEXT,	"
									+ "start		INTEGER,"
									+ "end			INTEGER,"
									+ "UNIQUE (anchors_id, chromosome, locus) ON CONFLICT REPLACE);");
				stat.executeUpdate(	"CREATE INDEX idx_anchors_id "
									+ "ON " + ANCHORS_TABLE + "(anchors_id, chromosome);");
			}
			
			//ajout des liens db externes
			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + DBLINKS_LIST_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + DBLINKS_LIST_TABLE + "(" +
									"id			INTEGER	PRIMARY KEY," +
									"comments	TEXT				" +
									");");
			}

			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + DBLINKS_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + DBLINKS_TABLE + "("
									+ "dblinks_id	INTEGER,"
									+ "dbname		TEXT,	"
									+ "link			TEXT"
									+ ");");
				//stat.executeUpdate(	"CREATE INDEX idx_dblinks_id "
				//					+ "ON " + DBLINKS_TABLE + "(anchors_id, chromosome);");
			}

			rs = stat.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + GENOME_VERSION_TABLE + "';");
			if (!rs.next()){
				stat.executeUpdate("CREATE TABLE " + GENOME_VERSION_TABLE + "("
									+ "id							INTEGER	PRIMARY KEY,"
									+ "name							TEXT,"
									+ "structural_annotations_id	INTEGER,"
									+ "functional_annotations_id	INTEGER,"
									+ "dblinks_id					INTEGER,"
									+ "anchors_id					INTEGER,"
									+ "comments						TEXT	"
									+ ");");
			} else {
				
				//Manage migration de v4.1.1 et pr�c�dentes � v4.2
				rs = stat.executeQuery("PRAGMA table_info(" + GENOME_VERSION_TABLE + ");");
				List<String> columnList = new ArrayList<String>();
				while (rs.next()) {
					String columnName = rs.getString(2);
					columnList.add(columnName);
				}
				if (!columnList.contains("dblinks_id")) {
					stat.executeUpdate("ALTER TABLE " + GENOME_VERSION_TABLE + " ADD COLUMN dblinks_id INTEGER;");
				}
			}


		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
	}

	public	Map<PhysicDataTypes, Pair>	getGenomeVersionPhysicDataPairs(Integer genomeVersionId){
		Map<PhysicDataTypes, Pair>		map		= null;
		Statement						stat	= null;
		ResultSet						rs		= null;

		try{
			stat = _connection.createStatement();
			// Store all ids
			rs = stat.executeQuery(""
					+ "SELECT structural_annotations_id, functional_annotations_id, anchors_id, dblinks_id "
					+ "FROM " + GENOME_VERSION_TABLE + " "
					+ "WHERE id = " + genomeVersionId + ";");

			map = new EnumMap<PhysicDataTypes, Pair>(PhysicDataTypes.class);
			while (rs.next()){
				map.put(PhysicDataTypes.STRUCTURAL_ANNOTATIONS,	new Pair(rs.getInt(1)));
				map.put(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS,	new Pair(rs.getInt(2)));
				map.put(PhysicDataTypes.ANCHORS,				new Pair(rs.getInt(3)));
				map.put(PhysicDataTypes.DBLINKS,				new Pair(rs.getInt(4)));
			}

			// Retrieve comments from structural annotations
			rs = stat.executeQuery(""
					+ "SELECT comments "
					+ "FROM " + STRUCTURAL_ANNOTATIONS_LIST_TABLE + " "
					+ "WHERE id = " + map.get(PhysicDataTypes.STRUCTURAL_ANNOTATIONS).id + ";");
			while (rs.next()){
				map.get(PhysicDataTypes.STRUCTURAL_ANNOTATIONS).comments = rs.getString(1);
			}

			// Retrieve comments from functional annotations
			rs = stat.executeQuery(""
					+ "SELECT comments "
					+ "FROM " + FUNCTIONAL_ANNOTATIONS_LIST_TABLE + " "
					+ "WHERE id = " + map.get(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS).id + ";");
			while (rs.next()){
				map.get(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS).comments = rs.getString(1);
			}

			// Retrieve comments from anchors
			rs = stat.executeQuery(""
					+ "SELECT comments "
					+ "FROM " + ANCHORS_LIST_TABLE + " "
					+ "WHERE id = " + map.get(PhysicDataTypes.ANCHORS).id + ";");
			while (rs.next()){
				map.get(PhysicDataTypes.ANCHORS).comments = rs.getString(1);
			}
			
			// Retrieve comments from dblinks
			rs = stat.executeQuery(""
					+ "SELECT comments "
					+ "FROM " + DBLINKS_LIST_TABLE + " "
					+ "WHERE id = " + map.get(PhysicDataTypes.DBLINKS).id + ";");
			while (rs.next()){
				map.get(PhysicDataTypes.DBLINKS).comments = rs.getString(1);
			}
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}

		return map;
	}

	public	Map<PhysicDataTypes, Integer>	getGenomeVersionPhysicDataIds(Integer genomeVersionId){
		Map<PhysicDataTypes, Integer>		map		= null;
		Statement							stat	= null;
		ResultSet							rs		= null;
		String								query	= null;

		try{
			stat = _connection.createStatement();
			query = "SELECT structural_annotations_id, functional_annotations_id, anchors_id "
					+ "FROM " + GENOME_VERSION_TABLE + " "
					+ "WHERE id = " + genomeVersionId + ";";
			rs = stat.executeQuery(query);

			map = new EnumMap<PhysicDataTypes, Integer>(PhysicDataTypes.class);
			while (rs.next()){
				map.put(PhysicDataTypes.STRUCTURAL_ANNOTATIONS,	rs.getInt(1));
				map.put(PhysicDataTypes.FUNCTIONAL_ANNOTATIONS,	rs.getInt(2));
				map.put(PhysicDataTypes.ANCHORS,				rs.getInt(3));
			}
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}

		return map;
	}

	public	void	insertNewGenomeVersion(	String	name,
											Integer	structural_annotations_id,
											Integer	functional_annotations_id,
											Integer	anchors_id,
											Integer dblinks_id,
											String	comments
											){
		Statement		stat	= null;

		try{
			stat = _connection.createStatement();
			stat.executeUpdate("INSERT INTO " + GENOME_VERSION_TABLE + " "
								+ "("
								+ "name,"
								+ "structural_annotations_id,"
								+ "functional_annotations_id,"
								+ "anchors_id,"
								+ "dblinks_id,"
								+ "comments"
								+ ") "
								+ "VALUES "
								+ "("
								+ "'" + name + "',"
								+ "'" + structural_annotations_id + "',"
								+ "'" + functional_annotations_id + "',"
								+ "'" + anchors_id + "',"
								+ "'" + dblinks_id + "',"
								+ "'" + comments + "'"
								+ ")");
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
	}

	/**
	 * Deletes the genome version corresponding to the given id.
	 * @param id The genome version id to delete
	 */
	public	void	deleteGenomeVersion(int id){
		Statement	stat		= null;
		String		query		= null;
		int			deleteCount	= -1;

		try{
			stat = _connection.createStatement();
			query = "DELETE "
					+ "FROM " + GENOME_VERSION_TABLE + " "
					+ "WHERE " + "id = " + id
					+ ";";
			deleteCount = stat.executeUpdate(query);
			System.out.println(query);
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
	}

	/**
	 * Deletes the genome version corresponding to the given id.
	 * @param id The genome version id to delete
	 */
	public	void	deletePhysicalType(PhysicDataTypes type, int id, List<Pair> genomeVersions){
		Statement	stat		= null;
		String		table		= null;
		String		tableList	= null;
		String		query		= null;
		String		idField		= null;
		int			deleteCount	= -1;


		switch (type){
			case STRUCTURAL_ANNOTATIONS:
				idField = "structural_annotations_id";
				table = STRUCTURAL_ANNOTATIONS_TABLE;
				tableList = STRUCTURAL_ANNOTATIONS_LIST_TABLE;
				break;
			case FUNCTIONAL_ANNOTATIONS:
				idField = "functional_annotations_id";
				table = FUNCTIONAL_ANNOTATIONS_TABLE;
				tableList = FUNCTIONAL_ANNOTATIONS_LIST_TABLE;
				break;
			case ANCHORS:
				idField = "anchors_id";
				table = ANCHORS_TABLE;
				tableList = ANCHORS_LIST_TABLE;
				break;
			case DBLINKS:
				idField = "dblinks_id";
				table = DBLINKS_TABLE;
				tableList = DBLINKS_LIST_TABLE;
				break;
		}
		try{
			// Delete from main list
			stat = _connection.createStatement();
			query = "DELETE "
					+ "FROM " + tableList + " "
					+ "WHERE " + "id = " + id
					+ ";";
			System.out.println(query);
			deleteCount = stat.executeUpdate(query);
			System.out.println("Deleted : " + deleteCount);

			// Delete all entries from table
			stat = _connection.createStatement();
			query = "DELETE "
					+ "FROM " + table + " "
					+ "WHERE " + idField + " = " + id
					+ ";";
			System.out.println(query);
			deleteCount = stat.executeUpdate(query);
			System.out.println("Deleted : " + deleteCount);

			// Delete given genome versions (the genome versions linked)
			if (null != genomeVersions){
				for (int i = 0; i < genomeVersions.size(); i++) {
					Pair pair = genomeVersions.get(i);

					stat = _connection.createStatement();
					query = "DELETE "
							+ "FROM " + GENOME_VERSION_TABLE + " "
							+ "WHERE " + "id = " + pair.id
							+ ";";
					System.out.println(query);
					deleteCount = stat.executeUpdate(query);
					System.out.println("Deleted : " + deleteCount);
				}
			}
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
	}

	public	List<Pair>	getGenomeVersionsUsing(PhysicDataTypes type, int id){
		List<Pair>		pairs	= new ArrayList<Pair>();
		Statement		stat	= null;
		ResultSet		rs		= null;
		String			idField	= null;

		switch (type){
			case STRUCTURAL_ANNOTATIONS:
				idField = "structural_annotations_id";
				break;
			case FUNCTIONAL_ANNOTATIONS:
				idField = "functional_annotations_id";
				break;
			case ANCHORS:
				idField = "anchors_id";
				break;
			case DBLINKS:
				idField = "dblinks_id";
				break;
		}
		try{
			stat = _connection.createStatement();
			rs = stat.executeQuery(""
					+ "SELECT id, name "
					+ "FROM " + GENOME_VERSION_TABLE + " "
					+ "WHERE " + idField + " = " + id + ";");

			while (rs.next()){
				pairs.add(new Pair(rs.getInt(1), rs.getString(2)));
			}
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}


		return pairs;
	}

	/**
	 * Returns a list of Pairs containing the id and name of the
	 * genome versions in the database.
	 * @return The list of genome versions in the database.
	 */
	public	List<Pair>	getGenomeVersionsPairs(){
		List<Pair>	list	= new ArrayList<Pair>();
		Statement	stat	= null;
		ResultSet	rs		= null;

		try{
			stat = _connection.createStatement();

			rs = stat.executeQuery("SELECT id, name FROM " + GENOME_VERSION_TABLE + ";");
			while (rs.next()){
				list.add(new Pair(rs.getInt(1), rs.getString(2)));
			}
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}

		return list;
	}

	/**
	 * Parses the database to return a list of structural annotation pairs.
	 * @return A list of structural annotation pairs.
	 */
	public	List<Pair>	getStructuralAnnotationPairs(){
		return getPairs(STRUCTURAL_ANNOTATIONS_LIST_TABLE, "structural_annotations_id");
	}

	/**
	 * Parses the database to return a list of functional annotation pairs.
	 * @return A list of functional annotation pairs.
	 */
	public	List<Pair>	getFunctionalAnnotationPairs(){
		return getPairs(FUNCTIONAL_ANNOTATIONS_LIST_TABLE, "functional_annotations_id");
	}

	/**
	 * Parses the database to return a list of anchors pairs.
	 * @return A list of anchors pairs.
	 */
	public	List<Pair>	getAnchorsPairs(){
		return getPairs(ANCHORS_LIST_TABLE, "anchors_id");
	}
	
	/**
	 * Parses the database to return a list of dblinks pairs.
	 * @return A list of dblinks pairs.
	 */
	public	List<Pair>	getDBLinksPairs(){
		return getPairs(DBLINKS_LIST_TABLE, "dblinks_id");
	}

	/**
	 * Returns a list of pairs for the given table.
	 * @param tableList The table's list name containing ids and comments.
	 * @param idField The ID field in the genome version table
	 * @return A list of pairs for the given table.
	 */
	private	List<Pair>	getPairs(String tableList, String idField){
		List<Pair>		pairs		= new ArrayList<Pair>();
		Statement		stat		= null;
		ResultSet		rs			= null;
		ResultSet		rsCount		= null;
		int				id			= -1;
		String			comments	= null;

		try{
			stat = _connection.createStatement();
			rs = stat.executeQuery(""
					+ "SELECT id, comments" + " "
					+ "FROM " + tableList + ";");

			while (rs.next()){
				id = rs.getInt("id");
				comments = rs.getString("comments");
				stat = _connection.createStatement();
				rsCount = stat.executeQuery(""
						+ "SELECT count(*)" + " "
						+ "FROM " + GENOME_VERSION_TABLE + " "
						+ "WHERE " + idField  + "=" + id
						+ ";");
				if (rsCount.getInt(1) == 0){
					comments = "* " + comments;
				}
				pairs.add(new Pair(id, comments));
			}
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}

		return pairs;
	}

	
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public	static String	getInsertStatementStructuralAnnotationsList(String comments){
		return "INSERT INTO " + STRUCTURAL_ANNOTATIONS_LIST_TABLE + " "
				+ "(comments) "
				+ "VALUES ('" + comments + "');";
	}

	public	static String	getInsertStatementStructuralAnnotations(){
		return "INSERT INTO " + STRUCTURAL_ANNOTATIONS_TABLE + " "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	}

	public	static String	getInsertStatementFunctionalAnnotationsList(String comments){
		return "INSERT INTO " + FUNCTIONAL_ANNOTATIONS_LIST_TABLE + " "
				+ "(comments) "
				+ "VALUES ('" + comments + "');";
	}

	public	static String	getInsertStatementFunctionalAnnotations(){
		return "INSERT INTO " + FUNCTIONAL_ANNOTATIONS_TABLE + " "
				+ "VALUES (?, ?, ?, ?);";
	}

	public	static String	getInsertStatementAnchorsList(String comments){
		return "INSERT INTO " + ANCHORS_LIST_TABLE + " "
				+ "(comments) "
				+ "VALUES ('" + comments + "');";
	}

	public	static String	getInsertStatementAnchors(){
		return "INSERT INTO " + ANCHORS_TABLE + " "
				+ "VALUES (?, ?, ?, ?, ?);";
	}

	public	static String getInsertStatementDBLinksList(String comments) {
		return "INSERT INTO " + DBLINKS_LIST_TABLE + " "
		+ "(comments) "
		+ "VALUES ('" + comments + "');";
	}
	
	public	static String getInsertStatementDBLinks() {
		return "INSERT INTO " + DBLINKS_TABLE + " "
		+ "VALUES (?, ?, ?);";
	}
	
	public	static String	getStatementChromosomeExtremes(	Integer	structural_annotations_id,
															String	seqname){
		return "SELECT min(start),max(end) "
				+ "FROM " + STRUCTURAL_ANNOTATIONS_TABLE + " "
				+ "WHERE "
				+	"structural_annotations_id = '"+ structural_annotations_id + "' AND "
				+	"seqname = '"+ seqname + "';";
	}

	public	static String	getStatementGetSeqNames(Integer structural_annotations_id){
		return "SELECT DISTINCT seqname"
				+ " FROM structural_annotations"
				+ " WHERE structural_annotations_id = " + structural_annotations_id
				+ ";";
	}

	public	static String	getSelectStatementFunctionalAnnotations(Integer	functional_annotations_id){
		return "SELECT transcript,accession "
				+ "FROM " + FUNCTIONAL_ANNOTATIONS_TABLE + " "
				+ "WHERE "
				+	"functional_annotations_id = '"+ functional_annotations_id + "' AND "
				+ 	"db = 'GO'"	+ ";";
	}

	public	static String	getSelectStatementStructuralAnnotations(Integer	structural_annotations_id,
																	String	seqname){
		return "SELECT feature_type, start, end, strand, attributes "
				+ "FROM " + STRUCTURAL_ANNOTATIONS_TABLE + " "
				+ "WHERE "
				+	"structural_annotations_id = '"+ structural_annotations_id + "' AND "
				+	"seqname = '"+ seqname + "' AND "
				+	"feature_type in ('gene', 'mRNA') "
				+ "ORDER BY start" + ";";
	}
	
	public static String getSelectStatementDBLinks(Integer dblink) {
		return "SELECT dbname, link FROM "+DBLINKS_TABLE+" WHERE dblinks_id="+dblink;
	}

	/**
	 * Returns as String the structural annotations query.
	 * @param structural_annotations_id
	 * @param seqname The sequence name (ie chromosome name)
	 * @param start_pb Can be null
	 * @param end_pb Can be null
	 * @return
	 */
	public	static String	getSelectStatementStructuralAnnotationsBounded(	Integer	structural_annotations_id,
																			String	seqname,
																			boolean	genesOnly,
																			Double	start_pb,
																			Double	end_pb){
		String	query	= "SELECT seqname,source,feature_type,start,end,score,strand,frame,attributes "
						+ "FROM " + STRUCTURAL_ANNOTATIONS_TABLE + " "
						+ "WHERE "
						+	"structural_annotations_id = '"+ structural_annotations_id + "'";

		if (null != seqname){
			query += " AND seqname = '" + seqname + "'";
		}
		if (genesOnly){
			query += " AND feature_type = 'gene'";
		}
		if (null != start_pb && null != end_pb){
			query += " AND start >= " + start_pb
					+ " AND end <= " + end_pb;
		}
		query += " ORDER BY seqname"
					+ " AND start"
					+ ";";

		System.out.println("query : " + query);

		return query;
	}

	/**
	 * Returns as String the structural annotations query.
	 * @param structural_annotations_id
	 * @param seqname The sequence name (ie chromosome name)
	 * @param start_pb Can be null
	 * @param end_pb Can be null
	 * @return
	 */
	public	static String	getSelectStatementStructuralAnnotationsChr(	Integer	structural_annotations_id,
																		String	seqname){
		String	query	= "SELECT seqname,source,feature_type,start,end,score,strand,frame,attributes "
						+ "FROM " + STRUCTURAL_ANNOTATIONS_TABLE + " "
						+ "WHERE "
						+	"structural_annotations_id = '"+ structural_annotations_id + "'"
						+ " AND seqname = '" + seqname + "'"
						+ " AND feature_type = 'chromosome'";

		System.out.println("query : " + query);

		return query;
	}

	public	static String	getSelectStatementAnchors(	Integer	anchors_id,
														String	chromosome){
		return "SELECT locus, start, end "
				+ "FROM " + ANCHORS_TABLE + " "
				+ "WHERE "
				+	"anchors_id = '"+ anchors_id + "' AND "
				+ 	"chromosome = '"	 + chromosome + "' "
				+ "ORDER BY start" + ";";
	}


	public	class	Id{
		public	Id(Integer id, String name){
			this.id = id;
			this.name = name;
		}

		@Override
		public	String	toString(){
			return name;
		}

		public	Integer	id		= null;
		public	String	name	= null;
	}

	private volatile static DatabaseManager	_instance;
	private					Connection		_connection	= null;

}
