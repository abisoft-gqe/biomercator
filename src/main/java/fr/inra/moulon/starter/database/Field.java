/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 05-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.database;


public class Field {
	public	final	static	int	NONE	= 0;
	public	final	static	int	STRING	= 1;
	public	final	static	int	INT		= 2;
	public	final	static	int	DOUBLE	= 3;

	/**
	 * Default field for any UNUSED values
	 */
	public	final	static	Field	DEFAULT = new Field("unused", 0, NONE);

	public	Field(String name, int index, int type){
		this.name = name;
		this.index = index;
		this.valType = type;
	}

	public	Field(String name){
		this(name, 0, NONE);
	}

	@Override
	public	String	toString(){
		return name;
	}

	public	String	name;
	public	Integer	index;
	public	Integer	valType;
}
