/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 26-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.database.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class ReaderGFF3Custom {
	private	final	static	int		_STRING	= 1;
	private	final	static	int		_INT	= 2;
	private	final	static	int		_DOUBLE	= 3;
	private	final	static	int[]	_INPUTS = new int[]{_STRING,
														_STRING,
														_STRING,
														_INT,
														_INT,
														_DOUBLE,
														_STRING,
														_STRING,
														_STRING
														};

	public	static	void	readBufferedReader(String fileName, Connection connection, int batchSize){
		PreparedStatement	prep		= null;
		BufferedReader		buf			= null;
		String				line		= null;
		String				field		= null;
		int					indexPrev	= 0;
		int					index		= 0;
		int					i			= 0;
		boolean				stay		= true;
		int					batchIndex	= 0;

		try {
			connection.setAutoCommit(false);
			prep = connection.prepareStatement("insert into gffr values (?, ?, ?, ?, ?, ?, ?, ?, ?);");
			buf = new BufferedReader(new FileReader(new File(fileName)));
			while (null != (line = buf.readLine())) {
				indexPrev = 0;
				i = 0;
				stay = true;
				while (i < 8 && stay){
					if (-1 != (index = line.indexOf("\t", indexPrev))){
						field = line.substring(indexPrev, index);
						launchTreatment(i, field, prep);
						indexPrev = index+1;
						++i;
					}else{
						stay = false;
					}
				}
				if (8 == i){
					index = line.length();
					field = line.substring(indexPrev, index);
					launchTreatment(i, field, prep);
					prep.addBatch();
					if (++batchIndex > batchSize){
						prep.executeBatch();
						batchIndex = 0;
					}

				}
			}
			prep.executeBatch();	//	For last batch
			connection.setAutoCommit(true);
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}

	private	static	void	launchTreatment(int index, String field, PreparedStatement prep){
		try {
			switch (_INPUTS[index]){
				case _STRING:
					prep.setString(index+1, field);
					break;
				case _INT:
					prep.setInt(index+1, Integer.valueOf(field));
					break;
				case _DOUBLE:
					prep.setDouble(index+1, Double.valueOf(field));
					break;
			}
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
	}
}
