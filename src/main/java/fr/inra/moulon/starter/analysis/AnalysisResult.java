/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 18-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.analysis;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import java.util.Comparator;


public class AnalysisResult {
	public	final	static	int	MAP_GENE_ID				= 1;
	public	final	static	int	META_ANALYSIS_ID		= 2;
	public	final	static	int	FILE_MAP_GENE_ID		= 3;
	public	final	static	int	FILE_MAP_GENE_STD_ID	= 4;
	public	final	static	int	FILE_ID					= 5;
	public	final	static	int	IMAGE_ID				= 6;
	public	final	static	int	DIRECTORY_ID			= 7;

	public	AnalysisResult(int type, Object obj){
		this.type = type;
		this.obj = obj;
	}

	public	AnalysisResult(int type, Object obj, String name){
		this(type, obj);
		this.name = name;
	}

	public	AnalysisResult(int type, Object obj, MapGene mapGene, LinkageGroup lkg){
		this(type, obj);
		this.mapGene = mapGene;
		this.lkg = lkg;
	}

	public static class AnalysisResultIDsComparator  implements Comparator<AnalysisResult>{
		@Override
		public int	compare(AnalysisResult a1, AnalysisResult a2){
			int		res = 0;

			if (a1.type == a2.type){
				res = 0;
			}else if (DIRECTORY_ID == a1.type){
				res = 1;
			}else{
				res = -1;
			}

			return res;
		}
	}


	public	int				type	= 0;
	public	Object			obj		= null;
	public	String			name	= null;
	public	MapGene			mapGene	= null;
	public	LinkageGroup	lkg		= null;
}
