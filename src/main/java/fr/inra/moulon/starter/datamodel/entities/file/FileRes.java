/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities.file;

import fr.inra.moulon.starter.datamodel.container.Content;
import java.awt.Component;


/**
 *
 * @author sosnowski
 */
public abstract class FileRes extends Content{
	/**
	 * Creates and returns the Swing component allowing to view the file's data.
	 * @return The Swing component allowing to view the file's data.
	 */
	public	abstract	Component	getComponentToAdd();

	protected FileRes(String name, String path){
		super(name);
		_path = path;
	}

	public String	getPath(){
		return _path;
	}

	private	String	_path = null;
}
