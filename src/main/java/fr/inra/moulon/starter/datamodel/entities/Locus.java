/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities;

import fr.inra.moulon.starter.datamodel.container.Content;

public abstract class Locus extends Content {
	public static final	int	NAME = 0;

	protected Locus(String name){
		super(name);
	}

	@Override
	public abstract Locus	copy();

	/**
	 * Returns true if the location belongs to the map element; false otherwise
	 * @param positionStart
	 * @param positionEnd
	 * @return if the location belongs to the map element
	 */
	public	boolean	isPositionWithin(double positionStart, double positionEnd){
		return _position >= positionStart && _position <= positionEnd;
	}

	/**
	 * Returns the locus position
	 * @return The locus position
	 */
	public	double	getPosition(){
		return _position;
	}

	/**
	 * Sets the locus position
	 * @param position The locus position
	 */
	public	void	setPosition(double position){
		_position = position;
	}

	/**
	 * Copies the attributes in the given AbstractQtl.
	 * @param aQtl The Qtl where to sets the attributes
	 */
	public	void	copyAttributes(Locus locus){
		locus._position = _position;
	}

	protected	double	_position	= 0;
}
