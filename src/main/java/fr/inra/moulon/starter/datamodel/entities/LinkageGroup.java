/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities;


import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.controller.visitors.VisitorExtremsPosition;
import fr.inra.moulon.starter.controller.visitors.VisitorReverse;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.entities.utils.LocusComparator;
import fr.inra.moulon.starter.fileTools.FileManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class LinkageGroup extends Container<Locus>{
	public static final String	NAME_DEFAULT	= "default";

	public LinkageGroup(String name){
		super(name);
		_metaAnalyses = new ArrayList<MetaAnalysis>();
		_hashClass = new HashMap<Class, List<Locus>>();
		_hashClass.put(Marker.class, new ArrayList<Locus>());
		_hashClass.put(Qtl.class, new ArrayList<Locus>());
		_hashClass.put(MetaQtl.class, new ArrayList<Locus>());
	}

	public LinkageGroup(){
		this(NAME_DEFAULT);
	}

	@Override
	public void	add(Locus eltNew){
		super.add(eltNew);
		_hashClass.get(eltNew.getClass()).add(eltNew);
		_maxBioPos = Math.max(_maxBioPos, eltNew.getPosition());
		_minBioPos = Math.min(_minBioPos, eltNew.getPosition());
	}

	@Override
	public LinkageGroup	copy(){
		LinkageGroup	lkgCopied = (LinkageGroup)super.copy(new LinkageGroup(super.getName()));

		lkgCopied._maxBioPos	= _maxBioPos;
		lkgCopied._minBioPos	= _minBioPos;
		lkgCopied._mapName		= _mapName;
		lkgCopied._chrName		= _chrName;
		lkgCopied._belongingMap	= _belongingMap;

		return lkgCopied;
	}

	/**
	 * Returns an iterator over the elements of the given class in this list
	 * in proper sequence.
	 * @return An iterator over the elements of the given class in this list
	 * in proper sequence.
	 */
	public	Iterator<Locus>	iterator(Class c){
		List				list = null;

		if (null == (list = _hashClass.get(c))){
			System.out.println("No such class in linkageGroup");
		}
		return list.iterator();
	}

	/**
	 * Gives the highest position found in the linkage group's element.
	 * @return The highest position found in the linkage group's element.
	 */
	public Double	getMaxBioPos(){
		return _maxBioPos;
	}

	/**
	 * Gives the lowest position found in the linkage group's element.
	 * @return The lowest position found in the linkage group's element.
	 */
	public Double	getMinBioPos(){
		return _minBioPos;
	}

	/**
	 * Gives the linkage group's size.
	 * @return The linkage group's size.
	 */
	public Double	getBioSize(){
		return _maxBioPos - _minBioPos;
	}


	@Override
	public	void accept(Visitor v){
		v.visit(this);
	}

	/**
	 * Sorts the array containing Locus classes.
	 */
	public void	sort(){
		Collections.sort(_listElements, new LocusComparator());
	}

	/**
	 * Adds a meta analysis to the linkage group.
	 * @param metaA The meta analysis to add
	 */
	public	void	addMetaAnalysis(MetaAnalysis metaA){
		_metaAnalyses.add(metaA);
	}

	/**
	 * Returns the number of meta analyses.
	 * @return the number of meta analyses
	 */
	public int getNbMetaAnalyses() {
		return _metaAnalyses.size();
	}

	/**
	 * Returns the meta analysis at the given index.
	 * @param index The index
	 * @return the meta analysis
	 */
	public MetaAnalysis getMetaAnalysis(int index) {
		return _metaAnalyses.get(index);
	}

	/**
	 * Returns an iterator over the meta analyses in this list in proper
	 * sequence.
	 * @return an iterator over the meta analyses in this list in proper
	 * sequence.
	 */
	public	Iterator<MetaAnalysis>	iteratorMetaAnalyses(){
		_itIndexMetaA = 0;
		_nbEltsMetaA = _metaAnalyses.size();
		Iterator<MetaAnalysis>		it = new Iterator<MetaAnalysis>() {
			@Override
			public boolean hasNext() {
				return _itIndexMetaA < _nbEltsMetaA;
			}

			@Override
			public MetaAnalysis next() {
				return _metaAnalyses.get(_itIndexMetaA++);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("Not supported yet.");
			}
		};

		return it;
	}

	/**
	 * Reverses all containing loci. (the first becomes the last etc.)
	 */
	public	void				reverse(){
		MapGene					belongingMap = getBelongingMap();
		VisitorReverse			vReverse = null;
		VisitorExtremsPosition	vExtrems = new VisitorExtremsPosition();

		accept(vExtrems);
		vReverse = new VisitorReverse(vExtrems.getExtrems()[1]);
		accept(vReverse);
		FileManager.writeMap(belongingMap, belongingMap.getBelongingProjectName());
	}

	/**
	 * Get the associated map name.
	 * @return the associated map name.
	 */
	public	String	getMapName(){
		return _mapName;
	}

	/**
	 * Set the associated map name.
	 * @param mapName the map name.
	 */
	public	void	setMapName(String mapName){
		_mapName = mapName;
	}

	/**
	 * Set the associated map.
	 * @param mapName the map.
	 */
	public	MapGene	getBelongingMap(){
		return _belongingMap;
	}

	/**
	 * Set the associated map.
	 * @param mapName the map.
	 */
	public	void	setBelongingMap(MapGene belongingMap){
		_belongingMap = belongingMap;
		_mapName = belongingMap.getName();
	}

	/**
	 * Get the associated chromosome name.
	 * @return the associated chromosome name.
	 */
	public	String	getChrName(){
		return _chrName;
	}

	/**
	 * Set the associated chromosome name.
	 * @param chrName the chromosome name
	 */
	public	void	setChrName(String chrName){
		_chrName = chrName;
	}

	private	double					_maxBioPos		= 0;
	private	double					_minBioPos		= 0;
	private	List<MetaAnalysis>		_metaAnalyses	= null;
	private	int						_itIndexMetaA	= 0;
	private	int						_nbEltsMetaA	= 0;
	private	Map<Class, List<Locus>>	_hashClass		= null;
	private	String					_mapName		= null;
	private	String					_chrName		= null;
	private	MapGene					_belongingMap	= null;
}
