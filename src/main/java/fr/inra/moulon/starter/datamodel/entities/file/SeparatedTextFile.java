/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities.file;

import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.datamodel.container.Content;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author sosnowski
 */
public class SeparatedTextFile extends FileRes{
	public		SeparatedTextFile(String name, String path, String separator){
		super(name, path);
		_separator = separator;
	}

	public void	load(){
		try{
			_buff = new BufferedReader(new FileReader(super.getPath()));
		}catch(IOException e){
			System.out.println(e.getLocalizedMessage());
		}
	}

	public boolean	hasFileLoaded(){
		return (null != _buff);
	}

	public BufferedReader	getBuffer(){
		return _buff;
	}

	public String[]	getTokens(){
		return _tokens;
	}

	public boolean	hasNextTokenOnLineLoaded(){
		boolean		res = false;

		if (null != _tokens && _iTokens < _tokens.length){
			res = true;
		}

		return res;
	}

	public String	getNextToken(){
		return _tokens[_iTokens++];
	}

	public boolean	readNextLine(){
		boolean		res = true;

		try{
			_line = _buff.readLine();
		}
		catch(IOException e){
			System.out.println(e.getLocalizedMessage());
		}

		if (null == _line){
			res = false;
		}else{
			_tokens = _line.split(_separator);
			_iTokens = 0;
		}

		return res;
	}

	public void	close(){
		try{
			_buff.close();
		}catch(IOException e){
			System.out.println(e.getLocalizedMessage());
		}
	}

	@Override
	public	Component		getComponentToAdd(){
		JTable				table		= new JTable();
		DefaultTableModel	model		= new DefaultTableModel(0, 100);
		BufferedReader		reader		= null;
		String				line		= null;
		String[]			tokens		= null;
		int					nbColsMax	= 0;

		try{
			reader = new BufferedReader(new FileReader(super.getPath()));
			while (null != (line = reader.readLine())){
				if (null != (tokens = line.split(_separator))){
					model.addRow(tokens);
					nbColsMax = Math.max(nbColsMax, tokens.length);
				}
			}
			reader.close();
			model.setColumnCount(nbColsMax);
			table.setModel(model);
		}catch (Exception e){
			System.out.println(e.getMessage());
		}

		return table;
	}

	private BufferedReader	_buff = null;
	private String			_separator = null;
	private String			_line = null;
	private String[]		_tokens = null;
	private int				_iTokens = 0;

	@Override
	public Content copy() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void accept(Visitor v) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
