/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities.file;

import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.datamodel.container.Content;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author sosnowski
 */
public class ImageFile extends FileRes{
	public ImageFile(String name, String path){
		super(name, path);
		//_image = Toolkit.getDefaultToolkit().getImage(path + name);
		_image = new ImageIcon(path).getImage();
	}

	public Image	getImage(){
		return _image;
	}

	public boolean	hasImage(){
		boolean		res = false;

		if (null != _image){
			res = true;
		}

		return res;
	}

	public int	getWidth(){
		return _image.getWidth(null);
	}

	public int	getHeight(){
		return _image.getHeight(null);
	}

	public Dimension	getDimension(){
		return new Dimension(getWidth(), getHeight());
	}

	@Override
	public Component getComponentToAdd() {
		return new JLabel(new ImageIcon(getPath()));
	}

	private Image	_image = null;

	@Override
	public Content copy() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void accept(Visitor v) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
