/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities;

import fr.inra.moulon.starter.controller.visitors.Visitor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * A metaQtl is a Qtl with special attributes such as a Qtls membership, ie a
 * list (map) of List<Double> corresponding to a Qtl's belonging to the metaQtl.
 * @author sosnowski
 */
public class MetaQtl extends AbstractQtl{
	private	static	String	CLASS_NAME	= "MetaQTL";

	public MetaQtl(String name, String trait){
		super(name, trait);
		_memberships = new HashMap<String, Double>();
	}

	@Override
	public MetaQtl	copy(){
		MetaQtl		res = new MetaQtl(super.getName(), super.getTrait());

		super.copyAttributes(res);

		return res;
	}

	/**
	 * Adds a qtl membership to the metaQtl.
	 * @param qtlName
	 * @param belonging The belonging of the Qtl to the metaQtl (between [0-1])
	 */
	public	void	addMembership(String qtlName, Double belonging){
		_memberships.put(qtlName, belonging);
	}

	/**
	 * Returns an iterator over the qtls name belonging to the metaQtl.
	 * @return an iterator over the qtls name belonging to the metaQtl
	 */
	public	Iterator<String>	iteratorQtlsUsed(){
		return _memberships.keySet().iterator();
	}

	/**
	 * Returns the belonging value (between [0-1]) of the given qtl to the
	 * metaQtl.
	 * @param qtlName The qtl name
	 * @return the belonging value of the given qtl to the metaQtl
	 */
	public	Double	getBelonging(String qtlName){
		Double		res = _memberships.get(qtlName);

		return (null != res)?res:0;
	}

	@Override
	public String classToString() {
		return CLASS_NAME;
	}

	@Override
	public	void accept(Visitor v){
		v.visit(this);
	}

	private Map<String, Double>	_memberships	= null;	// corresponding to a Qtl's belonging percentage to the meta Qtl.

}
