/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities;

import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.entities.utils.ContentNameComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MapGene extends Container<Chromosome> {
	public static final	int		LOCUS_LOCATION_RELATIVE	= 0;
	public static final	int		LOCUS_LOCATION_ABSOLUTE	= 1;

	public static final	String	DEFAULT_GENUS				= "unknown";
	public static final	String	DEFAULT_SPECIES				= "unknown";
	public static final	String	DEFAULT_CROSS_TYPE			= "unknown";
	public static final	Integer	DEFAULT_CROSS_SIZE			= 0;
	public static final	String	DEFAULT_MAPPING_CROSS_TYPE	= "unknown";
	public static final	String	DEFAULT_MAPPING_FUNCTION	= "unknown";
	public static final	String	DEFAULT_UNIT				= "cM";
	public static final	int		DEFAULT_QUALITY				= 0;
	public static final	int		DEFAULT_EXPANSION			= 0;

	public MapGene(String str){
		super(str);
		_mapsUsed = new ArrayList<String>();
	}

	@Override
	public MapGene	copy(){
		MapGene		mapCopied = (MapGene)super.copy(new MapGene(super.getName()));

		mapCopied._crossSize			= _crossSize;
		mapCopied._crossType			= _crossType;
		mapCopied._genus				= _genus;
		mapCopied._mapExpansion			= _mapExpansion;
		mapCopied._mapQuality			= _mapQuality;
		mapCopied._mappingCrossType		= _mappingCrossType;
		mapCopied._mappingFunction		= _mappingFunction;
		mapCopied._mappingUnit			= _mappingUnit;
		mapCopied._species				= _species;
		mapCopied._belongingProjectName	= _belongingProjectName;
		mapCopied._mapsUsed				= new ArrayList<String>(_mapsUsed);

		return mapCopied;
	}

	/**
	 * Sorts the array containing Chromosome classes in alphabetic order.
	 */
	public void sort(){
		Collections.sort(_listElements, new ContentNameComparator());
	}

	/**
	 * Adds a map to the used map list. (this method is for keeping track of a
	 * complied map's used maps .
	 * @param mapName The used map name
	 */
	public	void	addMapUsed(String mapName){
		_mapsUsed.add(mapName);
	}

	/**
	 * Returns an iterator on the used maps name.
	 * @return An iterator on the used maps name.
	 */
	public	Iterator<String>	iteratorUsedMaps(){
		return _mapsUsed.iterator();
	}
	/**
	 * This methods returns if the map has been created by compilation of
	 * others.
	 * @return If the map contains used maps.
	 */
	public	boolean	hasUsedMaps(){
		return _mapsUsed.size() > 0;
	}

	/////////////////// SETTER ///////////////////

	/**
	 * Sets the map genus
	 * @param genus The map genus
	 */
	public void	setGenus(String genus){
		_genus = genus;
	}

	/**
	 * Sets the map species
	 * @param species The map species
	 */
	public void	setSpecies(String species){
		_species = species;
	}

	/**
	 * Sets the map crossSize
	 * @param crossSize The map crossSize
	 */
	public void	setCrossSize(int crossSize){
		_crossSize = crossSize;
	}

	/**
	 * Sets the map crossType
	 * @param crossType The map crossType
	 */
	public void	setCrossType(String crossType){
		_crossType = crossType;
	}

	/**
	 * Sets the map mappingCrossType
	 * @param mappingCrossType The map mappingCrossType
	 */
	public void	setMappingCrossType(String mappingCrossType){
		_mappingCrossType = mappingCrossType;
	}

	/**
	 * Sets the map mappingFunction
	 * @param mappingFunction The map mappingFunction
	 */
	public void	setMappingFunction(String mappingFunction){
		_mappingFunction = mappingFunction;
	}

	/**
	 * Sets the map mappingUnit
	 * @param mappingUnit The map mappingUnit
	 */
	public void	setMappingUnit(String mappingUnit){
		_mappingUnit = mappingUnit;
	}

	/**
	 * Sets the map expansion
	 * @param mapExpansion The map expansion
	 */
	public void setMapExpansion(int mapExpansion){
		_mapExpansion = mapExpansion;
	}

	/**
	 * Sets the map quality
	 * @param mapQuality The map quality
	 */
	public void	setMapQuality(int mapQuality){
		_mapQuality = mapQuality;
	}


	/////////////////// GETTER ///////////////////

	/**
	 * Gets the map genus
	 * @return The map genus
	 */
	public String	getGenus(){
		return _genus;
	}

	/**
	 * Gets the map species
	 * @return The map species
	 */
	public String	getSpecies(){
		return _species;
	}

	/**
	 * Gets the map crossSize
	 * @return The map crossSize
	 */
	public int	getCrossSize(){
		return _crossSize;
	}

	/**
	 * Gets the map crossType
	 * @return The map crossType
	 */
	public String	getCrossType(){
		return _crossType;
	}

	/**
	 * Gets the map mappingCrossType
	 * @return The map mappingCrossType
	 */
	public String	getMappingCrossType(){
		return _mappingCrossType;
	}

	/**
	 * Gets the map mappingUnit
	 * @return The map mappingUnit
	 */
	public String	getMappingUnit(){
		return _mappingUnit;
	}

	/**
	 * Gets the map mappingFunction
	 * @return The map mappingFunction
	 */
	public String	getMappingFunction(){
		return _mappingFunction;
	}

	/**
	 * Gets the map expansion
	 * @return The map expansion
	 */
	public int	getMapExpansion(){
		return _mapExpansion;
	}

	/**
	 * Gets the map quality
	 * @return The map quality
	 */
	public int	getMapQuality(){
		return _mapQuality;
	}

	/**
	 * Set the associated project name.
	 * @param mapName the map.
	 */
	public	String	getBelongingProjectName(){
		return _belongingProjectName;
	}

	/**
	 * Set the associated map.
	 * @param mapName the map.
	 */
	public	void	setBelongingProjectName(String belongingProjectName){
		_belongingProjectName = belongingProjectName;
	}

	@Override
	public	void accept(Visitor v){
		v.visit(this);
	}
	
	private String			_genus					= DEFAULT_GENUS;
	private String			_species				= DEFAULT_SPECIES;
	private String			_crossType				= DEFAULT_CROSS_TYPE;
	private int				_crossSize				= DEFAULT_CROSS_SIZE;
	private String			_mappingCrossType		= DEFAULT_MAPPING_CROSS_TYPE;
	private String			_mappingFunction		= DEFAULT_MAPPING_FUNCTION;
	private String			_mappingUnit			= DEFAULT_UNIT;
	private int				_mapExpansion			= DEFAULT_EXPANSION;
	private int				_mapQuality				= DEFAULT_QUALITY;
	private	List<String>	_mapsUsed				= null;
	private	String			_belongingProjectName	= null;
}
