/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities;

import fr.inra.moulon.starter.utils.NumericalUtilities;

public abstract class AbstractQtl extends Locus {
	public AbstractQtl(String name, String trait){
		super(name);
		_trait = trait;
	}

	public	abstract	String	classToString();

	/**
	 * Copies the attributes in the given AbstractQtl.
	 * @param aQtl The Qtl where to sets the attributes
	 */
	public	void	copyAttributes(AbstractQtl aQtl){
		super.copyAttributes(aQtl);

		aQtl._trait				= _trait;
		aQtl._traitOntologyID	= _traitOntologyID;
		aQtl._lodscore			= _lodscore;
		aQtl._r2				= _r2;
		aQtl._positionStart		= _positionStart;
		aQtl._positionEnd		= _positionEnd;
		aQtl._sim				= _sim;
		aQtl._ignore			= _ignore;
		aQtl._crossName			= _crossName;
		aQtl._crossType			= _crossType;
		aQtl._crossSize			= _crossSize;
		aQtl._effectAddictive	= _effectAddictive;
		aQtl._expPlace			= _expPlace;
		aQtl._expDate			= _expDate;
	}


	/**
	 * Returns true if the location belongs to the map element; false otherwise
	 * @param positionStart
	 * @param positionEnd
	 * @return if the location belongs to the map element
	 */
	@Override
	public boolean isPositionWithin(double positionStart, double positionEnd){
		return positionStart <= _positionEnd && positionEnd >= _positionStart;
	}

	/**
	 * Sets the Qtl trait
	 * @param trait The Qtl trait
	 */
	public void	setTrait(String trait){
		_trait = trait;
	}

	/**
	 * Gets the Qtl trait
	 * @return The Qtl trait
	 */
	public String	getTrait(){
		return _trait;
	}

	/**
	 * Gets the qtl's origin map
	 * @return The qtl's origin map
	 */
	public void	setOriginMap(String originMap){
		_originMap = originMap;
	}

	/**
	 * Gets the qtl's origin map
	 * @return The qtl's origin map
	 */
	public String	getOriginMap(){
		return _originMap;
	}

	/**
	 * Sets the Qtl trait ontology ID
	 * @param trait The Qtl trait ontology ID
	 */
	public void	setTraitOntologyId(String traitOntologyID){
		_traitOntologyID = traitOntologyID;
	}

	/**
	 * Gets the Qtl trait ontology ID
	 * @return The Qtl trait ontology ID
	 */
	public String	getTraitOntologyId(){
		return _traitOntologyID;
	}

	/**
	 * Sets the Qtl lodscrore
	 * @param lodscrore The Qtl lodscrore
	 */
	public void	setLodscore(double lodscrore){
		_lodscore = lodscrore;
	}

	/**
	 * Gets the Qtl lodscrore
	 * @return The Qtl lodscrore
	 */
	public double	getLodscore(){
		return _lodscore;
	}

	/**
	 * Sets the Qtl r2
	 * @param r2 The Qtl r2
	 */
	public void	setR2(double r2){
		_r2 = r2;
	}

	/**
	 * Gets the Qtl r2
	 * @return The Qtl r2
	 */
	public double	getR2(){
		return _r2;
	}

	/**
	 * Sets the Qtl positionStart
	 * @param positionStart The Qtl positionStart
	 */
	public void	setPositionStart(double positionStart){
		_positionStart = positionStart;
	}

	/**
	 * Gets the Qtl positionStart
	 * @return The Qtl positionStart
	 */
	public double	getPositionStart(){
		return _positionStart;
	}

	/**
	 * Sets the Qtl positionEnd
	 * @param positionEnd The Qtl positionEnd
	 */
	public void	setPositionEnd(double positionEnd){
		_positionEnd = positionEnd;
	}

	/**
	 * Gets the Qtl positionEnd
	 * @return The Qtl positionEnd
	 */
	public double	getPositionEnd(){
		return _positionEnd;
	}

	/**
	 * Sets the Qtl sim
	 * @param sim The Qtl sim
	 */
	public void	setSim(String sim){
		_sim = sim;
	}

	/**
	 * Gets the Qtl sim
	 * @return The Qtl sim
	 */
	public String	getSim(){
		return _sim;
	}

	/**
	 * Sets the Qtl crossSize
	 * @param crossSize The Qtl crossSize
	 */
	public void	setCrossSize(int crossSize){
		_crossSize = crossSize;
	}

	/**
	 * Gets the Qtl crossSize
	 * @return The Qtl crossSize
	 */
	public int	getCrossSize(){
		return _crossSize;
	}

	/**
	 * Sets the Qtl crossType
	 * @param crossType The Qtl crossType
	 */
	public void	setCrossType(String crossType){
		_crossType = crossType;
	}

	/**
	 * Gets the Qtl crossType
	 * @return The Qtl crossType
	 */
	public String	getCrossType(){
		return _crossType;
	}

	/**
	 * Sets the Qtl crossName
	 * @param crossName The Qtl crossName
	 */
	public void	setCrossName(String crossName){
		_crossName = crossName;
	}

	/**
	 * Gets the Qtl crossName
	 * @return The Qtl crossName
	 */
	public String	getCrossName(){
		return _crossName;
	}

	/**
	 * Sets the Qtl effectAddictive
	 * @param effectAddictive The Qtl effectAddictive
	 */
	public void	setEffectAddictive(double effectAddictive){
		_effectAddictive = effectAddictive;
	}

	/**
	 * Gets the Qtl effectAddictive
	 * @return The Qtl effectAddictive
	 */
	public double	getEffectAddictive(){
		return _effectAddictive;
	}

	/**
	 * Sets the Qtl experiment place
	 * @param effectAddictive The Qtl experiment place
	 */
	public void	setExpPlace(String expPlace){
		_expPlace = expPlace;
	}

	/**
	 * Gets the Qtl experiment place
	 * @return The Qtl experiment place
	 */
	public String	getExpPlace(){
		return _expPlace;
	}

	/**
	 * Sets the Qtl experiment date
	 * @param effectAddictive The Qtl experiment date
	 */
	public void	setExpDate(String expDate){
		_expDate = expDate;
	}

	/**
	 * Gets the Qtl experiment date
	 * @return The Qtl experiment date
	 */
	public String	getExpDate(){
		return _expDate;
	}

	/**
	 * This methods returns true if the QTL must be ignored.
	 * 
	 * @return true if the QTL must be ignored.
	 */
	public boolean getIgnore() {
		return _ignore;
	}
	/**
	 * Set the status of the QTL.
	 * 
	 * @param ignore the status of the QTL.
	 */
	public void setIgnore(boolean ignore) {
		this._ignore = ignore;
	}

	/**
	 * Returns a HTML string for a pretty information printing.
	 * @return A HTML string for a pretty information printing.
	 */
	public	String	toHTML(){
		return	"<html>"
				+ "<table>"
				+ " <tr><td>" + "Name"
				+ "  </td><td>" + getName()
				+ " </td></tr>"
				+ " <tr><td>" + "Trait"
				+ "  </td><td>" + getTrait()
				+ " </td></tr>"
				+ " <tr><td>" + "R2"
				+ "  </td><td>" + getR2()
				+ " </td></tr>"
				+ " <tr><td>" + "Position"
				+ "  </td><td>" + getPosition()
				+ " </td></tr>"
				+ "</table>"
				+ "<html>";
	}

		/**
	 * Returns a string for a pretty information printing.
	 * @return A string for a pretty information printing.
	 */
	public	String	toPretty(){
		return	getName() + "\n"
				+ getTrait() + "\n"
				+ NumericalUtilities.doubleToStr(getPosition()) + "cM ("
				+ NumericalUtilities.doubleToStr(getPositionStart()) + "cM - "
				+ NumericalUtilities.doubleToStr(getPositionEnd()) + "cM)\n"
				+ "R2: " + NumericalUtilities.doubleToStr(getR2());
	}

	private String	_trait				= null;
	private String	_originMap			= null;
	private String	_traitOntologyID	= null;
	private double	_lodscore			= 0;
	private double	_r2					= 0;
	private double	_positionStart		= 0;
	private double	_positionEnd		= 0;
	private String	_sim				= null;
	private boolean	_ignore				= false;
	private String	_crossName			= null;
	private String	_crossType			= null;
	private int		_crossSize			= 0;
	private double	_effectAddictive	= 0;
	private String	_expPlace			= null;
	private String	_expDate			= null;
}
