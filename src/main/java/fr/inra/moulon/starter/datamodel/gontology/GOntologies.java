/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 25-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.gontology;

import java.util.Map;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class GOntologies {
	protected	GOntologies(){
		_ontologies = new HashMap<Integer, GOntology>();
		_ontologiesList = new ArrayList<GOntology>();

		_ontologies.put(GOntology.BIOLOGICAL_PROCESS_ID,	new GOntology(GOntology.BIOLOGICAL_PROCESS_ID));
		_ontologies.put(GOntology.CELL_COMPONENT_ID,		new GOntology(GOntology.CELL_COMPONENT_ID));
		_ontologies.put(GOntology.MOLECULAR_FUNCTION_ID,	new GOntology(GOntology.MOLECULAR_FUNCTION_ID));

		for (Iterator<Integer> it = _ontologies.keySet().iterator(); it.hasNext();) {
			GOntology ontology = _ontologies.get(it.next());

			_ontologiesList.add(ontology);
		}

		_synonyms = new HashMap<String, String>();
	}

	public static GOntologies instance(){
		if (null == _instance){
			synchronized (GOntology.class){
				if (null == _instance){
					_instance = new GOntologies();
				}
			}
		}
		return _instance;
	}

	/**
	 * Returns the wanted term. This methods iterate on its ontologies until
	 * one is found.
	 * @param termID The GOTerm id
	 * @return The wanted term.
	 */
	public GOTerm	getTermTMP(String termID){
		return TMP_terms.get(termID);
	}

	/**
	 * Returns the wanted term. This methods iterate on its ontologies until
	 * one is found.
	 * @param termID The GOTerm id
	 * @return The wanted term.
	 */
	public GOTerm	getTerm(String termID){
		GOTerm		term = null;

		for (Iterator<Integer> it = _ontologies.keySet().iterator(); null == term && it.hasNext();) {
			GOntology	ontology = _ontologies.get(it.next());
			GOTerm		termTMP = null;

			if (null != (termTMP = ontology.getTerm(termID))){
				term = termTMP;
			}
		}

		return term;
	}

//	/**
//	 * Returns the wanted term. This methods iterate on its ontologies until
//	 * one is found.
//	 * @param termID The GOTerm id
//	 * @return The wanted term.
//	 */
//	public GOTerm	getTerm(String termID){
//		GOTerm		term = null;
//
//		for (int i = 0; null == term && i < _ontologiesList.size(); ++i) {
//			GOntology	ontology = _ontologiesList.get(i);
//
//			term = ontology.getTerm(termID);
//		}
//
//		return term;
//	}

	public Integer	getTermNamespace(String termID){
		Integer		res = null;

		for (Iterator<Integer> it = _ontologies.keySet().iterator(); null == res && it.hasNext();) {
			GOntology	ontology = _ontologies.get(it.next());
			GOTerm		termTMP = null;

			if (null != (termTMP = ontology.getTerm(termID))){
				res = termTMP.getNamespace();
			}
		}

		return res;
	}

	/**
	 * Returns an iterator on GOTerm's sons.
	 * @return an iterator on GOTerms's son.
	 */
	public	Iterator<GOTerm>	iteratorSons(String termID){
		Iterator<GOTerm>		it		= null;
		GOTerm					term	= null;

		if (null != (term = getTerm(termID))){
			it = _ontologies.get(term.getNamespace()).iteratorSons(term);
		}

		return it;
	}

	/**
	 * Returns if the goTerm corresponding to the given ID has at least one son.
	 * @return If the goTerm corresponding to the given ID has at least one son.
	 */
	public	boolean	hasSon(String termID){
		GOTerm		term	= null;
		boolean		hasSon	= false;

		if (null != (term = getTerm(termID))){
			hasSon = hasSon(term);
		}

		return hasSon;
	}

	/**
	 * Returns if the given goTerm has at least one son.
	 * @return If the given goTerm has at least one son.
	 */
	public	boolean	hasSon(GOTerm term){
		boolean		hasSon	= false;

		if (null != term){
			hasSon = _ontologies.get(term.getNamespace()).hasSon(term);
		}

		return hasSon;
	}

	/**
	 * Returns an iterator on GOTerm's descendants.
	 * @return an iterator on GOTerms's descendants.
	 */
	public	Iterator<GOTerm>	iteratorDescendants(String termID){
		Iterator<GOTerm>		it		= null;
		GOTerm					term	= null;

		if (null != (term = getTerm(termID))){
			it = _ontologies.get(term.getNamespace()).iteratorDescendants(term);
		}

		return it;
	}

	/**
	 * Returns a set of all given GOTerm's descendents.
	 * @param goTermID The GOTerm
	 * @return A set of all given GOTerm's descendents.
	 */
	public	Set<String>		fillGoTermFamily(String goTermID){
		Set<String>			goTermsFamily = new HashSet<String>();
		Iterator<GOTerm>	it = iteratorDescendants(goTermID);

		goTermsFamily.add(goTermID);
		if (null != it){
			while (it.hasNext()) {
				goTermsFamily.add(it.next().getID());
			}
		}

		return goTermsFamily;
	}

	public GOTerm	getRoot(int classID){
		return _ontologies.get(classID).getRoot();
	}

	public	void	init(String fileName){
		System.out.println("parse");
		if (parse(fileName)){
			System.out.println("find relations");
			findRelations();
			System.out.println("done. nb links : " + NB_LINKS);
			System.out.println("Fill son map");
			fillSonsMap();
			System.out.println("done.");
			System.out.println("initialise depths");
			initDepths();
			System.out.println("done.");
		}
	}

	/**
	 * Notifies the Ontology of a lower level. (for chart name)
	 * @param classID The ontology class ID
	 */
	public	void	lowerLevel(int classID){
		_ontologies.get(classID).lowerLevel();
	}

	/**
	 * Notifies the Ontology of a lower level. (for chart name)
	 * @param classID The ontology class ID
	 */
	public	void	upperLevel(int classID){
		_ontologies.get(classID).upperLevel();
	}

	/**
	 * Returns the GO term ID reference to the given ID. Returns itself if it
	 * already is a reference.
	 * @param synonym
	 * @return The GO term ID reference to the given ID.
	 */
	public	String	getRefGOTermID(String goTermID){
		String		res = _synonyms.get(goTermID);

		return (null != res)?res:goTermID;
	}


	private boolean	parse(String fileName){
		boolean		succeeded = false;

		try {
			_buf = new BufferedReader(new FileReader(fileName));
			parse();
			_buf.close();
			succeeded = true;
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		} catch (IOException ex){
			System.out.println(ex);
		}

		return succeeded;
	}

	private void	parse(){
		String		line	= null;
		GOTerm		term	= null;
		int			classID	= 0;
		String		val		= null;
		String		key		= null;

		try {
			while (null != (line = _buf.readLine()) && !line.equalsIgnoreCase("[Term]")) {}
			while (null != (line = _buf.readLine())) {
				int index = line.indexOf(": ");
				int indexEnd = -1;
				if (index > 0){
					key = line.substring(0, index);
					if (-1 != (indexEnd = line.indexOf(" !"))){
						val = line.substring(index+2, indexEnd);
					}else{
						val = line.substring(index+2, line.length());
					}

					if ("id".equals(key)){
						term = new GOTerm(val);
					}else if ("namespace".equals(key)){
						classID = GOTerm.namespaceToID(val);
						term.setNamespace(classID);
						_ontologies.get(classID).addTerm(term);
						TMP_terms.put(term.getID(), term);
					}else if ("alt_id".equals(key)){
						_synonyms.put(val, term.getID());
						term.addAttributes(key, val);
					}else{
						term.addAttributes(key, val);
					}
				}
			}
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private	void	findRelations(){
		for (Iterator<Integer> it = _ontologies.keySet().iterator(); it.hasNext();) {
			GOntology ontology = _ontologies.get(it.next());

			for (Iterator<GOTerm> itTerms = ontology.iterator(); itTerms.hasNext();) {
				GOTerm				subject = itTerms.next();
				ArrayList<String>	relations = subject.getAttributes("is_a");

				if (null != relations){
					for (Iterator<String> itIs_a = relations.iterator(); itIs_a.hasNext();) {
						String		is_a = itIs_a.next();
						GOTerm		object = _ontologies.get(subject.getNamespace()).getTerm(is_a);

						if (null != object){
							ontology.addRelation(subject, 1, object);
							++NB_LINKS;
						}
					}
				}
			}
			ontology.suppressObsoletes();
		}
	}

	private void	fillSonsMap(){
		for (Iterator<Integer> it = _ontologies.keySet().iterator(); it.hasNext();) {
			GOntology ontology = _ontologies.get(it.next());

			ontology.fillSonsMap();
		}
	}

	private void	initDepths(){
		for (Iterator<Integer> it = _ontologies.keySet().iterator(); it.hasNext();) {
			GOntology ontology = _ontologies.get(it.next());

			ontology.initDepths();
		}
	}

	/**
	 * Returns an iterator the Ontology class identifiers.
	 * @return
	 */
	public	Iterator<Integer>	iterator(){
		return _ontologies.keySet().iterator();
	}

	public	GOntology	getOntology(int ontologyId){
		return _ontologies.get(ontologyId);
	}

	/**
	 * Returns the path to the given termID.
	 * @param goTermID
	 * @return The path to the given termID.
	 */
	public	List<String>	getPath(String goTermID){
		GOTerm				goTerm	= getTerm(goTermID);
		List<String>		path	= null;

		if (null != goTerm){
			path = getOntology(goTerm.getNamespace()).getPath(goTerm);
		}

		return path;
	}

	private	Map<Integer, GOntology>		_ontologies		= null;
	private	List<GOntology>				_ontologiesList	= null;
	private BufferedReader				_buf			= null;
	private	int							NB_LINKS		= 0;
	private	Map<String, String>			_synonyms		= null;

	private	Map<String, GOTerm>			TMP_terms		= new HashMap<String, GOTerm>();

	private volatile static GOntologies	_instance	= null;
}
