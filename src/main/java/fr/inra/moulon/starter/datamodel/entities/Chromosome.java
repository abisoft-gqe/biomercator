/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities;

import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.container.Content;
import java.util.Iterator;
import java.util.List;


public class	Chromosome extends Container<LinkageGroup> {
	public		Chromosome(String name){
		super(name);
	}

	@Override
	public Chromosome	copy(){
		return (Chromosome)super.copy(new Chromosome(super.getName()));
	}

	/**
	 * If the chromsome contains only one linkage group, this on e is returned;
	 * otherwise, the default Linkage group is returned.
	 * @return The default linkage group
	 */
	public LinkageGroup	getDefaultLkg(){
		LinkageGroup	res = null;

		if (this.size() == 1){
			res = this.get(0);
		}else{
			res = this.get(LinkageGroup.NAME_DEFAULT);
		}

		return res;
	}

	/**
	 * Returns the size of all linkage groups joined.
	 * @return The size of all linkage groups joined
	 */
	public Double	getHeight(){
		Double		res = new Double(0);

		for (Iterator<LinkageGroup> it = _listElements.iterator(); it.hasNext();) {
			LinkageGroup linkageGroup = it.next();
			res += linkageGroup.getBioSize();
		}

		return res;
	}

	/**
	 * Returns if the chromosome contains at least one meta analysis.
	 * @return If the chromosome contains at least one meta analysis.
	 */
	public	boolean	hasMetaAnalysis(){
		VisitorEltGet<MetaAnalysis>	v = new VisitorEltGet<MetaAnalysis>(MetaAnalysis.class);
		List<Content>				list = null;

		this.accept(v);
		list = v.getList();

		return list.size() > 0;
	}

	@Override
	public	void accept(Visitor v){
		v.visit(this);
	}
}
