/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 6, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.container;

import fr.inra.moulon.starter.datamodel.entities.file.FileRes;
import fr.inra.moulon.starter.datamodel.entities.utils.ContentNameComparator;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A container is a list of elements; this class contains a list and a map of
 * element to make the insertions, deletion and research fast and easy;
 * this class has been created to allow multiple inclusion due to the wanted
 * data model, for instance : a chromosome contains several linkage groups,
 * which contains several locus.
 * This class is a Generic, and takes a parameter that must extends the Content
 * class. A container extending this very class is therefor a potential
 * parameter.
 * @author sosnowski
 * @param <T> The class of the contained elements.
 */
public abstract class Container<T extends Content> extends Content{
	public	Container(String name){
		super(name);
		_hashElements	= new HashMap<String, T>();
		_listElements	= new ArrayList<T>();
		_files			= new ArrayList<FileRes>();
	}

	/**
	 * Adds an element to the Container. If an element with the same name
	 * already exists, then if it is a container, the new container will be
	 * append to the existing container; no action will be done otherwise, and a
	 * notification will be logged.
	 * @param eltNew The new element to add.
	 */
	public void	add(T eltNew){
		T		eltCur = null;
		String	name = eltNew.getName();

		eltCur = _hashElements.get(name);
		if (null == eltCur){
			_hashElements.put(name, eltNew);
			_listElements.add(eltNew);
		}
		else{
			if (eltCur instanceof Container){
				Container<Content>	cCur = (Container<Content>)eltCur;
				Container<Content>	cNew = (Container<Content>)eltNew;

				for (Iterator<Content> it = cNew.iterator(); it.hasNext();) {
					Content content = it.next();
					cCur.add(content);
				}
			}else{
				System.out.println("Redondance !(Container.java) pour : " + eltCur.getName());
			}
		}
	}

	/**
	 * Delete the element with the corresponding name. If the element isn't found,
	 * no action will be done.
	 * @param name The name of the element to delete.
	 */
	public void	remove(String name){
		T		elt = _hashElements.get(name);
		int		index = -1;

		if (null != elt){
			index = _listElements.indexOf(elt);
			_hashElements.remove(name);
			_listElements.remove(index);
		}
	}


	/**
	 * Returns the number of elements.
	 * @return the number of elements.
	 */
	public int size() {
		return _listElements.size();
	}

	/**
	 * Returns the number of files.
	 * @return the number of files.
	 */
	public int getNbFiles() {
		return _files.size();
	}

	/**
	 * Returns if the Container contains the element with a given name.
	 * @param name The wanted element's name.
	 * @return If the element belongs to the Container.
	 */
	public boolean	contains(String name){
		return _hashElements.containsKey(name);
	}

	/**
	 * Returns the wanted object or null if it doesn't exist.
	 * @param name The wanted element's name.
	 * @return The element if found, null otherwise.
	 */
	public T	get(String name){
		T		elt = null;

		elt = _hashElements.get(name);

		return elt;
	}

	/**
	 * Returns the wanted object at the wanted index. If the index is below zero
	 * or above the number of elements, the method returns null.
	 * @param name The wanted element's name.
	 * @return The element index is correct, null otherwise.
	 */
	public T	get(int index){
		T		elt = null;

		if (index >= 0 && index < _listElements.size()){
			elt = _listElements.get(index);
		}

		return elt;
	}

	/**
	 * Returns an iterator over the elements in this list in proper sequence.
	 * @return an iterator over the elements in this list in proper sequence.
	 */
	public	Iterator<T>	iterator(){
		return new ContainerIterator(_listElements);
	}

	class	ContainerIterator implements Iterator<T>{

		public ContainerIterator(List<T> list) {
			it_list = list;
			it_nbElts = it_list.size();
		}


		@Override
		public boolean hasNext() {
			return it_index < it_nbElts;
		}

		@Override
		public T next() {
			return it_list.get(it_index++);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Not supported yet.");
		}

		private	int		it_nbElts	= 0;
		private	int		it_index	= 0;
		private	List<T>	it_list		= null;
	}

	/**
	 * Returns an iterator over the files in this list in proper sequence.
	 * @return an iterator over the files in this list in proper sequence.
	 */
	public	Iterator<FileRes>	iteratorFiles(){
		return new FilesIterator(_files);
	}

	class	FilesIterator implements Iterator<FileRes>{

		public FilesIterator(List<FileRes> _files) {
			it_list = _files;
			it_nbElts = it_list.size();
		}


		@Override
		public boolean hasNext() {
			return it_index < it_nbElts;
		}

		@Override
		public FileRes next() {
			return it_list.get(it_index++);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Not supported yet.");
		}

		private	int				it_nbElts	= 0;
		private	int				it_index	= 0;
		private	List<FileRes>	it_list		= null;
	}


	/**
	 * Returns the wanted file at the wanted index. If the index is below zero
	 * or above the number of file, the method returns null.
	 * @param index The file's index.
	 * @return The element index is correct, null otherwise.
	 */
	public FileRes	getFile(int index){
		FileRes		fileRes = null;

		if (index >= 0 && index < _files.size()){
			fileRes = _files.get(index);
		}

		return fileRes;
	}

	/**
	 * Removes the given file if it belongs to the container.
	 * @param fileRes The file to remove
	 */
	public	void	removeFile(FileRes fileRes){
		File		file = null;

		if (null != fileRes && _files.contains(fileRes)){
			file = new File(fileRes.getPath());
			if (null != file){
				file.delete();
				_files.remove(fileRes);
			}
		}
	}

	/**
	 * Adds a analysis result file to the container.
	 * @param file
	 */
	public void	addFile(FileRes file){
		_files.add(file);
	}

	public	Container<T>	copy(Container<T> container){
		for (Iterator<T> it = this.iterator(); it.hasNext();) {
			T content = it.next();
			container.add((T)content.copy());
		}

		return container;
	}

	protected	void	sortEltsByName(){
		Collections.sort(_listElements, new ContentNameComparator());
	}

	protected	Map<String, T>	_hashElements	= null;
	protected	List<T>			_listElements	= null;
	private		List<FileRes>	_files			= null;
}
