/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 6, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.container;

import fr.inra.moulon.starter.controller.visitors.Visitor;

/**
 * A Content is an element with few methods; its purpose is to be contained by
 * a Container class.
 * @author sosnowski
 */
public abstract class Content {
	public	Content(String name){
		_name = name;
	}

	/**
	 * Returns an independent copy of the object.
	 * @return An independent copy of the object.
	 */
	public	abstract Content	copy();

	/**
	 * Returns the element's name.
	 * @return the element's name.
	 */
	public	String getName() {
		return _name;
	}

	/**
	 * Sets the element's name.
	 * @param name The element's name.
	 */
	public	void setName(String name) {
		_name = name;
	}

	/**
	 * This accept method allows visitors (the controllers) to 'visit' the
	 * element. For more information, please check the 'Visitor' class comments.
	 * @param v the visitor.
	 */
	public	abstract	void accept(Visitor v);


    /**
	 * Returns a human readable information on the class.
	 * @return the element's name.
	 */
	@Override
	public	String toString() {
		return getName();
	}

	private	String	_name = null;
}
