/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 6, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities;

import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.entities.utils.LocusComparator;
import java.util.Collections;
import java.util.Iterator;

/**
 * A meta model is a model of a meta analysis. It contains the metaQTLs created
 * by the meta analysis.
 * @author sosnowski
 */
public class MetaModel extends Container<MetaQtl> {
	public	MetaModel(String name){
		super(name);
	}

	@Override
	public MetaModel	copy() {
		return (MetaModel)super.copy(new MetaModel(super.getName()));
	}

	@Override
	public	void accept(Visitor v){
		v.visit(this);
	}

	@Override
	public	Iterator<MetaQtl>	iterator(){
		Collections.sort(_listElements, new LocusComparator());

		return _listElements.iterator();
	}
}
