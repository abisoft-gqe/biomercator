/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities;

import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.datamodel.container.Container;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * The MetaAnalysis class is a container of MetaModel with specific methods and
 * attributes.
 * One of its attribute is a map of Qtl designed to list all Qtl used for the
 * analysis.
 * @author sosnowski
 */
public class MetaAnalysis extends Container<MetaModel> {
	public MetaAnalysis(String name, String method){
		super(name);
		_method = method;
		_qtlUsed = new HashSet<Qtl>();
	}

	@Override
	public MetaAnalysis	copy() {
		return (MetaAnalysis)super.copy(new MetaAnalysis(super.getName(), _method));
	}

	/**
	 * Adds a Qtl name to the list (map) of Qtls used for the meta analysis.
	 * @param qtlName the Qtl name to add.
	 */
	public	void	addUsedQtls(Qtl qtl){
		_qtlUsed.add(qtl);
	}

	/**
	 * Returns if a specific Qtl has been used in the meta analysis.
	 * @param qtlName the Qtl name.
	 * @return if the qtl has been used in the meta analysis.
	 */
	public	Boolean	containsQtl(Qtl qtl){
		return _qtlUsed.contains(qtl);
	}

	/**
	 * Sets the meta analysis method.
	 * @param method The meta analysis method.
	 */
	public	void	setMethod(String method){
		_method = method;
	}

	/**
	 * Gets the meta analysis method.
	 * @return The meta analysis method.
	 */
	public	String	getMethod(){
		return _method;
	}

	/**
	 * Returns an iterator over the qtls used.
	 * @return an iterator over the qtls used
	 */
	public	Iterator<Qtl>	iteratorQtlsUsed(){
		return _qtlUsed.iterator();
	}

	@Override
	public	void accept(Visitor v){
		v.visit(this);
	}

	private	String		_method		= null;	// The method's name used
	private	Set<Qtl>	_qtlUsed	= null;	// The set (java.utils) of all qtls used for the meta analysis
}
