/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 23-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.gontology;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class	GOTerm implements Comparable{
	public	final	static	String	ID		= "id";
	public	final	static	String	NAME	= "name";

	public		GOTerm(String id){
		_id = id;
		_attributes = new HashMap<String, ArrayList<String>>();
		_nbSons = new Integer(-1);
		_dynNbSons = new Integer(-1);
		_nb = new Integer(0);
		_depth = new Integer(-1);
	}

	public	static	int	namespaceToID(String namespace){
		int				classID = 0;

		if (null != namespace){
			if (namespace.equalsIgnoreCase("cellular_component")){
				classID = GOntology.CELL_COMPONENT_ID;
			}else if (namespace.equalsIgnoreCase("biological_process")){
				classID = GOntology.BIOLOGICAL_PROCESS_ID;
			}else if (namespace.equalsIgnoreCase("molecular_function")){
				classID = GOntology.MOLECULAR_FUNCTION_ID;
			}else{
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! : " + namespace);
			}
		}

		return classID;
	}

	public	void	addAttributes(String key, String val){
		if (!_attributes.containsKey(key)){
			_attributes.put(key, new ArrayList<String>());
		}
		_attributes.get(key).add(val);
	}

	public	ArrayList<String>	getAttributes(String key){
		return _attributes.get(key);
	}

	/**
	 * Returns the first of contained the list attribute 'name'.
	 * @return The first of contained the list attribute 'name'.
	 */
	public	String		getName(){
		List<String>	atts	= null;
		String			name	= null;

		atts = getAttributes(GOTerm.NAME);
		if (null != atts && atts.size() > 0){
			name = atts.get(0);
		}

		return name;
	}

	public	String	getID(){
		return _id;
	}

	public	void	setNamespace(int classID){
		_classID = classID;
	}

	public	int	getNamespace(){
		return _classID;
	}

	public	void	incrNbSons(){
		++_nbSons;
	}

	public	void	initNbSons(int nbSons){
		_nbSons = nbSons;
	}

	public	void	setDynNbSons(int dynNbSons){
		_dynNbSons = dynNbSons;
	}


	public	Integer	getNbSons(){
		return _nbSons;
	}

	public	void	setNb(int nbSons){
		_nb = nbSons;
	}

	public	Integer	getNb(){
		return _nb;
	}

	public	void	setDepth(int depth){
		_depth = depth;
	}

	public	Integer	getDepth(){
		return _depth;
	}

	public	Integer	getDynNbSons(){
		return _dynNbSons;
	}

	@Override
	public	String	toString(){
		List<String>	names = _attributes.get(NAME);
		String			res = _id;

		if (null != names && names.size() > 0){
			res = names.get(0);
		}

		return res;
	}

	private	String							_id			= null;
	private	int								_classID	= 0;
	private	Map<String, ArrayList<String>>	_attributes	= null;
	private	Integer							_nbSons		= null;
	private	Integer							_dynNbSons	= null;
	private	Integer							_nb			= null;
	private	Integer							_depth		= null;

	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
