/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 23-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.gontology;


public class GOTriple {
	public	GOTriple(GOTerm subject, int relation_type, GOTerm object){
		this.subject = subject;
		this.relation_type = relation_type;
		this.object = object;
	}

	public	GOTerm	subject			= null;
	public	GOTerm	object			= null;
	public	int		relation_type	= 0;
}
