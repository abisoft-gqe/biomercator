/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 23-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.gontology;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class	GOntology {
	public	final	static	int	UNKOWN_ID				= 0;
	public	final	static	int	CELL_COMPONENT_ID		= 1;
	public	final	static	int	BIOLOGICAL_PROCESS_ID	= 2;
	public	final	static	int	MOLECULAR_FUNCTION_ID	= 3;

	/**
	 * Returns the corresponding ontology string to the given ID.
	 * @param ontologyId
	 * @return
	 */
	public	static	String	idToString(Integer ontologyId){
		String				str = null;

		switch (ontologyId){
			case BIOLOGICAL_PROCESS_ID:
				str = "Biological process";
				break;
			case CELL_COMPONENT_ID:
				str = "Cellular component";
				break;
			case MOLECULAR_FUNCTION_ID:
				str = "Molecular function";
				break;
		}

		return str;
	}

	public	GOntology(Integer nameID){
		this(idToString(nameID));
	}

	public	GOntology(String name){
		_name			= name;
		_terms			= new HashMap<String, GOTerm>();
		_triples		= new ArrayList<GOTriple>();
		_subjectTriples	= new HashMap<GOTerm, List<GOTriple>>();
		_objectTriples	= new HashMap<GOTerm, List<GOTriple>>();
		_termsSons		= new HashMap<GOTerm, Set<GOTerm>>();
	}

	/**
	 * Adds a GOTerm into the ontology.
	 * @param term The GOTerm to add
	 */
	public	void	addTerm(GOTerm term){
		_terms.put(term.getID(), term);
	}

	/**
	 * Returns the GOTerm corresponding to the given id.
	 * @param id The term id
	 * @return The GOTerm corresponding to the given id.
	 */
	public	GOTerm	getTerm(String id){
		return _terms.get(id);
	}

	/**
	 * Returns the term's namespace.
	 * @param id The GOTerm id
	 * @return The term's namespace.
	 */
	public	Integer	getTermNamespace(String id){
		GOTerm		term = null;

		return (null != (term = _terms.get(id)))?term.getNamespace():UNKOWN_ID;
	}

	/**
	 * Returns the path to the given term.
	 * @param goTermID
	 * @return The path to the given term.
	 */
	public	List<String>	getPath(GOTerm goTermSon){
		List<String>		path	= new ArrayList<String>();
		GOTerm				goTerm	= goTermSon;
		List<GOTriple>		list	= null;

		while (null != (list = _subjectTriples.get(goTerm)) && list.size() > 0){
			path.add(0, goTerm.getID());
			goTerm = list.get(0).object;
		}

		path.add(0, goTerm.getID());

		return path;
	}

	/**
	 * Adds a relation between 2 given GOTerms.
	 * @param subject The source term
	 * @param relation_type The relation type
	 * @param object The destination term
	 */
	public	void	addRelation(GOTerm subject, int relation_type, GOTerm object){
		GOTriple	triple = new GOTriple(subject, 1, object);

		_triples.add(triple);

		if (!_subjectTriples.containsKey(subject)){
			_subjectTriples.put(subject, new ArrayList<GOTriple>());
		}
		_subjectTriples.get(subject).add(triple);

		if (!_objectTriples.containsKey(object)){
			_objectTriples.put(object, new ArrayList<GOTriple>());
		}
		_objectTriples.get(object).add(triple);
	}

	/**
	 * Returns an iterator on GOTerms.
	 * @return an iterator on GOTerms.
	 */
	public	Iterator<GOTerm>	iterator(){
		return _terms.values().iterator();
	}

	/**
	 * Returns an iterator on GOTerm's sons.
	 * @return an iterator on GOTerms's sons.
	 */
	public	Iterator<GOTerm>	iteratorSons(GOTerm term){
		List<GOTriple>			sons = null;
		List<GOTerm>			list = new ArrayList<GOTerm>();

		if (null != (sons = _objectTriples.get(term))){
			for (Iterator<GOTriple> it = sons.iterator(); it.hasNext();) {
				GOTriple triple = it.next();
				list.add(triple.subject);
			}
		}

		return list.iterator();
	}

	/**
	 * Returns if the given goTerm has at least one son.
	 * @return If the given goTerm has at least one son.
	 */
	public	boolean		hasSon(GOTerm term){
		List<GOTriple>	sons	= null;
		boolean			hasSon	= false;

		if (null != (sons = _objectTriples.get(term))){
			hasSon = sons.size() > 0;
		}

		return hasSon;
	}

	/**
	 * Returns an iterator on GOTerm's descendants.
	 * @return an iterator on GOTerms's descendants.
	 */
	public	Iterator<GOTerm>	iteratorDescendants(GOTerm term){
		Iterator<GOTerm>		it = null;

		if (_termsSons.containsKey(term)){
			it = _termsSons.get(term).iterator();
		}else{
			it = new ArrayList<GOTerm>().iterator();
		}

		return it;
	}

	/**
	 * Removes terms containing the 'is_obsolete' attribute.
	 */
	public	void	suppressObsoletes(){
		for (Iterator<String>	it = _terms.keySet().iterator(); it.hasNext();) {
			GOTerm				term = _terms.get(it.next());
			ArrayList<String>	obsoleteArray = term.getAttributes("is_obsolete");

			if (null != obsoleteArray && obsoleteArray.size() > 0 && obsoleteArray.get(0).equalsIgnoreCase("true")){
				it.remove();
				if (_subjectTriples.containsKey(term)){
					for (Iterator<GOTriple> itRelations = _subjectTriples.get(term).iterator(); itRelations.hasNext();) {
						itRelations.next();
						itRelations.remove();
					}
				}
				if (_objectTriples.containsKey(term)){
					for (Iterator<GOTriple> itRelations = _objectTriples.get(term).iterator(); itRelations.hasNext();) {
						itRelations.next();
						itRelations.remove();
					}
				}
			}
		}
	}

	/**
	 * Fills the map containing all sons of each GOTerm.
	 */
	public	void	fillSonsMap(){
		for (Iterator<GOTerm> it = iterator(); it.hasNext();) {
			GOTerm term = it.next();

			fillSons(term);
		}
	}

	/**
	 * Notifies the Ontology of a lower level. (for chart name)
	 */
	public	void	lowerLevel(){
		++_level;
	}

	/**
	 * Notifies the Ontology of a lower level. (for chart name)
	 */
	public	void	upperLevel(){
		--_level;
	}


	/**
	 * Recursive function filling the sons' sons.
	 * @param term The GOTerm
	 */
	private	void	fillSons(GOTerm term){
		List<GOTriple>	sons = null;

		if (null != (sons = _objectTriples.get(term))){
			if (!_termsSons.containsKey(term)){
				_termsSons.put(term, new HashSet<GOTerm>());
			}
			for (Iterator<GOTriple> itTriple = sons.iterator(); itTriple.hasNext();) {
				GOTerm son = itTriple.next().subject;

				_termsSons.get(term).add(son);
				fillSons(son);
				if (_termsSons.containsKey(son)){
					_termsSons.get(term).addAll(_termsSons.get(son));
				}
			}
		}
	}

	public	void	initDepths(){
		GOTerm		root = getRoot();

		initDepths(root, 0);
	}

	private	void		initDepths(GOTerm term, int depth){
		List<GOTriple>	sons		= null;
		int				sonDepth	= -1;

		term.setDepth(depth);
		if (null != (sons = _objectTriples.get(term))){
			for (Iterator<GOTriple> itTriple = sons.iterator(); itTriple.hasNext();) {
				GOTerm son = itTriple.next().subject;

				sonDepth = son.getDepth();

				if (-1 == sonDepth){
					initDepths(son, depth+1);
				}else{
					if (sonDepth > depth){
						son.setDepth(depth);
					}
				}
			}
		}
	}

	/**
	 * Returns the tree root.
	 * @return
	 */
	public GOTerm	getRoot(){
		GOTerm		root = null;

		for (Iterator<String> it = _terms.keySet().iterator(); it.hasNext();) {
			GOTerm term = _terms.get(it.next());

			if (!_subjectTriples.containsKey(term)){
				root = term;
			}
		}

		return root;
	}

	/**
	 * Returns the ontology's name.
	 * @return the ontology's name.
	 */
	public	String	getName(){
		return _name;
	}

	/**
	 * Returns the ontology's level.
	 * @return the ontology's level.
	 */
	public	String	getLevel(){
		return String.valueOf(_level);
	}

	/**
	 * Set the ontology selected attribute.
	 * @param selected
	 */
	public	void	setSelected(boolean selected){
		_selected = selected;
	}

	/**
	 * Returns if the ontology is selected.
	 * @return If the ontology is selected.
	 */
	public	boolean	getSelected(){
		return _selected;
	}

	private	String						_name				= null;
	private	int							_level				= 0;
	private	Map<String, GOTerm>			_terms				= null;
	private List<GOTriple>				_triples			= null;
	private Map<GOTerm, List<GOTriple>>	_subjectTriples		= null;
	private Map<GOTerm, List<GOTriple>>	_objectTriples		= null;
	private	Map<GOTerm, Set<GOTerm>>	_termsSons			= null;
	private	boolean						_selected			= false;
}
