/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.datamodel.entities.utils;

import fr.inra.moulon.starter.datamodel.container.Content;
import java.text.Collator;
import java.util.Comparator;

/**
 * This comparator is made to compare chromosome's name in an alphabetical
 * order.
 * @author sosnowski
 */
public class ContentNameComparator implements Comparator<Content>{
	@Override
	public int	compare(Content c1, Content c2){
		int		res = 0;

		try {
			int i1 = Integer.parseInt(c1.getName());
			int i2 = Integer.parseInt(c2.getName());

			res = i1 - i2;
		} catch (NumberFormatException e) {
			res = Collator.getInstance().compare(c1.getName(), c2.getName());
		}

		return  res;
	}
}
