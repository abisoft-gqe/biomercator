/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 10-Aug-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.utils;


public class DistributionUtilities {
	public	static	double	fact(double a) {
		double retVal = 1;

		for (double i = a; i > 1; i--) {
			retVal *= i;
		}
		return retVal;
	}

	public	static	double	binomialCoefFact(double n, double k) {
		return (fact(n)) / (fact(k) * fact((n - k)));
	}

	public	static	double	binomialCoefFallingFact(double n, double k) {
		double				res = 1;

		for (int i = 1; i <= k; ++i){
			res *= (n - (k - i)) / i;
		}

		return res;
	}

	public	static	double	binomialCoefGamma(double n, double k) {
		return	Gamma.logGamma(n+1)
				- Gamma.logGamma(k+1)
				- Gamma.logGamma(n-k+1);
	}

	/**
	 * Return the hypergeometric distribution.
	 * @param k
	 * @param n1 the tagged population
	 * @param n2 the untagged population
	 * @param t the number of samples taken
	 * @return the number of tagged samples found
	 */
	public	static	double	hyperGeometric(	double	k,
											double	n1,
											double	n2,
											double	t){
		double				c1	= 0;
		double				c2	= 0;
		double				c3	= 0;
		double				p	= 0;

		if (t > n1 + n2){
			t = n1 + n2;
		}
		if (k > n1 || k > t){
			p = 0;
		}else if (t > n2 && ((k + n2) < t)){
			p = 0;
		}else{
			c1 = binomialCoefGamma(n1, k);
			c2 = binomialCoefGamma(n2, t - k);
			c3 = binomialCoefGamma(n1 + n2, t);

			p = Math.exp(c1 + c2 - c3);
		}

		return p;
	}

	/**
	 * Return the hypergeometric distribution.
	 * @param k
	 * @param n1 the tagged population
	 * @param n2 the untagged population
	 * @param t the number of samples taken
	 * @return the number of tagged samples found
	 */
	public	static	double	hyperGeometricOld(	double	k,
												double	n1,
												double	n2,
												double	t){
		double				c1	= 0;
		double				c2	= 0;
		double				c3	= 0;
		double				p	= 0;

		if (t > n1 + n2){
			t = n1 + n2;
		}
		if (k > n1 || k > t){
			p = 0;
		}else if (t > n2 && ((k + n2) < t)){
			p = 0;
		}else{
			c1 = Math.log(binomialCoefFallingFact(n1, k));
			c2 = Math.log(binomialCoefFallingFact(n2, t - k));
			c3 = Math.log(binomialCoefFallingFact(n1 + n2, t));

			p = Math.exp(c1 + c2 - c3);
		}

		return p;
	}
}
