/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 21-Sep-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.utils;

/**
 * This class contains tools for transforming a string into a HTML tagged
 * String.
 * @author sosnowski
 */
public class HTMLTools {
	public enum Style{BOLD, ITALIC};
	private	static	String	HTML_TAG_START	= "<html>";
	private	static	String	HTML_TAG_END	= "</html>";

	public static String	setHTMLTag(String input){
		return HTML_TAG_START + input + HTML_TAG_END;
	}

	public static String	setFontBold(String input){
		return HTMLTools.setFont(input, HTMLTools.Style.BOLD);
	}

	public static String	setFont(String input, Style... styles){
		String output = input;

		if (input.startsWith(HTML_TAG_START)){
			input.replaceAll(HTML_TAG_START, "");
			input.replaceAll(HTML_TAG_END, "");
		}
		for (int i = 0; i < styles.length; i++) {
			switch (styles[i]){
				case BOLD:
					output = "<b>" + output + "</b>";
					break;
				case ITALIC:
					output = "<i>" + output + "</i>";
					break;
			}
		}

		return setHTMLTag(output);
	}
}
