/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 21-Sep-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.utils;

/**
 * This class is used to store a fraction in several ways :
 *  - Numerator and denominator
 *  - The string illustrating the fraction
 *  - The fraction result
 * @author sosnowski
 */
public class FractionString {
	public	FractionString(int a, int b){
		_a = a;
		_b = b;
		_res = (double)_a/(double)_b;
		_str = a + FRACTION_SEPARATOR + b;
	}

	/**
	 * Returns the fraction's result
	 * @return the fraction's result.
	 */
	public	double	getRes(){
		return _res;
	}

	@Override
	public	String	toString(){
		return _str;
	}

	private	int		_a;
	private	int		_b;
	private	String	_str;
	private	double	_res;

	private	static	String	FRACTION_SEPARATOR	= " / ";
}
