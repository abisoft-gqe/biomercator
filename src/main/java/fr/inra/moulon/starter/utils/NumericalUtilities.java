/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 *
 * @author sosnowski
 */
public class NumericalUtilities {
	/**
	 * This routine returns the value at x of the normal
	 * distribution
	 */
	public	static	double	normalDistrib(double x, double pi, double si){
		double				d1 = 1/(si*Math.sqrt(2*Math.PI));
		float				f1 = (float)(Math.pow((x - pi)/si, 2))/-2;

		return (d1 * Math.exp(f1));
	}

	/**
	 * This routine returns the value at x of a affine function with a shifting
	 * in the abscisse
	 */
	public	static	double	fx(double x, double x1, double x2, double fx2){
		return ((x-x1) * fx2) / (x2 - x1);
	}

	/**
	 * This routine returns a string from the given double with a 2 digits
	 * precision.
	 * @param inValue the double value
	 * @return a string from the given double with a 2 digits precision.
	 */
	public	static	String	doubleToStr(double inValue){
		String shortString = "";

		DecimalFormat format = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.ENGLISH));
		shortString = (format.format(inValue));

		return shortString;
	}

	/**
	 * This routine returns a string from the given double with no digit
	 * precision.
	 * @param inValue the double value
	 * @return a string from the given double with no digit precision.
	 */
	public	static	String	doubleToStrInt(double inValue){
		String shortString = "";

		DecimalFormat format = new DecimalFormat("0", new DecimalFormatSymbols(Locale.ENGLISH));
		shortString = (format.format(inValue));

		return shortString;
	}
}
