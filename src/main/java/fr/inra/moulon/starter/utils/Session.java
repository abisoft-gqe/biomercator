/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 13-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.utils;

import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesignerGenetic;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


/**
 * The session class is designed to keep in active memory all tiny data useful
 * for a better software experience.
 * @author sosnowski
 */
public class Session {
	protected Session(){
		_values = new HashMap<Integer, Object>();
		setDefaultValues();
	}

	protected	final	void	setDefaultValues(){
		setValue(CHOOSER_FILE_DIR, new File(""));
		setValue(LKGDES_SIZE, new Double(600));

		// General display
		setValue(DISPLAY_MAP_NAME,				Boolean.TRUE);
		setValue(DISPLAY_CHR_NAME,				Boolean.TRUE);
		setValue(DISPLAY_LKG_NAME,				Boolean.FALSE);
		setValue(DISPLAY_MINI_LKG,				Boolean.TRUE);

		//	Locus display
		setValue(LOCUS_DISPLAY,					Boolean.TRUE);
		setValue(LOCUS_NAME_DISPLAY,			Boolean.TRUE);
		setValue(LOCUS_POSITION_DISPLAY,		Boolean.TRUE);
		setValue(LOCUS_SHOW_ALL,				Boolean.FALSE);
		//setValue(FONT,							new Font("biomercator_auto", Font.PLAIN, 11));
		setValue(FONT,							new Font("Courier", Font.PLAIN, 11));

		//	Qtl display
		setValue(QTL_DISPLAY,					Boolean.TRUE);
		setValue(QTL_NAME_DISPLAY,				Boolean.FALSE);
		setValue(QTL_DENSITY,					LinkageGroupDesignerGenetic.MODE_QTL_STANDARD);

		//	Meta Qtl display
		setValue(META_QTL_DISPLAY,				Boolean.TRUE);

		// Physic map
		setValue(DISPLAY_PHY_BLOW_ANCHORS,		Boolean.TRUE);
		setValue(DISPLAY_PHY_BLOW_GENES_NO_GO,	Boolean.TRUE);
		setValue(DISPLAY_GENOME_VERSION,		Boolean.TRUE);

		// Misc
		setValue(LOADING_BLOCKING,				Boolean.FALSE);
		setValue(BACKGROUND_NEED_UPDATE,		Boolean.TRUE);
		setValue(COMMON_MARKERS,				Boolean.TRUE);
		
		final Properties properties = new Properties();
		String version = new String();
		try {
			properties.load(getClass().getClassLoader().getResourceAsStream(FileManager.RESOURCES_PROJECT));
			version = properties.getProperty("version");
		} catch (IOException e) {
			version = "Unknown";
			System.out.println("Impossible to find version number");
		}
		setValue(VERSION,						version);
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		setValue(DATE,							sdf.format(date));
	}

	public static Session instance(){
		if (null == _instance){
			synchronized (Session.class){
				if (null == _instance){
					_instance = new Session();
				}
			}
		}
		return _instance;
	}

	public	void	setValue(Integer id, Object value){
		_values.put(id, value);
	}

	public	Object	getValue(Integer id){
		return _values.get(id);
	}

	/**.
	 * Returns the URL corresponding to the given ID. The ID should be picked
	 * from attributes in FileManager class. (RESOURCES_*)
	 * @param id The FileManager class' attribute (starting with RESOURCES_*)
	 * @return The URL.
	 */
	public	URL	getResource(String id){
		return _classLoader.getResource(id);
	}

	public	static	final	Integer	CHOOSER_FILE_DIR				= 1;
	public	static	final	Integer	LKGDES_SIZE						= 2;
	public	static	final	Integer	LKGDES_DEFAULT_SIZE				= 3;
	public	static	final	Integer	RESSOURCE_BUNDLE				= 4;
	public	static	final	Integer	LOCUS_DISPLAY					= 5;
	public	static	final	Integer	LOCUS_NAME_DISPLAY				= 6;
	public	static	final	Integer	LOCUS_POSITION_DISPLAY			= 7;
	public	static	final	Integer	QTL_DISPLAY						= 8;
	public	static	final	Integer	QTL_NAME_DISPLAY				= 9;
	public	static	final	Integer	META_QTL_DISPLAY				= 10;
	public	static	final	Integer	QTL_DENSITY						= 11;
	public	static	final	Integer	LOCUS_SHOW_ALL					= 12;
	public	static	final	Integer	FONT							= 13;
	public	static	final	Integer	FONT_METRICS					= 14;
	public	static	final	Integer	DISPLAY_GENOME_VERSION			= 15;
	public	static	final	Integer	LKGDES_SLIDER_DEFAULT_POS		= 16;
	public	static	final	Integer	LOADING_BLOCKING				= 17;
	public	static	final	Integer	BACKGROUND_NEED_UPDATE			= 18;
	public	static	final	Integer	COMMON_MARKERS					= 19;
	public	static	final	Integer	VERSION							= 20;
	public	static	final	Integer	DATE							= 21;
	public	static	final	Integer	DISPLAY_MAP_NAME				= 22;
	public	static	final	Integer	DISPLAY_CHR_NAME				= 23;
	public	static	final	Integer	DISPLAY_LKG_NAME				= 24;
	public	static	final	Integer	DISPLAY_MINI_LKG				= 25;
	public	static	final	Integer	DISPLAY_PHY_BLOW_ANCHORS		= 26;
	public	static	final	Integer	DISPLAY_PHY_BLOW_GENES_NO_GO	= 27;
	public	static	final	Integer	GO_ANALYSIS_BIAS				= 28;
	public	static	final	Integer	GO_ANALYSIS_REFERENCE			= 29;

	private	Map<Integer, Object>	_values			= null;
	private	ClassLoader				_classLoader	= getClass().getClassLoader();

	// The singleton instance.
	private volatile static Session	_instance;
}
