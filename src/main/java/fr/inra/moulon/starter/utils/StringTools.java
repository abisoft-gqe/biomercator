/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 29-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.utils;

import fr.inra.moulon.starter.datamodel.gontology.GOTerm;
import fr.inra.moulon.starter.datamodel.gontology.GOntologies;
import java.util.Iterator;
import java.util.Set;


public class StringTools {
	public	static	String	getAttribute(String atts, String field){
		String				res			= null;
		int					indexStart	= -1;
		int					indexEnd	= -1;

		if (null != atts && null != field && -1 != (indexStart = atts.indexOf(field))){
			indexStart += field.length();
			if (-1 != (indexEnd = atts.indexOf(";", indexStart))){
				res = atts.substring(indexStart, indexEnd);
			}
		}

		return res;
	}

	public	static String	addAttributeGo(String fieldName, Set<String> goTermIds){
		String				res		= "";
		String				desc	= null;

		if (null != goTermIds){
			for (Iterator<String> it = goTermIds.iterator(); it.hasNext();) {
				String goTermId = it.next();
				GOTerm goTerm = GOntologies.instance().getTerm(goTermId);
				desc = "";
				if (null != goTerm){
					desc = "," + goTerm.getName();
				}

				res += ";" + fieldName + "=" + goTermId + desc;
			}
		}

		return res;
	}

	public	static	String	repeatString(String str, int nbOcc){
		String				res = "";

		for (int i = 0; i < nbOcc; ++i){
			res += str;
		}

		return res;
	}


	/**
	 * Returns a comments empty string (comments character is '#').
	 * <it>For instance, a line su as "(.*)#(.*)" will return only group 1.</it>
	 * @param str the string probably containing a comment
	 * @return A comments empty string
	 */
	public	static String	removeComment(String str){
		String				res = str;
		int					index = 0;

		if (null != str && -1 != (index = str.indexOf("#"))){
			res = str.substring(0, index);
		}

		return res.trim();
	}

	/**
	 * Replace the end of the string by '...' if the string exceeds the maximum
	 * length given.
	 * @param str
	 * @param maxLength
	 * @return the string shortened
	 */
	public static String	shorten(String str, int maxLength){
		if (null != str && maxLength > 3){
			if (str.length() >= maxLength){
				str = str.substring(0, maxLength-3);
				str += "...";
			}
		}

		return str;
	}

	/**
	 * Adds the given extension only if the extension isn't already present.
	 * @param str
	 * @param ext
	 * @return
	 */
	public static String	addExtensionIfNeeded(String str, String ext){
		if (null != str){
			if (!str.endsWith("." + ext)){
				str += "." + ext;
			}
		}

		return str;
	}

	/**
	 * Adds the given suffix to the string before the extension.
	 * @param str
	 * @param suffix
	 * @param ext
	 * @return
	 */
	public static String	addSuffix(String str, String suffix, String ext){
		String				resName	= null;
		String				resExt	= null;
		int					extPos	= -1;

		if (null != str){
			str = addExtensionIfNeeded(str, ext);
			extPos = str.lastIndexOf("." + ext);
			resName = str.substring(0, extPos);
			resExt = str.substring(extPos, str.length());
			str = resName + suffix + resExt;
		}

		return str;
	}

	/**
	 * Replaces the file extension with the given one.
	 * @param str the file name.
	 * @param ext the new extension
	 * @return the new file name with the changed extension.
	 */
	public static String	replaceExtension(String str, String ext){
		if (null != str){
			str = str.replaceAll("[.][^.]+$", "." + ext);
		}

		return str;
	}
	
	public static String replaceGeneTag(String url, String geneid) {
		if (url != null && geneid != null) {
			url = url.replaceAll("<gene_name>", geneid);
		}
		return url;
	}
}
