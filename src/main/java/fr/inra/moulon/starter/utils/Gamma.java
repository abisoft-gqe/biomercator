/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-Aug-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.utils;

/*************************************************************************
 *  Compilation:  javac Gamma.java
 *  Execution:    java Gamma 5.6
 *
 *  Reads in a command line input x and prints Gamma(x) and
 *  log Gamma(x). The Gamma function is defined by
 *
 *        Gamma(x) = integral( t^(x-1) e^(-t), t = 0 .. infinity)
 *
 *  Uses Lanczos approximation formula. See Numerical Recipes 6.1.
 *
 *
 *
 *************************************************************************/

public class Gamma {

	static	double	logGamma(double x) {
		double		tmp = (x - 0.5) * Math.log(x + 4.5) - (x + 4.5);
		double		ser = 1.0	+ 76.18009173    / (x + 0)   - 86.50532033    / (x + 1)
								+ 24.01409822    / (x + 2)   -  1.231739516   / (x + 3)
								+  0.00120858003 / (x + 4)   -  0.00000536382 / (x + 5);
		return tmp + Math.log(ser * Math.sqrt(2 * Math.PI));
	}

	static	double	gamma(double x){
		return Math.exp(logGamma(x));
	}
}
