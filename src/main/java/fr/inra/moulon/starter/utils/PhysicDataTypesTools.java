/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 06-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.utils;

import java.util.ResourceBundle;

/**
 * This enum class lists the physic data types for an easier use in other
 * classes.
 * @author sosnowski
 */
public class PhysicDataTypesTools {
	public	static	String	typeToString(PhysicDataTypes type){
		String				res = null;

		switch (type){
			case STRUCTURAL_ANNOTATIONS:
				res = _bundle.getString("tab_genome_version_annot_struct");
				break;
			case FUNCTIONAL_ANNOTATIONS:
				res = _bundle.getString("tab_genome_version_annot_funct");
				break;
			case ANCHORS:
				res = _bundle.getString("tab_genome_version_anchors");
				break;
			case DBLINKS:
				res = _bundle.getString("tab_genome_version_dblinks");
				break;
		}

		return res;
	}

	private	static	ResourceBundle	_bundle	= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
