/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Locus;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.datamodel.entities.Workspace;
import java.util.Iterator;

/**
 * This visitor is used to parse all the data model from the object it is
 * accepted.
 * This idea here is to create a visitor that is only parsing the data so that
 * other visitors (needing a data browsing) should work only their specific
 * treatment and leave the browsing to this class by extending it.
 * @author sosnowski
 */
public class VisitorBrowser extends Visitor{
	@Override
	public void	visit(Workspace w){
		for (Iterator<Project> it = w.iterator(); it.hasNext();) {
			it.next().accept(this);
		}
	}

	@Override
	public void	visit(Project p){
		for (Iterator<MapGene> it = p.iterator(); it.hasNext();) {
			it.next().accept(this);
		}
	}

	@Override
	public void	visit(MapGene m){
		for (Iterator<Chromosome> it = m.iterator(); it.hasNext();) {
			it.next().accept(this);
		}
	}

	@Override
	public void	visit(Chromosome chr){
		for (Iterator<LinkageGroup> it = chr.iterator(); it.hasNext();) {
			it.next().accept(this);
		}
	}

	@Override
	public void	visit(LinkageGroup lkg){
		// Iterate on markers
		for (Iterator<Locus> it = lkg.iterator(Marker.class); it.hasNext();) {
			it.next().accept(this);
		}

		// Iterate on Qtls
		for (Iterator<Locus> it = lkg.iterator(Qtl.class); it.hasNext();) {
			it.next().accept(this);
		}

		// Iterate on meta Qtls
		for (Iterator<Locus> it = lkg.iterator(MetaQtl.class); it.hasNext();) {
			it.next().accept(this);
		}

		// Iterate on meta-analyses
		for (Iterator<MetaAnalysis> it = lkg.iteratorMetaAnalyses(); it.hasNext();) {
			it.next().accept(this);
		}
	}

	@Override
	public void	visit(MetaAnalysis metaA){
		for (Iterator<MetaModel> it = metaA.iterator(); it.hasNext();) {
			it.next().accept(this);
		}
	}

	@Override
	public void	visit(MetaModel metaModel){
		for (Iterator<MetaQtl> it = metaModel.iterator(); it.hasNext();) {
			it.next().accept(this);
		}
	}

}
