/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 11-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller;

import fr.inra.moulon.starter.controller.visitors.VisitorTreeCreator;
import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Workspace;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.gui.combomodels.ComboBoxLkgModel;
import fr.inra.moulon.starter.gui.combomodels.ComboBoxTraitModel;
import it.cnr.imaa.essi.lablib.gui.checkboxtree.CheckboxTree;
import it.cnr.imaa.essi.lablib.gui.checkboxtree.TreeCheckingModel.CheckingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


public class ControllerGUI {

	/**
	 * Creates and returns an object array containing the existing projects.
	 * @return An object array containing the existing projects.
	 */
	public	static	List<String>	getListExistingProjects(){
		List<Project>				projects	= Controller.getProjects();
		List<String>				res			= new ArrayList<String>();

		for (Iterator<Project> it = projects.iterator(); it.hasNext();) {
			res.add(it.next().getName());
		}
		Collections.sort(res);

		return res;
	}

	/**
	 * Creates and returns a 2 length array of comboBoxes : in first position is
	 * the combobox listing the projects in the workspace; in second position
	 * is the combobox listing the maps contained in the project selected in the
	 * first combobox.(StateChangeListener is already taken care of)
	 * @return
	 */
	public	static	JComboBox[]	getProjectNMapComboBoxes(){
		final	JComboBox		comboProjects	= new JComboBox();
		final	JComboBox		comboMaps		= new JComboBox();
		JComboBox[]				combos			= new JComboBox[2];

		comboMaps.setEnabled(false);
		comboProjects.setModel(new ComboBoxLkgModel(Controller.getWorkspace(), Project.class));
		comboProjects.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				comboMaps.setModel(new ComboBoxLkgModel((Project)comboProjects.getSelectedItem(), MapGene.class));
				comboMaps.setEnabled(true);
			}
		});

		combos[0] = comboProjects;
		combos[1] = comboMaps;
		

		return combos;
	}

	/**
	 * Creates and returns an array of comboBoxes exploring each steps of data :
	 * first the Project combo-box choice, then the mapGene, then the chromosome
	 * combo-box, the linkage group combo-box and finally the button launching
	 * the QTL choosing wizard.
	 * Events between them (data update when a combo is
	 * changed) are already set up.
	 * @return The array of combo-boxes
	 */
	public	static	JComboBox[]	getLkgChoiceBoxes(final JButton qtlChoiceButton){
		final	JComboBox		comboProjects	= new JComboBox();
		final	JComboBox		comboMaps		= new JComboBox();
		final	JComboBox		comboChrs		= new JComboBox();
		final	JComboBox		comboLkgs		= new JComboBox();

		comboMaps.setEnabled(false);
		comboProjects.setModel(new ComboBoxLkgModel(Controller.getWorkspace(), Project.class));
		comboProjects.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				comboMaps.setModel(new ComboBoxLkgModel((Project)comboProjects.getSelectedItem(), MapGene.class));
				comboMaps.setEnabled(true);
				comboChrs.setEnabled(false);
				comboLkgs.setEnabled(false);
				if (null != qtlChoiceButton){
					qtlChoiceButton.setEnabled(false);
				}
			}
		});

		comboChrs.setEnabled(false);
		comboMaps.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				comboChrs.setModel(new ComboBoxLkgModel((MapGene)comboMaps.getSelectedItem(), Chromosome.class));
				comboChrs.setEnabled(true);
				comboLkgs.setEnabled(false);
				if (null != qtlChoiceButton){
					qtlChoiceButton.setEnabled(false);
				}
			}
		});

		comboLkgs.setEnabled(false);
		comboChrs.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				comboLkgs.setModel(new ComboBoxLkgModel((Chromosome)comboChrs.getSelectedItem(), LinkageGroup.class));
				comboLkgs.setEnabled(true);
				if (null != qtlChoiceButton){
					qtlChoiceButton.setEnabled(false);
				}
			}
		});

		if (null != qtlChoiceButton){
			qtlChoiceButton.setEnabled(false);
		}
		comboLkgs.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				if (null != qtlChoiceButton){
					qtlChoiceButton.setEnabled(true);
				}
			}
		});

		return new JComboBox[]{	comboProjects,
								comboMaps,
								comboChrs,
								comboLkgs
								};
	}

	/**
	 * Creates and returns an array of comboBoxes exploring each steps of data :
	 * first the Project combo-box choice, then the mapGene, then the chromosome
	 * combo-box, the linkage group combo-box and finally the meta traits combo
	 * box (if applicable). Events between them (data update when a combo is
	 * changed) are already set up.
	 * @return The array of combo-boxes
	 */
	public	static	JComboBox[]	getMetaAResFileChoiceBoxes(){
		final	JComboBox		comboProjects	= new JComboBox();
		final	JComboBox		comboMaps		= new JComboBox();
		final	JComboBox		comboChrs		= new JComboBox();
		final	JComboBox		comboLkgs		= new JComboBox();
		final	JComboBox		comboMetaA		= new JComboBox();
		final	JComboBox		comboTraits		= new JComboBox();

		comboProjects.setModel(new ComboBoxLkgModel(Controller.getWorkspace(), Project.class));

		comboMaps.setEnabled(false);
		comboProjects.setModel(new ComboBoxLkgModel(Controller.getWorkspace(), Project.class));
		comboProjects.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				comboMaps.setModel(new ComboBoxLkgModel((Project)comboProjects.getSelectedItem(), MapGene.class));
				comboMaps.setEnabled(true);
				comboChrs.setEnabled(false);
				comboLkgs.setEnabled(false);
				comboMetaA.setEnabled(false);
				comboTraits.setEnabled(false);
			}
		});

		comboChrs.setEnabled(false);
		comboMaps.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				comboChrs.setModel(new ComboBoxLkgModel((MapGene)comboMaps.getSelectedItem(), Chromosome.class));
				comboChrs.setEnabled(true);
				comboLkgs.setEnabled(false);
				comboMetaA.setEnabled(false);
				comboTraits.setEnabled(false);
			}
		});

		comboLkgs.setEnabled(false);
		comboChrs.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				comboLkgs.setModel(new ComboBoxLkgModel((Chromosome)comboChrs.getSelectedItem(), LinkageGroup.class));
				comboLkgs.setEnabled(true);
				comboMetaA.setEnabled(false);
				comboTraits.setEnabled(false);
			}
		});

		comboMetaA.setEnabled(false);
		comboLkgs.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public void	itemStateChanged(java.awt.event.ItemEvent evt) {
				comboMetaA.setModel(new ComboBoxLkgModel((LinkageGroup)comboLkgs.getSelectedItem(), MetaAnalysis.class));
				comboMetaA.setEnabled(true);
				comboTraits.setEnabled(false);
			}
		});

		comboTraits.setEnabled(false);
		comboMetaA.addItemListener(new java.awt.event.ItemListener() {
			@Override
			public	void		itemStateChanged(java.awt.event.ItemEvent evt) {
				MetaAnalysis	metaAnalysis	= (MetaAnalysis)comboMetaA.getSelectedItem();
				Project			project			= (Project)comboProjects.getSelectedItem();

				comboTraits.setModel(new ComboBoxTraitModel(FileManager.DIR_SOFT_MAP
														+ project.getName() + "/"
														+ metaAnalysis.getName() + "/"
														+ metaAnalysis.getName()
														+ "_model.txt"));
				comboTraits.setEnabled(true);
			}
		});

	
		return new JComboBox[]{	comboProjects,
								comboMaps,
								comboChrs,
								comboLkgs,
								comboMetaA,
								comboTraits
								};
	}

	/**
	 * Creates and returns a CheckBoxTree. (a tree with check-box nodes)
	 * @return The check-box tree
	 */
	public	static	 JTree	createCheckTree(boolean multiple_checks){
		CheckboxTree		tree = null;
		VisitorTreeCreator	v = new VisitorTreeCreator();

		Workspace.instance().accept(v);
		tree = new CheckboxTree(v.getNode());
		tree.getCheckingModel().setCheckingMode(multiple_checks?CheckingMode.PROPAGATE:CheckingMode.SINGLE);

		return tree;
	}

	/**
	 * Returns the selected maps in the check-box tree.
	 * @param tree The checkBoxTree
	 * @return An not-null list of the selected maps
	 */
	public	static	List<MapGene>	getCheckedMaps(JTree tree){
		TreePath[]					tp			= ((CheckboxTree)tree).getCheckingPaths();
		List<MapGene>				mapsList	= new ArrayList<MapGene>();

		if (null != tp && tp.length > 0){
			for (int i = 0; i < tp.length; ++i){
				DefaultMutableTreeNode def = (DefaultMutableTreeNode)tp[i].getLastPathComponent();
				if (def.getUserObject() instanceof MapGene){
					mapsList.add((MapGene)def.getUserObject());
				}
			}
		}

		return mapsList;
	}
}
