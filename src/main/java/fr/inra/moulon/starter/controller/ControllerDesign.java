/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller;

import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Locus;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.gontology.GOntologies;
import fr.inra.moulon.starter.graphic.designers.PhysicalManager;
import fr.inra.moulon.starter.graphic.utils.MarkerColorManager;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignLinked;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignPhyElt;
import fr.inra.moulon.starter.utils.StringTools;
import java.awt.Color;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ControllerDesign {
	/**
	 * Returns a list filled with markers ShapeDesigns with only attributes
	 * 'bioPosition' and 'color' filled. The ShapeDesign class used is a bigger
	 * one and can contain a string and a link. (array of lines)
	 * @param markers The biological markers list
	 * @return A ShapeDesign list with only attribute 'bioPosition', 'str' and
	 * 'color' filled.
	 */
	public	static	List<ShapeDesign>	getMarkersShapeDesigns(List<Marker> markers){
		List<ShapeDesign>				list		= null;
		ShapeDesignLinked				shapeDes	= null;

		list = new ArrayList<ShapeDesign>();
		for (Iterator<Marker> it = markers.iterator(); it.hasNext();) {
			Marker marker = it.next();

			shapeDes = new ShapeDesignLinked(marker.getPosition());
			shapeDes.str = marker.getName();
			shapeDes.color = MarkerColorManager.instance().getColor(marker.getType());
			list.add(shapeDes);
		}

		return list;
	}

	/**
	 * Returns a list filled with markers ShapeDesigns with only attributes
	 * 'bioPosition' filled. The ShapeDesign class used is a bigger one and can
	 * contain a string and a link. (array of lines)
	 * @param lkg The biological linkage group
	 * @return A chromosome designer filled with corresponding shapes designs
	 * with only attribute 'bioPosition' filled.
	 */
	public	static	List<ShapeDesign>	getMetaQtlsShapeDesigns(MetaModel	metaModel,
																double		start_cM,
																double		end_cM){
		VisitorEltGet					v			= null;
		List<ShapeDesign>				list		= null;
		ShapeDesign						shapeDes	= null;

		v = new VisitorEltGet(MetaQtl.class, start_cM, end_cM);
		metaModel.accept(v);
		list = new ArrayList<ShapeDesign>();
		for (Iterator<MetaQtl> it = v.getList().iterator(); it.hasNext();) {
			MetaQtl metaQtl = it.next();

			shapeDes = new ShapeDesign(metaQtl.getPositionStart(), metaQtl.getPositionEnd());
//			shapeDes = new ShapeDesign(metaQtl.getPosition());
//			shapeDes.bioPosition = metaQtl.getPositionStart();
//			shapeDes.bioPositionEnd = metaQtl.getPositionEnd();
//			shapeDes.str = metaQtl.getName();
			list.add(shapeDes);
		}

		return list;
	}

	/**
	 * Returns a list filled with genes ShapeDesigns with only bioPositions
	 * attributes filled.
	 * @return A list of shapes designs with only bioPositions attributes filled
	 */
	public	static	List<ShapeDesignPhyElt>	getGenesShapeDesigns(	Integer	structural_annotations_id,
																	Integer	functional_annotations_id,
																	String	chromosomeName){
		List<ShapeDesignPhyElt>				list		= new ArrayList<ShapeDesignPhyElt>();
		Map<String, ShapeDesignPhyElt>		mapShapes	= new HashMap<String, ShapeDesignPhyElt>();
		Map<String, Set<String>>			annotations	= PhysicalManager.instance().getFunctionalAnnotation();
		Connection							connection	= DatabaseManager.instance().getConnection();
		Statement							stat		= null;
		ResultSet							rs			= null;
		ShapeDesignPhyElt					shape		= null;
		String								queryFunct	= DatabaseManager.getSelectStatementFunctionalAnnotations(functional_annotations_id);
		String								queryStruct	= DatabaseManager.getSelectStatementStructuralAnnotations(structural_annotations_id, chromosomeName);
		long								start		= 0L;

		try {

			stat = connection.createStatement();

			// Functionnal annotations retrieving
			if (null == annotations){
				System.out.println("Start query : " + queryFunct);
				annotations = new HashMap<String, Set<String>>();
				start = System.currentTimeMillis();
				rs = stat.executeQuery(queryFunct);
				while (rs.next()) {
					String transcript = rs.getString(1);
					String accesssion = rs.getString(2);
					String realAccession = GOntologies.instance().getRefGOTermID(accesssion);
					if (!annotations.containsKey(transcript)){
						annotations.put(transcript, new HashSet<String>());
					}
					annotations.get(transcript).add(realAccession);
				}
				rs.close();
				System.out.println("Done ! (" + (System.currentTimeMillis()-start) + " ms)");
				PhysicalManager.instance().setFunctionalAnnotation(annotations);
			}

			
			// GFFRecords retrieving
			list.clear();
			System.out.println("Start query : " + queryStruct);
			start = System.currentTimeMillis();
			rs = stat.executeQuery(queryStruct);
			while (rs.next()) {
				Set<String>			terms		= null;
				String				attributes	= rs.getString(5);
				String				feature		= rs.getString(1);
				String				id			= StringTools.getAttribute(attributes, "ID=");
				String				parent		= StringTools.getAttribute(attributes, "Parent=");
				ShapeDesignPhyElt	shapeAnnot	= null;

				if ("gene".equals(feature)){
					shape = new ShapeDesignPhyElt(rs.getInt(2), rs.getInt(3));
					shape.id = id;
					shape.strand = "+".equals(rs.getString(4))?true:false;
					mapShapes.put(id, shape);
					list.add(shape);
				}
				if (annotations.containsKey(id)){
					terms = annotations.get(id);
				}
				if (null != terms && (null != (shapeAnnot = mapShapes.get(id))	||
										(null != (shapeAnnot = mapShapes.get(parent))))){
					if (null == shapeAnnot.go_terms){
						shapeAnnot.go_terms = terms;
					}else{
						shapeAnnot.go_terms.addAll(terms);
					}
				}
			}
			rs.close();
			System.out.println("Done ! (" + (System.currentTimeMillis()-start) + " ms)");
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}

		return list;
	}

	/**
	 * Returns a list filled with markers ShapeDesigns with only attributes
	 * 'bioPosition' filled. The ShapeDesign class used is a bigger one and can
	 * contain a string and a link. (array of lines)
	 * @param lkg The biological linkage group
	 * @return A chromosome designer filled with corresponding shapes designs
	 * with only attribute 'bioPosition' filled.
	 */
	public	static	List<ShapeDesign>	getLocusShapeDesigns(	Class			locusClass,
																LinkageGroup	lkg,
																double			start_cM,
																double			end_cM){
		VisitorEltGet					v			= null;
		List<ShapeDesign>				list		= null;
		ShapeDesignLinked				shapeDes	= null;

		v = new VisitorEltGet(locusClass, start_cM, end_cM);
		lkg.accept(v);
		list = new ArrayList<ShapeDesign>();
		for (Iterator<Locus> it = v.getList().iterator(); it.hasNext();) {
			Locus locus = it.next();

			shapeDes = new ShapeDesignLinked(locus.getPosition());
			shapeDes.str = locus.getName();
			list.add(shapeDes);
		}

		return list;
	}

//	/**
//	 * Returns a list filled with qtls ShapeDesigns.
//	 * @param chr The biological chromosome
//	 * @return A chromosome designer filled with corresponding shapes designs.
//	 */
//	public	static	List<ShapeDesign>	getQtlsShapeDesigns(Chromosome chr){
//		VisitorEltGet					v		= null;
//		List<ShapeDesign>				list	= null;
//
//		v = new VisitorEltGet(Qtl.class, Double.MIN_VALUE, Double.MAX_VALUE);
//		chr.accept(v);
//		list = new ArrayList<ShapeDesign>();
//		for (Iterator<Qtl> it = v.getList().iterator(); it.hasNext();) {
//			Qtl qtl = it.next();
//			list.add(new ShapeDesign(ShapeDesign.LINE, 0, 0, 0, 0, Color.BLACK));
//		}
//		return list;
//	}

//	/**
//	 * Returns a list filled with qtls.
//	 * @param chr The biological chromosome
//	 * @return A chromosome designer filled qtls.
//	 */
//	public	static	List<Qtl>	getQtlsShapeDesigns(LinkageGroup lkg){
//		VisitorEltGet			v		= null;
//
//		v = new VisitorEltGet(Qtl.class, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
//		lkg.accept(v);
//
//		return v.getList();
//	}

	/**
	 * Returns a list filled with markers ShapeDesigns.
	 * @param chr The biological chromosome
	 * @return A chromosome designer filled with corresponding shapes designs.
	 */
	public	static	List<ShapeDesign>	getMetaQtlsShapeDesigns(Chromosome chr){
		VisitorEltGet					v		= null;
		List<ShapeDesign>				list	= null;

		v = new VisitorEltGet(MetaQtl.class, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
		chr.accept(v);
		list = new ArrayList<ShapeDesign>();
		for (Iterator<MetaQtl> it = v.getList().iterator(); it.hasNext();) {
			MetaQtl metaQtl = it.next();
			list.add(new ShapeDesign(ShapeDesign.LINE, 0, 0, 0, 0, Color.BLACK));
		}
		return list;
	}
}
