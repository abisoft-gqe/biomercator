/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.datamodel.entities.utils.LocusComparator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This Visitor is made to browse and return a list of NOT ABSTRACT subclasses
 * of the Content class belonging to a given position window.
 * @author sosnowski
 */
public class VisitorSaveStdBioM3  extends VisitorBrowser{
	public	VisitorSaveStdBioM3(File outputMap, File outputQtl) {
		try {
			_writerMapFile = new PrintWriter(outputMap);
			_writerQtlFile = new PrintWriter(outputQtl);
			_writersNotNull = true;
			_iMarker = 0;
			_markersList = new ArrayList<Marker>();
			_qtlsList = new ArrayList<Qtl>();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(VisitorSaveStdBioM3.class.getName()).log(Level.SEVERE, null, ex);
			_writersNotNull = false;
		}
	}

	@Override
	public void	visit(MapGene map){
		if (_writersNotNull){
			_writerMapFile.println("mapName="			+ map.getName());
			_writerMapFile.println("Organism Genus="	+ map.getGenus());
			_writerMapFile.println("Organism Species="	+ map.getSpecies());
			_writerMapFile.println("crossType="			+ map.getCrossType());
			_writerMapFile.println("popSize="			+ map.getCrossSize());
			_writerMapFile.println("mappingCrossType="	+ map.getMappingCrossType());
			_writerMapFile.println("mappingFunction="	+ map.getMappingFunction());
			_writerMapFile.println("mapUnit=cM");
			_writerMapFile.println("mapExpansion="		+ map.getMapExpansion());
			_writerMapFile.println("mapQuality="		+ map.getMapQuality());
			_writerMapFile.println("locusLocation="		+ MapGene.LOCUS_LOCATION_ABSOLUTE);
			_writerQtlFile.println("mapName=" + map.getName());
			super.visit(map);
			_writerMapFile.close();
			_writerQtlFile.close();
		}
	}

	@Override
	public void	visit(Chromosome chr){
		if (_writersNotNull){
			_chrName = chr.getName();
			_writerMapFile.println("chr=" + _chrName);
			super.visit(chr);
		}
	}

	@Override
	public void	visit(LinkageGroup lkg){
		if (_writersNotNull){
			_lkgName = lkg.getName();
			_writerMapFile.println("lg=" + _lkgName);
			super.visit(lkg);

			Collections.sort(_markersList, new LocusComparator());
			for (Iterator<Marker> it = _markersList.iterator(); it.hasNext();) {
				Marker marker = it.next();

				_writerMapFile.println(""
						+ (++_iMarker)		+ SEP
						+ marker.getName()	+ SEP
						+ marker.getPosition()
					);
			}

			Collections.sort(_qtlsList, new LocusComparator());
			for (Iterator<Qtl> it = _qtlsList.iterator(); it.hasNext();) {
				Qtl qtl = it.next();

				_writerQtlFile.println(""
						+ qtl.getName()				+ SEP
						+ qtl.getTrait()			+ SEP
						+ qtl.getTraitOntologyId()	+ SEP
						+ qtl.getExpPlace()			+ SEP
						+ qtl.getExpDate()			+ SEP
						+ _chrName					+ SEP
						+ _lkgName					+ SEP
						+ qtl.getLodscore()			+ SEP
						+ qtl.getR2()				+ SEP
						+ qtl.getPosition()			+ SEP
						+ qtl.getPositionStart()	+ SEP
						+ qtl.getPositionEnd()
					);
			}
			_markersList.clear();
			_qtlsList.clear();
		}
	}

	@Override
	public void	visit(Marker marker){
		_markersList.add(marker);
	}

	@Override
	public void	visit(Qtl qtl){
		_qtlsList.add(qtl);
	}

	private	static	String	SEP	= "\t";
	private	int				_iMarker;
	private	String			_chrName;
	private	String			_lkgName;
	private	List<Marker>	_markersList;
	private	List<Qtl>		_qtlsList;
	private	PrintWriter		_writerMapFile;
	private	PrintWriter		_writerQtlFile;
	private	boolean			_writersNotNull;
}
