/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Project;

/**
 * This Visitor is made to find a meta analysis in the given Container.
 * @author sosnowski
 */
public class	VisitorFindMetaAnalysis extends VisitorBrowser{
	public		VisitorFindMetaAnalysis(){
		_found = false;
	}

	@Override
	public void	visit(Project p){
		if (!_found){
			super.visit(p);
		}
	}

	@Override
	public void	visit(MapGene m){
		if (!_found){
			super.visit(m);
		}
	}

	@Override
	public void	visit(Chromosome chr){
		if (!_found){
			super.visit(chr);
		}
	}

	@Override
	public void	visit(LinkageGroup lkg){
		_found = (lkg.getNbMetaAnalyses() > 0);
	}

	public	boolean	hasMetaAnalysis(){
		return _found;
	}

	private	boolean	_found;
}
