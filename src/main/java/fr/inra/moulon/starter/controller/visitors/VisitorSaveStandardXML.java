/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 18-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;
import fr.inra.moulon.starter.utils.StringTools;
import java.util.Iterator;


public class VisitorSaveStandardXML extends VisitorBrowser{
	public	VisitorSaveStandardXML(String fileName){
		_filePath = fileName;
	}

	public	String	getFinalFileName(){
		return _finalFileName;
	}

	@Override
	public	void	visit(MapGene m){
		_mapName = m.getName();
		_finalFileName = StringTools.addSuffix(_filePath, _mapName, "xml");
		_writer = new SAXWriter(_finalFileName);
		_writer.startDocument();
		_writer.startElement(XMLStandardTagIds.MAPS);
		_writer.startElement(XMLStandardTagIds.MAP, XMLStandardTagIds.NAME, m.getName());
		_writer.addElement(XMLStandardTagIds.GENUS,				m.getGenus());
		_writer.addElement(XMLStandardTagIds.SPECIES,			m.getSpecies());
		_writer.addElement(XMLStandardTagIds.PARENT_NAME,		"");
		_writer.addElement(XMLStandardTagIds.CROSSTYPE,			m.getCrossType());
		_writer.addElement(XMLStandardTagIds.POPSIZE,			String.valueOf(m.getCrossSize()));
		_writer.addElement(XMLStandardTagIds.MAPPING_CROSSTYPE,	m.getMappingCrossType());
		_writer.addElement(XMLStandardTagIds.MAPPING_FUNCTION,	m.getMappingFunction());
		_writer.addElement(XMLStandardTagIds.MAP_UNIT,			m.getMappingUnit());
		_writer.addElement(XMLStandardTagIds.MAP_EXPANSION,		String.valueOf(m.getMapExpansion()));
		_writer.addElement(XMLStandardTagIds.MAP_QUALITY,		String.valueOf(m.getMapQuality()));
		if (m.hasUsedMaps()){
			_writer.startElement(XMLStandardTagIds.USED_MAPS);
			for (Iterator<String> it = m.iteratorUsedMaps(); it.hasNext();) {
				_writer.addElement(XMLStandardTagIds.USED_MAP, new String[]{XMLStandardTagIds.NAME}, new String[]{it.next()});
			}
			_writer.endElement(XMLStandardTagIds.USED_MAPS);
		}
		super.visit(m);
		_writer.endElement(XMLStandardTagIds.MAP);
		_writer.endElement(XMLStandardTagIds.MAPS);
		_writer.endDocument();
	}

	@Override
	public	void	visit(Chromosome chr){
		_writer.startElement(XMLStandardTagIds.CHROMOSOME, XMLStandardTagIds.NAME, chr.getName());
		super.visit(chr);
		_writer.endElement(XMLStandardTagIds.CHROMOSOME);
	}

	@Override
	public	void	visit(LinkageGroup lkg){
		_writer.startElement(XMLStandardTagIds.LINKAGE_GROUP, XMLStandardTagIds.NAME, lkg.getName());
		super.visit(lkg);
		_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
	}

	@Override
	public	void	visit(Marker marker){
		_writer.startElement(XMLStandardTagIds.MARKER, XMLStandardTagIds.NAME, marker.getName());
		_writer.addElement(XMLStandardTagIds.MARKER_ALIASE,		"");
		_writer.addElement(XMLStandardTagIds.MARKER_LOCATION,	String.valueOf(marker.getPosition()));
		_writer.addElement(XMLStandardTagIds.MARKER_TYPE,		marker.getType());
		_writer.endElement(XMLStandardTagIds.MARKER);
	}

	@Override
	public	void	visit(Qtl qtl){
		String		originMap = qtl.getOriginMap();

		_writer.startElement(XMLStandardTagIds.QTL, XMLStandardTagIds.NAME, qtl.getName());
		_writer.addElement(XMLStandardTagIds.QTL_TRAIT,			qtl.getTrait());
		_writer.addElement(XMLStandardTagIds.QTL_ORIGIN_MAP,	(null != originMap)?originMap:_mapName);
		_writer.addElement(XMLStandardTagIds.QTL_TRAIT_OID,		qtl.getTraitOntologyId());
		_writer.addElement(XMLStandardTagIds.QTL_EXP_PLACE,		qtl.getExpPlace());
		_writer.addElement(XMLStandardTagIds.QTL_EXP_DATE,		qtl.getExpDate());
		_writer.addElement(XMLStandardTagIds.QTL_LOD,			String.valueOf(qtl.getLodscore()));
		_writer.addElement(XMLStandardTagIds.QTL_R2,			String.valueOf(qtl.getR2()));
		_writer.addElement(XMLStandardTagIds.QTL_LOCATION,		String.valueOf(qtl.getPosition()));
		_writer.addElement(XMLStandardTagIds.QTL_CI_FROM,		String.valueOf(qtl.getPositionStart()));
		_writer.addElement(XMLStandardTagIds.QTL_CI_TO,			String.valueOf(qtl.getPositionEnd()));
		_writer.addElement(XMLStandardTagIds.QTL_CROSS_NAME,	qtl.getCrossName());
		_writer.addElement(XMLStandardTagIds.QTL_CROSS_SIZE,	String.valueOf(qtl.getCrossSize()));
		_writer.addElement(XMLStandardTagIds.QTL_CROSS_TYPE,	qtl.getCrossType());
		_writer.endElement(XMLStandardTagIds.QTL);
	}

	@Override
	public	void	visit(MetaAnalysis metaA){
		_writer.startElement(XMLStandardTagIds.META_A, XMLStandardTagIds.NAME, metaA.getName());
		_writer.addElement(XMLStandardTagIds.META_A_METHOD, metaA.getMethod());
		_writer.startElement(XMLStandardTagIds.META_A_USED_QTL_IDS);
		for (Iterator<Qtl> it = metaA.iteratorQtlsUsed(); it.hasNext();) {
			_writer.addElement(XMLStandardTagIds.META_A_USED_QTL_ID, new String[]{XMLStandardTagIds.NAME}, new String[]{it.next().getName()});
		}
		_writer.endElement(XMLStandardTagIds.META_A_USED_QTL_IDS);
		super.visit(metaA);
		_writer.endElement(XMLStandardTagIds.META_A);
	}

	@Override
	public	void	visit(MetaModel metaModel){
		_writer.startElement(XMLStandardTagIds.META_MODEL, XMLStandardTagIds.NAME, metaModel.getName());
		super.visit(metaModel);
		_writer.endElement(XMLStandardTagIds.META_MODEL);
	}

	@Override
	public	void	visit(MetaQtl metaQtl){
		_writer.startElement(XMLStandardTagIds.MQTL, XMLStandardTagIds.NAME, metaQtl.getName());
		_writer.addElement(XMLStandardTagIds.MQTL_TRAIT,		metaQtl.getTrait());
		_writer.addElement(XMLStandardTagIds.MQTL_TRAIT_OID,	metaQtl.getTraitOntologyId());
		_writer.addElement(XMLStandardTagIds.MQTL_EXP_PLACE,	metaQtl.getExpPlace());
		_writer.addElement(XMLStandardTagIds.MQTL_EXP_DATE,		metaQtl.getExpDate());
		_writer.addElement(XMLStandardTagIds.MQTL_LOCATION,		String.valueOf(metaQtl.getPosition()));
		_writer.addElement(XMLStandardTagIds.MQTL_CI_FROM,		String.valueOf(metaQtl.getPositionStart()));
		_writer.addElement(XMLStandardTagIds.MQTL_CI_TO,		String.valueOf(metaQtl.getPositionEnd()));
		_writer.addElement(XMLStandardTagIds.MQTL_CROSS_NAME,	metaQtl.getCrossName());
		_writer.addElement(XMLStandardTagIds.MQTL_CROSS_SIZE,	String.valueOf(metaQtl.getCrossSize()));
		_writer.addElement(XMLStandardTagIds.MQTL_CROSS_TYPE,	metaQtl.getCrossType());

		_writer.startElement(XMLStandardTagIds.META_A_MEMERBSHIPS);
		for (Iterator<String> it = metaQtl.iteratorQtlsUsed(); it.hasNext();) {
			String	qtlName = it.next();
			Double	belonging = metaQtl.getBelonging(qtlName);
			String[]	fields	= new String[]{XMLStandardTagIds.NAME, XMLStandardTagIds.BELONGING};
			String[]	values	= new String[]{qtlName, String.valueOf(belonging)};

			if (null != belonging){
				_writer.addElement(XMLStandardTagIds.MQTL_MEMBERSHIP, fields, values);
			}
		}
		_writer.endElement(XMLStandardTagIds.META_A_MEMERBSHIPS);
		_writer.endElement(XMLStandardTagIds.MQTL);
	}

	private SAXWriter	_writer			= null;
	private	String		_mapName		= null;
	private	String		_filePath		= null;
	private	String		_finalFileName	= null;
}
