/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller;

import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.datamodel.container.Container;
import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Locus;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Workspace;
import fr.inra.moulon.starter.datamodel.entities.file.ImageFile;
import fr.inra.moulon.starter.datamodel.entities.file.SeparatedTextFile;
import fr.inra.moulon.starter.graphic.utils.Anchor;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.io.IOException;
//import javax.activation.MimetypesFileTypeMap;
import java.nio.file.Files;

public class Controller {
	/**
	 * Returns a double array containing the smallest and biggest positions of
	 * markers on the specified chromosome.
	 * @param chr The chromosome
	 * @return A double array : [smallest marker position, biggest maker position]
	 */
	public	static	double[]	getExtremsMarkerPositions(LinkageGroup lkg){
		double[]				res = new double[]{Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
		VisitorEltGet			v = new VisitorEltGet(Marker.class, res[1], res[0]);

		lkg.accept(v);
		for (Iterator<Marker> it = v.getList().iterator(); it.hasNext();) {
			Marker marker = it.next();
			res[0] = Math.min(res[0], marker.getPosition());
			res[1] = Math.max(res[1], marker.getPosition());
		}

		return res;
	}

	public	static	Set<String>	getLkgNames(Integer structural_annotations_id){
		Set<String>				names = new HashSet<String>();
		Statement				stat	= null;
		ResultSet				rs		= null;
		Connection				connection = DatabaseManager.instance().getConnection();

		try{
			stat = connection.createStatement();
			System.out.println("Start query  : " + DatabaseManager.getStatementGetSeqNames(structural_annotations_id));
			long start = System.currentTimeMillis();
			rs = stat.executeQuery(DatabaseManager.getStatementGetSeqNames(structural_annotations_id));
			while (rs.next()) {
				names.add(rs.getString(1));
			}
			rs.close();
			System.out.println("Done ! (" + (System.currentTimeMillis()-start) + " ms)");
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}

		return names;
	}

	public	static	List<Anchor>	getAnchors(Integer structural_annotations_id, Integer anchors_id, LinkageGroup	lkg){
		List<Anchor>				anchors			= new ArrayList<Anchor>();
		Anchor						anchor			= null;
		String						queryAnchors	= DatabaseManager.getSelectStatementAnchors(anchors_id, lkg.getName());
		Connection					connection		= DatabaseManager.instance().getConnection();
		Statement					stat			= null;
		ResultSet					rs				= null;
		Locus						locus			= null;
		int							rank			= 0;

		try {
			System.out.println("Start query : " + queryAnchors);
			long start = System.currentTimeMillis();
			stat = connection.createStatement();
			rs = stat.executeQuery(queryAnchors);
			while (rs.next()) {
				if (null != (locus = lkg.get(rs.getString(1)))){
					anchor = new Anchor(locus.getName(), (rs.getInt(2) + rs.getInt(3))/2, locus.getPosition());
					anchor.rank_phy = rank++;
					anchors.add(anchor);
				}
			}
			rs.close();
			System.out.println("Done ! (" + (System.currentTimeMillis()-start) + " ms)");
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}

		return anchors;
	}

	public	static	double[]	getExtremsPhysicPositions(Integer structural_annotations_id, String chromosomeName){
		double[]				res = new double[]{0, 0};
		Statement				stat	= null;
		ResultSet				rs		= null;
		Connection				connection = DatabaseManager.instance().getConnection();

		try{
			stat = connection.createStatement();
			System.out.println("Query get extrems : " + DatabaseManager.getStatementChromosomeExtremes(structural_annotations_id, chromosomeName));
			rs = stat.executeQuery(DatabaseManager.getStatementChromosomeExtremes(structural_annotations_id, chromosomeName));
			while (rs.next()) {
				res[0] = rs.getInt(1);
				res[1] = rs.getInt(2);
			}
			rs.close();
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}

		return res;
	}

	public	static	Workspace	getWorkspace(){
		return Workspace.instance();
	}

	public	static	int	getNbContained(Container c){
		int				res = 0;

		if (c instanceof LinkageGroup){
			res = ((LinkageGroup)c).getNbMetaAnalyses();
		}else{
			res = c.size();
		}

		return res + c.getNbFiles();
	}

	public	static	Content	getContained(Container c, int index){
		LinkageGroup		lkg		= null;
		Content				content	= null;
		int					nbMetaA	= 0;

		if (c instanceof LinkageGroup){
			lkg = (LinkageGroup)c;
			nbMetaA = lkg.getNbMetaAnalyses();
			if (index < nbMetaA){					//	Returning meta analysis content
				content = lkg.getMetaAnalysis(index);
			}else{									//	Returning FileRes
				content = c.getFile(index - c.size());
			}
		}else{
			if (index < c.size()){					//	Returning standard content
				content = c.get(index);
			}else{									//	Returning FileRes
				content = c.getFile(index - c.size());
			}
		}

		return content;
	}

	/**
	 * Returns the project with the given name; if none exists in the Workspace,
	 * a project will be created.
	 * @param projectName
	 */
	public	static	Project	getProject(String projectName){
		if (!Workspace.instance().contains(projectName)){
			Workspace.instance().add(new Project(projectName));
		}
		return Workspace.instance().get(projectName);
	}

	/**
	 * Returns the existing projects list.
	 * @return The existing projects list.
	 */
	public static	List<Project>	getProjects(){
		List<Project>				projects = new ArrayList<Project>();

		for (Iterator<Project> it = Workspace.instance().iterator(); it.hasNext();) {
			projects.add(it.next());

		}

		return projects;
	}

	/**
	 * Adds the given project to the Workspace.
	 * @param project the project
	 */
	public	static	void	addProject(Project project){
		if (null != project){
			Workspace.instance().add(project);
		}
	}

	/**
	 * Adds the content of a project created during an analysis. ie, the project
	 * won't be added to the Workspace, but into the project with the given
	 * name. If no project with the given name exists, it will be created.
	 * @param p
	 */
	public	static	void	addMapToProject(String projectName, MapGene mapGene){
		Project				project = null;

		if (null != projectName){
			project = getProject(projectName);
			project.add(mapGene);
		}
	}

	/**
	 * Adds a flat file to the workspace.
	 * @param file The file to add
	 * @param projectName The project where to add the file
	 */
	public	static	void	addFileToProject(File file, String projectName){
		Project				project		= null;
		String				mimeType	= null;

		if (null != projectName){
			project = getProject(projectName);
			if (null != project){
//				mimeType = new MimetypesFileTypeMap().getContentType(file);
				try {
					mimeType = Files.probeContentType(file.toPath());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (mimeType.startsWith("image/")){
					project.addFile(new ImageFile(file.getName(), file.getAbsolutePath()));
				}else{
					project.addFile(new SeparatedTextFile(file.getName(), file.getAbsolutePath(), "\t"));
				}
			}
		}
	}

	/**
	 * Adds a flat file to the meta analysis with the corresponding name in the
	 * project.
	 * @param file The file to add
	 * @param projectName The project where to add the file
	 */
	public	static	void			addMetaFileToProject(File file, String projectName, String metaAName){
		Project						project		= null;
		String						mimeType	= null;
		VisitorEltGet<MetaAnalysis>	v			= new VisitorEltGet<MetaAnalysis>(MetaAnalysis.class);

		if (null != projectName){
			project = getProject(projectName);
			if (null != project){
				project.accept(v);
				for (Iterator<Content> it = v.getList().iterator(); it.hasNext();) {
					MetaAnalysis metaA = (MetaAnalysis)it.next();
					
					if (metaA.getName().equals(metaAName)){
//						mimeType = new MimetypesFileTypeMap().getContentType(file);
						try {
							mimeType = Files.probeContentType(file.toPath());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (mimeType.startsWith("image/")){
							metaA.addFile(new ImageFile(file.getName(), file.getAbsolutePath()));
						}else{
							metaA.addFile(new SeparatedTextFile(file.getName(), file.getAbsolutePath(), "\t"));
						}
					}
				}
			}
		}
	}

	/**
	 * Changes a map name.
	 * @param projectName The project containing the project
	 * @param mapNameOld The old name
	 * @param mapNameNew The new name
	 */
	public	static	void	changeMapName(String projectName, String mapNameOld, String mapNameNew){
		Project				project	= null;
		MapGene				mapGene	= null;

		if (Workspace.instance().contains(projectName)){
			project = Workspace.instance().get(projectName);
			if (project.contains(mapNameOld)){
				mapGene = project.get(mapNameOld);
				mapGene.setName(mapNameNew);
			}
			
		}
	}

	/**
	 * Returns the list of elements (of class c) in the given LinkageGroup.
	 * @param lkg the linkage group
	 * @param c The wanted class
	 * @return the list of elements (of class c) in the given LinkageGroup
	 */
	public	static	List	getElements(Container container, Class c){
		VisitorEltGet		v	= null;

		v = new VisitorEltGet(c);
		container.accept(v);

		return v.getList();
	}

	/**
	 * Returns the list of elements (of class c) in the given LinkageGroup in
	 * the given window.
	 * @param lkg the linkage group
	 * @param c The wanted class
	 * @param positionStart The window start position
	 * @param positionEnd The window end position
	 * @return the list of elements (of class c) in the given LinkageGroup
	 */
	public	static	List	getElements(Container	container,
										Class		c,
										double		positionStart,
										double		positionEnd){
		VisitorEltGet		v	= null;

		v = new VisitorEltGet(c, positionStart, positionEnd);
		container.accept(v);

		return v.getList();
	}

	/**
	 * Returns the content situated at the given 'path'. If no content can be
	 * found (wrong 'path', or no content), the method will return null.
	 * @return The content situated at the given 'path'. If no content can be
	 * found returns null.
	 */
	public	static	Content	getContent(String[] containersName){
		Content				content		= Workspace.instance();
		Container			container	= Workspace.instance();
		String				eltName		= null;

		for (int i = 0; i < containersName.length && null != container; ++i){
			try{
				container = (Container)content;
			}catch(Exception e){
				container = null;
			}

			if (null != container){
				eltName = containersName[i];
				if (container.contains(eltName)){
					content = container.get(eltName);
				}
			}
		}

		return content;
	}

	public	static	MapGene	getMap(String projectName, String mapName){
		Project				project	= null;
		MapGene				map		= null;

		if (Workspace.instance().contains(projectName)){
			project = Workspace.instance().get(projectName);
			if (project.contains(mapName)){
				map = project.get(mapName);
			}
		}

		return map;
	}
}
