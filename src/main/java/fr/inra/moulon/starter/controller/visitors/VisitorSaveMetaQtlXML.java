/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 18-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.fileTools.utils.XMLMetaQTLTagIds;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * This methods parses the Data in order to
 * @author sosnowski
 */
public class VisitorSaveMetaQtlXML extends VisitorBrowser{
	public	VisitorSaveMetaQtlXML(String fileName){
		_writer = new SAXWriter(fileName);
	}

	public	VisitorSaveMetaQtlXML(String fileName, List<Qtl> dubious){
		this(fileName);
		_dubious = new HashSet<Qtl>();

		if (null != dubious){
			for (Iterator<Qtl> it = dubious.iterator(); it.hasNext();) {
				Qtl qtl = it.next();
				_dubious.add(qtl);
			}
		}
	}

	@Override
	public void	visit(MapGene m){
		_writer.startDocument();
		_writer.startElement(XMLMetaQTLTagIds.GENOME_MAP, XMLMetaQTLTagIds.NAME, m.getName());
		_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
							new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
							new String[]{XMLMetaQTLTagIds.PROP_CROSS_SIZE, String.valueOf(m.getCrossSize())});
		_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
							new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
							new String[]{XMLMetaQTLTagIds.PROP_CROSS_TYPE, m.getCrossType()});
		_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
							new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
							new String[]{XMLMetaQTLTagIds.PROP_MAPPING_UNIT, m.getMappingUnit()});
		_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
							new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
							new String[]{XMLMetaQTLTagIds.PROP_MAPPING_FUN, m.getMappingFunction()});
		super.visit(m);
		_writer.endElement(XMLMetaQTLTagIds.GENOME_MAP);
		_writer.endDocument();
	}

	@Override
	public void	visit(LinkageGroup lkg){
		_writer.startElement(XMLMetaQTLTagIds.LINKAGE_GROUP, 
				               new String[] {XMLMetaQTLTagIds.NAME},
				               new String[] {lkg.getChrName()+"%"+lkg.getName()});  
		super.visit(lkg);
		_writer.endElement(XMLMetaQTLTagIds.LINKAGE_GROUP);
	}

	@Override
	public void	visit(Marker marker){
		_writer.startElement(	XMLMetaQTLTagIds.LOCUS,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.TYPE, XMLMetaQTLTagIds.POSITION},
								new String[]{marker.getName(), "M", String.valueOf(marker.getPosition())});
		_writer.endElement(XMLMetaQTLTagIds.LOCUS);
	}

	@Override
	public void	visit(Qtl qtl){
		if (null == _dubious ||	!_dubious.contains(qtl)){
			_writer.startElement(	XMLMetaQTLTagIds.LOCUS,
									new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.TYPE, XMLMetaQTLTagIds.POSITION},
									new String[]{qtl.getName(), "Q", String.valueOf(qtl.getPosition())});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_CI_FROM, String.valueOf(qtl.getPositionStart())});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_CI_TO, String.valueOf(qtl.getPositionEnd())});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_CROSS_NAME, qtl.getCrossName()});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_CROSS_SIZE, String.valueOf(qtl.getCrossSize())});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_CROSS_TYPE, qtl.getCrossType()});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_LOD, String.valueOf(qtl.getLodscore())});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_EFFECT_ADD, String.valueOf(qtl.getEffectAddictive())});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_R2, String.valueOf(qtl.getR2())});

			_writer.addElement(	XMLMetaQTLTagIds.PROPERTY,
								new String[]{XMLMetaQTLTagIds.NAME, XMLMetaQTLTagIds.VALUE},
								new String[]{XMLMetaQTLTagIds.PROP_QTL_TRAIT, qtl.getTrait()});


			_writer.endElement(XMLMetaQTLTagIds.LOCUS);
		}
	}

	private SAXWriter	_writer		= null;
	private	Set<Qtl>	_dubious	= null;
}
