/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.datamodel.entities.Workspace;


public class VisitorPrinter extends VisitorBrowser{
	@Override
	public void	visit(Workspace w){
		System.out.println("w");
		super.visit(w);
	}

	@Override
	public void	visit(Project p){
		System.out.println("p");
		super.visit(p);
	}

	@Override
	public void	visit(MapGene m){
		System.out.println("m");
		super.visit(m);
	}

	@Override
	public void	visit(MetaAnalysis metaA){
		System.out.println("metaA");
		super.visit(metaA);
	}

	@Override
	public void	visit(Chromosome chr){
		System.out.println("chr");
		super.visit(chr);
	}

	@Override
	public void	visit(LinkageGroup lkg){
		System.out.println("lkg");
		super.visit(lkg);
	}

	@Override
	public void	visit(Marker marker){
		System.out.println("marker");
		super.visit(marker);
	}

	@Override
	public void	visit(Qtl qtl){
		System.out.println("qtl");
		super.visit(qtl);
	}

	@Override
	public void	visit(MetaQtl mqtl){
		System.out.println("mqtl");
		super.visit(mqtl);
	}
}
