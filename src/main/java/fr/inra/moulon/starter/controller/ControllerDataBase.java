/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 02-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller;

import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.datamodel.entities.AbstractQtl;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.graphic.designers.PhysicalManager;
import fr.inra.moulon.starter.utils.NumericalUtilities;
import fr.inra.moulon.starter.utils.StringTools;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Set;


public class ControllerDataBase {
	public	static	void	writeInFileGenes(	String		path,
												Integer		structural_annotations_id,
												String		seqname,
												boolean		allFeatures){
		writeInFileGenes(path, structural_annotations_id, seqname, allFeatures, null, null, null, null);
	}

	/**
	 * Writes the genes contained within the given positions into a file located
	 * at the given path.
	 * name.
	 * @param path The output file path
	 * @param start_pb The starting pb
	 * @param end_pb The end pb
	 * @param seqname The seqname (chromosome name)
	 */
	public	static	void	writeInFileGenes(	String		path,
												Integer		structural_annotations_id,
												String		seqname,
												boolean		genesOnly,
												Double		start_pb,
												Double		end_pb,
												Set<String>	goGenesFamily,
												MetaModel	metaModel){
		Connection			connection	= DatabaseManager.instance().getConnection();
		File				file		= new File(path);
		BufferedWriter		buffer		= null;
		String				query		= null;

		query = DatabaseManager.getSelectStatementStructuralAnnotationsBounded(structural_annotations_id, seqname, genesOnly, start_pb, end_pb);

		try {
			buffer = new BufferedWriter(new FileWriter(file));
			if (null != goGenesFamily){
				parseFilter(buffer, connection, structural_annotations_id, query, start_pb, end_pb, goGenesFamily, metaModel);
			}else{
				parseFilter(buffer, connection, structural_annotations_id, query, start_pb, end_pb, metaModel);
			}
			buffer.close();
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}catch(IOException ex){
			System.out.println(ex.getMessage());
		}
	}

	/**
	 * Writes to the given buffer the chromosome GFF3 line even if the
	 * chromosome feature isn't found in the loaded database.
	 * @param buffer
	 * @param connection
	 * @param structural_annotations_id
	 * @param seqname
	 * @throws SQLException
	 * @throws IOException
	 */
	private	static	void	writeChrLine(	BufferedWriter	buffer,
											Connection		connection,
											Integer			structural_annotations_id,
											String			seqname)throws SQLException, IOException{
		double[]			extrems		= Controller.getExtremsPhysicPositions(structural_annotations_id, seqname);
		Statement			stat		= connection.createStatement();
		String				query		= DatabaseManager.getSelectStatementStructuralAnnotationsChr(structural_annotations_id, seqname);
		ResultSet			rs			= stat.executeQuery(query);

		if (rs.next()) {
			buffer.write(	rs.getString(1)	+ "\t"	+
							rs.getString(2)	+ "\t"	+
							rs.getString(3)	+ "\t"	+
							rs.getInt(4)	+ "\t"	+
							rs.getInt(5)	+ "\t"	+
							rs.getString(6)	+ "\t"	+
							rs.getString(7)	+ "\t"	+
							rs.getString(8)	+ "\t"	+
							rs.getString(9) + "\r\n"
					);
		}else{
			buffer.write(	seqname	+ "\t"	+
							"biomercatorV4"	+ "\t"	+
							"chromosome"	+ "\t"	+
							"1"	+ "\t"	+
							extrems[1]	+ "\t"	+
							"."	+ "\t"	+
							"."	+ "\t"	+
							"." + "\t"	+
							"ID=" + seqname + ";Name=chromosome:1:1:" + extrems[1] + "\r\n"
						);
		}

	}

	/**
	 * Writes to the given buffer the chromosome GFF3 line even if the
	 * chromosome feature isn't found in the loaded database.
	 * @param buffer
	 * @param connection
	 * @param structural_annotations_id
	 * @param seqname
	 * @throws SQLException
	 * @throws IOException
	 */
	private	static	void	writeQtlsLines(	BufferedWriter		buffer,
											String				seqname,
											Double				start_pb,
											Double				end_pb,
											MetaModel			metaModel)throws IOException{
		List<PhysicalManager.PositionnedObject<AbstractQtl>>	aQtls	= PhysicalManager.instance().getIntersectQtls(seqname, start_pb, end_pb, metaModel);
		PhysicalManager.PositionnedObject<AbstractQtl>			posQtl	= null;
		AbstractQtl												aQtl	= null;
		String													line	= null;

		if (null != aQtls){
			for (int i = 0; i < aQtls.size(); i++) {
				posQtl = aQtls.get(i);
				aQtl = posQtl.getObject();
				line =	seqname				+ "\t"	+
						"biomercatorV4"		+ "\t"	+
						aQtl.classToString().toLowerCase()						+ "\t"	+
						NumericalUtilities.doubleToStrInt(posQtl.getPosStart())	+ "\t"	+
						NumericalUtilities.doubleToStrInt(posQtl.getPosEnd())	+ "\t"	+
						"."					+ "\t"	+
						"."					+ "\t"	+
						"."					+ "\t"	+
						"ID="			+ aQtl.getName()			+ ";" +
						"Trait="		+ aQtl.getTrait()		+ ";" +
						"pos_cM="		+ NumericalUtilities.doubleToStr(aQtl.getPosition())		+ ";" +
						"pos_start_cM="	+ NumericalUtilities.doubleToStr(aQtl.getPositionStart())	+ ";" +
						"pos_end_cM="	+ NumericalUtilities.doubleToStr(aQtl.getPositionEnd());
						if (MetaQtl.class != aQtl.getClass()){
							line += ";r2=" + aQtl.getR2();
						}
						line += "\r\n";
				buffer.write(line);
			}
		}
	}

	private	static	void	parseFilter(BufferedWriter	buffer,
										Connection		connection,
										Integer			structural_annotations_id,
										String			query,
										Double			start_pb,
										Double			end_pb,
										MetaModel		metaModel) throws SQLException, IOException{
		Statement			stat		= connection.createStatement();
		String				id			= null;
		String				attributes	= null;
		ResultSet			rs			= stat.executeQuery(query);
		Set<String>			goTerms		= null;
		String				goTermsStr	= null;
		int					count		= 0;
		String				seqName		= null;
		String				seqNameTmp	= null;
		double[]			extrems		= null;
		boolean				hasWindow	= (null != start_pb && null != end_pb);

		while (rs.next()) {
			++count;
			attributes = rs.getString(9);
			seqNameTmp = rs.getString(1);
			if (null != seqNameTmp && !seqNameTmp.equals(seqName)){
				seqName = seqNameTmp;
				writeChrLine(buffer, connection, structural_annotations_id, seqName);
				if (!hasWindow){
					extrems = Controller.getExtremsPhysicPositions(structural_annotations_id, seqName);
					start_pb = extrems[0];
					end_pb = extrems[1];
				}
				writeQtlsLines(buffer, seqName, start_pb, end_pb, metaModel);
			}
			if (null != (id = getId(attributes))){
				goTerms = PhysicalManager.instance().getGoTermsAssociated(id);
				goTermsStr = StringTools.addAttributeGo("GO", goTerms);
			}else{
				goTermsStr = "";
			}
			buffer.write(	rs.getString(1)	+ "\t"	+
							rs.getString(2)	+ "\t"	+
							rs.getString(3)	+ "\t"	+
							rs.getInt(4)	+ "\t"	+
							rs.getInt(5)	+ "\t"	+
							rs.getString(6)	+ "\t"	+
							rs.getString(7)	+ "\t"	+
							rs.getString(8)	+ "\t"	+
							attributes		+ goTermsStr + "\r\n"
					);
		}
		rs.close();
	}

	private	static	void	parseFilter(BufferedWriter	buffer,
										Connection		connection,
										Integer			structural_annotations_id,
										String			query,
										Double			start_pb,
										Double			end_pb,
										Set<String>		goGenesFamily,
										MetaModel		metaModel) throws SQLException, IOException{
		String				attributes	= null;
		String				id			= null;
		Statement			stat		= connection.createStatement();
		ResultSet			rs			= stat.executeQuery(query);
		Set<String>			goTerms		= null;
		String				goTermsStr	= null;
		int					count		= 0;
		String				seqName		= null;
		String				seqNameTmp	= null;
		double[]			extrems		= null;
		boolean				hasWindow	= (null != start_pb && null != end_pb);

		while (rs.next()) {
			++count;
			attributes = rs.getString(9);
			seqNameTmp = rs.getString(1);
			if (null != seqNameTmp && !seqNameTmp.equals(seqName)){
				seqName = seqNameTmp;
				writeChrLine(buffer, connection, structural_annotations_id, seqName);
				if (!hasWindow){
					extrems = Controller.getExtremsPhysicPositions(structural_annotations_id, seqName);
					start_pb = extrems[0];
					end_pb = extrems[1];
				}
				writeQtlsLines(buffer, seqName, start_pb, end_pb, metaModel);
			}
			if (null != (id = getId(attributes))){
				if (goGenesFamily.contains(id)){
					goTerms = PhysicalManager.instance().getGoTermsAssociated(id);
					goTermsStr = StringTools.addAttributeGo("GO", goTerms);
					buffer.write(	rs.getString(1)	+ "\t"	+
									rs.getString(2)	+ "\t"	+
									rs.getString(3)	+ "\t"	+
									rs.getInt(4)	+ "\t"	+
									rs.getInt(5)	+ "\t"	+
									rs.getString(6)	+ "\t"	+
									rs.getString(7)	+ "\t"	+
									rs.getString(8)	+ "\t"	+
									attributes		+ goTermsStr + "\r\n"
							);
				}
			}
		}
		rs.close();
	}

	private	static	String	getId(String gffAttributes){
		String				id			= null;
		int					indexStart	= 0;
		int					indexStop	= 0;

		if (null != gffAttributes){
			indexStart = gffAttributes.indexOf("ID=");
			if (-1 != indexStart){
				indexStop = gffAttributes.indexOf(";", indexStart+3);
				if (-1 == indexStop){
					indexStop = gffAttributes.length();
				}
				id = gffAttributes.substring(indexStart+3, indexStop);
			}
		}

		return id;
	}
}
