/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.utils;

import fr.inra.moulon.starter.datamodel.entities.Qtl;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author sosnowski
 */
public class SimulatedAnnealingQTL extends SimulatedAnnealing<Qtl>{
	public SimulatedAnnealingQTL(List<Qtl> qtls) {
		super(qtls);
	}

	@Override
	protected double	energy(){
		double			res = 0;
		Qtl				qtl = null;

		for (int i = 0; i < _length; ++i){
			qtl = _list.get(i);

			res += Math.pow(_length-_positions.get(qtl.getName()), 2) * (qtl.getPositionEnd()-qtl.getPositionStart());
		}

		return res;
	}

	@Override
	protected	void	rearrange(){
		int				posNew	= 0;
		boolean			found	= false;

		for (int i = 0; !found && i < 10; ++i){
			_posToMove = (int)Math.floor(Math.random()*_length);
			posNew = (int)Math.floor(Math.random()*_length);
			if (fits(_posToMove, posNew)){	///////////////////////// ELSE CASE
				_qtlToMove = _list.get(_posToMove);
				_posSave = _positions.get(_qtlToMove.getName());
				_positions.put(_qtlToMove.getName(), posNew);
				found = true;
			}
		}
	}

	@Override
	protected	void	undo(){
		_positions.put(_qtlToMove.getName(), _posSave);
	}

	private boolean	fits(int qtlIndex, int pos){
		boolean		res = true;
		Qtl			qtlToMove = _list.get(qtlIndex);

		for (Iterator<Qtl> it = _list.iterator(); it.hasNext();) {
			Qtl qtlTmp = it.next();

			if (_positions.get(qtlTmp.getName()) == pos){
				if (!(
					(qtlToMove.getPositionStart() > qtlTmp.getPositionStart() &&
					 qtlToMove.getPositionStart() > qtlTmp.getPositionEnd())
					 ||
					(qtlToMove.getPositionEnd() < qtlTmp.getPositionStart() &&
					 qtlToMove.getPositionEnd() < qtlTmp.getPositionEnd())))
					res = false;
			}
		}

		return res;
	}

	private int	_posToMove	= 0;
	private int	_posSave	= 0;
	private Qtl	_qtlToMove	= null;
}
