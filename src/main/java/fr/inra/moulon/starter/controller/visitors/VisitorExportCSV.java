/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.utils.LocusComparator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * This Visitor is made to browse and return a list of NOT ABSTRACT subclasses
 * of the Content class belonging to a given position window.
 * @author sosnowski
 */
public class VisitorExportCSV  extends VisitorBrowser{
	public	VisitorExportCSV(File output) throws FileNotFoundException {
		_writer = new PrintWriter(output);
		_markersList = new ArrayList<Marker>();
	}

	@Override
	public void	visit(Project p){
		_writer.println(""
				+ "map" + SEP
				+ "chr" + SEP
				+ "marker1" + SEP
				+ "marker2" + SEP
				+ "distance"
		);
		super.visit(p);
		if (null != _writer){
			_writer.close();
		}
	}

	@Override
	public void	visit(MapGene map){
		_mapName = map.getName();
		super.visit(map);
	}

	@Override
	public void	visit(Chromosome chr){
		Marker	prevMarker;
		double	dist;

		_chrName = chr.getName();
		super.visit(chr);
		prevMarker = null;
		Collections.sort(_markersList, new LocusComparator());
		for (Iterator<Marker> it = _markersList.iterator(); it.hasNext();) {
			Marker marker = it.next();

			if (null != prevMarker){
				dist = marker.getPosition() - prevMarker.getPosition();

				_writer.println(""
						+ _mapName + SEP
						+ _chrName + SEP
						+ prevMarker.getName() + SEP
						+ marker.getName() + SEP
						+ dist
				);
			}
			prevMarker = marker;
		}
		_markersList.clear();
	}

	@Override
	public void	visit(Marker marker){
		_markersList.add(marker);
	}

	private	static	String	SEP	= "\t";
	private	String			_mapName;
	private	String			_chrName;
	private	List<Marker>	_markersList;
	private	PrintWriter		_writer;
}
