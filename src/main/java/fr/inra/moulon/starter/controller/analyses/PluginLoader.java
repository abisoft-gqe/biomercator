/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 8, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.analyses;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.java.plugin.ObjectFactory;
import org.java.plugin.PluginManager;
import org.java.plugin.registry.Extension;
import org.java.plugin.registry.ExtensionPoint;
import org.java.plugin.registry.PluginDescriptor;
import org.java.plugin.PluginManager.PluginLocation;
import org.java.plugin.standard.StandardPluginLocation;


public class PluginLoader {
	public	static	List<IAnalysis>	getPlugins(){
		String						corePath		= "analysis.core";
		PluginDescriptor			core			= null;
		ExtensionPoint				ext_point		= null;
		PluginManager				pluginManager	= fillPluginManager();
		List<IAnalysis>				analyses		= new ArrayList<IAnalysis>();

		if (null != pluginManager){
			core = pluginManager.getRegistry().getPluginDescriptor(corePath);
			ext_point = pluginManager.getRegistry().getExtensionPoint(core.getId(), "Section");

			for (Iterator<Extension> it = ext_point.getConnectedExtensions().iterator(); it.hasNext();) {
				try {
					Extension ext = it.next();
					PluginDescriptor descr = ext.getDeclaringPluginDescriptor();
					pluginManager.activatePlugin(descr.getId());
					ClassLoader classLoader = pluginManager.getPluginClassLoader(descr);
					Class pluginCls = classLoader.loadClass(ext.getParameter("class").valueAsString());
					analyses.add((IAnalysis) pluginCls.newInstance());
				}catch (Exception ex) {
					Logger.getLogger(PluginLoader.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}

		return analyses;
	}

	/**
	 * loadPlugin plugin situated in the 'path' into the application.
	 *
	 * @param path : The directory in which the plugin should be.
	 * @return
	 */
	private	static	PluginManager	fillPluginManager(){
		PluginManager		pluginManager	= ObjectFactory.newInstance().createManager();
		File				pluginsDir		= new File("plugins/");
		File[]				pluginFiles		= null;

		try {
		    pluginFiles = pluginsDir.listFiles(new FilenameFilter() {
				@Override
		        public boolean accept(File dir, String name) {
		            return name.toLowerCase().endsWith(".zip");
		        }
		    });
	        PluginLocation[] locations = new PluginLocation[pluginFiles.length];
	        for (int i = 0; i < pluginFiles.length; i++) {
	        	locations[i] = StandardPluginLocation.create(pluginFiles[i]);
	        }

	        pluginManager.publishPlugins(locations);
	    } catch (Exception e) {
			System.out.println(e.getMessage());
			pluginManager = null;
	    }

		return pluginManager;
	}
}
