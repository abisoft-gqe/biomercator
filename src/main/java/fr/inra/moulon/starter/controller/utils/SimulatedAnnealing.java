/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.utils;

import fr.inra.moulon.starter.datamodel.container.Content;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author sosnowski
 */
public abstract	class SimulatedAnnealing<T extends Content> {
	/**
	 * Returns the current solution's energy.
	 * @return The current solution's energy.
	 */
	protected	abstract	double	energy();

	/**
	 * Sets the chosen solution to the previous one.
	 */
	protected abstract	void	undo();

	/**
	 * Rearranges into a new solution.
	 */
	protected abstract	void	rearrange();


	public SimulatedAnnealing(List<T> list) {
		if (null != list){
			_length = list.size();
			_list = list;
			_positions = new HashMap<String, Integer>();
			for (int i = 0; i < _length; ++i){
				Content content = _list.get(i);
				_positions.put(content.getName(), i);
			}
		}
	}

	/**
	 * Executes the simulated annealing analysis.
	 */
	public void	execute(){
		double	energy = 0;
		double	energyNew = 0;
		int		stabilizer = 0;

		if (null != _list && _list.size() > 0){
			_t = 1;
			energy = energy();
			while (_t > 0.1){
				rearrange();
				energyNew = energy();
				if (energyNew < energy || metropolis(energyNew-energy)){
					energy = energyNew;
				} else {
					undo();
				}
				if (++stabilizer > 10){
					stabilizer = 0;
					_t *= .99;
				}
			}
		}
		compactPositions();
	}

	public Map<String, Integer>	getResult(){
		return _positions;
	}

	public int	getNbCols(){
		int		nbCols = 0;

		for (Iterator<Integer> it = _positions.values().iterator(); it.hasNext();) {
			Integer position = it.next();

			nbCols = Math.max(nbCols, position);
		}

		return nbCols+1;
	}


	private boolean	metropolis(double dEnergy){
		boolean		res = false;
		double		p = 0;
		double		metro = 0;

		p = Math.random();
		metro = Math.exp(-(dEnergy/_t));

		if (p < metro){
			res = true;
		}

		return res;
	}

	/**
	 * Suppresses all empty column and compacts the column positions array.
	 */
	private	void		compactPositions(){
		List<Integer>	emptyPositions	= new ArrayList<Integer>();
		int				index			= 0;
		int[]			pos				= null;
		Set<String>		names			= null;

		// INITIALIZATION
		names = _positions.keySet();
		pos = new int[names.size()];
		for (Iterator<String> it = names.iterator(); it.hasNext();) {
			pos[index++] = _positions.get(it.next());
		}

		// MAIN
		for (int i = 0; i < _length; ++i){
			emptyPositions.add(i);
		}
		for (int i = 0; i < _length; ++i){
			emptyPositions.set(pos[i], -1);
		}
		for (Iterator<Integer> it = emptyPositions.iterator(); it.hasNext();) {
			Integer integer = it.next();
			if (-1 == integer){
				it.remove();
			}
		}
		for (int iPos = 0; iPos < _length; ++iPos){
			index = 0;
			while (index < emptyPositions.size() && pos[iPos] > emptyPositions.get(index)){
				++index;
			}
			pos[iPos] -= index;
		}

		// FINALIZATION
		index = 0;
		for (Iterator<String> it = names.iterator(); it.hasNext();) {
			_positions.put(it.next(), pos[index++]);
		}
	}

	protected	List<T>					_list		= null;
	protected	Map<String, Integer>	_positions	= null;
	protected	int						_length		= 0;
	private		double					_t			= 0;
}
