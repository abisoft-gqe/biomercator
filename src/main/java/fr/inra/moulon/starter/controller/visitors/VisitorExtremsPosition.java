/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Locus;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import java.util.Iterator;

/**
 * This visitor is used to parse all the data model from the object it is
 * accepted.
 * This idea here is to create a visitor that is only parsing the data so that
 * other visitors (needing a data browsing) should work only their specific
 * treatment and leave the browsing to this class by extending it.
 * Several visits can be done as the extreme positions are reseted when a
 * linkage group is visited.
 * @author sosnowski
 */
public class VisitorExtremsPosition extends Visitor{
	@Override
	public void	visit(LinkageGroup lkg){
		// Reseting positions
		_minPos	= Double.POSITIVE_INFINITY;
		_maxPos	= Double.NEGATIVE_INFINITY;

		// Iterate on markers
		for (Iterator<Locus> it = lkg.iterator(Marker.class); it.hasNext();) {
			Marker m = (Marker)it.next();

			_minPos = Math.min(_minPos, m.getPosition());
			_maxPos = Math.max(_maxPos, m.getPosition());
		}
	}

	public	double[]	getExtrems(){
		return new double[]{_minPos, _maxPos};
	}

	private	double	_minPos;
	private	double	_maxPos;
}
