/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Locus;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import java.util.Iterator;

/**
 * This Visitor is made to reverse a linkage group.
 * @author sosnowski
 */
public class	VisitorReverse extends VisitorBrowser{
	public		VisitorReverse(double maxSize){
		_maxSize = maxSize;
	}

	@Override
	public void	visit(LinkageGroup lkg){
		//System.out.println("MetaAnalysis Nb "+lkg.getNbMetaAnalyses());
		for (Iterator<Locus> it = lkg.iterator(); it.hasNext();) {
			Locus locus = it.next();
			//System.out.println("LocusName "+locus.getName()+" of class "+locus.getClass().toString());
			locus.setPosition(_maxSize - locus.getPosition());
			locus.accept(this);
		}
		
		for (int i = 0; i < lkg.getNbMetaAnalyses(); i++) {
			for (Iterator<MetaModel> it = lkg.getMetaAnalysis(i).iterator(); it.hasNext();) {
				MetaModel metamodel = it.next();
				for (Iterator<MetaQtl> itt = metamodel.iterator(); itt.hasNext();) {
					MetaQtl mqtl = itt.next();
					mqtl.setPosition(_maxSize - mqtl.getPosition());
					mqtl.accept(this);
				}
			}
		}
		lkg.sort();
	}

	@Override
	public void	visit(Qtl qtl){
		double	start = qtl.getPositionStart();

		qtl.setPositionStart(_maxSize - qtl.getPositionEnd());
		qtl.setPositionEnd(_maxSize - start);
	}

	@Override
	public void	visit(MetaQtl mqtl){
		double	start = mqtl.getPositionStart();

		mqtl.setPositionStart(_maxSize - mqtl.getPositionEnd());
		mqtl.setPositionEnd(_maxSize - start);
	}

	private	double	_maxSize	= 0;
}
