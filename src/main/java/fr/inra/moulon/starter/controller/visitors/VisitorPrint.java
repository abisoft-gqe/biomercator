/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 06-Jan-2012
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Workspace;


public class VisitorPrint extends VisitorBrowser{
	@Override
	public void	visit(Workspace w){
		System.out.println("Workspace : " + w.getName());
		super.visit(w);
	}

	@Override
	public void	visit(Project p){
		System.out.println("Project : " + p.getName());
		super.visit(p);
	}

	@Override
	public void	visit(MapGene m){
		System.out.println("Map : " + m.getName());
		super.visit(m);
	}
}
