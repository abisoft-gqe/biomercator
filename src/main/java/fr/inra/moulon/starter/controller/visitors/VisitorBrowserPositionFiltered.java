/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Locus;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import java.util.Iterator;

/**
 * This Visitor is made to browse elements only in the position window given in
 * constructor.
 * @author sosnowski
 */
public class	VisitorBrowserPositionFiltered extends VisitorBrowser{
	public		VisitorBrowserPositionFiltered(	double positionStart,
												double positionEnd){
		_positionStart	= positionStart;
		_positionEnd	= positionEnd;
	}

	@Override
	public void	visit(LinkageGroup lkg){
		for (Iterator<Locus> it = lkg.iterator(); it.hasNext();) {
			Locus locus = it.next();
			if (locus.isPositionWithin(_positionStart, _positionEnd)){
				locus.accept(this);
			}
		}

		// Iterate on meta-analyses
		for (Iterator<MetaAnalysis> it = lkg.iteratorMetaAnalyses(); it.hasNext();) {
			it.next().accept(this);
		}
	}

	@Override
	public void	visit(MetaModel metaModel){
		for (Iterator<MetaQtl> it = metaModel.iterator(); it.hasNext();) {
			MetaQtl metaQtl = it.next();
			if (metaQtl.isPositionWithin(_positionStart, _positionEnd)){
				metaQtl.accept(this);
			}
		}
	}

	private	double	_positionStart	= 0;
	private	double	_positionEnd	= 0;
}
