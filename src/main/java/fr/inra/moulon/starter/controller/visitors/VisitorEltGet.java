/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.datamodel.entities.Workspace;
import java.util.ArrayList;
import java.util.List;

/**
 * This Visitor is made to browse and return a list of NOT ABSTRACT subclasses
 * of the Content class belonging to a given position window.
 * @author sosnowski
 */
public class VisitorEltGet<T extends Content>	extends VisitorBrowserPositionFiltered{
	public	VisitorEltGet(Class<?> classToGet){
		super(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
		_list = new ArrayList<Content>();
		_class = classToGet;
	}

	public	VisitorEltGet(	Class<?> classToGet,
							double positionStart,
							double positionEnd){
		super(positionStart, positionEnd);
		_list = new ArrayList<Content>();
		_class = classToGet;
	}

	/**
	 * Returns the list of the contents belonging to the wanted class. If not
	 * element is found, the returned list will be empty.
	 * @return The list of the contents belonging to the wanted class. (cannot
	 * be null)
	 */
	public	List<Content>	getList(){
		return _list;
	}

	/**
	 * Resets the list for a new visit.
	 */
	public	void	resetList(){
		_list = new ArrayList<Content>();
	}

	@Override
	public void	visit(Workspace w){
		if (!insertObject(w)){
			super.visit(w);
		}
	}

	@Override
	public void	visit(Project p){
		if (!insertObject(p)){
			super.visit(p);
		}
	}

	@Override
	public void	visit(MapGene m){
		if (!insertObject(m)){
			super.visit(m);
		}
	}

	@Override
	public void	visit(MetaAnalysis metaA){
		if (!insertObject(metaA)){
			super.visit(metaA);
		}
	}

	@Override
	public void	visit(Chromosome chr){
		if (!insertObject(chr)){
			super.visit(chr);
		}
	}

	@Override
	public void	visit(LinkageGroup lkg){
		if (!insertObject(lkg)){
			super.visit(lkg);
		}
	}

	@Override
	public void	visit(Marker marker){
		if (!insertObject(marker)){
			super.visit(marker);
		}
	}

	@Override
	public void	visit(Qtl qtl){
		if (!insertObject(qtl)){
			super.visit(qtl);
		}
	}

	@Override
	public void	visit(MetaQtl mqtl){
		if (!insertObject(mqtl)){
			super.visit(mqtl);
		}
	}

	/**
	 * This methods checks if the given object's class is the wanted one; in
	 * such a case, the method will insert the object in the list and return
	 * true, false otherwise.
	 * @param obj The given object
	 * @return If the object class is the wanted one
	 */
	private	boolean	insertObject(Content content){
		boolean		res = false;

		if (content.getClass() == _class){
			_list.add(content);
			res = true;
		}

		return res;
	}

	List<Content>	_list	= null;
	Class<?>		_class	= null;
}
