/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.controller.visitors;

import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Workspace;
import fr.inra.moulon.starter.datamodel.entities.utils.ContentNameComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.tree.DefaultMutableTreeNode;


/**
 *
 * @author sosnowski
 */
public class VisitorTreeCreator extends VisitorBrowser {
	public VisitorTreeCreator(){
		super();
		_root = new DefaultMutableTreeNode("Root");
		_projectsMaps = new HashMap<Project, List<MapGene>>();
	}

	@Override
	public void	visit(Workspace w){
		_projects = new ArrayList<Project>();
		super.visit(w);
	}

	@Override
	public void	visit(Project p){
		_maps = new ArrayList<MapGene>();
		super.visit(p);
		_projectsMaps.put(p, _maps);
		_projects.add(p);
	}

	@Override
	public void	visit(MapGene m){
		_maps.add(m);
	}

	public DefaultMutableTreeNode	getNode(){
		DefaultMutableTreeNode		nodeProject	= null;
		DefaultMutableTreeNode		nodeMap		= null;
		List<MapGene>				maps		= null;

		Collections.sort(_projects, new ContentNameComparator());
		for (Iterator<Project> itProjects = _projects.iterator(); itProjects.hasNext();) {
			Project project = itProjects.next();

			nodeProject = new DefaultMutableTreeNode(project.getName());
			nodeProject.setUserObject(project);
			maps = _projectsMaps.get(project);
			Collections.sort(maps, new ContentNameComparator());
			for (Iterator<MapGene> itMaps = maps.iterator(); itMaps.hasNext();) {
				MapGene mapGene = itMaps.next();
				nodeMap = new DefaultMutableTreeNode(mapGene.getName());
				nodeMap.setUserObject(mapGene);
				nodeProject.add(nodeMap);
			}
			if (!nodeProject.isLeaf()){
				_root.add(nodeProject);
			}
		}

		return _root;
	}

	private	List<Project>				_projects		= null;
	private	Map<Project, List<MapGene>>	_projectsMaps	= null;
	private	List<MapGene>				_maps			= null;
	private DefaultMutableTreeNode _root = null;
//	private DefaultMutableTreeNode _project = null;
}

//	@Override
//	public void	visit(Project p){
//		_project = new DefaultMutableTreeNode(p.getName());
//		_project.setUserObject(p);
//		super.visit(p);
//		if (!_project.isLeaf()){
//			_root.add(_project);
//		}
//	}
//
//	@Override
//	public void					visit(MapGene m){
//		DefaultMutableTreeNode	map = new DefaultMutableTreeNode(m.getName());
//
//		map.setUserObject(m);
//		_project.add(map);
//	}
//
//	public DefaultMutableTreeNode	getNode(){
//		return _root;
//	}
//
//	private DefaultMutableTreeNode _root = null;
//	private DefaultMutableTreeNode _project = null;
//}
