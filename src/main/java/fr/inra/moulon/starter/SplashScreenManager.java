/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 15-Nov-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;

import javax.swing.JFrame;
import javax.swing.JLabel;

import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.gui.utils.GridBagLayoutUtils;
import fr.inra.moulon.starter.utils.Session;


public class SplashScreenManager extends JFrame{
	/**
	 * Opens the splash screen for info settings.
	 */
	public SplashScreenManager() {
		addComponentsToMainPane(this.getContentPane());
		/*if (_splash == null) {
			System.out.println("SplashScreen.getSplashScreen() returned null");
		}*/
		setSize(new Dimension(400, 420));
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public	void	close(){
		/*if (null != _splash){
			_splash.close();
		}*/
		this.setVisible(false);
	}

	public	void	setInfo(String info){
		//Graphics2D	g = (Graphics2D)getContentPane().getGraphics();

		//if (null != _splash && null != (g = _splash.createGraphics())){
		//g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		//g.setComposite(AlphaComposite.Clear);
		//g.fillRect(40,330,300,40);
		
		//g.setPaintMode();
		//g.setColor(Color.BLACK);
		//g.drawString(info, 40, 340);
			//_splash.update();
		//getContentPane().update(g);
		//getContentPane().repaint();
		//}
		
		_infos = info;
		paintComponent(getGraphics());
	}
	
	public void paintComponent(Graphics g){
		g.clearRect(40, 380, 300, 40);
		g.drawString(_infos, 40, 390);
	}
	
	protected	void		addComponentsToMainPane(Container pane) {
		GridBagConstraints	c			= new GridBagConstraints();
		JLabel				label		= null;
		pane.setLayout(new GridBagLayout());
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		label = new JLabel(new javax.swing.ImageIcon(Session.instance().getResource(FileManager.RESOURCES_IMAGE_LOGO_UMRGV)));
		GridBagLayoutUtils.set_gbc(c, 0, 0, 1, 1, GridBagConstraints.NONE);
		c.insets = new Insets(5, 5, 5, 5);
		pane.add(label, c);

		label = new JLabel("BioMercator");
		label.setFont(new Font("Lucida Grande", 0, 24));
		GridBagLayoutUtils.set_gbc(c, 1, 0, 1, 1, GridBagConstraints.NONE);
		pane.add(label, c);

		label = new JLabel(	"<html><body>" +
							"BioMercator is a genetic map compilation and meta-analysis software.<br/>" +
							"It includes directly algorithms from BioMercator previous version v2.1, and MetaQTL's algorithms in a plugin way.");
		GridBagLayoutUtils.set_gbc(c, 0, 1, 2, 1, GridBagConstraints.HORIZONTAL);
		c.insets = new Insets(20, 5, 10, 5);
		pane.add(label, c);

		label = new JLabel(	"<html><body>" + 
							"<b>Version : </b>" + Session.instance().getValue(Session.VERSION) + "<br/>" +
							"<b>Authors : </b>Yannick DE OLIVEIRA, Lydia AIT BRAHAM, Olivier SOSNOWSKI, Johann JOETS<br/>" +
							"<b>Company : </b>INRA - UMR GQE-Le Moulon<br/>" +
							"<b>Date : </b>" + Session.instance().getValue(Session.DATE) + "<br/>" +
								"</body></html>");
		GridBagLayoutUtils.set_gbc(c, 0, 2, 2, 1, GridBagConstraints.HORIZONTAL);
		c.weighty = 1;
		c.insets = new Insets(10, 5, 10, 5);
		pane.add(label, c);

		GridBagLayoutUtils.set_gbc(c, 0, 3, 2, 1, GridBagConstraints.NONE);

	}
	String _infos = null;
	//private	final	SplashScreen _splash = SplashScreen.getSplashScreen();
}