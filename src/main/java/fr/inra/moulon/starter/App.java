package fr.inra.moulon.starter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

//import javax.jnlp.ServiceManager;
//import javax.jnlp.SingleInstanceListener;
//import javax.jnlp.SingleInstanceService;
//import javax.jnlp.UnavailableServiceException;
import javax.swing.UIManager;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.datamodel.gontology.GOntologies;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.config.ConfigWriter;
import fr.inra.moulon.starter.fileTools.config.ConfigXMLHandler;
import fr.inra.moulon.starter.gui.MainFrame;
import fr.inra.moulon.starter.utils.Session;

public class App
{
	/**
	 * This method loads the BioMercator V3 XML configuration file.
	 * @param fileName The url to load
	 */
	private	static	void	loadConfig(String path){
		XMLReader			saxReader = null;
		File				file = new File(path);

		if (file.exists()){
			System.out.println("Trying to read Config file ...");
			System.out.println(path);
			try {
				saxReader = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
				//saxReader = XMLReaderFactory.createXMLReader("javax.xml.parsers.SAXParser");
				saxReader.setContentHandler(new ConfigXMLHandler());
				FileInputStream stream = new FileInputStream(file);
				saxReader.parse(new InputSource(stream));
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}else{
			System.out.println("Trying to write Config file ...");
			ConfigWriter.writeConfigFile(path);
		}
	}

	private	static	void	loadConfigFiles(){
		Map<String, String>	configFiles	= new HashMap<String, String>();

		configFiles.put(FileManager.CONFIG_FILE_LOCAL, FileManager.RESOURCES_CONFIG_FILE_LOCAL);
		configFiles.put(FileManager.CONFIG_FILE_PAR, FileManager.RESOURCES_CONFIG_FILE_PAR);
		configFiles.put(FileManager.GENE_ONTOLOGY_OBO_FILE, FileManager.RESOURCES_GENE_ONTOLOGY_OBO_FILE);

		for (Iterator<String> it = configFiles.keySet().iterator(); it.hasNext();) {
			String	fileName = it.next();
			File	file = new File(fileName);

			if (!file.exists()){
				System.out.println("Copying resource "+fileName);
				copyResource(	Session.instance().getResource(configFiles.get(fileName)),
								fileName);
			}
		}
	}

	private	static	void	copyResource(URL urlFrom, String pathTo){
		OutputStream		out		= null;
		InputStream			in		= null;
		byte[]				cbuf	= new byte[2048];
		int					len		= 0;

		try {
			out = new FileOutputStream(pathTo);
			in = urlFrom.openStream();

			while ((len = in.read(cbuf)) > 0){
				out.write(cbuf, 0, len);
			}
		} catch (IOException ex) {
			System.out.println("Problem while copying file "+pathTo);
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

    public static void	main(String[] args)
    {
		final SplashScreenManager screenManager = new SplashScreenManager();
		String			ontologyFileName = FileManager.DIR_SOFT + "gene_ontology_ext.obo";
		List<File>					files;
		_writer.println("STARTING !");
		/*try {
			SingleInstanceService singleInstanceService = (SingleInstanceService)ServiceManager.lookup("javax.jnlp.SingleInstanceService");
			singleInstanceService.addSingleInstanceListener(new SISListener());
		} catch(UnavailableServiceException use) {
			screenManager.setInfo("JWS services not found..."); 
			//System.exit(-1);
		} catch (Exception e) {
			screenManager.setInfo("A problem occured with Java web start ..."); 
		}*/

		screenManager.setInfo("Loading local...");
		Locale.setDefault(Locale.FRENCH);	// For MetaQTL parser

		screenManager.setInfo("Initializing directories...");
		FileManager.initDirectories();

		screenManager.setInfo("Loading configuration...");
		loadConfigFiles();
		loadConfig(FileManager.CONFIG_FILE_LOCAL);
		
		screenManager.setInfo("Loading workspace...");
		FileManager.loadWorkspace();

		screenManager.setInfo("Loading files...");
		files = getFiles(args);
		for (Iterator<File> it = files.iterator(); it.hasNext();) {
			FileManager.loadFromDistant(it.next(), "GnpMap");
		}

		screenManager.setInfo("Loading database...");
		DatabaseManager.instance().getConnection();
		DatabaseManager.instance().init();

		screenManager.setInfo("Loading Gene Ontology...");
		GOntologies.instance().init(ontologyFileName);

		screenManager.setInfo("Loading GUI...");
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {}
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				screenManager.close();
				MainFrame.createAndShowGUI();
			}
		});
    }

	/*static	class	SISListener implements SingleInstanceListener {
		@Override
		public	void	newActivation(String[] strings) {
			try {
				_writer = new PrintWriter("jnlp.txt");
			} catch (FileNotFoundException ex) {
				Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
			}

			_writer.println("NEW ACTIVATION !");
			List<File>	files = getFiles(strings);

			for (Iterator<File> it = files.iterator(); it.hasNext();) {
				FileManager.loadFromDistant(it.next(), "GnpMap");
			}
			MainFrame.refreshExplorer();
			_writer.close();
		}
	}*/

	static	PrintWriter	_writer;
	
	static{
		try {
			_writer = new PrintWriter("jnlp.txt");
		} catch (FileNotFoundException ex) {
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private	static	List<File>	getFiles(String[] args){
		List<File>				files	= new ArrayList<File>();
		File					file	= null;
		URL						url		= null;
	
		_writer.println("args : ");
		if (null != args){
			for (int i = 0; i < args.length; i++) {
				_writer.println("args[" + i + "] : " + args[i]);
				try {
					url = new URL(args[i]);
					if (null != (file = FileManager.fileFromURL(url, i))){
						files.add(file);
					}
				} catch (MalformedURLException ex) {
					Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	
		return files;
	}
}
