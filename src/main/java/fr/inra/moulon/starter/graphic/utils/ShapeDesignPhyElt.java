/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 28-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class ShapeDesignPhyElt extends ShapeDesign{
	public	ShapeDesignPhyElt(double bioPosition, double bioPositionEnd){
		super(bioPosition, bioPositionEnd);
	}

	@Override
	public	ShapeDesignPhyElt	clone(){
		ShapeDesignPhyElt		res = new ShapeDesignPhyElt(bioPosition, bioPositionEnd);

		res.shapeId = shapeId;
		res.x = x;
		res.y = w;
		res.w = w;
		res.h = h;
		res.id = id;
		res.strand = strand;
		if (null != go_terms){
			res.go_terms = new HashSet<String>();
			for (Iterator<String> it = go_terms.iterator(); it.hasNext();) {
				res.go_terms.add(it.next());
			}
		}

		return res;
	}

	public	String		id			= null;	// ID field contained in attributes of GFF3 file (9th column)
	public	boolean		strand		= false;// true means '+' and false means '-'
	public	Set<String>	go_terms	= null;	// GO Terms
}
