/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 29-Apr-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;


public class ColorManager {
	protected ColorManager(){
		setDefaultColors();
	}

	public static ColorManager instance(){
		if (null == _instance){
			synchronized (ColorManager.class){
				if (null == _instance){
					_instance = new ColorManager();
				}
			}
		}
		return _instance;
	}

	public	Color	nextColor(){
		Color		color = null;

		if (_iColor < _colorsList.size()){
			color = _colorsList.get(_iColor++);
		}else{
			_iColor = 0;
//			System.out.println("No more color");
			color = Color.BLACK;
		}

		return color;
	}
	private int	_countColors = 0;

	public	void	prevColor(){
		if (_iColor > 0){
			--_countColors;
			--_iColor;
		}
	}

	public	void	resetColorIndex(){
		_iColor = 0;
	}

	private void	setDefaultColors(){
		_colorsList = new ArrayList<Color>();

		//_colorsList.add(new Color(0,0,0));		//1
		_colorsList.add(new Color(153,51,0));		//53
		_colorsList.add(new Color(51,51,0));		//52
		_colorsList.add(new Color(255,102,0));		//46
		_colorsList.add(new Color(0,0,255));		//55
		_colorsList.add(new Color(128,0,0));		//9
		_colorsList.add(new Color(51,51,51));		//56
		_colorsList.add(new Color(255,0,0));		//3
		_colorsList.add(new Color(0,0,128));		//47
		_colorsList.add(new Color(255,153,0));		//45
		_colorsList.add(new Color(0,51,0));			//51
		_colorsList.add(new Color(153,204,0));		//43
		_colorsList.add(new Color(0,51,102));		//49
		_colorsList.add(new Color(51,51,153));		//16
		_colorsList.add(new Color(128,128,0));		//12
		_colorsList.add(new Color(0,128,0));		//10
		_colorsList.add(new Color(22,131,131));		//14
		_colorsList.add(new Color(102,102,153));	//56
		_colorsList.add(new Color(128,128,128));	//48
		_colorsList.add(new Color(52,153,102));		//50 ?
		_colorsList.add(new Color(51,204,204));		//42
		_colorsList.add(new Color(51,102,255));		//41
		_colorsList.add(new Color(128,0,128));		//50 ?
		_colorsList.add(new Color(149,149,149));	//16
		_colorsList.add(new Color(255,0,255));		//7
		_colorsList.add(new Color(255,204,0));		//44
		_colorsList.add(new Color(255,255,0));		//6
		_colorsList.add(new Color(0,255,0));		//35
		_colorsList.add(new Color(0,255,255));		//8
		_colorsList.add(new Color(0,204,255));		//33
		_colorsList.add(new Color(153,51,102));		//54
		_colorsList.add(new Color(192,192,192));	//15
		_colorsList.add(new Color(255,153,204));	//38
		_colorsList.add(new Color(255,204,153));	//40
		_colorsList.add(new Color(255,255,153));	//36
		_colorsList.add(new Color(204,255,204));	//4
		_colorsList.add(new Color(204,255,255));	//34
		_colorsList.add(new Color(153,204,255));	//37
		_colorsList.add(new Color(204,153,255));	//39
		//_colorsList.add(new Color(255,255,255));	//2
	}

	private volatile	static	ColorManager	_instance	= null;
	private						List<Color>		_colorsList	= null;
	private						int				_iColor		= 0;

}
