/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import java.util.Comparator;


public class AnchorsComparator  implements Comparator<Anchor>{
	public	AnchorsComparator(int unitID){
		super();
		_unitID = unitID;
	}
	@Override
	public int	compare(Anchor a1, Anchor a2){
		int		res = 0;

		if (a1.getPosition(_unitID) < a2.getPosition(_unitID)){
			res = -1;
		}else if (a1.getPosition(_unitID) > a2.getPosition(_unitID)){
			res = 1;
		}
//		res = (int)(e1.getPosition() - e2.getPosition());
		return  res;
	}

	private	int	_unitID = 0;
}
