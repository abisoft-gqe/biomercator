/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 23-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.charts;

import fr.inra.moulon.starter.datamodel.gontology.GOTerm;
import fr.inra.moulon.starter.datamodel.gontology.GOntologies;
import fr.inra.moulon.starter.datamodel.gontology.GOntology;
import fr.inra.moulon.starter.graphic.designers.GOStatisticsManager;
import fr.inra.moulon.starter.graphic.utils.ColorManager;
import fr.inra.moulon.starter.gui.Canvas;
import java.awt.Color;
import java.text.AttributedString;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.PieToolTipGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;


public class Charts {
	public	final	static	int	STATS_LOCAL		= 0;
	public	final	static	int	STATS_GLOBAL	= 1;

	private	final	static	List<Integer>	STATS	= Arrays.asList(STATS_LOCAL,
																	STATS_GLOBAL);


	protected Charts(){
		super();

		_pieDatasets		= new HashMap<Integer, Map<Integer, DefaultPieDataset>>();
		_toolTips			= new HashMap<Integer, Map<String, String>>();
		_chartPanels		= new HashMap<Integer, Map<Integer, ChartPanel>>();
		_customListeners	= new HashMap<Integer, Map<Integer, CustomChartMouseListener>>();
		_colors				= new HashMap<Integer, Map<String, Color>>();
		_colorsParent		= new HashMap<Integer, Map<String, Color>>();
		_hierarchy			= new HashMap<Integer, Map<Integer, Deque<String>>>();

		//	Fill with the statistics IDs
		for (Iterator<Integer> it = STATS.iterator(); it.hasNext();) {
			Integer	statID = it.next();
			_pieDatasets.put(statID, new HashMap<Integer, DefaultPieDataset>());
			_chartPanels.put(statID, new HashMap<Integer, ChartPanel>());
			_customListeners.put(statID, new HashMap<Integer, CustomChartMouseListener>());
			_hierarchy.put(statID, new HashMap<Integer, Deque<String>>());
		}

		//	Fill the colors maps
		for (Iterator<Integer> it = GOntologies.instance().iterator(); it.hasNext();) {
			Integer ontologyID = it.next();
			_colors.put(ontologyID, new HashMap<String, Color>());
			_colorsParent.put(ontologyID, new HashMap<String, Color>());
		}

		//	Fill the global and local statistics charts
		for (Iterator<Integer> it = STATS.iterator(); it.hasNext();) {
			Integer statID = it.next();

			for (Iterator<Integer> itOntologies = GOntologies.instance().iterator(); itOntologies.hasNext();) {
				Integer						ontologyId = itOntologies.next();
				JFreeChart					chart = null;
				ChartPanel					chartPanel = null;
				PieToolTipGenerator			ptt = null;
				CustomChartMouseListener	listener = null;

				_pieDatasets.get(statID).put(ontologyId, new DefaultPieDataset());
				_toolTips.put(ontologyId, new HashMap<String, String>());
				ptt = new CustomPieToolTipGenerator(ontologyId);

				// CHARTS INITIALIZATION
				chart = ChartFactory.createPieChart("",
													_pieDatasets.get(statID).get(ontologyId),
													false,
													true,
													false);
				((PiePlot)chart.getPlot()).setToolTipGenerator(ptt);
				((PiePlot)chart.getPlot()).setLabelGenerator(new CustomLabelGenerator());
				chartPanel = new ChartPanel(chart);
				listener = new CustomChartMouseListener(statID, ontologyId);
				chartPanel.addChartMouseListener(listener);
				_customListeners.get(statID).put(ontologyId, listener);
				_chartPanels.get(statID).put(ontologyId, chartPanel);
				_hierarchy.get(statID).put(ontologyId, new ArrayDeque<String>());
				_hierarchy.get(statID).get(ontologyId).add(GOntologies.instance().getRoot(ontologyId).getID());
			}
		}
	}

	public	void					startFlashing(Integer statsID, GOTerm goTerm){
		int							ontologyId	= goTerm.getNamespace();
		ChartPanel					chartPanel	= _chartPanels.get(statsID).get(ontologyId);
		JFreeChart					chart		= chartPanel.getChart();
		PiePlot						plot		= (PiePlot)chart.getPlot();
		DefaultPieDataset			dataset		= (DefaultPieDataset)plot.getDataset();
		CustomChartMouseListener	lst			= _customListeners.get(statsID).get(ontologyId);
		int							sectionID	= dataset.getIndex(goTerm.getID());

		if (-1 != sectionID){
			lst.startFlashing(goTerm.getID(), sectionID, plot);
		}
	}

	class CustomLabelGenerator implements PieSectionLabelGenerator{

		@Override
		public String generateSectionLabel(PieDataset pd, Comparable cmprbl) {
			return generateLabel(pd, cmprbl);
		}

		@Override
		public AttributedString generateAttributedSectionLabel(PieDataset pd, Comparable cmprbl) {
			return new AttributedString(generateLabel(pd, cmprbl));
		}

		private	String generateLabel(final PieDataset dataset, final Comparable key) {
			GOTerm			goTerm	= null;
			String			name	= null;

			goTerm = GOntologies.instance().getOntology(_selectedOntologyID).getTerm((String)key);
			if (null != goTerm && null != dataset) {
				//nb = dataset.getValue(key);
				name = goTerm.getName();
			}
			if (null == name){
				name = (String)key;
			}

			return name;
		}
	}

	public	static	String	idToString(Integer id){
		String				str = null;

		switch (id){
			case STATS_GLOBAL:
				str = "Global";
				break;
			case STATS_LOCAL:
				str = "Local";
				break;
		}
		return str;
	}

	public static Charts instance(){
		if (null == _instance){
			synchronized (Charts.class){
				if (null == _instance){
					_instance = new Charts();
				}
			}
		}
		return _instance;
	}

	/**
	 * Returns an iterator on chart panels.
	 * @return An iterator on chart panels.
	 */
	public	Iterator<ChartPanel>	iteratorChartPanels(Integer statsID){
		return _chartPanels.get(statsID).values().iterator();
	}

	public	Iterator<Integer>	iteratorStatisticsID(){
		return STATS.iterator();
	}

	public	void	setCanvas(Canvas canvas){
		_canvas = canvas;
	}

	/**
	 * Removes all elements from the given dataset.
	 * @param dataset The dataset to empty
	 */
	private	void			emptyDataset(DefaultPieDataset dataset){
		List<Comparable>	list = dataset.getKeys();

		if (null != list){
			for (Iterator<Comparable> it = list.iterator(); it.hasNext();) {
				Comparable comparable = it.next();
				dataset.remove(comparable);
			}
		}
	}

	/**
	 * Stores the map containing the terms identifiers and their occurrence,
	 * then parses it and adds the GO terms in the dataset corresponding
	 * to the ontology class ID.
	 * @param goTermsCounts The map containing goterms and their occurrence.
	 */
	public	void	drawPieCharts(Map<String, Integer> goTermsCounts){
		_goTermsCounts = goTermsCounts;
		drawPieCharts();
	}

	/**
	 * Adds the GO terms stored in the dataset corresponding to the ontology
	 * class ID.
	 * @param goTermsCounts The map containing goterms and their occurrence.
	 */
	public	void	drawPieCharts(){
		for (Iterator<Integer> it = GOntologies.instance().iterator(); it.hasNext();) {
			Integer	ontologyId = it.next();

			drawPieChart(	_selectedStatsID,
							ontologyId,
							GOntologies.instance().getRoot(ontologyId).getID(),
							false);
		}
	}

	private	boolean	drawPieChart(Integer statsID, Integer ontologyId, String dadTermId, boolean emphasize){
		return updateChart(getGOTermsCounts(statsID), ontologyId, dadTermId, emphasize);
	}

	/**
	 * Returns the corresponding goTersCount map.
	 * @param statsID
	 * @return The corresponding goTersCount map.
	 */
	private	Map<String, Integer>	getGOTermsCounts(Integer statsID){
		Map<String, Integer>		goTermsCounts = null;

		switch (statsID){
			case STATS_GLOBAL:
				goTermsCounts = _goTermsCountsGlobal;
				break;
			case STATS_LOCAL:
				goTermsCounts = _goTermsCounts;
				break;
		}

		return goTermsCounts;
	}

	/**
	 * Parses the stored map and adds the GO terms in the dataset corresponding
	 * to the given ontology class ID.
	 * @param goTermsCounts The map containing goterms and their occurrence.
	 */
	private	boolean			updateChart(Map<String, Integer> goTermsCounts, Integer ontologyId, String dadTermId, boolean emphasize){
		Iterator<GOTerm>	itGOTerms		= GOntologies.instance().iteratorSons(dadTermId);
		DefaultPieDataset	dataset			= null;
		JFreeChart			chart			= null;
		PiePlot				plot			= null;
		GOTerm				dadTerm			= GOntologies.instance().getTerm(dadTermId);
		boolean				res				= false;
		String				termId			= null;
		Color				color			= null;
		int					indexSection	= 0;
		int					nb				= 0;

		dataset	= _pieDatasets.get(_selectedStatsID).get(ontologyId);
		chart = _chartPanels.get(_selectedStatsID).get(ontologyId).getChart();
		emptyDataset(dataset);
		plot = (PiePlot)_chartPanels.get(_selectedStatsID).get(ontologyId).getChart().getPlot();

		indexSection = 0;
		_colors.get(ontologyId).clear();
		while (itGOTerms.hasNext() && indexSection < 20) {
			termId = itGOTerms.next().getID();
			if (!_colorsParent.get(ontologyId).containsKey(termId)){
				_colorsParent.get(ontologyId).put(termId, ColorManager.instance().nextColor());
			}
			color = _colorsParent.get(ontologyId).get(termId);
			nb = 0;
			if (goTermsCounts.containsKey(termId)){
				nb += goTermsCounts.get(termId);
				_colors.get(ontologyId).put(termId, color);
			}
			for (Iterator<GOTerm> itSons = GOntologies.instance().iteratorDescendants(termId); itSons.hasNext();) {
				String sonId = itSons.next().getID();
				if (goTermsCounts.containsKey(sonId)){
					nb += goTermsCounts.get(sonId);
					_colors.get(ontologyId).put(sonId, color);
				}
			}
			if (0 != nb){
				dataset.setValue(termId, nb);
				plot.setSectionPaint(indexSection++, color);
			}
		}
		if (0 != indexSection){
			res = true;
			chart.setTitle(dadTermId + "(" + dadTerm.getName() + ")");
			if (STATS_GLOBAL != _selectedStatsID){
				_canvas.geneColorChange(emphasize);
			}
		}
		return res;
	}

	/**
	 * Parses the stored map and adds the GO terms in the dataset corresponding
	 * to the given ontology class ID.
	 * @param goTermsCounts The map containing goterms and their occurrence.
	 */
	private	void	singleDadTermColor(Map<String, Integer> goTermsCounts, Integer ontologyId, String dadTermId, boolean emphasize){
		Color		color	= null;

		_colors.get(ontologyId).clear();
		if (!_colorsParent.get(ontologyId).containsKey(dadTermId)){
			_colorsParent.get(ontologyId).put(dadTermId, ColorManager.instance().nextColor());
		}
		color = _colorsParent.get(ontologyId).get(dadTermId);
		if (goTermsCounts.containsKey(dadTermId)){
			_colors.get(ontologyId).put(dadTermId, color);
		}
		for (Iterator<GOTerm> itSons = GOntologies.instance().iteratorDescendants(dadTermId); itSons.hasNext();) {
			String sonId = itSons.next().getID();
			if (goTermsCounts.containsKey(sonId)){
				_colors.get(ontologyId).put(sonId, color);
			}
		}
		if (STATS_GLOBAL != _selectedStatsID){
			_canvas.geneColorChange(emphasize);
		}
	}

//	/**
//	 * Parses the stored map and adds the GO terms in the dataset corresponding
//	 * to the given ontology class ID.
//	 * @param goTermsCounts The map containing goterms and their occurrence.
//	 */
//	private	void			drawPieChart(Map<String, Integer> goTermsCounts, Integer ontologyId, String dadTermId, boolean emphasize){
//		Iterator<GOTerm>	itGOTerms		= GOntologies.instance().iteratorSons(dadTermId);
//		DefaultPieDataset	dataset			= null;
//		PiePlot				plot			= null;
//		String				termId			= null;
//		Color				color			= null;
//		int					indexSection	= 0;
//		int					nb				= 0;
//
//		dataset	= _pieDatasets.get(_selectedStatsID).get(ontologyId);
//		emptyDataset(dataset);
//		plot = (PiePlot)_chartPanels.get(_selectedStatsID).get(ontologyId).getChart().getPlot();
//
//		indexSection = 0;
//		_colors.get(ontologyId).clear();
//		while (itGOTerms.hasNext() && indexSection < 10) {
//			termId = itGOTerms.next().getID();
//			if (!_colorsParent.get(ontologyId).containsKey(termId)){
//				_colorsParent.get(ontologyId).put(termId, ColorManager.instance().nextColor());
//			}
//			color = _colorsParent.get(ontologyId).get(termId);
//			nb = 0;
//			if (goTermsCounts.containsKey(termId)){
//				nb += goTermsCounts.get(termId);
//				_colors.get(ontologyId).put(termId, color);
//			}
//			for (Iterator<GOTerm> itSons = GOntologies.instance().iteratorDescendants(termId); itSons.hasNext();) {
//				String sonId = itSons.next().getID();
//				if (goTermsCounts.containsKey(sonId)){
//					nb += goTermsCounts.get(sonId);
//					_colors.get(ontologyId).put(sonId, color);
//				}
//			}
//			if (0 != nb){
//				dataset.setValue(termId, nb);
//				plot.setSectionPaint(indexSection++, color);
//			}
//		}
//		if (STATS_GLOBAL != _selectedStatsID){
//			_canvas.geneColorChange(emphasize);
//		}
//	}

	/**
	 * Go deeper in the ontology. (stores the term for easier exploration)
	 * @param statsID
	 * @param ontologyId
	 * @param dadTermId
	 * @param emphasize
	 */
	public	void	goDeeper(Integer statsID, Integer ontologyId, String dadTermId, boolean emphasize){
		_hierarchy.get(statsID).get(ontologyId).push(dadTermId);
		if (!drawPieChart(statsID, ontologyId, dadTermId, emphasize)){
			goUpper(statsID, ontologyId);
		}
	}

	public	void	emphasize(Integer statsID, Integer ontologyId, String dadTermId){
		singleDadTermColor(getGOTermsCounts(statsID), ontologyId, dadTermId, true);
	}

	public	void		unemphasize(Integer statsID, Integer ontologyId){
		Deque<String>	hierarchy = _hierarchy.get(statsID).get(ontologyId);
		String			goTermID = hierarchy.peek();

		//	Display density
		GOStatisticsManager.instance().getSelected().computeDensity(goTermID);
		drawPieChart(statsID, ontologyId, goTermID, hierarchy.size() > 1);
	}

	/**
	 * Go upper in the ontology; the shown term is the previously stored one.
	 * @param statsID
	 * @param ontologyId
	 */
	public	void		goUpper(Integer statsID, Integer ontologyId){
		String			termID		= null;
		Deque<String>	hierarchy	= _hierarchy.get(statsID).get(ontologyId);
		boolean			moreThanOne	= hierarchy.size() > 1;

		if (hierarchy.size() > 0){
			if (moreThanOne){
				hierarchy.pop();
			}
			moreThanOne = (hierarchy.size() > 1)?true:false;
			termID = hierarchy.peek();
			drawPieChart(statsID, ontologyId, termID, moreThanOne);
			GOStatisticsManager.instance().setInfos(termID);
			GOStatisticsManager.instance().getSelected().computeDensity(termID);
		}
	}

	/**
	 * Find (if possible) the given term ID in the ontology and make it possible
	 * to explore from here.
	 * @param statsID
	 * @param refTermID
	 */
	public	GOTerm		goTo(Integer statsID, GOTerm goTerm){
		Integer			ontologyID	= null;
		List<String>	path		= null;
		Deque<String>	hierarchy	= null;
		String			termID		= null;

		if (null != goTerm){
			ontologyID = goTerm.getNamespace();
			path = GOntologies.instance().getPath(goTerm.getID());

			if (null != path && path.size() > 1){
				hierarchy = _hierarchy.get(statsID).get(ontologyID);
				hierarchy.clear();
//				System.out.println("Path : ");
				for (Iterator<String> it = path.iterator(); it.hasNext();) {
					String goTermID = it.next();

//					System.out.println(" " + goTermID);
					hierarchy.push(goTermID);
				}
				hierarchy.pop();
				termID = goTerm.getID();
				if (!GOntologies.instance().hasSon(termID)){
					termID = hierarchy.pop();
				}
				goDeeper(statsID, ontologyID, termID, true);
			}
		}

		return goTerm;
	}

	public	void	repaintCanvas(){
		_canvas.repaint();
	}

	public	void	setGlobalPieCharts(Map<String, Integer> goTermsCounts){
		_selectedStatsID = STATS_GLOBAL;
		_goTermsCountsGlobal = goTermsCounts;
		for (Iterator<Integer> it = GOntologies.instance().iterator(); it.hasNext();) {
			Integer	ontologyId = it.next();

			updateChart(	goTermsCounts,
							ontologyId,
							GOntologies.instance().getRoot(ontologyId).getID(),
							false);
		}
		_selectedStatsID = STATS_LOCAL;
	}

	public	ChartPanel	getChartPanel(){
		return _chartPanels.get(_selectedStatsID).get(_selectedOntologyID);
	}

	/**
	 * Sets the selected statistics ID.
	 * @param statsID
	 */
	public	void	setSelectedStats(Integer statsID){
		_selectedStatsID = statsID;
	}

	/**
	 * Sets the selected ontology ID.
	 * @param ontologyID
	 */
	public	void	setSelectedOntology(Integer ontologyID){
		_selectedOntologyID = ontologyID;
	}

	public	void	changeCanvasObjColor(){
		_canvas.geneColorChange(false);
	}

	class	CustomPieToolTipGenerator implements PieToolTipGenerator{
		public	CustomPieToolTipGenerator(Integer classID){
			super();
			c_toolTips = _toolTips.get(classID);
		}

		@Override
		public String generateToolTip(PieDataset dataSet, Comparable key) {
			return c_toolTips.get((String)key);
		}

		Map<String, String>	c_toolTips = null;
	}

	public	Color	getGOColor(String goId){
		return _colors.get(_selectedOntologyID).get(goId);
	}

	/**
	 * Returns the number of genes under the given GO term.
	 * @param termID
	 * @return The number of genes under the given GO term.
	 */
	public	int	getNbGenes(String termID){
		Integer	nbGenes = _goTermsCountsGlobal.get(termID);

		if (null == nbGenes){
			nbGenes = -1;
		}

		return nbGenes;
	}

	private volatile	static	Charts											_instance				= null;
	private						Map<String, Integer>							_goTermsCounts			= null;
	private						Map<String, Integer>							_goTermsCountsGlobal	= null;
	private						Map<Integer, Map<Integer, DefaultPieDataset>>	_pieDatasets			= null;
	private						Map<Integer, Map<String, String>>				_toolTips				= null;
	private						Map<Integer, Map<Integer, ChartPanel>>			_chartPanels			= null;
	private						Map<Integer, Map<Integer, Deque<String>>>		_hierarchy				= null;
	private						Canvas											_canvas					= null;
	private						Map<Integer, Map<String, Color>>				_colorsParent			= null;	// colors map for coherence within exploration
	private						Map<Integer, Map<String, Color>>				_colors					= null;	// colors map for dynamic storing
	private						Integer											_selectedStatsID		= STATS_LOCAL;
	private						Integer											_selectedOntologyID		= GOntology.CELL_COMPONENT_ID;

	private						Map<Integer, Map<Integer, CustomChartMouseListener>>		_customListeners	= null;
}
