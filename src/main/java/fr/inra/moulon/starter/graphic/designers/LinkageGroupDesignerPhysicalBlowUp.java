/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.designers;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.graphic.charts.Charts;
import fr.inra.moulon.starter.graphic.utils.Anchor;
import fr.inra.moulon.starter.graphic.utils.FontTools;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignIconNamed;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignNamed;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignPhyElt;
import fr.inra.moulon.starter.utils.Session;
import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class	LinkageGroupDesignerPhysicalBlowUp extends LinkageGroupDesigner {
	public		LinkageGroupDesignerPhysicalBlowUp(	final	LinkageGroup	lkg,
													final	MetaModel		metaModel){
		super(lkg, metaModel);

		double[]	extrems;

		extrems = PhysicalManager.instance().getExtrems(_LKG.getName());
		initExtremValues(extrems[0], extrems[1]);
		_goTermsCounts = new HashMap<String, Integer>();
		PhysicalManager.instance().initializeGOStatistics(_LKG, super.getMaxPxlSize());
	}

	@Override
	public	double	getWidth(){
		return	LEFT_MARGIN		+
				_lengthLine		+
				RIGHT_MARGIN;
	}

	@Override
	public double getHeight() {
		return 600;
	}

	/**
	 * Removes all shapes.
	 */
	private	void	clearShapes(){
		_elements.put(MISC, new ArrayList<ShapeDesign>());
		_elements.put(GENE, new ArrayList<ShapeDesign>());
		_elements.put(ANCHOR, new ArrayList<ShapeDesign>());
	}

	@Override
	public	final void recreateShapes() {
		clearShapes();
		setMiscShapeDesigns();
		setGenesShapeDesigns();
		setAnchorsDesign();
		Charts.instance().drawPieCharts(_goTermsCounts);
	}

	/**
	 * Method called by the Charts class when the genes' colors should be
	 * changed.
	 */
	public	void	colorChange(){
		setGenesShapeDesignsColor();
	}

	public	String	getGeneInfos(Point p){
		String		infos = null;

		if (_elements.containsKey(LinkageGroupDesigner.GENE)){
			for (Iterator<ShapeDesign> it = _elements.get(LinkageGroupDesigner.GENE).iterator(); null == infos && it.hasNext();) {
				ShapeDesignPhyElt shapeDesign = (ShapeDesignPhyElt)it.next();

				if (shapeDesign.contains(p)){
					infos = shapeDesign.id;
				}
			}
		}

		return infos;
	}

	@Override
	public Point2D	position_data_to_pxl(double position) {
		Point2D		res		= new Point2D.Double();
		int			r		= 0;
		int			iLine	= 0;

		r = (int)((position - super._windowStart)*(_lengthLine*_nbLines)/(super._windowEnd - super._windowStart));
		if (r < 0){
			r = 0;
		}else if (r > _lengthLine*_nbLines){
			r = _lengthLine*_nbLines;
		}
		iLine = r / _lengthLine;

		res.setLocation(r - _lengthLine*iLine, (iLine+1) * _lengthInter);

		return res;
	}

	@Override
	public double	position_pxl_to_data(Point point) {
		int			iLine = (int)Math.round(point.y*_nbLines/super.getMaxPxlSize()) - 1;
		double		xLine = 0;

		if (iLine >= _nbLines){
			iLine = _nbLines - 1;
		}else if (iLine < 0){
			iLine = 0;
		}
		xLine = iLine*_lengthLine + point.x;
		
		return _windowStart + xLine*(_windowEnd - _windowStart)/(_lengthLine*_nbLines);
	}

	/**
	 * Sets the shape designs genes colors corresponding to their GOTerm color in
	 * the charts.
	 */
	private	void			setGenesShapeDesignsColor(){
		List<ShapeDesign>	genesList	= _elements.get(GENE);
//		List<String>		goTerms		= null;
		Set<String>			goTerms		= null;
		Color				color		= null;

		if (null != genesList){
			for (Iterator<ShapeDesign> it = genesList.iterator(); it.hasNext();) {
				ShapeDesignPhyElt shape = (ShapeDesignPhyElt)it.next();

				if (null != (goTerms = shape.go_terms)){
					color = null;
					for (Iterator<String> itGOTerms = goTerms.iterator(); itGOTerms.hasNext() && null == color;) {
						color = Charts.instance().getGOColor(itGOTerms.next());
					}
					if (null != color){
						shape.color = color;
						shape.shapeId = ShapeDesign.FILLED_RECTANGLE;
					}else{
						shape.color = Color.GRAY;
						shape.shapeId = ShapeDesign.RECTANGLE;
					}
				}else{
					shape.color = Color.LIGHT_GRAY;
					shape.shapeId = ShapeDesign.RECTANGLE;
				}
			}
		}
	}

	/**
	 * Adds the genes ShapeDesigns from the super's linkage group.
	 * (between the class positions)
	 */
	private	void			setGenesShapeDesigns(){
		List<ShapeDesign>	genesList		= new ArrayList<ShapeDesign>();
		Point2D				positionStart	= null;
		Point2D				positionEnd		= null;
		boolean				stay			= true;
		double				diff			= 0;
		ShapeDesignPhyElt	shapeNew		= null;

		_goTermsCounts.clear();
		for (Iterator<ShapeDesignPhyElt> it = PhysicalManager.instance().iteratorGenesShape(); stay && it.hasNext();) {
			ShapeDesignPhyElt shape = it.next();

			if (shape.bioPosition >= super._windowStart || shape.bioPositionEnd >= super._windowStart){
				if (shape.bioPosition <= super._windowEnd){
					positionStart = position_data_to_pxl(shape.bioPosition);
					positionEnd = position_data_to_pxl(shape.bioPositionEnd);
					if (0 == (diff = positionEnd.getY() - positionStart.getY())){	// Gene is within 1 line
						shape.shapeId = ShapeDesign.FILLED_RECTANGLE;
						shape.w = positionEnd.getX() - positionStart.getX();
						shape.h = 5;
						shape.x = positionStart.getX();
						shape.y = positionStart.getY() - (!shape.strand?shape.h:0);
					}else{
						// First gene line
						shape.shapeId = ShapeDesign.FILLED_RECTANGLE;
						shape.w = _lengthLine - positionStart.getX();
						shape.h = 5;
						shape.x = positionStart.getX();
						shape.y = positionStart.getY() - (!shape.strand?shape.h:0);
						// Middle gene line(s)
						for (int i = 1; i < (int)Math.round(diff/_lengthInter); ++i){
							shapeNew = shape.clone();
							shapeNew.w = _lengthLine;
							shapeNew.x = 0;
							shapeNew.y = positionStart.getY()  + _lengthInter*i - (!shape.strand?shape.h:0);
							genesList.add(shapeNew);
						}
						// Last gene line
						shapeNew = shape.clone();
						shapeNew.w = positionEnd.getX();
						shapeNew.x = 0;
						shapeNew.y = positionEnd.getY() - (!shape.strand?shape.h:0);
						genesList.add(shapeNew);
					}
					if (shape.w < 1){
						shape.w = 1;
					}
					if (null != shape.go_terms){
						for (Iterator<String> itGOTerms = shape.go_terms.iterator(); itGOTerms.hasNext();) {
							String goterm = itGOTerms.next();
							Integer	nb = null;
							if (!_goTermsCounts.containsKey(goterm)){
								_goTermsCounts.put(goterm, 1);
							}
							nb = _goTermsCounts.get(goterm);
							_goTermsCounts.put(goterm, nb+1);
						}
					}
					if (null != shape.go_terms || (Boolean)Session.instance().getValue(Session.DISPLAY_PHY_BLOW_GENES_NO_GO)){
						genesList.add(shape);
					}
				}else{
					stay = false;
				}
			}
		}
		_elements.put(GENE, genesList);
	}

	private	void				setAnchorsDesign(){
		List<ShapeDesign>		anchorsList	= null;
		ShapeDesignIconNamed	shapeAnchor	= null;
		Point2D					p			= null;
		double					pos_pb		= 0;

		if (null != _anchors && (Boolean)Session.instance().getValue(Session.DISPLAY_PHY_BLOW_ANCHORS)){
			anchorsList	= new ArrayList<ShapeDesign>();
			for (Iterator<Anchor> it = _anchors.iterator(); it.hasNext();) {
				Anchor anchor = it.next();

				pos_pb = anchor.getPosition(Anchor.PB);
				if (pos_pb >= _windowStart && pos_pb <= _windowEnd){
					p = position_data_to_pxl(pos_pb);
					shapeAnchor = new ShapeDesignIconNamed(	Session.instance().getResource(FileManager.RESOURCES_IMAGE_ANCHOR),
															anchor.getName(),
															p.getX(),
															p.getY(),
															ShapeDesignNamed.HORIZONTAL);
					anchorsList.add(shapeAnchor);
				}
			}
			_elements.put(ANCHOR, anchorsList);
		}
	}

	/**
	 * Sets the misc list filled with ShapeDesigns.(such as chromosome's edge)
	 */
	private	void			setMiscShapeDesigns(){
		List<ShapeDesign>	miscList	= new ArrayList<ShapeDesign>();
		ShapeDesignNamed	shapeUnit	= null;
		ShapeDesign			shape		= null;
		String				units		= null;
		int					widthPxl	= 0;
		int					heightPxl	= 0;

		// Edge
		miscList.add(new ShapeDesign(	ShapeDesign.ROUND_RECTANGLE,
										0,
										0,
										_lengthLine,
										super.getMaxPxlSize(),
										Color.BLACK));

		// Inner lines
		for (int i = 1; i <= _nbLines; ++i){
			shape = new ShapeDesign(ShapeDesign.LINE,
									0,
									i*_lengthInter,
									_lengthLine,
									0,
									Color.GRAY);
			miscList.add(shape);
		}

		// Position on top
		units = (int)Math.round(_windowStart/1000) + " kpb";
		heightPxl = FontTools.getHeight();
		shapeUnit = new ShapeDesignNamed(	ShapeDesign.NONE,
											units,
											5,
											heightPxl+2,
											0,
											0,
											Color.GRAY,
											ShapeDesignNamed.HORIZONTAL);
		miscList.add(shapeUnit);

		// Position on bottom
		units = (int)Math.round(_windowEnd/1000) + " kpb";
		widthPxl = FontTools.getWidth(units);
		heightPxl = FontTools.getHeight();
		shapeUnit = new ShapeDesignNamed(	ShapeDesign.NONE,
											units,
											_lengthLine-widthPxl,
											super.getMaxPxlSize()-(heightPxl),
											0,
											0,
											Color.GRAY,
											ShapeDesignNamed.HORIZONTAL);
		miscList.add(shapeUnit);



		_elements.put(MISC, miscList);
	}

	public	void			setEmphasizeShapes(boolean emphasize){
		List<ShapeDesign>	list	= new ArrayList<ShapeDesign>();
		List<ShapeDesign>	genesList	= _elements.get(GENE);
		Set<String>			goTerms		= null;
		Color				color		= null;

		if (emphasize){
			// BACKGROUND
			list.add(new ShapeDesign(	ShapeDesign.FILLED_RECTANGLE,
										1,
										1,
										_lengthLine-1,
										super.getMaxPxlSize()-1,
										new Color(0, 0, 0, 50)));

			// CORRECT GO GENES OVALS
			if (null != genesList){
				for (Iterator<ShapeDesign> it = genesList.iterator(); it.hasNext();) {
					ShapeDesignPhyElt shape = (ShapeDesignPhyElt)it.next();

					if (null != (goTerms = shape.go_terms)){
						color = null;
						for (Iterator<String> itGOTerms = goTerms.iterator(); itGOTerms.hasNext() && null == color;) {
							color = Charts.instance().getGOColor(itGOTerms.next());
						}
						if (null != color){
							for (int i = 0; i < 5; ++i){
								int	transparency = (255-20*i < 100)?100:255-50*i;
								list.add(new ShapeDesign(	ShapeDesign.FILLED_OVAL,
															shape.x-2-i,
															shape.y-2-i,
															shape.w+4+2*i,
															shape.h+4+2*i,
															new Color(255, 255, 255, transparency)));
							}
						}
					}
				}
			}
		}

		_elements.put(EMPHASISER, list);
	}

	private	int									_lengthLine					= 500;
	private	int									_nbLines					= 10;
	private	double								_lengthInter				= super.getMaxPxlSize()/(_nbLines+1);
	private	Map<String, Integer>				_goTermsCounts				= null;

	@Override
	public void darkenShapes(boolean darken) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public ShapeDesign onWhichElt(Point p) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public double getScrollerX() {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
