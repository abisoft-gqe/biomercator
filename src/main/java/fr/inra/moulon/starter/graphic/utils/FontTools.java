/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 06-Jul-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import fr.inra.moulon.starter.utils.Session;
import java.awt.Font;
import java.awt.FontMetrics;


public class FontTools {
	/**
	 * This method predicts the given string's width according to the current
	 * font.
	 * @param str The String
	 * @return The given string's width according to the current Font.
	 */
	public	static	int	getWidth(String str){
		Font			font		= (Font)Session.instance().getValue(Session.FONT);
		FontMetrics		fontMetrics	= (FontMetrics)Session.instance().getValue(Session.FONT_METRICS);
		int				width		= 0;

		if (null != str){
			if (null != fontMetrics){
				width = fontMetrics.stringWidth(str);
			}else if (null != font){
				width = (int)(((double)font.getSize()/1.5)*str.length());
			}
		}

		return width;
	}

	/**
	 * This method predicts the font height.
	 * @return The font height.
	 */
	public	static	int	getHeight(){
		Font			font		= (Font)Session.instance().getValue(Session.FONT);
		FontMetrics		fontMetrics	= (FontMetrics)Session.instance().getValue(Session.FONT_METRICS);
		int				height	= 0;

		if (null != fontMetrics){
			height = fontMetrics.getHeight();
		}else if (null != font){
			height = font.getSize();
		}

		return height;
	}
}
