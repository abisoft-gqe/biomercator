/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.designers;

import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.graphic.utils.Anchor;
import fr.inra.moulon.starter.graphic.utils.Scroller;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import fr.inra.moulon.starter.gui.utils.PointTools;
import fr.inra.moulon.starter.utils.Session;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Method 'initExtremValues' must be called in subclasses' constructors of the
 * LinkageGroupDesigner class.
 * @author sosnowski
 */
public abstract	class	LinkageGroupDesigner {
	public	final	static	double	LEFT_MARGIN		= 20;
	public	final	static	double	RIGHT_MARGIN	= 20;
	public	final	static	double	BOTTOM_MARGIN	= 20;
	public	final	static	double	TOP_MARGIN		= 20;

	public	final	static	Integer	MARKER			= 1;
	public	final	static	Integer	QTL				= 2;
	public	final	static	Integer	META_QTL		= 3;
	public	final	static	Integer	MISC			= 4;
	public	final	static	Integer	MARKER_STRING	= 5;
	public	final	static	Integer	GENE			= 6;
	public	final	static	Integer	EMPHASISER		= 7;
	public	final	static	Integer	ANCHOR			= 8;

	public		LinkageGroupDesigner(	final	LinkageGroup lkg,
										final	MetaModel	metaModel){
		int		width	= 20;

		_elements	= new HashMap<Integer, List<ShapeDesign>>();
		_LKG		= lkg;
		_META_MODEL	= metaModel;
		_scroller	= new Scroller(	this,
									0,
									width
									);
	}

	/**
	 * Enables the scroller : shows it and begins managing its linkage group
	 * designer son if any.
	 */
	public	void	enableScroller(	double	posY_pxl,
									boolean	scrollerOrdered){
		_scroller.enable(posY_pxl, scrollerOrdered);
	}

	/**
	 * Enables the scroller : shows it and begins managing the its linkage group
	 * designer son if any.
	 */
	public	void	enableScroller(	double	posY_pxl,
									double	height_pxl,
									boolean	scrollerOrdered){
		_scroller.enable(posY_pxl, height_pxl, scrollerOrdered);
	}

	/**
	 * Disables the scroller.
	 */
	public	void	disableScroller(){
		_scroller.disable();
	}

	/**
	 * This method has to be used if a scroller is wanted. It initialises the
	 * linkage group designer's scroller.
	 * @param lkgDesSon The linkage group designer to be controlled by the
	 * scroller
	 */
	public	void	initScroller(	LinkageGroupDesigner	lkgDesSon,
									Scroller				scrollerLink,
									int						unitConversionId){
		_scroller.setLkgSon(lkgDesSon);
		_scroller.setScrollerLink(scrollerLink);
		_scroller.setUnitConversionId(unitConversionId);
	}

	public	abstract	double	getWidth();

	public	abstract	double	getHeight();

	/**
	 * Returns the scroller's x position. (relative to the linkageGroup)
	 * @return The scroller's x position. (relative to the linkageGroup)
	 */
	public	abstract	double	getScrollerX();

	public	abstract	ShapeDesign	onWhichElt(Point p);

	public	abstract	void	recreateShapes();

	public	abstract	void	darkenShapes(boolean darken);

	public	double	getLeftAreaWidth(){
		return 0;
	}

	public	final void	initExtremValues(double min, double max){
		_MIN_BIOPOS		= min;
		_MAX_BIOPOS		= max;
		_windowStart	= _MIN_BIOPOS;
		_windowEnd		= _MAX_BIOPOS;
	}

	public	final void	setWindow(double start, double end){
		_windowStart	= start;
		_windowEnd		= end;
	}

	public	Double	getMaxPxlSize(){
		return _max_pxl_size;
//		return (Double)Session.instance().getValue(Session.LKGDES_SIZE);
	}

	public	void	setMaxPxlSize(double max_pxl_size){
		_max_pxl_size = max_pxl_size;
//		return (Double)Session.instance().getValue(Session.LKGDES_SIZE);
	}

	public	String	getLkgName(){
		return _LKG.getName();
	}

	/**
	 * Returns the horizontal margin size.
	 * @return The horizontal margin size.
	 */
	public	double	getHorizontalMargin(){
		return LEFT_MARGIN + RIGHT_MARGIN;
	}

	/**
	 * Returns the vertical margin size.
	 * @return The vertical margin size.
	 */
	public	double	getVerticalMargin(){
		return TOP_MARGIN + BOTTOM_MARGIN;
	}

	/**
	 * Returns the extremes values of biological positions.
	 * @return The extremes values of biological positions.
	 */
	public	double[]	getExtremPositions(){
		return new double[]{_MIN_BIOPOS, _MAX_BIOPOS};
	}

	/**
	 * Returns the scoller's biological positions in cM.
	 * @return The scoller's biological positions in cM.
	 */
	public	double[]	getGeneticScrollerPosition(){
		double[]		res = new double[2];

		res[0] = position_pxl_to_data(_scroller.getPositionTop());
		res[1] = position_pxl_to_data(_scroller.getPositionBottom());

		return res;
	}

	/**
	 * Returns the extremes values of biological positions within the window.
	 * @return The extremes values of biological positions within the window.
	 */
	public	double[]	getExtremWindowPositions(){
		return new double[]{_windowStart, _windowEnd};
	}

	/**
	 * Sets the anchors list.
	 * @param anchors The anchors list
	 */
	public	void	setAnchors(List<Anchor> anchors){
		_scroller.setAnchors(anchors);
		_anchors = anchors;
	}

	/**
	 * Get the anchors list.
	 * @return The anchors list.
	 */
	public	List<Anchor>	getAnchors(){
		return _anchors;
	}

	/**
	 * Returns an iterator on the shapeDesigns corresponding to the dataId; if
	 * no shape designs are found, the method returns an iterator on an empty
	 * collection. (The iterator return is never null)
	 * @return an iterator on the shapeDesigns corresponding to the dataId.
	 */
	public	Iterator<ShapeDesign>	iterator(Integer dataId){
		Iterator<ShapeDesign>		it = null;

		if (_elements.containsKey(dataId)){
			it = _elements.get(dataId).iterator();
		}else{
			it = new ArrayList().iterator();
		}

		return it;
	}


	public void setDataPositions(double start, double end) {
		_windowStart	= start;
		_windowEnd		= end;
	}

	/**
	 * Returns the point with pixels coordinates corresponding to the given
	 * biological position.
	 * @param position The biological position
	 * @return the point with pixels coordinates corresponding to the given
	 * biological position
	 */
	public Point2D	position_data_to_pxl(double position) {
		double		pxl = (position-_windowStart)*getMaxPxlSize()/(_windowEnd-_windowStart);

		if (pxl < 0){
			pxl = 0;
		}else if (pxl > getMaxPxlSize()){
			pxl = getMaxPxlSize();
		}

		return new Point2D.Double(0, pxl);
	}

	/**
	 * Returns the biological position corresponding to the given position.
	 * @param point The pixels coordinates
	 * @return the biological position corresponding to the given position.
	 */
	public double position_pxl_to_data(double y) {
		return _windowStart + y*(_windowEnd-_windowStart)/getMaxPxlSize();
	}

	/**
	 * Returns the biological position corresponding to the given point.
	 * @param point The point with pixels coordinates
	 * @return the biological position corresponding to the given point
	 */
	public double position_pxl_to_data(Point point) {
		return position_pxl_to_data(point.y);
	}

	/**
	 * Returns the movement in pixels corresponding to the given biological
	 * movement.
	 * @param mvtBio The biological movement
	 * @return The movement in pixels corresponding to the given biological
	 * movement.
	 */
	public double	movement_data_to_pxl(double mvtBio) {
		return mvtBio*getMaxPxlSize()/(_windowEnd-_windowStart);
	}

	/**
	 * Returns the biological movement corresponding to the given movement in
	 * pixel.
	 * @param mvtPxl The movement in pixels
	 * @return The biological movement corresponding to the given movement in
	 * pixel.
	 */
	public double	movement_pxl_to_data(double mvtPxl) {
		return mvtPxl*(_windowEnd-_windowStart)/getMaxPxlSize();
	}

	/**
	 * Returns an iterator on the scroller shapeDesigns.
	 * @return an iterator on the scroller shapeDesigns
	 */
	public	Iterator<ShapeDesign>	iteratorScrollerShapes(){
		List<ShapeDesign>			list = new ArrayList<ShapeDesign>();

		if (_scroller.isEnabled()){
			list = _scroller.getshapes();
		}

		return list.iterator();
	}

	/**
	 * Returns the identifier corresponding to the scroller'S area of the given
	 * point.
	 * @param p The point
	 * @return The area containing the point.
	 */
	public	int	scrollerArea(Point p){
		Point	pTranslated = PointTools.copyNtranslate(p, (int)-getScrollerX(), 0);

		return _scroller.getArea(pTranslated);
	}

	/**
	 * Returns if the linkage group designer contains the given point.
	 * @param p The point
	 * @return if the linkage group designer contain the given point.
	 */
	public	boolean	contains(Point p){
		return	p.x > 0 &&
				p.x < getWidth() &&
				p.y > 0 &&
				p.y < getHeight();
	}

	/**
	 * Returns if the given point is close to the linkage group design.
	 * @param p The point
	 * @return if the given point is close to the linkage group design.
	 */
	public	boolean	containsCloseToLkg(Point p, int margin){
		return contains(p);
	}

	/**
	 * This method manages the scrolling of a linkage group designer's son.
	 * It moves the linkage group designer's scroller.
	 * @param start_pt The starting point
	 * @param end_pt The ending point
	 * @return if the scrolling has been done.
	 */
	public	boolean	scrollSon(Point start_pt, Point end_pt){
		double		posStart_bio	= _scroller._lkgDesSon.position_pxl_to_data(start_pt);
		double		posEnd_bio		= _scroller._lkgDesSon.position_pxl_to_data(end_pt);
		double		mvt_bio			= posEnd_bio - posStart_bio;
		double		mvt_pxl			= position_data_to_pxl(posEnd_bio).getY() -
										position_data_to_pxl(posStart_bio).getY();

		return _scroller.moveScroller(mvt_pxl, mvt_pxl, true, false);
	}

	/**
	 * Moves the linkage group designer's scroller from the given staring and
	 * ending points.
	 * @param start_pt The starting point
	 * @param end_pt The ending point
	 * @return if the scrolling has been done
	 */
	public	boolean	moveScroller(Point start_pt, Point end_pt){
		double		mvt_pxl		= end_pt.y - start_pt.y;

		return _scroller.moveScroller(mvt_pxl, mvt_pxl, true, false);
	}

	/**
	 * Zooms out the view to the maximum.
	 */
	public	void	zoomOut(){
		setWindow(_MIN_BIOPOS, _MAX_BIOPOS);
		recreateShapes();
	}

	/**
	 * Modifies the extreme points limiting the linkage group view; return if
	 * the zooming operation succeeded.(So that a redraw can be launched)
	 * @param point The point translated to the gridCell
	 * @param nb_cM The number of cM used to move forward
	 * @return If the zooming operation succeeded
	 */
	public	boolean	zoom(Point point, double step){
		double		y			= position_pxl_to_data(point);
		double		mvtTop		= step * (y - _windowStart)/(_windowEnd - _windowStart);
		double		mvtBottom	= step * (y - _windowEnd)/(_windowEnd - _windowStart);

		return moveWindowIfPossible(mvtTop, mvtBottom, false);
	}

	/**
	 * Modifies the extreme points limiting the linkage group view; return if
	 * the zooming operation succeeded.(So that a redraw can be launched)
	 * @param point The point translated to the gridCell
	 * @param nb_cM The number of cM used to move forward
	 * @return If the zooming operation succeeded
	 */
	public	boolean					zoomSon(Point point, double step){
		double		y				= position_pxl_to_data(point);
		double		mvtTop			= step * (y - _windowStart)/(_windowEnd - _windowStart);
		double		mvtBottom		= step * (y - _windowEnd)/(_windowEnd - _windowStart);
		double		mvtTopPxl		= movement_data_to_pxl(mvtTop);
		double		mvtBottomPxl	= movement_data_to_pxl(mvtBottom);

		System.out.println("step : " + step);
		return _scroller.moveScroller(mvtTopPxl, mvtBottomPxl, false, false);
	}

//	/**
//	 * Modifies the extreme points limiting the linkage group view; return if
//	 * the zooming operation succeeded.(So that a redraw can be launched)
//	 * @param point The point translated to the gridCell
//	 * @param nb_cM The number of cM used to move forward
//	 * @return If the zooming operation succeeded
//	 */
//	public	boolean				zoomSon(Point point, double step){
//		LinkageGroupDesigner	son = _scroller._lkgDesSon;
//
//		double	pos_bio			= son.position_pxl_to_data(point);
//		double	pos_pxl			= position_data_to_pxl(pos_bio).getY();
//		double	mvtTop_bio		= step * (pos_bio - son._windowStart)/(son._windowEnd - son._windowStart);
//		double	mvtBottom_bio	= step * (pos_bio - son._windowEnd)/(son._windowEnd - son._windowStart);
//		double	mvtTop_pxl		= -pos_pxl + position_data_to_pxl(pos_bio + mvtTop_bio).getY();
//		double	mvtBottom_pxl	= -pos_pxl + position_data_to_pxl(pos_bio + mvtBottom_bio).getY();
//
//		return _scroller.moveScroller(mvtTop_pxl, mvtBottom_pxl, false, false);
//	}

	/**
	 * Modifies the extreme points limiting the linkage group window view;
	 * Returns if the operation succeeded.(So that a redraw can be launched)
	 * @param mvtTop The biological distance to move the window starting point
	 * @param mvtBottom The biological distance to move the window ending point
	 * @param mandatoryDualMoves If sets to true, apply the movements only if
	 * both movements can be done
	 * @return If the operation succeeded.
	 */
	public	void	moveWindow(double mvtTop, double mvtBottom){
		_windowStart += mvtTop;
		_windowEnd += mvtBottom;
	}

	/**
	 * Modifies the extreme points limiting the linkage group window view;
	 * Returns if the operation succeeded.(So that a redraw can be launched)
	 * @param mvtStart The biological distance to move the window starting point
	 * @param mvtEnd The biological distance to move the window ending point
	 * @param mandatoryDualMoves If sets to true, apply the movements only if
	 * both movements can be done
	 * @return If the operation succeeded.
	 */
	public	boolean	moveWindowIfPossible(double mvtTop, double mvtBottom, boolean mandatoryDualMoves){
		boolean		moved		= false;
		double		diffTop		= 0;
		double		diffBottom	= 0;
		double		start		= _windowStart;
		double		end			= _windowEnd;

		if (mvtTop < 0 && start > _MIN_BIOPOS || mvtTop > 0 && start < end){
			start += mvtTop;
			if (start < _MIN_BIOPOS){diffTop = start - _MIN_BIOPOS;}
		}else{
			mvtTop = 0;
		}
		if (mvtBottom < 0 && end > start || mvtBottom > 0 && end < _MAX_BIOPOS){
			end += mvtBottom;
			if (end > _MAX_BIOPOS){diffBottom = end - _MAX_BIOPOS;}
		}else{
			mvtBottom = 0;
		}
		if (!mandatoryDualMoves || (_windowStart != start && _windowEnd != end)){
			moveWindow(mvtTop - diffTop, mvtBottom - diffBottom);
			moved = true;
		}


		return moved;
	}

	/**
	 * Tells if the linkage group's top is seen on the screen (during a zoom for
	 * instance)
	 * @return True if the linkage group's top is seen on the screen.
	 */
	public	boolean	isLkgTopSeen(){
		return _windowStart <= _MIN_BIOPOS;
	}

	/**
	 * Tells if the linkage group's bottom is seen on the screen (during a zoom
	 * for instance)
	 * @return True if the linkage group's bottom is seen on the screen.
	 */
	public	boolean	isLkgBottomSeen(){
		return _windowEnd >= _MAX_BIOPOS;
	}

	/**
	 * Resizes the scroller from top according to the pixels and biological unit
	 * values.
	 * @param start_pxl The starting point
	 * @param end_pxl The ending point
	 * @return If the resizing has been done
	 */
	public	boolean	resizeScrollerTop(Point start_pt, Point end_pt){
		double		mvt_pxl = end_pt.y - start_pt.y;

		return _scroller.moveScroller(mvt_pxl, 0, false, false);
	}

	/**
	 * Resizes the scroller from bottom according to the pixels and biological
	 * unit values.
	 * @param start_pxl The starting point
	 * @param end_pxl The ending point
	 * @return If the resizing has been done
	 */
	public	boolean	resizeScrollerBottom(Point start_pt, Point end_pt){
		double		mvt_pxl = end_pt.y - start_pt.y;

		return _scroller.moveScroller(0, mvt_pxl, false, false);
	}

	/**
	 * Scrolls on the LinkageGroupDesigner of distance between the 2 values in
	 * pixels.
	 * @param start_pxl The starting point
	 * @param end_pxl The ending point
	 * @return If the scrolling has been done
	 */
	public	boolean	scroll(Point start_pt, Point end_pt){
		return scroll(findMovement(start_pt, end_pt));
	}

	/**
	 * Scrolls the linkage group of the given movement if possible.
	 * @param mvt The movement in the linkage group unit (cM for genetic, pb for
	 * physic)
	 * @return If the scroll has been made
	 */
	public	boolean	scroll(double mvt) {
		return moveWindowIfPossible(mvt, mvt, true);
	}

	public	Scroller	getScroller(){
		return _scroller;
	}

	/**
	 * Returns the linkage group associated to this LinkageGroupDesigner.
	 * @return the linkage group associated to this LinkageGroupDesigner.
	 */
	public	LinkageGroup	getLinkageGroup(){
		return _LKG;
	}

	/**
	 * Returns if the scroller has a linkage group designer as son.
	 * @return If the scroller has a linkage group designer as son.
	 */
	public	boolean	hasSon(){
		return (null != _scroller && _scroller.hasSon());
	}

	/**
	 * Returns if the scroller has a linkage group designer as son.
	 * @return If the scroller has a linkage group designer as son.
	 */
	public	void	removeSon(){
		if (null != _scroller){
			_scroller.removeSon();
		}
	}

	/**
	 * Returns if the given linkage group designer is the son of the current
	 * one.
	 * @param prevLkgDes
	 * @return If the given linkage group designer is the son of the current
	 * one.
	 */
	public	boolean	hasSon(LinkageGroupDesigner prevLkgDes){
		return (null != _scroller && _scroller.hasSon(prevLkgDes));
	}

	/**
	 * Reverses all containing loci. (the first becomes the last etc.)
	 */
	public	void	reverse(){
		_LKG.reverse();
	}


	/**
	 * Returns the meta model associated to this LinkageGroupDesignerGenetic.
	 * @return the meta model associated to this LinkageGroupDesignerGenetic.
	 */
	public	MetaModel	getMetaModel(){
		return _META_MODEL;
	}


	/**
	 * Returns the movement corresponding to the starting and ending positions
	 * in pixels.
	 * @param start_pxl The starting position
	 * @param end_pxl The ending position
	 * @return
	 */
	private	double	findMovement(Point start_pt, Point end_pt){
		double		start_pos	= position_pxl_to_data(start_pt);
		double		end_pos		= position_pxl_to_data(end_pt);

		return end_pos - start_pos;
	}

	protected			Map<Integer, List<ShapeDesign>>	_elements		= null;
	protected			Scroller						_scroller		= null;
	protected			List<ShapeDesign>				_edgeShapes		= null;
	protected			double							_windowStart	= 0;
	protected			double							_windowEnd		= 0;
	protected			double							_MIN_BIOPOS		= 0;
	protected			double							_MAX_BIOPOS		= 0;
	protected			double							_max_pxl_size	= (Double)Session.instance().getValue(Session.LKGDES_SIZE);
	protected			List<Anchor>					_anchors		= null;
	protected	final	LinkageGroup					_LKG;
	protected	final	MetaModel						_META_MODEL;
}
