/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;


public class ShapeDesignNamed extends ShapeDesign{
	public	final static	Integer	FONT_DEFAULT_ID	= 0;
	public	final static	Integer	FONT_BOLD_ID	= 1;
	public	final static	Integer	FONT_SMALL_ID	= 2;

	public	final static	int	VERTICAL	= 0;
	public	final static	int	HORIZONTAL	= 1;

	public	static	Font	FONT_DEFAULT	= new Font("Default",	Font.PLAIN,	11);
	public	static	Font	FONT_BOLD		= new Font("Bold",		Font.BOLD,	11);
	public	static	Font	FONT_SMALL		= new Font("Small",		Font.PLAIN, 5);


	public	ShapeDesignNamed(int	shapeId,
							String	text,
							double	x,
							double	y,
							double	w,
							double	h,
							Color	color,
							int		position){
		this(shapeId, text, x, y, w, h, color);
		this.position = position;
	}

	public	ShapeDesignNamed(int	shapeId,
							String	text,
							double	x,
							double	y,
							double	w,
							double	h,
							Color	color){
		super(shapeId, x, y, w, h, color);
		this.str = text;
		this.strX = x;
		this.strY = y;
	}

	public	ShapeDesignNamed(double bioPosition){
		super(bioPosition);
	}

	@Override
	public	void	displayOnGraphics(Graphics2D g2d){
		super.displayOnGraphics(g2d);
		g2d.setColor(color);

		if (null != str){
			switch (position){
				case VERTICAL:// Display name vertically
					g2d.translate((int)strX, (int)strY);
					g2d.rotate(-Math.PI/2.0);
					g2d.drawString(str, 0, 0);
					g2d.rotate(Math.PI/2.0);
					g2d.translate((int)-strX, (int)-strY);
					break;
				case HORIZONTAL:// Display name horizontally
					g2d.translate((int)strX, (int)strY);
					g2d.drawString(str, 0, 0);
					g2d.translate((int)-strX, (int)-strY);
					break;
			}
		}
	}

	public	String		str			= null;
	public	double		strX		= 0;
	public	double		strY		= 0;
	public	int			position	= VERTICAL;
}
