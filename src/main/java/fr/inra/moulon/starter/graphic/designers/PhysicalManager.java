/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 23-Nov-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.designers;

import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.controller.ControllerDesign;
import fr.inra.moulon.starter.controller.visitors.VisitorEltGet;
import fr.inra.moulon.starter.controller.visitors.VisitorExtremsPosition;
import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.datamodel.container.Content;
import fr.inra.moulon.starter.datamodel.entities.AbstractQtl;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.datamodel.gontology.GOntologies;
import fr.inra.moulon.starter.graphic.charts.Charts;
import fr.inra.moulon.starter.graphic.utils.Anchor;
import fr.inra.moulon.starter.graphic.utils.AnchorsComparator;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignPhyElt;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.swing.JProgressBar;


public class PhysicalManager {
	protected PhysicalManager(){
		_genes = new HashMap<String, List<ShapeDesignPhyElt>>();
		_genesHash = new HashMap<String, ShapeDesignPhyElt>();
		_extrems = new HashMap<String, double[]>();
		_anchors = new HashMap<String, List<Anchor>>();
		_anchorsRemoved = new HashMap<String, List<Anchor>>();
	}

	public static PhysicalManager instance(){
		if (null == _instance){
			synchronized (PhysicalManager.class){
				if (null == _instance){
					_instance = new PhysicalManager();
				}
			}
		}
		return _instance;
	}

	public	Boolean	isLoaded(){
		return _annotationsLoaded;
	}

	public	void	clean(){
		_annotationsLoaded = Boolean.FALSE;
		_genes = new HashMap<String, List<ShapeDesignPhyElt>>();
		_genesHash = new HashMap<String, ShapeDesignPhyElt>();
		_extrems = new HashMap<String, double[]>();
		_structural_annotations_id = null;
		_functional_annotations_id = null;
		_anchors_id = null;
		_annotationsFunct = null;
		_anchors = new HashMap<String, List<Anchor>>();
		_anchorsRemoved = new HashMap<String, List<Anchor>>();
	}

	/**
	 * Parses all genes and their GOTerms, and fill the global charts.
	 */
	private	void				fillGlobalStats(){
		Map<String, Integer>	goTermsCounts = new HashMap<String, Integer>();
		List<ShapeDesignPhyElt>	genesList = _genes.get(_lkgName);

		for (Iterator<ShapeDesignPhyElt> it = genesList.iterator(); it.hasNext();) {
			ShapeDesignPhyElt shape = it.next();
			if (null != shape.go_terms){
				for (Iterator<String> itGOTerms = shape.go_terms.iterator(); itGOTerms.hasNext();) {
					String goterm = itGOTerms.next();
					Integer	nb = null;
					if (!goTermsCounts.containsKey(goterm)){
						goTermsCounts.put(goterm, 1);
					}
					nb = goTermsCounts.get(goterm);
					goTermsCounts.put(goterm, nb+1);
				}
			}
		}
		Charts.instance().setGlobalPieCharts(goTermsCounts);
	}

	public	void	initialize(	int							structural_annotations_id,
								int							functional_annotations_id,
								int							anchors_id,
								int                         dblinks_id,
								JProgressBar				progressBar){
		int			iLkg	= 0;
		int			nbLkgs	= 0;

		_structural_annotations_id = structural_annotations_id;
		_functional_annotations_id = functional_annotations_id;
		_anchors_id = anchors_id;
		_dblinks_id = dblinks_id;
		
		//we get dblinks
		Connection connection = DatabaseManager.instance().getConnection();
		try {
			_map_dblinks = new HashMap<String,String>();
			Statement stat = connection.createStatement();
			String queryLinks = DatabaseManager.getSelectStatementDBLinks(_dblinks_id);
			System.out.println(queryLinks);
			ResultSet rs = stat.executeQuery(queryLinks);
			while (rs.next()) {
				String dbname = rs.getString(1);
				String link = rs.getString(2);
				_map_dblinks.put(dbname, link);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		_lkgs = getLkgNames();
		nbLkgs = _lkgs.size();
		for (Iterator<String> it = _lkgs.iterator(); it.hasNext();) {
			double[]	extrems = null;
			String		lkgName = it.next();
			++iLkg;
			System.out.println("------------------ Loading linkage group : " + lkgName + " ------------------");
			progressBar.setValue(100*iLkg/nbLkgs);

			// Add genes list to genes map
			_genes.put(lkgName, ControllerDesign.getGenesShapeDesigns(
									_structural_annotations_id,
									_functional_annotations_id,
									lkgName));

			for (int i = 0; i < _genes.get(lkgName).size(); i++) {
				ShapeDesignPhyElt shape = _genes.get(lkgName).get(i);

				_genesHash.put(shape.id, shape);
			}

			// Add extrems to extrems map
			extrems = Controller.getExtremsPhysicPositions(_structural_annotations_id, lkgName);
			_extrems.put(lkgName, extrems);
		}

		_annotationsLoaded = Boolean.TRUE;
	}

	public	Integer	getStructuralAnnotationId(){
		return _structural_annotations_id;
	}

//	public	Integer	getStructuralAnnotationId(){
//		return _structural_annotations_id;
//	}
//
//	public	Integer	getStructuralAnnotationId(){
//		return _structural_annotations_id;
//	}

	public Map<String,String> getDBLinks() {
		return _map_dblinks;
	}

	public	void	initializeGOStatistics(LinkageGroup lkgMain, double maxPxl){
		VisitorEltGet<LinkageGroup>	v			= new VisitorEltGet(LinkageGroup.class);
		GOStatistics				goStats		= null;
		MapGene						mapBelong	= lkgMain.getBelongingMap();

		_lkgName = lkgMain.getName();
		if (GOStatisticsManager.instance().getLoadedMap() != mapBelong){
			GOStatisticsManager.instance().clean();
			v.visit(mapBelong);
			for (Iterator<Content> it = v.getList().iterator(); it.hasNext();) {
				LinkageGroup	lkg = (LinkageGroup)it.next();
				String			lkgName = lkg.getName();

				goStats = new GOStatistics();
				goStats.init(_genes.get(lkgName), 300, getExtrems(lkgName)[1], maxPxl);

				GOStatisticsManager.instance().addGOStatistics(lkgName, goStats);

				_anchors.put(lkgName, findAnchors(lkg, _extrems.get(lkgName)));
			}
			GOStatisticsManager.instance().setSelected(_lkgName);
			fillGlobalStats();
		}
	}

	/**
	 * Finds the anchors between the linkage group genetic and physic.
	 * @return
	 */
	private	List<Anchor>		findAnchors(LinkageGroup lkg, double[] extremsPhy){
		List<Anchor>			anchors	= null;
		VisitorExtremsPosition	v	= new VisitorExtremsPosition();
		double[]				extremGenPositions	= null;

		lkg.accept(v);
		extremGenPositions = v.getExtrems();

		anchors = Controller.getAnchors(
								_structural_annotations_id,
								_anchors_id,
								lkg);

		System.out.println("I/ anchors.size() : " + anchors.size());
		anchors.add(0, new Anchor("first", extremsPhy[0], extremGenPositions[0]));
		anchors.add(new Anchor("last", extremsPhy[1], extremGenPositions[1]));
		anchors = suppressInvertions(anchors, lkg.getName());
		System.out.println("I/ anchorsRemoved.size() : " + _anchorsRemoved.get(lkg.getName()).size());

		return anchors;
	}

	public	List<Anchor>	getAnchors(LinkageGroup lkg){
		return _anchors.get(lkg.getName());
	}

	public	List<Anchor>	getAnchorsRemoved(LinkageGroup lkg){
		return _anchorsRemoved.get(lkg.getName());
	}

	/**
	 * Returns an iterator on the linkage group's genes set in the
	 * <strong>initializeWithLkg</strong> method.
	 * @return An iterator on the linkage group's genes.
	 */
	public	Iterator<ShapeDesignPhyElt>	iteratorGenesShape(){
		return _genes.get(_lkgName).iterator();
	}

	public	double[]	getExtrems(String lkgName){
		return _extrems.get(lkgName);
	}

	public	void	setFunctionalAnnotation(Map<String, Set<String>> annotationsFunct){
		_annotationsFunct = annotationsFunct;
	}

	public	Map<String, Set<String>>	getFunctionalAnnotation(){
		return _annotationsFunct;
	}

	/**
	 * Removes inverted anchors.
	 * @param anchors the anchors list to clean.
	 */
	private	List<Anchor>	suppressInvertions(List<Anchor> anchors_phy, String lkgName){
		List<Anchor>		anchors_gen		= new LinkedList<Anchor>();
		List<Anchor>		anchors			= new ArrayList<Anchor>();
		List<Anchor>		anchorsRemoved	= new ArrayList<Anchor>();
		Iterator<Anchor>	it_gen			= null;
		Iterator<Anchor>	it_phy			= null;
		Anchor				anchorGen		= null;
		Anchor				anchorPhy		= null;
		int					rank_diff		= 0;
		boolean				incr_anchor_gen	= true;
		boolean				incr_anchor_phy	= true;
		boolean				end_of_anchors	= false;

		if (null != anchors_phy){
			for (int i = 0; i < anchors_phy.size(); ++i){
				anchors_gen.add(anchors_phy.get(i));
			}
			Collections.sort(anchors_gen, new AnchorsComparator(Anchor.CM));
			for (int i = 0; i < anchors_gen.size(); ++i){
				anchors_gen.get(i).rank_gen = i;
			}

			it_gen = anchors_gen.iterator();
			it_phy = anchors_phy.iterator();
			while (it_gen.hasNext() && it_phy.hasNext()){
				if (incr_anchor_gen){
					anchorGen = it_gen.next();
					while (!anchorGen.enable && !end_of_anchors){
						try{
							anchorGen = it_gen.next();
						}catch (NoSuchElementException e){
							end_of_anchors = true;
						}
					}
				}
				if (incr_anchor_phy){
					anchorPhy = it_phy.next();
					while (!anchorPhy.enable && !end_of_anchors){
						try{
							anchorPhy = it_phy.next();
						}catch (NoSuchElementException e){
							end_of_anchors = true;
						}
					}
				}

				if (!end_of_anchors){
					if (anchorGen != anchorPhy){
						rank_diff = anchorGen.rank_phy - anchorPhy.rank_gen;
						if (rank_diff > 0){
							anchorGen.enable = false;
							anchorsRemoved.add(anchorGen);
							incr_anchor_gen = true;
							incr_anchor_phy = false;
						}else if (rank_diff < 0){
							anchorPhy.enable = false;
							anchorsRemoved.add(anchorPhy);
							incr_anchor_gen = false;
							incr_anchor_phy = true;
						}else{
							anchorGen.enable = false;
							anchorPhy.enable = false;
							anchorsRemoved.add(anchorGen);
							anchorsRemoved.add(anchorPhy);
							incr_anchor_gen = true;
							incr_anchor_phy = true;
						}
					}else{
						anchors.add(anchorGen);
						incr_anchor_gen = true;
						incr_anchor_phy = true;
					}
				}
			}
		}

		_anchorsRemoved.put(lkgName, anchorsRemoved);

		return anchors;
	}

	public	double		cM_to_pb(LinkageGroup lkg, double pos_bio){
		double			anchorsDistance		= 0;
		double			anchorsNewDistance	= 0;
		double			posNew_bio			= 0;
		List<Anchor>	anchors				= findFlankingAnchors(lkg, pos_bio, Anchor.CM);
		int				curUnitId			= Anchor.CM;
		int				lkUnitId			= Anchor.PB;
		double[]		extremsPositions	= _extrems.get(lkg.getName());

		if (null != anchors){
			if (anchors.size() == 2){
				// The position is between 2 anchors
				anchorsDistance = anchors.get(1).getPosition(curUnitId) - anchors.get(0).getPosition(curUnitId);
				anchorsNewDistance = anchors.get(1).getPosition(lkUnitId) - anchors.get(0).getPosition(lkUnitId);
				posNew_bio = (pos_bio - anchors.get(0).getPosition(curUnitId)) * anchorsNewDistance / anchorsDistance;
				posNew_bio += anchors.get(0).getPosition(lkUnitId);
			}else{
				// The position is bigger than the last anchor's
				posNew_bio = extremsPositions[1];
			}
		}else{
			// The position is prior to the first anchor
			posNew_bio = extremsPositions[0];
		}

		return posNew_bio;
	}

	public	List<Anchor>	findFlankingAnchors(LinkageGroup lkg, double pos_bio, int unitId){
		List<Anchor>		anchorsLkg	= _anchors.get(lkg.getName());
		List<Anchor>		anchors	= null;
		int					index	= 0;

		while (index < anchorsLkg.size() &&
				pos_bio >= anchorsLkg.get(index).getPosition(unitId)) {
				++index;
		}
		if (index > 0){
			anchors = new ArrayList<Anchor>();
			anchors.add(anchorsLkg.get(index-1));
			if (index < anchorsLkg.size()){
				anchors.add(anchorsLkg.get(index));
			}
		}

		return anchors;
	}

	public	Set<String>	getGenesFamily(String goTermID){
		Set<String>		goGenesFamily	= new HashSet<String>();
		Set<String>		goTermsFamily	= null;
		int				nbGo			= 0;

		goGenesFamily.clear();
		goTermsFamily = GOntologies.instance().fillGoTermFamily(goTermID);

		for (Iterator<String> itLkg = _genes.keySet().iterator(); itLkg.hasNext();) {
			List<ShapeDesignPhyElt>	genesList = _genes.get(itLkg.next());

			for (Iterator<ShapeDesignPhyElt> itGenes = genesList.iterator(); itGenes.hasNext();) {
				ShapeDesignPhyElt shape = itGenes.next();

				if (null != shape.go_terms){
					boolean	found = false;
					for (Iterator<String> it = shape.go_terms.iterator(); !found && it.hasNext();) {
						String goTermId = it.next();
						found = goTermsFamily.contains(goTermId);
					}
					if (found){
						goGenesFamily.add(shape.id);
						++nbGo;
					}
				}
			}
		}

		return goGenesFamily;
	}

	public	Set<String>	getGoTermsAssociated(String geneId){
		ShapeDesignPhyElt shape = _genesHash.get(geneId);

		return (null != shape)?shape.go_terms:null;
	}

	private	Set<String>	getLkgNames(){
		return Controller.getLkgNames(_structural_annotations_id);
	}

	public	void	addCurrentMap(MapGene map){
		_currentMap = map;
	}

	private	List<PositionnedObject<AbstractQtl>>	findQtlsPhysicalPositions(LinkageGroup lkg, VisitorEltGet<AbstractQtl> v, MetaModel metamodel){
		List<PositionnedObject<AbstractQtl>>		qtls	= new ArrayList<PositionnedObject<AbstractQtl>>();
		double										start	= 0;
		double										end		= 0;

		if (null != metamodel){
			metamodel.accept(v);
		}else{
			lkg.accept(v);
		}
		for (Iterator<Content> it = v.getList().iterator(); it.hasNext();) {
			AbstractQtl aQtl = (AbstractQtl)it.next();

			start = cM_to_pb(lkg, aQtl.getPositionStart());
			end = cM_to_pb(lkg, aQtl.getPositionEnd());
			qtls.add(new PositionnedObject<AbstractQtl>(start, end, aQtl));
		}

		return qtls;
	}

	private	List<PositionnedObject<AbstractQtl>>	findQtlsPhysicalPositions(LinkageGroup lkg, MetaModel metaModel){
		List<PositionnedObject<AbstractQtl>>		qtls	= new ArrayList<PositionnedObject<AbstractQtl>>();

		qtls = findQtlsPhysicalPositions(lkg, new VisitorEltGet<AbstractQtl>(Qtl.class), null);
		if (null != metaModel){
			qtls.addAll(findQtlsPhysicalPositions(lkg, new VisitorEltGet<AbstractQtl>(MetaQtl.class), metaModel));
		}

		return qtls;
	}

	/**
	 * Returns a list of all QTLs intersecting the given positions.
	 * @param positionStart
	 * @param positionEnd
	 * @return A list of all QTLs intersecting the given positions.
	 */
	public	List<PositionnedObject<AbstractQtl>>	getIntersectQtls(String lkgName, double positionStart, double positionEnd, MetaModel metaModel){
		List<PositionnedObject<AbstractQtl>>		qtls	= null;
		VisitorEltGet<LinkageGroup>					v		= new VisitorEltGet<LinkageGroup>(LinkageGroup.class);
		List<LinkageGroup>							lkgs	= new ArrayList<LinkageGroup>();

		if (null != _currentMap){
			_currentMap.accept(v);
			for (Iterator<Content> it = v.getList().iterator(); it.hasNext();) {
				LinkageGroup lkg = (LinkageGroup)it.next();
				if (lkg.getName().equals(lkgName)){
					lkgs.add(lkg);
				}
			}
			if (lkgs.size() == 1){
				qtls = findQtlsPhysicalPositions(lkgs.get(0), metaModel);
				for (Iterator<PositionnedObject<AbstractQtl>> it = qtls.iterator(); it.hasNext();) {
					PositionnedObject<AbstractQtl> positionnedQtl = it.next();

					if (!positionnedQtl.intersects(positionStart, positionEnd)){
						it.remove();
					}
				}
			}
		}

		return qtls;
	}

	private	Map<String, List<ShapeDesignPhyElt>>	_genes						= null;

	/* All genes associated to their name */
	private	Map<String, ShapeDesignPhyElt>			_genesHash					= null;

	private	Map<String, Set<String>>				_annotationsFunct			= null;
	private	Map<String, List<Anchor>>				_anchors					= null;
	private	Map<String, List<Anchor>>				_anchorsRemoved				= null;
	private Map<String, String>                     _map_dblinks                = null;
	private	Integer									_structural_annotations_id	= null;
	private	Integer									_functional_annotations_id	= null;
	private	Integer									_anchors_id					= null;
	private	Integer									_dblinks_id					= null;
	private	String									_lkgName					= null;
	private	Map<String, double[]>					_extrems					= null;
	private	Set<String>								_lkgs						= null;
	private	Boolean									_annotationsLoaded			= Boolean.FALSE;
	private	MapGene									_currentMap					= null;


	/**
	 * This class is an object wrapper for 2 additional position fields.
	 * @param <T> The contained object class
	 */
	public	class	PositionnedObject<T>{
		PositionnedObject(double start, double end, T object){
			p_start = start;
			p_end = end;
			p_object = object;
		}

		/**
		 * Returns if the PositionnedObject intersects with the given interval.
		 * @param positionStart
		 * @param positionEnd
		 * @return true if the PositionnedObject belongs to the given interval;
		 * else otherwise.
		 */
		public	boolean	intersects(double positionStart, double positionEnd){
			boolean		res = false;

			if (positionStart <= p_start){
				if (positionEnd >= p_start){
					res = true;
				}
			}else{
				if (positionStart <= p_end){
					res = true;
				}
			}

			return res;
		}

		public	T	getObject(){
			return p_object;
		}

		public	double	getPosStart(){
			return p_start;
		}

		public	double	getPosEnd(){
			return p_end;
		}

		T		p_object;
		double	p_start;
		double	p_end;
	}

	// singleton attribute
	private volatile	static	PhysicalManager		_instance	= null;
}
