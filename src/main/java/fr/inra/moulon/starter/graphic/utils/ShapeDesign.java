/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;


public class ShapeDesign {
	public	final	static	int	NONE						= 0;
	public	final	static	int	STRING						= 1;
	public	final	static	int	LINE						= 2;
	public	final	static	int	RECTANGLE					= 3;
	public	final	static	int	ROUND_RECTANGLE				= 4;
	public	final	static	int	FILLED_RECTANGLE			= 5;
	public	final	static	int	FILLED_ROUND_RECTANGLE		= 6;
	public	final	static	int	FILLED_ROUND_RECTANGLE_3D	= 7;
	public	final	static	int	FILLED_OVAL					= 8;
	public	final	static	int	ARC_UP						= 9;
	public	final	static	int	ARC_DOWN					= 10;

	public	ShapeDesign(int		shapeId,
						double	x,
						double	y,
						double	w,
						double	h,
						Color	color){
		this.shapeId	= shapeId;
		this.x			= x;
		this.y			= y;
		this.w			= w;
		this.h			= h;
		this.color		= color;
	}

	public	ShapeDesign(double bioPosition){
		this.bioPosition = bioPosition;
	}

	public	ShapeDesign(double bioPosition, double bioPositionEnd){
		this(bioPosition);
		this.bioPositionEnd = bioPositionEnd;
	}

	/**
	 * Draw the shape on the given Graphics.
	 * @param g2d The Graphics object on which drawing must be performed
	 */
	public	void	displayOnGraphics(Graphics2D g2d){
		g2d.setColor(color);
		switch (shapeId){
			case LINE:
				g2d.drawLine(	(int)x,
								(int)y,
								(int)(x + w),
								(int)(y + h));
				break;
			case ROUND_RECTANGLE:
				g2d.drawRoundRect(	(int)x,
									(int)y,
									(int)w,
									(int)h, 10, 10);
				break;
			case RECTANGLE:
				g2d.drawRect(	(int)x,
								(int)y,
								(int)w,
								(int)h);
				break;
			case FILLED_RECTANGLE:
				g2d.fillRect(	(int)x,
								(int)y,
								(int)w,
								(int)h);
				break;
			case FILLED_ROUND_RECTANGLE:
				g2d.fillRoundRect(	(int)x,
									(int)y,
									(int)w,
									(int)h, 10, 10);
				break;
			case FILLED_ROUND_RECTANGLE_3D:
				g2d.fill3DRect(	(int)x,
								(int)y,
								(int)w,
								(int)h,
								true);
			case FILLED_OVAL:
				g2d.fillOval(	(int)x,
								(int)y,
								(int)w,
								(int)h);
				break;
			case ARC_UP:
				g2d.drawArc(	(int)x,
								(int)y,
								(int)w,
								(int)h,
								0,
								180);
				break;
			case ARC_DOWN:
				g2d.drawArc(	(int)x,
								(int)y,
								(int)w,
								(int)h,
								0,
								-180);
				break;
		}
	}

	public	boolean	contains(Point p){
		return	p.x >= x		&&
				p.x <= x + w	&&
				p.y >= y		&&
				p.y <= y + h;
	}

	public	int		shapeId			= NONE;
	public	double	x				= 0;
	public	double	y				= 0;
	public	double	w				= 0;
	public	double	h				= 0;
	public	double	bioPosition		= 0;	// The biological position
	public	double	bioPositionEnd	= 0;	// The biological ending position if applicable
	public	Color	color			= null;
	public	String	info			= null;
}
