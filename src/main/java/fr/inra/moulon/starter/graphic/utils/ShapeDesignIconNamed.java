/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;


public class ShapeDesignIconNamed extends ShapeDesignNamed{
	/**
	 * Creates a new named shape image.
	 * @param imagePath The image path
	 * @param txt The text
	 * @param x The x position where the center of the image should be
	 * @param y The y position where the center of the image should be
	 * @param position The Text position
	 */
	public	ShapeDesignIconNamed(URL	imageURL,
								String	txt,
								double	x,
								double	y,
								int		position){
		super(NONE, txt, x, y, 0, 0, Color.BLACK);
		this.position = position;

		try {
			this.image = ImageIO.read(imageURL);
			this.w = image.getWidth();
			this.h = image.getHeight();
			this.strX = x - FontTools.getWidth(this.str)/2;
			this.strY = y - h/2 - 5;
			this.x -= w/2;
			this.y -= h/2;
		} catch (IOException ex) {
			Logger.getLogger(ShapeDesignIconNamed.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public	void	displayOnGraphics(Graphics2D g2d){
		super.displayOnGraphics(g2d);
		if (null != image){
			g2d.drawImage(image, (int)x, (int)y, null);
		}
	}

	public	BufferedImage	image = null;
}
