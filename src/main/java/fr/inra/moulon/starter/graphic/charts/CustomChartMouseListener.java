/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 28-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.charts;

import fr.inra.moulon.starter.datamodel.gontology.GOntologies;
import fr.inra.moulon.starter.graphic.designers.GOStatisticsManager;
import fr.inra.moulon.starter.gui.animations.AnimationChartColor;
import java.awt.Color;
import java.awt.event.MouseEvent;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.entity.PieSectionEntity;
import org.jfree.chart.plot.PiePlot;


public class CustomChartMouseListener implements ChartMouseListener {
	public	CustomChartMouseListener(Integer statsID, Integer ontologyId){
		super();
		_statsID = statsID;
		_ontologyId = ontologyId;
		_animation = new AnimationChartColor();
	}

	private	void	stopAnimation(){
		_animation.stop();
		_selectedSection = -1;
	}

	@Override
	public void				chartMouseClicked(ChartMouseEvent cme) {
		PieSectionEntity	pse			= (PieSectionEntity)cme.getEntity();
		PiePlot				plot		= (PiePlot)cme.getChart().getPlot();
		String				termId		= null;
		Color				color		= null;
		int					sectionID	= 0;

		if (cme.getTrigger().getButton() == MouseEvent.BUTTON1){
			if (null != pse){
				// Clic on a section
				termId = (String)pse.getSectionKey();
				sectionID = pse.getSectionIndex();
				color = (Color)plot.getSectionPaint(sectionID);
				if (_selectedSection == sectionID){
					//	Second clic on the same section -> go deeper.
					stopAnimation();
					GOntologies.instance().lowerLevel(_ontologyId);
					GOStatisticsManager.instance().cleanInfos();
					//	Set info
					GOStatisticsManager.instance().setInfos(termId);
					//	 Repaint all
					Charts.instance().goDeeper(_statsID, _ontologyId, termId, true);
				}else{
					//	Start flashing
					_animation.start(600, plot, sectionID, color);
					_selectedSection = sectionID;
					//	Set info
					GOStatisticsManager.instance().setInfos(termId);
					//	Display density
					GOStatisticsManager.instance().getSelected().computeDensity(termId);
					//	Empasize selected genes
					Charts.instance().emphasize(_statsID, _ontologyId, termId);
					//	 Repaint all
					Charts.instance().repaintCanvas();
				}
			}else{
				// Clic outside of a section
				if (-1 != _selectedSection){
					//	Stop flashing
					stopAnimation();
					//	Clear informations
					GOStatisticsManager.instance().cleanInfos();
					//	Unemphasize genes
					Charts.instance().unemphasize(_statsID, _ontologyId);
				}else{
					//	go upper level.
					GOStatisticsManager.instance().cleanInfos();
					Charts.instance().repaintCanvas();
					GOntologies.instance().upperLevel(_ontologyId);
					Charts.instance().goUpper(_statsID, _ontologyId);
				}
			}
		}
	}

	public	void	startFlashing(String termId, int sectionID, PiePlot plot){
		Color		color		= (Color)plot.getSectionPaint(sectionID);

		_animation.start(600, plot, sectionID, color);
		_selectedSection = sectionID;
		//	Set info
		GOStatisticsManager.instance().setInfos(termId);
		//	Display density
		GOStatisticsManager.instance().getSelected().computeDensity(termId);
		//	Empasize selected genes
		Charts.instance().emphasize(_statsID, _ontologyId, termId);
		//	 Repaint all
		Charts.instance().repaintCanvas();
	}

	@Override
	public void chartMouseMoved(ChartMouseEvent cme) {}

	public	void	emptyHsiory(){
	}

	private	AnimationChartColor	_animation			= null;
	private	Integer				_ontologyId			= null;
	private	Integer				_statsID			= null;
	private	int					_selectedSection	= -1;
}
