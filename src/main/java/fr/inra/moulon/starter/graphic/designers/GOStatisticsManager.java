/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 02-Aug-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.designers;

import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.gontology.GOTerm;
import fr.inra.moulon.starter.datamodel.gontology.GOntologies;

import fr.inra.moulon.starter.utils.DistributionUtilities;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import org.jfree.chart.ChartPanel;


public class GOStatisticsManager {
	protected GOStatisticsManager(){
		_map = new HashMap<String, GOStatistics>();
		_labels = new HashMap<Integer, JLabel>();
		_infos = new HashMap<Integer, String>();
	}

	public static GOStatisticsManager instance(){
		if (null == _instance){
			synchronized (GOStatisticsManager.class){
				if (null == _instance){
					_instance = new GOStatisticsManager();
				}
			}
		}
		return _instance;
	}


	public	void	addLabel(Integer infoID, JLabel label){
		_labels.put(infoID, label);
		_infos.put(infoID, "");
	}

	/**
	 * Sets the information string of the given information ID.
	 * @param ontologyId
	 * @param infoID
	 * @param infoStr
	 */
	public	void	setInfo(Integer infoID, String infoStr){
		JLabel		label = _labels.get(infoID);

		_infos.put(infoID, infoStr);
		if (null != label){
			if (null != infoStr){
				label.setText(infoStr);
			}else{
				label.setText("-NA");
			}
		}
	}

	/**
	 * Sets the standard informations about the given goTerm ID.
	 * @param goTermID
	 */
	public	void	setInfos(String goTermID){
		GOTerm		goTerm = GOntologies.instance().getTerm(goTermID);

		if (null != goTerm){
			setInfo(GOStatistics.INFO_GOTERM_ID, goTerm.getID());
			setInfo(GOStatistics.INFO_GOTERM_NAME, goTerm.getName());
		}
	}

	/**
	 * Returns any information previously stored at the given ID.
	 * @param infoID
	 * @return Any information previously stored at the given ID.
	 */
	public	String	getInfo(Integer infoID){
		return _infos.get(infoID);
	}

	/**
	 * Cleans all the information strings.
	 * @param ontologyId
	 */
	public	void	cleanInfos(){
		JLabel		label = null;

		for (Iterator<Integer> it = _labels.keySet().iterator(); it.hasNext();){
			label = _labels.get(it.next());
			label.setText("");
		}
		_infos.clear();
	}

	public	void	addGOStatistics(String lkgName, GOStatistics goStats){
		_map.put(lkgName, goStats);
	}

	public	GOStatistics	setSelected(String lkgName){
		_selectedLkgName = lkgName;
		_selected = _map.get(lkgName);

		return _selected;
	}

	public	boolean	hasSelected(){
		return null != _selected;
	}

	public	GOStatistics	getSelected(){
		return _selected;
	}

	public	String	getSelectedLkgName(){
		return _selectedLkgName;
	}

	public	void	addPanel(JPanel panel){
		_panel = panel;
	}

	public	void	setChartPanel(ChartPanel chartPanel){
		_panel.removeAll();
		_panel.add(chartPanel);
		_panel.revalidate();
		_panel.repaint();
	}

	public	MapGene	getLoadedMap(){
		return _mapGene;
	}

	public	void	clean(){
		_selected = null;
		_map = new HashMap<String, GOStatistics>();
		_labels = new HashMap<Integer, JLabel>();
		_infos = new HashMap<Integer, String>();
		for (Iterator<String> it = _map.keySet().iterator(); it.hasNext();) {
			_map.get(it.next()).clear();
		}
	}

	///////////////////////// ANALYSIS /////////////////////////////
	/**
	 * Returns a List of String arrays corresponding to the pValues and qValue
	 * of each goTerm into the given window.
	 * @param icStart
	 * @param icEnd
	 * @param winEndMax
	 * @param progressBar
	 * @return A list of String array : List<[goTerm, pValue, qValue]>
	 */
	public	List<Distribution>		analyseBoyle(	Map<String, List<double[]>>	ics,
													Map<String, List<double[]>>	icsRef,
													Boolean						wholeGenome,
													JProgressBar				progressBar){
		Map<String, List<int[]>>	indexesGenes	= new HashMap<String, List<int[]>>();
		Map<String, List<int[]>>	indexesGenesRef	= null;
		Set<String>					goTerms			= null;
		List<Distribution>			valuesAll		= new ArrayList<Distribution>();
		List<Distribution>			valuesSelected	= new ArrayList<Distribution>();
		Set<String>					goTermsFamily	= null;
		double						hyp				= 0;
		int[]						counts			= null;
		int							nbWinGoSelect	= 0;
		int							nbWinGoAll		= 0;
		int							nbChrGoSelect	= 0;
		int							nbChrGoAll		= 0;
		int							nbGOTerms		= 0;
		int							nbGenesSameDepth= 0;
		int							idxCur			= 0;
		GOStatistics				goStats			= null;

		// Get all GO Terms in the genome or the chromosome
		if (wholeGenome){
			goTerms = new HashSet<String>();
			for (Iterator<String> it = _map.keySet().iterator(); it.hasNext();) {
				String lkgName = it.next();

				goTerms = _map.get(lkgName).getGOTermsPresent(goTerms);
			}
		}else{
			goTerms = _selected.getGOTermsPresent();
		}
		nbGOTerms = goTerms.size();

		// Finding genes indexes
		for (Iterator<String> it = _map.keySet().iterator(); it.hasNext();) {
			String			lkgName = it.next();
			List<double[]>	icList	= null;

			icList = ics.get(lkgName);
			goStats = _map.get(lkgName);

			if (!indexesGenes.containsKey(lkgName)){
				indexesGenes.put(lkgName, new ArrayList<int[]>());
			}
			if (null != icList){
				for (int i = 0; i < icList.size(); i++) {
					double[] ic = icList.get(i);
					indexesGenes.get(lkgName).add(goStats.getIdxGenesIC(ic[0], ic[1]));
				}
			}
		}

		// Finding genes indexes for REF
		if (null != icsRef){
			indexesGenesRef = new HashMap<String, List<int[]>>();
			for (Iterator<String> it = _map.keySet().iterator(); it.hasNext();) {
				String			lkgName = it.next();
				List<double[]>	icList	= null;

				icList = icsRef.get(lkgName);
				goStats = _map.get(lkgName);

				if (!indexesGenesRef.containsKey(lkgName)){
					indexesGenesRef.put(lkgName, new ArrayList<int[]>());
				}
				if (null != icList){
					for (int i = 0; i < icList.size(); i++) {
						double[] ic = icList.get(i);
						indexesGenesRef.get(lkgName).add(goStats.getIdxGenesIC(ic[0], ic[1]));
					}
				}
			}
		}

		// Iterator on all terms for computing pValue
		for (Iterator<String> itTermID = goTerms.iterator(); itTermID.hasNext();){
			String	goTermID = itTermID.next();
			GOTerm	term	= GOntologies.instance().getTerm(goTermID);
			int		percent = (int)Math.round(100*idxCur++/(double)nbGOTerms);
			int		depth	= (null != term)?term.getDepth():-1;

			progressBar.setValue(percent);
			// Fill the go terms's descendents.
			goTermsFamily = GOntologies.instance().fillGoTermFamily(goTermID);

//****************************************************************************//
			if (null != indexesGenesRef){
				counts = getCountGOGenesICs(goTermsFamily, indexesGenesRef, depth);
			}else if (wholeGenome){
				counts = getCountGOGenesGenome(goTermsFamily, depth);
			}else{
				counts = getCountGOGenesChr(goTermsFamily, depth);
			}
			nbChrGoSelect = counts[0];
			nbChrGoAll = counts[1];
			nbGenesSameDepth = counts[2];
			valuesAll.clear();

			counts = getCountGOGenesICs(goTermsFamily, indexesGenes, depth);
			nbWinGoSelect = counts[0];
			nbWinGoAll = counts[1];
			hyp = 0;
			for (double i = nbWinGoSelect; i < nbWinGoAll; ++i){
				double p = DistributionUtilities.hyperGeometric(i, nbChrGoSelect, nbChrGoAll-nbChrGoSelect, nbWinGoAll);
				hyp += p;
			}
			if (0 != hyp && !(0 == nbWinGoSelect && 0 == nbChrGoSelect)){
				valuesAll.add(new Distribution(goTermID, hyp, nbWinGoSelect, nbChrGoSelect, nbGenesSameDepth));
			}

			// Sorting values list by p-Values
			Collections.sort(valuesAll, new DistributionPValueComparator());

			// Verifying pValues with qValues and threshold
			int idx = 0;
			for (Iterator<Distribution> it = valuesAll.iterator(); it.hasNext();) {
				Distribution	distr = it.next();

				valuesSelected.add(distr);
				++idx;
			}
//****************************************************************************//
		}
		// Sorting values list by p-Values (with keeping the window index)
		Collections.sort(valuesSelected, new DistributionPValueComparator());

		return valuesSelected;
	}

	int[]		getCountGOGenesICs(	Set<String>					goTermsFamily,
									Map<String, List<int[]>>	indexesGenes,
									int							depth){
		int[]	countsTmp = null;
		int[]	countsRes = new int[]{0, 0, 0};

		for (Iterator<String> it = indexesGenes.keySet().iterator(); it.hasNext();) {
			String			lkgName = it.next();
			GOStatistics	goStats	= _map.get(lkgName);

			for (Iterator<int[]> itIdx = indexesGenes.get(lkgName).iterator(); itIdx.hasNext();) {
				int[] idx = itIdx.next();

				countsTmp = goStats.countGOGenesIC(goTermsFamily, idx[0], idx[1], depth, false);
				countsRes[0] = countsRes[0] + countsTmp[0];
				countsRes[1] = countsRes[1] + countsTmp[1];
				countsRes[2] = countsRes[2] + countsTmp[2];
			}
		}

		return countsRes;
	}

	int[]		getCountGOGenesChr(Set<String> goTermsFamily, int depth){
		int[]	countsRes = null;

		countsRes = _selected.countGOGenesIC(goTermsFamily, 0, _selected.getGenesNb()-1, depth, true);

		return countsRes;
	}

	int[]		getCountGOGenesGenome(Set<String> goTermsFamily, int depth){
		int[]	countsTmp = null;
		int[]	countsRes = new int[]{0, 0, 0};

		for (Iterator<String> it = _map.keySet().iterator(); it.hasNext();) {
			String			lkgName = it.next();
			GOStatistics	goStats	= _map.get(lkgName);

			countsTmp = goStats.countGOGenesIC(goTermsFamily, 0, goStats.getGenesNb()-1, depth, true);
			countsRes[0] = countsRes[0] + countsTmp[0];
			countsRes[1] = countsRes[1] + countsTmp[1];
			countsRes[2] = countsRes[2] + countsTmp[2];
		}

		return countsRes;
	}

	public class	Distribution{
		public	Distribution(String goTermID, double pValue){
			this.goTermID		= goTermID;
			this.pValue			= pValue;
		}

		public	Distribution(String goTermID, double pValue, Integer nbCountWin, Integer nbCountTotal, Integer nbGenesSameDepth){
			this(goTermID, pValue);
			this.nbCountWin			= nbCountWin;
			this.nbCountTotal		= nbCountTotal;
			this.nbGenesSameDepth	= nbGenesSameDepth;
		}

		@Override
		public	String	toString(){
			return	"GO : " + goTermID		+ " ** "
					+ "pValue : " + pValue	+ " ** "
					+ "qValue : " + qValue;
		}

		public	Double	pValue			= null;
		public	Double	qValue			= null;
		public	Integer	nbCountWin		= null;
		public	Integer	nbCountTotal	= null;
		public	String	goTermID		= null;
		public	Integer	nbGenesSameDepth= null;
	}

	class	Window extends Distribution{
		public	Window(String goTerm, int iWindow, double pValue){
			super(goTerm, pValue);
			this.iWindow = iWindow;
		}

		@Override
		public	String	toString(){
			return	"GO : " + goTermID		+ " ** "
					+ "iWin : " + iWindow	+ " ** "
					+ "pValue : " + pValue	+ " ** "
					+ "qValue : " + qValue;
		}

		int		iWindow	= 0;
	}

	class	DistributionPValueComparator implements Comparator<Distribution>{
		@Override
		public int compare(Distribution distr1, Distribution distr2) {
			return distr1.pValue.compareTo(distr2.pValue);
		}
	}

	private	Map<String, GOStatistics>	_map				= null;
	private	GOStatistics				_selected			= null;
	private	Map<Integer, JLabel>		_labels				= null;
	private	Map<Integer, String>		_infos				= null;
	private	JPanel						_panel				= null;
	private	MapGene						_mapGene			= null;
	private	String						_selectedLkgName	= null;

	// singleton attribute
	private volatile	static	GOStatisticsManager	_instance	= null;
}