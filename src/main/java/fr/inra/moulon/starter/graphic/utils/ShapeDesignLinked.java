/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;


public class ShapeDesignLinked extends ShapeDesign{
	public	static	Font	FONT_DEFAULT	= new Font("Default",	Font.PLAIN,	11);
	public	static	Font	FONT_BOLD		= new Font("Bold",		Font.BOLD,	11);
	public	static	Font	FONT_SMALL		= new Font("Small",		Font.PLAIN, 5);

	public	ShapeDesignLinked(int		shapeId,
							double	x,
							double	y,
							double	w,
							double	h,
							Color	color){
		super(shapeId, x, y, w, h, color);
		linkCoords = new double[4][2];
	}

	public	ShapeDesignLinked(double bioPosition){
		super(bioPosition);
		linkCoords = new double[4][2];
	}

	@Override
	public	void	displayOnGraphics(Graphics2D g2d){
//		Font		fontSave = null;

		super.displayOnGraphics(g2d);

		if (show){
			g2d.setColor(color);
			if (null != linkCoords && linkCoords[0].length > 1){	// Display name
//				fontSave = g2d.getFont();
//				g2d.setFont(FONT_DEFAULT);
				g2d.drawString(str, (int)strX, (int)strY);
				for (int i = 0; i < linkCoords.length-1; i++) {
					double[] prevPoint	= linkCoords[i];
					double[] curPoint	= linkCoords[i+1];

					g2d.drawLine(	(int)prevPoint[0],
									(int)prevPoint[1],
									(int)curPoint[0],
									(int)curPoint[1]);
				}
//				g2d.setFont(fontSave);
			}
		}
	}

	public	double[][]	linkCoords	= null;
	public	String		str			= null;
	public	double		strX		= 0;
	public	double		strY		= 0;
	public	Boolean		show		= Boolean.TRUE;
}
