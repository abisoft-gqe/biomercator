/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

import fr.inra.moulon.starter.graphic.designers.LinkageGroupDesigner;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;


public class Scroller {
	public	static final	int	OUSIDE	= 0;
	public	static final	int	INSIDE	= 1;
	public	static final	int	TOP		= 2;
	public	static final	int	BOTTOM	= 3;

	public	Scroller(	LinkageGroupDesigner	lkgDes,
						double					x,
						double					w
						){
		_lkgDes	= lkgDes;
		_edge	= new ShapeDesign(ShapeDesign.ROUND_RECTANGLE, x, 0, w, 0, Color.BLACK);
		_inner	= new ShapeDesign(ShapeDesign.FILLED_RECTANGLE, x, 0, w, 0, new Color(200, 200, 200, 100));
	}

	/**
	 * Enables the scroller : shows it and begins managing the its linkage group
	 * designer son if any.
	 */
	public	void	enable(	double	y,
							boolean	scrollerOrdered){
		enable(y, _edge.h, scrollerOrdered);
	}

	/**
	 * Enables the scroller : shows it and begins managing the its linkage group
	 * designer son if any.
	 */
	public	void	enable(	double	y,
							double	h,
							boolean	scrollerOrdered){
		Scroller	scroller	= null;
		double		yStartLink	= 0;
		double		yEndLink	= 0;
		double		hLink		= 0;
		double		posStart	= 0;
		double		posEnd		= 0;

		_edge.y = y-h/2;
		_inner.y = y-h/2;
		_edge.h = h;
		_inner.h = h;
		_enabled = true;
		if (null != _scrollerLink && !scrollerOrdered && null != _anchors){
			yStartLink = pxl_to_pxl(_edge.y);
			yEndLink = pxl_to_pxl(_edge.y+_edge.h);
			hLink = yEndLink - yStartLink;
			_scrollerLink._lkgDes.enableScroller(yStartLink+hLink/2, hLink, true);
		}
		if (null != _lkgDesSon){
			posStart = _lkgDes.position_pxl_to_data(new Point(0, (int)_edge.y));
			posEnd = _lkgDes.position_pxl_to_data(new Point(0, (int)(_edge.y+_edge.h)));
			_lkgDesSon.setWindow(posStart, posEnd);
			_lkgDesSon.recreateShapes();
			if (null != (scroller = _lkgDesSon.getScroller()) && scroller.isEnabled()){
				scroller.enable(scroller._edge.y, scroller._edge.h, false);
			}
		}
	}

	public	double	getPositionTop(){
		return _inner.y;
	}

	public	double	getPositionBottom(){
		return _inner.y + _inner.h;
	}

	/**
	 * Disables the scroller.
	 */
	public	void	disable(){
		_enabled = false;
	}

//	public	double		pxl_to_pxl(double pos_pxl){
//		double			anchorsDistance		= 0;
//		double			anchorsNewDistance	= 0;
//		double			pos_bio				= _lkgDes.position_pxl_to_data(new Point(0, (int)pos_pxl));
//		double			posNew_bio			= 0;
//		double			posNew_pxl			= 0;
//		List<Anchor>	anchors				= findFlankingAnchors(pos_bio, _unitConversionId);
//		int				curUnitId			= Anchor.getUnitFrom(_unitConversionId);
//		int				lkUnitId			= Anchor.getUnitTo(_unitConversionId);
//
//		if (null != anchors){
//
//			anchorsDistance = anchors.get(1).getPosition(curUnitId) - anchors.get(0).getPosition(curUnitId);
//			anchorsNewDistance = anchors.get(1).getPosition(lkUnitId) - anchors.get(0).getPosition(lkUnitId);
//			posNew_bio = (pos_bio - anchors.get(0).getPosition(curUnitId)) * anchorsNewDistance / anchorsDistance;
//			posNew_bio += anchors.get(0).getPosition(lkUnitId);
//			posNew_pxl = _scrollerLink._lkgDes.position_data_to_pxl(posNew_bio).getY();
//		}
//
//
//		return posNew_pxl;
//	}

	public	double		pxl_to_pxl(double pos_pxl){
		double			pos_bio				= _lkgDes.position_pxl_to_data(new Point(0, (int)pos_pxl));
		double			posNew_bio			= 0;
		double			posNew_pxl			= 0;

		posNew_bio = data_to_data(pos_bio);
		posNew_pxl = _scrollerLink._lkgDes.position_data_to_pxl(posNew_bio).getY();

		return posNew_pxl;
	}

	public	double		data_to_data(double pos_bio){
		double[]		extremsPositions	= null;
		double			anchorsDistance		= 0;
		double			anchorsNewDistance	= 0;
		double			posNew_bio			= 0;
		List<Anchor>	anchors				= findFlankingAnchors(pos_bio, _unitConversionId);
		int				curUnitId			= Anchor.getUnitFrom(_unitConversionId);
		int				lkUnitId			= Anchor.getUnitTo(_unitConversionId);

		if (null != anchors){
			if (anchors.size() == 2){
				// The position is between 2 anchors
				anchorsDistance = anchors.get(1).getPosition(curUnitId) - anchors.get(0).getPosition(curUnitId);
				anchorsNewDistance = anchors.get(1).getPosition(lkUnitId) - anchors.get(0).getPosition(lkUnitId);
				posNew_bio = (pos_bio - anchors.get(0).getPosition(curUnitId)) * anchorsNewDistance / anchorsDistance;
				posNew_bio += anchors.get(0).getPosition(lkUnitId);
			}else{
				// The position is bigger than the last anchor's
				extremsPositions = _lkgDes.getExtremPositions();
				posNew_bio = extremsPositions[1];
			}
		}else{
			// The position is prior to the first anchor
			extremsPositions = _lkgDes.getExtremPositions();
			posNew_bio = extremsPositions[0];
		}


		return posNew_bio;
	}

	public	List<Anchor>	findFlankingAnchors(double pos_bio, int unitId){
		List<Anchor>		anchors	= null;
		int					index	= 0;

		while (index < _anchors.size() &&
				pos_bio >= _anchors.get(index).getPosition(unitId)) {
				++index;
		}
		if (index > 0){
			anchors = new ArrayList<Anchor>();
			anchors.add(_anchors.get(index-1));
			if (index < _anchors.size()){
				anchors.add(_anchors.get(index));
			}
		}

		return anchors;
	}

	/**
	 * Returns the area containing the the given point.
	 * @param p The point
	 * @return the area containing the the given point
	 */
	public	int	getArea(Point p){
		int		id		= OUSIDE;
		int		margin	= 5;

		if (_enabled					&&
			p.x >= _edge.x				&&
			p.x <= _edge.x + _edge.w	&&
			p.y >= _edge.y				&&
			p.y <= _edge.y + _edge.h){
			if (p.y <= _edge.y + margin){
				id = TOP;
			}else if (p.y >= _edge.y + _edge.h - margin){
				id = BOTTOM;
			}else{
				id = INSIDE;
			}
		}

		return id;
	}

	/**
	 * Sets the scroller "selected" attribute and changes its display in
	 * consequence.
	 * @param selected If the scroller is to be selected
	 * @return if the scroller's display has been changed
	 */
	public	boolean	setSelected(boolean selected){
		boolean		res = false;

		if (_selected != selected){
			_selected = selected;
			res = true;
			if (_selected){
				_inner.color = new Color(50, 50, 50, 100);
			}else{
				_inner.color = new Color(50, 50, 50, 255);
			}
		}

		return res;
	}

//	/**
//	 * Returns if the scroller is selected.
//	 * @return if the scroller is selected
//	 */
//	public	boolean	isSelected(){
//		return _selected;
//	}

	/**
	 * Returns if the scroller is selected.
	 * @return if the scroller is selected
	 */
	public	boolean	isEnabled(){
		return _enabled;
	}

	private	boolean	isScrollerHighEnough(double	size){
		return size > 1;
	}

	/**
	 * Moves the scroller of the corresponding pixels movements, the scroller's
	 * son (if any) and the linked scroller (if any).
	 * @param mvtTop_pxl The movement to achieve to the top border
	 * @param mvtBottom_pxl The movement to achieve to the bottom border
	 * @param mandatoryDualMoves If the movements must be done both together
	 * @param scrollerOrderer If the function is called from a scroller (to
	 * avoid infinite loop)
	 * @return If the movements have been done
	 */
	public	boolean	moveScroller(	double	mvtTop_pxl,
									double	mvtBottom_pxl,
									boolean	mandatoryDualMoves,
									boolean	scrollerOrderer){
		boolean		res			= false;
		double[]	extrems		= null;
		double		posStart	= 0;
		double		posEnd		= 0;

		if (isScrollerHighEnough(_edge.h - mvtTop_pxl + mvtBottom_pxl)){
			// MOVING SHAPE COORDINATE
			res			= true;
			_edge.y		+= mvtTop_pxl;
			_inner.y	+= mvtTop_pxl;
			_edge.h		-= mvtTop_pxl;
			_inner.h	-= mvtTop_pxl;
			_edge.h		+= mvtBottom_pxl;
			_inner.h	+= mvtBottom_pxl;

			res = refreshAllSubScrollers();
			if (res){
				posStart = _lkgDes.position_pxl_to_data(new Point(0, (int)_edge.y));
				posEnd = _lkgDes.position_pxl_to_data(new Point(0, (int)(_edge.y+_edge.h)));
				extrems = _lkgDes.getExtremWindowPositions();
				if (posStart < extrems[0] || posEnd > extrems[1]){
					res = false;
				}
				if (res && null != _scrollerLink && !scrollerOrderer){
					double	yStartLink = pxl_to_pxl(_edge.y);
					double	yEndLink = pxl_to_pxl(_edge.y+_edge.h);
					_scrollerLink.moveScroller(	yStartLink - _scrollerLink._edge.y,
												yEndLink - (_scrollerLink._edge.y + _scrollerLink._edge.h),
												false,
												true);
				}
			}
			if (!res){
				// UNDO CHANGES
				_edge.y		-= mvtTop_pxl;
				_inner.y	-= mvtTop_pxl;
				_edge.h		+= mvtTop_pxl;
				_inner.h	+= mvtTop_pxl;
				_edge.h		-= mvtBottom_pxl;
				_inner.h	-= mvtBottom_pxl;
			}
		}

		return res;
	}

	/**
	 * Allow a scroller to refresh all descending sons.
	 */
	private	boolean	refreshAllSubScrollers(){
		double		posStart	= 0;
		double		posEnd		= 0;
		Scroller	scroller	= null;
		double[]	extrems		= null;
		boolean		res			= true;

		if (null != _lkgDesSon){
			posStart = _lkgDes.position_pxl_to_data(new Point(0, (int)_edge.y));
			posEnd = _lkgDes.position_pxl_to_data(new Point(0, (int)(_edge.y+_edge.h)));
			extrems = _lkgDes.getExtremWindowPositions();
			if (posStart < extrems[0] || posEnd > extrems[1]){
				res = false;
			}
			if (res && null != (scroller = _lkgDesSon.getScroller()) && scroller.hasSon()){
				res = scroller.refreshAllSubScrollers();
			}
			if (res){
				_lkgDesSon.setWindow(posStart, posEnd);
				_lkgDesSon.recreateShapes();
			}
		}
		return res;
	}

	/**
	 * Returns the list of shapeDesigns representing the scroller.
	 * @return The list of shapeDesigns representing the scroller.
	 */
	public	List<ShapeDesign>	getshapes(){
		List<ShapeDesign>		list = new ArrayList<ShapeDesign>();

		list.add(_edge);
		list.add(_inner);

		return list;
	}

	/**
	 * Returns the scroller's width.
	 * @return the scroller's width
	 */
	public	double	getWidth(){
		return _edge.w;
	}

	/**
	 * Sets the scroller's width.
	 * @param width The scroller's width
	 */
	public	void	setWidth(double width){
		_edge.w = width;
		_inner.w = width;
	}

	public	void	setLkgSon(LinkageGroupDesigner lkgDesSon){
		_lkgDesSon = lkgDesSon;
	}

	public	void	setScrollerLink(Scroller scrollerLink){
		_scrollerLink = scrollerLink;
	}

	/* Sets the anchors list.
	 * @param anchors The anchors list
	 */
	public	void	setAnchors(List<Anchor> anchors){
		_anchors = anchors;
	}

	/* Gets the anchors list.
	 * @return The anchors list.
	 */
	public	List<Anchor>	getAnchors(){
		return _anchors;
	}

	/**
	 * The the scroller unit (CM_TO_PB or PB_TO_CM).
	 * @param unitId the scroller unit
	 */
	public	void	setUnitConversionId(int unitConversionId){
		_unitConversionId = unitConversionId;
	}

	public	int	getOpposedConversionId(int unitConversionId){
		int		opposedId = 0;

		switch(unitConversionId){
			case Anchor.CM_TO_PB:
				opposedId = Anchor.PB_TO_CM;
				break;
			case Anchor.PB_TO_CM:
				opposedId = Anchor.CM_TO_PB;
				break;
		}

		return opposedId;
	}

	/**
	 * Returns if the scroller has a linkage group designer as son.
	 * @return If the scroller has a linkage group designer as son.
	 */
	public	boolean	hasSon(){
		return (null != _lkgDesSon);
	}

	/**
	 * Removes the scoller's son.
	 */
	public	void	removeSon(){
		_lkgDesSon = null;
	}

	/**
	 * Returns if the scroller has the given linkage group designer as son.
	 * @return If the scroller has the given linkage group designer as son.
	 */
	public	boolean	hasSon(LinkageGroupDesigner lkgDes){
		return (lkgDes == _lkgDesSon);
	}

	public	void	setEnabled(boolean enabled){
		_enabled = enabled;
	}

	private	boolean					_selected			= false;
	private	boolean					_enabled			= false;
	private	ShapeDesign				_edge				= null;
	private	ShapeDesign				_inner				= null;
	public	LinkageGroupDesigner	_lkgDes				= null;	// The linkage group designer
	public	LinkageGroupDesigner	_lkgDesSon			= null;	// The linkage group designer in relation with the scroller
	public	Scroller				_scrollerLink		= null;	// The scroller linked to the current one
	private	List<Anchor>			_anchors			= null;	// A sorted list of anchors
	private	int						_unitConversionId	= 0;
}
