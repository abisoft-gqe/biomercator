/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.designers;

import fr.inra.moulon.exceptions.ExceptionPhysical;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.graphic.utils.Anchor;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import fr.inra.moulon.starter.utils.Session;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;


public class	LinkageGroupDesignerPhysical extends LinkageGroupDesigner {
	public			LinkageGroupDesignerPhysical(	final	LinkageGroup	lkg,
													final	MetaModel		metaModel) throws Exception{
		super(lkg, metaModel);

		double[]	extrems	= null;

		extrems = PhysicalManager.instance().getExtrems(_LKG.getName());
		if (null != extrems){
			initExtremValues(extrems[0], extrems[1]);
			_scroller.setWidth(16);
		}else{
			throw new ExceptionPhysical(_bundle.getString("exception_no_lkg_corresponding"));
		}
	}

	@Override
	public	double	getWidth(){
		return	LEFT_MARGIN				+
				_scroller.getWidth()	+
				RIGHT_MARGIN;
	}

	@Override
	public double getHeight() {
		return 600;
	}

	@Override
	public	final void recreateShapes() {
		setMiscShapeDesigns();
		setAnchorsDesign();
	}

	/**
	 * Sets the misc list filled with ShapeDesigns.(such as chromosome's edge)
	 */
	private	void			setMiscShapeDesigns(){
		List<ShapeDesign>	miscList = new ArrayList<ShapeDesign>();

		miscList.add(new ShapeDesign(ShapeDesign.LINE, 0, 0, 0, super.getMaxPxlSize(), Color.BLACK));
		_elements.put(MISC, miscList);
	}

	private	void			setAnchorsDesign(){
		List<ShapeDesign>	anchorsList	= null;
		int					anchorWidth	= 6;

		if (null != _anchors){
			anchorsList	= new ArrayList<ShapeDesign>();
			for (Iterator<Anchor> it = _anchors.iterator(); it.hasNext();) {
				Anchor anchor = it.next();

				anchorsList.add(new ShapeDesign(ShapeDesign.LINE,
												-anchorWidth/2,
												position_data_to_pxl(anchor.getPosition(Anchor.PB)).getY(),
												anchorWidth,
												0,
												Color.BLUE));
			}
			_elements.put(ANCHOR, anchorsList);
		}
	}

	@Override
	public void darkenShapes(boolean darken) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public ShapeDesign onWhichElt(Point p) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public double getScrollerX() {
		return -_scroller.getWidth()/2;
	}

	private	static	ResourceBundle	_bundle = (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
}
