/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 02-Aug-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.designers;

import fr.inra.moulon.starter.datamodel.gontology.GOTerm;
import fr.inra.moulon.starter.datamodel.gontology.GOntologies;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignPhyElt;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class GOStatistics {
	public	final	static	Integer	WIDTH	= 100;

	public	final	static	Integer	INFO_GOTERM_ID		= 1;
	public	final	static	Integer	INFO_GOTERM_NAME	= 2;
	public	final	static	Integer	INFO_CHART_LEVEL	= 3;
	public	final	static	Integer	INFO_GENES_NB		= 4;
	public	final	static	Integer	INFO_GOTERM_NB		= 5;

	public	GOStatistics(){
	}

//	protected GOStatistics(){
//		_labels = new HashMap<Integer, JLabel>();
//		_infos = new HashMap<Integer, String>();
//	}
//
//	public static GOStatistics instance(){
//		if (null == _instance){
//			synchronized (GOStatistics.class){
//				if (null == _instance){
//					_instance = new GOStatistics();
//				}
//			}
//		}
//		return _instance;
//	}

	public	void	init(	List<ShapeDesignPhyElt>	genesList,
							int						nbdots,
							double					maxSize,
							double					heightPxl){
		_genesList		= genesList;
		_nbdots			= nbdots;
		_maxSize		= maxSize;
		_heightPxl		= heightPxl;
	}

	private	void			fillGenesFamily(String goTermID){
		ShapeDesignPhyElt	shape			= null;
		Set<String>			goTermsFamily	= null;
		int					nbGenesGOWin	= 0;


		_goGenesFamily.clear();
		goTermsFamily = GOntologies.instance().fillGoTermFamily(goTermID);
		for (int i = 0; i < _genesList.size(); ++i){
			shape = _genesList.get(i);
			if (null != shape.go_terms){
				boolean	found = false;

				for (Iterator<String> it = shape.go_terms.iterator(); !found && it.hasNext();) {
					String goTermId = it.next();
					found = goTermsFamily.contains(goTermId);
				}
				if (found){
					++nbGenesGOWin;
					_goGenesFamily.add(shape.id);
				}
			}
		}
	}

	private	double[]		getRatios(String goTermID){
		ShapeDesignPhyElt	shape			= null;
		Set<String>			goTermsFamily	= null;
		double[]			ratios			= new double[_nbdots];
		double				startPos		= 0;
		double				winSize			= _maxSize/_nbdots;
		double				endPos			= winSize;
		int					i_shape			= 0;
		int					nbGenesGO		= 0;
		int					nbGenesGOWin	= 0;

		goTermsFamily = GOntologies.instance().fillGoTermFamily(goTermID);
		shape = _genesList.get(i_shape);
		for (int i = 0; i < ratios.length; ++i){
			nbGenesGOWin = 0;
			while (i_shape < _genesList.size()-1 &&
					((shape.bioPosition >= startPos && shape.bioPosition <= endPos) ||
					(shape.bioPositionEnd >= startPos && shape.bioPositionEnd <= endPos))){
				if (null != shape.go_terms){
					boolean	found = false;

					for (Iterator<String> it = shape.go_terms.iterator(); !found && it.hasNext();) {
						String goTermId = it.next();
						found = goTermsFamily.contains(goTermId);
					}
					if (found){
						++nbGenesGOWin;
					}
				}
				shape = _genesList.get(++i_shape);
			}
			ratios[i] = nbGenesGOWin;
			startPos += winSize;
			endPos += winSize;
			nbGenesGO += nbGenesGOWin;
		}
		for (int i = 0; i < ratios.length; ++i){
			ratios[i] = ratios[i]*100/(double)nbGenesGO;
		}
		GOStatisticsManager.instance().setInfo(GOStatistics.INFO_GENES_NB, String.valueOf(nbGenesGO));
		return ratios;
	}

	private	void	createDensityShapes(double[] density){
		ShapeDesign	shape = null;

		_shapes = new ArrayList<ShapeDesign>();
		for (int i = 0; i < density.length; ++i) {
			shape = new ShapeDesign(ShapeDesign.LINE,
									0,
									i*_heightPxl/_nbdots,
									density[i],
									0,
									Color.BLUE);
			_shapes.add(shape);
		}

	}

	public	void	computeDensity(String goTermID){
		fillGenesFamily(goTermID);
		_density = getRatios(goTermID);
		createDensityShapes(_density);
	}

	public	Iterator<ShapeDesign>	iterator(){
		return null != _shapes ? _shapes.iterator() : null;
	}

	public	Set<String>	getGOGenesFamily(){
		return _goGenesFamily;
	}

	/**
	 * Returns if a GO density has been computed and shapes have been created.
	 * @return If a GO density has been computed and shapes have been created.
	 */
	public	boolean	exists(){
		return null != _shapes;
	}

	/**
	 * Clears all shapes; a call right after to exist() will return false.
	 */
	public	void	clear(){
		_shapes = null;
		GOStatisticsManager.instance().cleanInfos();
	}

	/**
	 * Returns the genes number.
	 * @return The genes number.
	 */
	public	int	getGenesNb(){
		return _genesList.size();
	}

	/**
	 * Returns an array of genes count. The first index of the returned array
	 * corresponds to the number of genes inside the window belonging to the
	 * chosen GOTerms; the second index is the number of genes inside the
	 * window.
	 * @param iGeneStart The first gene index
	 * @param iGeneEnd The last gene (inclusive) index
	 * @return [nb_genes_in_window_filterGO, nb_genes_in_window, nb_genes_with_same_depth]
	 */
	public	int[]	countGOGenesIC(	Set<String>	goTermsFamily,
									int			idxStart,
									int			idxEnd,
									int			depth,
									boolean		findNbGenesDepth){
		int			nbGOGenesIC		= 0;
		int			nbGenes			= 0;
		int			nbGenesDepth	= 0;
		GOTerm		term			= null;

		for (int iGenes = idxStart; iGenes <= idxEnd; ++iGenes) {
			ShapeDesignPhyElt shape = _genesList.get(iGenes);

			if (null != shape.go_terms){
				boolean	found = false;
				boolean	foundSameDepth = !findNbGenesDepth;
				for (Iterator<String> it = shape.go_terms.iterator(); !found && it.hasNext();) {
					String goTermId = it.next();

					if (!foundSameDepth && null != (term = GOntologies.instance().getTermTMP(goTermId))){
						if (term.getDepth() >= depth){
							++nbGenesDepth;
							foundSameDepth = true;
						}
					}
					found = goTermsFamily.contains(goTermId);
				}
				if (found){
					++nbGOGenesIC;
				}
			}
			++nbGenes;
		}

		return new int[]{nbGOGenesIC, nbGenes, nbGenesDepth};
	}

	/**
	 * Returns an array of genes count. The first index of the returned array
	 * corresponds to the number of genes inside the window belonging to the
	 * chosen GOTerms; the second index is the number of genes inside the
	 * window.
	 * @param iGeneStart The first gene index
	 * @param iGeneEnd The last gene (inclusive) index
	 * @return [nb_genes_in_window_filerGO, nb_genes_in_window]
	 */
	public	int[]			getIdxGenesIC(	double	icStart,
											double	icEnd){
		ShapeDesignPhyElt	shape		= null;
		int					idxStart	= 0;
		int					idxEnd		= 0;
		int					iGenes		= 0;
		boolean				under		= true;
		boolean				above		= false;

		// Get the first gene inside the IC
		while (iGenes < _genesList.size() && under){
			shape = _genesList.get(iGenes);
			if (shape.bioPositionEnd > icStart){
				under = false;
			}else{
				++iGenes;
			}
		}

		if (!under){
			idxStart = iGenes;
			while (iGenes < _genesList.size() && !above){
				shape = _genesList.get(iGenes);
				if (shape.bioPosition > icEnd){
					above = true;
				}else{
					++iGenes;
				}
			}
			idxEnd = iGenes;
		}

		return new int[]{idxStart, idxEnd};
	}

	/**
	 * Returns the GO terms present in the chromosome.
	 * @return The GO terms present in the chromosome.
	 */
	public	Set<String>	getGOTermsPresent(){
		Set<String>		goTermsPresent = new HashSet<String>();

		return getGOTermsPresent(goTermsPresent);
	}

	/**
	 * Appends and return the GO terms present in the chromosome to the given
	 * set.
	 * @return The GO terms present in the chromosome.
	 */
	public	Set<String>	getGOTermsPresent(Set<String> goTermsPresent){
		for (Iterator<ShapeDesignPhyElt> itGenes = _genesList.iterator(); itGenes.hasNext();) {
			ShapeDesignPhyElt shape = itGenes.next();

			if (null != shape.go_terms){
				for (Iterator<String> itGoTerms = shape.go_terms.iterator(); itGoTerms.hasNext();) {
					goTermsPresent.add(itGoTerms.next());
				}
			}
		}

		return goTermsPresent;
	}

	public class	Distribution{
		public	Distribution(String goTermID, double pValue){
			this.goTermID		= goTermID;
			this.pValue			= pValue;
		}

		public	Distribution(String goTermID, double pValue, Integer nbCountWin, Integer nbCountTotal){
			this(goTermID, pValue);
			this.nbCountWin		= nbCountWin;
			this.nbCountTotal	= nbCountTotal;
		}

		@Override
		public	String	toString(){
			return	"GO : " + goTermID		+ " ** "
					+ "pValue : " + pValue	+ " ** "
					+ "qValue : " + qValue;
		}

		public	Double	pValue			= null;
		public	Double	qValue			= null;
		public	Integer	nbCountWin		= null;
		public	Integer	nbCountTotal	= null;
		public	String	goTermID		= null;
	}

	class	Window extends Distribution{
		public	Window(String goTerm, int iWindow, double pValue){
			super(goTerm, pValue);
			this.iWindow = iWindow;
		}

		@Override
		public	String	toString(){
			return	"GO : " + goTermID		+ " ** "
					+ "iWin : " + iWindow	+ " ** "
					+ "pValue : " + pValue	+ " ** "
					+ "qValue : " + qValue;
		}

		int		iWindow	= 0;
	}

	class	DistributionPValueComparator implements Comparator<Distribution>{
		@Override
		public int compare(Distribution distr1, Distribution distr2) {
			return distr1.pValue.compareTo(distr2.pValue);
		}
	}


	private	Set<String>				_goGenesFamily		= new HashSet<String>();
	private	List<ShapeDesignPhyElt>	_genesList			= null;
	private	double					_maxSize			= 0;
	private	int						_nbdots				= 0;
	private	double[]				_density			= null;
	private	List<ShapeDesign>		_shapes				= null;
	private	double					_heightPxl			= 0;
}