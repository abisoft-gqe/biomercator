/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 04-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;

public class Anchor {
	public	final	static	int	PB = 1;
	public	final	static	int	CM = 2;
	public	final	static	int	PB_TO_CM = 1;
	public	final	static	int	CM_TO_PB = 2;
	public	final	static	int	CM_TO_CM = 3;
	public	final	static	int	PB_TO_PB = 4;

	public	Anchor(String name, double pos_pb, double pos_cM){
		_name = name;
		_pos_cM = pos_cM;
		_pos_pb = pos_pb;
	}

	/**
	 * Returns the anchor's name.
	 * @return The anchor's name.
	 */
	public	String	getName(){
		return _name;
	}

	/**
	 * Returns the wanted position (genetic or physical); the parameters will
	 * determine the unit.
	 * @param unitID PB or CM
	 * @return The wanted position.
	 */
	public	double	getPosition(int unitID){
		double		res = 0;

		switch(unitID){
			case PB:
				res = _pos_pb;
				break;
			case CM:
				res = _pos_cM;
				break;
		}

		return res;
	}

	public	static	int	getUnitFrom(int conversionId){
		int				res = 0;

		switch(conversionId){
			case PB_TO_CM:
				res = PB;
				break;
			case CM_TO_PB:
				res = CM;
				break;
			case CM_TO_CM:
				res = CM;
				break;
			case PB_TO_PB:
				res = PB;
				break;
		}

		return res;
	}

	public	static	int	getUnitTo(int conversionId){
		int				res = 0;

		switch(conversionId){
			case PB_TO_CM:
				res = CM;
				break;
			case CM_TO_PB:
				res = PB;
				break;
			case CM_TO_CM:
				res = CM;
				break;
			case PB_TO_PB:
				res = PB;
				break;
		}

		return res;
	}

	private	String	_name		= null;
	private	double	_pos_pb		= 0;
	private	double	_pos_cM		= 0;
	public	int		rank_gen	= 0;
	public	int		rank_phy	= 0;
	public	boolean	enable		= true;
}
