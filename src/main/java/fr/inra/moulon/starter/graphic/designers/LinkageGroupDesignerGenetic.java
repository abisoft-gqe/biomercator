/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 7, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.designers;

import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.controller.ControllerDesign;
import fr.inra.moulon.starter.controller.utils.SimulatedAnnealingQTL;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.graphic.utils.Anchor;
import fr.inra.moulon.starter.graphic.utils.FontTools;
import fr.inra.moulon.starter.graphic.utils.MarkerColorManager;
import fr.inra.moulon.starter.graphic.utils.QTLColorManager;
import fr.inra.moulon.starter.graphic.utils.ShapeDesign;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignLinked;
import fr.inra.moulon.starter.graphic.utils.ShapeDesignNamed;
import fr.inra.moulon.starter.utils.NumericalUtilities;
import fr.inra.moulon.starter.utils.Session;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public	class	LinkageGroupDesignerGenetic extends LinkageGroupDesigner {
	public	final	static	int	MODE_QTL_STANDARD	= 0;
	public	final	static	int	MODE_QTL_CONDENSED	= 1;
	public	final	static	int	MODE_QTL_CURVED		= 2;
	public	final	static	int	MODE_QTL_OVERVIEW	= 3;
	public	final	static	int	MODE_QTL_CURVED_R2	= 4;

	public	final	static	int		WIDTH_LINKAGE_GROUP				= 10;
	public	final	static	double	GAP_NAMES						= 20;
	public	final	static	double	MINI_CHROMOSOME_HEIGHT			= 4;
	public	final	static	double	MINI_CHROMOSOME_WIDTH			= 100;
	public	final	static	double	MINI_CHROMOSOME_SCROLLER_HEIGHT	= MINI_CHROMOSOME_HEIGHT + 4;
	public	final	static	double	GAP_MINI_CHROMOSOME				= 6;

	public	LinkageGroupDesignerGenetic(final	LinkageGroup	lkg,
										final	MetaAnalysis	metaAnalysis,
										final	MetaModel		metaModel){
		super(lkg, metaModel);

		double[]	extrems = Controller.getExtremsMarkerPositions(super._LKG);

		_META_ANALYSIS = metaAnalysis;
		_names = new HashMap<String, ShapeDesignLinked>();
		initExtremValues(extrems[0], extrems[1]);
		_scroller.setWidth(30);
	}

	@Override
	public	final	void	recreateShapes(){
		Boolean				showMarkersName		= (Boolean)Session.instance().getValue(Session.LOCUS_NAME_DISPLAY);
		Boolean				showMarkersPosition	= (Boolean)Session.instance().getValue(Session.LOCUS_POSITION_DISPLAY);

		clearShapes();
		if ((Boolean)Session.instance().getValue(Session.QTL_DISPLAY)){
			setQtlsShapeDesigns();
		}else{
			_widthQtls = 0;
		}
		if ((Boolean)Session.instance().getValue(Session.META_QTL_DISPLAY)){
			setMetaQtlsShapeDesigns(_widthQtls);
		}
		if ((Boolean)Session.instance().getValue(Session.LOCUS_DISPLAY)){
			setMarkersShapeDesigns(_widthQtls);
			if ((Boolean)Session.instance().getValue(Session.LOCUS_SHOW_ALL)){
				setMarkersStringsPositions(_elements.get(MARKER));
			}else{
				setMarkersStringsPositionsWithRandomHide(_elements.get(MARKER));
			}
			if (showMarkersName || showMarkersPosition){
				setMarkersLinks(_elements.get(MARKER));
			}
		}
		setMiscShapeDesigns(_widthQtls);
		setAnchorsDesign(_widthQtls);
	}

	@Override
	public	final	void	darkenShapes(boolean darken){
		if (darken){
			if (_elements.containsKey(MARKER)){
				for (Iterator<ShapeDesign> it = _elements.get(MARKER).iterator(); it.hasNext();) {
					ShapeDesign shapeDesign = it.next();
					shapeDesign.color = Color.LIGHT_GRAY;
				}
			}
		}else{
			if (_elements.containsKey(MARKER)){
				for (Iterator<ShapeDesign> it = _elements.get(MARKER).iterator(); it.hasNext();) {
					ShapeDesign shapeDesign = it.next();
					shapeDesign.color = Color.BLACK;
				}
			}
		}
	}

	/**
	 * Removes all shapes.
	 */
	private	void	clearShapes(){
		_elements.put(QTL, new ArrayList<ShapeDesign>());
		_elements.put(META_QTL, new ArrayList<ShapeDesign>());
		_elements.put(MARKER, new ArrayList<ShapeDesign>());
		_elements.put(MARKER_STRING, new ArrayList<ShapeDesign>());
		_elements.put(ANCHOR, new ArrayList<ShapeDesign>());
		_elements.put(MISC, new ArrayList<ShapeDesign>());
	}

	/**
	 * Creates and adds the anchors shapes.
	 * @param qtlWidthPxl
	 */
	private	void			setAnchorsDesign(double qtlWidthPxl){
		List<ShapeDesign>	anchorsList			= null;
		int					anchorExtraWidth	= 4;

		if (null != _anchors && (Boolean)Session.instance().getValue(Session.DISPLAY_PHY_BLOW_ANCHORS)){
			anchorsList	= new ArrayList<ShapeDesign>();
			for (Iterator<Anchor> it = _anchors.iterator(); it.hasNext();) {
				Anchor anchor = it.next();

				anchorsList.add(new ShapeDesign(ShapeDesign.LINE,
												qtlWidthPxl-anchorExtraWidth/2,
												position_data_to_pxl(anchor.getPosition(Anchor.CM)).getY(),
												WIDTH_LINKAGE_GROUP+anchorExtraWidth,
												0,
												Color.BLUE));
			}
			_elements.put(ANCHOR, anchorsList);
		}
	}

	/**
	 * Sets the markers list with markers ShapeDesigns from the super's
	 * linkage group. (between the class positions)
	 */
	private	void			setMarkersShapeDesigns(double qtlWidthPxl){
		List<String>		positionStrings	= new ArrayList<String>();
		int					max_pos_length	= 0;
		DecimalFormat		df				= new DecimalFormat();
		int					indexMarker		= 0;
		List<Marker>		markersBioList	= Controller.getElements(_LKG, Marker.class, super._windowStart, super._windowEnd);
		List<ShapeDesign>	markersList		= null;
		Boolean				showNames		= (Boolean)Session.instance().getValue(Session.LOCUS_NAME_DISPLAY);
		Boolean				showPositions	= (Boolean)Session.instance().getValue(Session.LOCUS_POSITION_DISPLAY);

		filterMarkers(markersBioList);
		markersList = ControllerDesign.getMarkersShapeDesigns(markersBioList);
		_names.clear();
		_longestMarkername = "";

		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);

		// Iteration to fill the position (string) array and to find the longest
		// position (in string).
		for (Iterator<ShapeDesign> it = markersList.iterator(); it.hasNext();) {
			ShapeDesignLinked shapeDes = (ShapeDesignLinked)it.next();
			String			positionString = df.format(shapeDes.bioPosition);

			_names.put(shapeDes.str, shapeDes);
			positionStrings.add(positionString);
			max_pos_length = Math.max(max_pos_length, positionString.length());
		}

		// Iteration for inserting shapeDesigns with their corresponding string.
		for (Iterator<ShapeDesign> it = markersList.iterator(); it.hasNext();) {
			ShapeDesignLinked	shapeDes		= (ShapeDesignLinked)it.next();
			String				positionString	= positionStrings.get(indexMarker++);
			String				name			= shapeDes.str;

			shapeDes.shapeId = ShapeDesign.LINE;
			shapeDes.x = qtlWidthPxl;
			shapeDes.y = position_data_to_pxl(shapeDes.bioPosition).getY();
			shapeDes.w = WIDTH_LINKAGE_GROUP;
			shapeDes.h = 0;
			shapeDes.str = "";
			if (showPositions){
				shapeDes.str += positionString;
			}
			if (showPositions && showNames){
				shapeDes.str += createStringSpaces(max_pos_length-positionString.length() + GAP_NB_SPACES_POS_NAME);
			}
			if (showNames){
				shapeDes.str += name;
			}
			if (shapeDes.str.length() > _longestMarkername.length()){
				_longestMarkername = shapeDes.str;
			}
		}

		_elements.put(MARKER, (List<ShapeDesign>)markersList);
	}

	/**
	 * Returns a String filled with <b>nbSpaces</b> space characters.
	 * @param nbSpaces The wanted number of spaces characters
	 * @return The String filled with <b>nbSpaces</b> space characters
	 */
	private String	createStringSpaces(int nbSpaces){
		String		res = "";

		for (int i = 0; i < nbSpaces; ++i) {
			res += " ";
		}

		return res;
	}

	/**
	 * Sets the markers name in the right position so that no string is above
	 * another.
	 * @param markerList the markers' list
	 */
	private	void	setMarkersStringsPositions(List<ShapeDesign> markerList){
		Font		font		= (Font)Session.instance().getValue(Session.FONT);
		double		minSpace	= font.getSize();
		double		prevPos		= -minSpace;

		for (Iterator<ShapeDesign> it = markerList.iterator(); it.hasNext();) {
			ShapeDesignLinked shapeDes = (ShapeDesignLinked)it.next();

			shapeDes.strX = shapeDes.x + shapeDes.w + LINK_WIDTH;
			if (shapeDes.y - prevPos >= minSpace){
				shapeDes.strY = shapeDes.y;
			}else{
				shapeDes.strY = prevPos + minSpace;
			}
			prevPos = shapeDes.strY;
		}
		for (int i = 0; i < markerList.size(); ++i) {
			ShapeDesignLinked shapeDes = (ShapeDesignLinked)markerList.get(i);

			if (shapeDes.y != shapeDes.strY && isSpaceAbove(markerList, i)){
				moveUp(markerList, i);
			}
		}
	}

	/**
	 * Sets the markers name in the right position so that no string is above
	 * another and hides some of them for a better visualisation.
	 * @param markerList the markers' list
	 */
	private	void	setMarkersStringsPositionsWithRandomHide(List<ShapeDesign> markerList){
		Font		font		= (Font)Session.instance().getValue(Session.FONT);
		double		minSpace	= font.getSize();
		double		maxSpace	= 20;
		double		prevPos		= -minSpace;

		for (Iterator<ShapeDesign> it = markerList.iterator(); it.hasNext();) {
			ShapeDesignLinked shapeDes = (ShapeDesignLinked)it.next();

			shapeDes.strX = shapeDes.x + shapeDes.w + LINK_WIDTH;
			if (shapeDes.y - prevPos >= minSpace){
				shapeDes.strY = shapeDes.y;
			}else if (prevPos + minSpace - shapeDes.y < maxSpace){
				shapeDes.strY = prevPos + minSpace;
			}else{
				shapeDes.strY = prevPos;
				shapeDes.show = false;
			}
			prevPos = shapeDes.strY;
		}
		_idxNoSpace = 0;
		for (int i = 0; i < markerList.size(); ++i) {
			ShapeDesignLinked shapeDes = (ShapeDesignLinked)markerList.get(i);

			if (shapeDes.y != shapeDes.strY && isSpaceAbove(markerList, i)){
				moveUp(markerList, i);
			}
		}
	}

	/**
	 * Returns (if it exists) the marker shape corresponding to the given name.
	 * @param markerName The marker's name
	 * @return the marker shape corresponding to the given name; null if none is
	 * found
	 */
	public	ShapeDesignLinked	getMarkerNameShape(String markerName){
		ShapeDesignLinked		res = null;

		if (_names.containsKey(markerName)){
			res = (ShapeDesignLinked)_names.get(markerName);
		}

		return res;
	}

	/**
	 * Returns an iterator on the makers name.
	 * @return an iterator on the makers name
	 */
	public	Iterator<String>	iteratorMarkersName(){
		return _names.keySet().iterator();
	}

	/**
	 * Returns if the marker at <b>index</b> position can be moved up; ie all
	 * above markers won't stack.
	 * @param markerList The markers list
	 * @param index The marker's index
	 * @return
	 */
	private	boolean	isSpaceAbove(List<ShapeDesign> markerList, int index){
		boolean		res = false;

		if (index > 0 && index < markerList.size()){
			ShapeDesignLinked	shapeDesPrev	= (ShapeDesignLinked)markerList.get(index-1);
			ShapeDesignLinked	shapeDesCur		= (ShapeDesignLinked)markerList.get(index);

			if (shapeDesCur.strY - 1 > shapeDesPrev.strY + _minSpace){
				res = true;
			}else{
				if (index-1 > _idxNoSpace){
					res = isSpaceAbove(markerList, index-1);
					if (!res){
						_idxNoSpace = index-1;
					}
				}
			}
		}

		return res;
	}

	/**
	 * Move up recursively the markers until no stack is found. The method <b>
	 * isSpaceAbove</b> should be called first to ensure the movements are
	 * possible.
	 * @param markerList
	 * @param index
	 */
	private	void	moveUp(List<ShapeDesign> markerList, int index){
		if (index > 0 && index < markerList.size()){
			ShapeDesignLinked	shapeDesPrev	= (ShapeDesignLinked)markerList.get(index-1);
			ShapeDesignLinked	shapeDesCur		= (ShapeDesignLinked)markerList.get(index);

			shapeDesCur.strY -= 1;
			if (shapeDesCur.strY <= shapeDesPrev.strY + _minSpace){
				moveUp(markerList, index-1);
			}
		}
	}

	/**
	 * Sets the markers links between their representation and their name.
	 * @param markerList the markers' list
	 */
	private	void	setMarkersLinks(List<ShapeDesign> markerList){
		for (Iterator<ShapeDesign> it = markerList.iterator(); it.hasNext();) {
			ShapeDesignLinked shapeDes = (ShapeDesignLinked)it.next();

			shapeDes.linkCoords = new double[4][2];

			// Starting point
			shapeDes.linkCoords[0][0] = shapeDes.x + shapeDes.w + GAP_LINK;
			shapeDes.linkCoords[0][1] = shapeDes.y;

			// Second point
			shapeDes.linkCoords[1][0] = shapeDes.linkCoords[0][0] + LINK_WIDTH/4;
			shapeDes.linkCoords[1][1] = shapeDes.linkCoords[0][1];

			// Third point
			shapeDes.linkCoords[2][0] = shapeDes.linkCoords[1][0] + LINK_WIDTH/2;
			shapeDes.linkCoords[2][1] = shapeDes.strY;

			// Arrival point
			shapeDes.linkCoords[3][0] = shapeDes.strX - GAP_LINK;
			shapeDes.linkCoords[3][1] = shapeDes.linkCoords[2][1];
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// QTL DESIGN ////////////////////////////////
	/**
	 * Sets the markers list with markers ShapeDesigns from the super's
	 * linkage group. (between the class positions)
	 */
	private	void			setQtlsShapeDesigns(){
		List<Qtl>			qtlsBioList	= Controller.getElements(_LKG, Qtl.class);
		List<ShapeDesign>	qtlsList	= null;

		filterQtls(qtlsBioList);
		switch((Integer)Session.instance().getValue(Session.QTL_DENSITY)){
			case MODE_QTL_STANDARD:
				filterOutSideQtls(qtlsBioList);
				qtlsList = (_META_MODEL != null)
							?getQtlsShapes_standard_MetaQTL(qtlsBioList)
							:getQtlsShapes_standard(qtlsBioList);
				break;
			case MODE_QTL_CONDENSED:
				qtlsList = (_META_MODEL != null)
							?getQtlsShapes_condensed_MetaQTL(qtlsBioList)
							:getQtlsShapes_condensed(qtlsBioList);
				break;
			case MODE_QTL_CURVED:
				qtlsList = getQtlsShapes_curved(qtlsBioList);
				break;
			case MODE_QTL_OVERVIEW:
				qtlsList = getQtlsShapes_overview(qtlsBioList);
				break;
			case MODE_QTL_CURVED_R2:
				qtlsList = getQtlsShapes_curvedR2(qtlsBioList);
				break;
		}

		if (null != qtlsList){
			_elements.put(QTL, qtlsList);
		}
	}

	/**
	 * Returns a qtl shapes list. The design is the standard one.
	 * @param qtlsBioList The Qtls list
	 * @return A qtl shapes list
	 */
	private	List<ShapeDesign>	getQtlsShapes_standard_MetaQTL(List<Qtl> qtlsBioList){
		List<ShapeDesign>		qtlsList		= null;
		ShapeDesignNamed		shapeDesQtl		= null;
		ShapeDesign				shapeDesR2		= null;
		Map<String, Integer>	qtlsPositions	= null;
		int						nbQtlCols		= 0;

		if (null == _annealingQtls){
			_annealingQtls = new SimulatedAnnealingQTL(qtlsBioList);
			_annealingQtls.execute();
		}
		qtlsPositions = _annealingQtls.getResult();
		nbQtlCols = _annealingQtls.getNbCols();
		_widthQtls = nbQtlCols*(QTL_STANDARD_WIDTH+QTL_STANDARD_GAP);

		qtlsList = new ArrayList<ShapeDesign>();
		for (int i = 0; i < qtlsBioList.size(); ++i) {
			Qtl		qtl			= qtlsBioList.get(i);
			double	positionX	= qtlsPositions.get(qtl.getName())*(QTL_STANDARD_WIDTH+QTL_STANDARD_GAP);

			double	posStartBelong = qtl.getPositionStart();
			for (Iterator<MetaQtl> it = _META_MODEL.iterator(); it.hasNext();) {
				MetaQtl	metaQtl	= it.next();
				Double	belong	= metaQtl.getBelonging(qtl.getName());
				double	posEndBelong = posStartBelong + (qtl.getPositionEnd()-qtl.getPositionStart())*belong;

				shapeDesQtl = new ShapeDesignNamed(
						ShapeDesign.FILLED_RECTANGLE, "",
						positionX,
						Math.floor(position_data_to_pxl(posStartBelong).getY()),
						QTL_STANDARD_WIDTH,
						Math.floor(position_data_to_pxl(posEndBelong).getY()) - Math.floor(position_data_to_pxl(posStartBelong).getY()),
						QTLColorManager.instance().getColor(metaQtl.getTrait()));
				if ((Boolean)Session.instance().getValue(Session.QTL_NAME_DISPLAY)){
					shapeDesQtl.str = it.hasNext()?"":qtl.getName();
					shapeDesQtl.strX = positionX-2;
					shapeDesQtl.strY = (position_data_to_pxl(posStartBelong).getY() + position_data_to_pxl(posEndBelong).getY())/2;
				}
				shapeDesQtl.info = qtl.toPretty();
				qtlsList.add(shapeDesQtl);
				posStartBelong = posEndBelong;
			}

			if (qtl.getPosition() >= super._windowStart && qtl.getPosition() <= super._windowEnd){
				shapeDesR2 = new ShapeDesign(
					ShapeDesign.FILLED_RECTANGLE,
					positionX - Math.floor(50*qtl.getR2()),
					position_data_to_pxl(qtl.getPosition()).getY()-QTL_R2_HEIGHT_STANDARD/2,
					Math.floor(50*qtl.getR2()),
					QTL_R2_HEIGHT_STANDARD,
					QTLColorManager.instance().getColor(qtl.getTrait()));
				shapeDesR2.info = qtl.toPretty();
				qtlsList.add(shapeDesR2);
			}
		}

		return qtlsList;
	}

	/**
	 * Returns a qtl shapes list. The design is the standard one.
	 * @param qtlsBioList The Qtls list
	 * @return A qtl shapes list
	 */
	private	List<ShapeDesign>	getQtlsShapes_standard(List<Qtl> qtlsBioList){
		List<ShapeDesign>		qtlsList		= null;
		ShapeDesignNamed		shapeDesQtl		= null;
		ShapeDesign				shapeDesR2		= null;
		Map<String, Integer>	qtlsPositions	= null;
		int						nbQtlCols		= 0;

		if (null == _annealingQtls){
			_annealingQtls = new SimulatedAnnealingQTL(qtlsBioList);
			_annealingQtls.execute();
		}
		qtlsPositions = _annealingQtls.getResult();
		nbQtlCols = _annealingQtls.getNbCols();
		_widthQtls = nbQtlCols*(QTL_STANDARD_WIDTH+QTL_STANDARD_GAP);

		qtlsList = new ArrayList<ShapeDesign>();
		for (int i = 0; i < qtlsBioList.size(); ++i) {
			Qtl		qtl			= qtlsBioList.get(i);
			double	positionX	= qtlsPositions.get(qtl.getName())*(QTL_STANDARD_WIDTH+QTL_STANDARD_GAP);

			shapeDesQtl = new ShapeDesignNamed(
					ShapeDesign.FILLED_RECTANGLE, "",
					positionX,
					position_data_to_pxl(qtl.getPositionStart()).getY(),
					QTL_STANDARD_WIDTH,
					position_data_to_pxl(qtl.getPositionEnd()).getY() - position_data_to_pxl(qtl.getPositionStart()).getY(),
					QTLColorManager.instance().getColor(qtl.getTrait()));
			if ((Boolean)Session.instance().getValue(Session.QTL_NAME_DISPLAY)){
				shapeDesQtl.str = qtl.getName();
				shapeDesQtl.strX = positionX-2;
				shapeDesQtl.strY = (position_data_to_pxl(qtl.getPositionStart()).getY() + position_data_to_pxl(qtl.getPositionEnd()).getY())/2;
				}
			shapeDesQtl.info = qtl.toPretty();
			qtlsList.add(shapeDesQtl);

			if (qtl.getPosition() >= super._windowStart && qtl.getPosition() <= super._windowEnd){
				shapeDesR2 = new ShapeDesign(
					ShapeDesign.FILLED_RECTANGLE,
					positionX - Math.floor(50*qtl.getR2()),
					position_data_to_pxl(qtl.getPosition()).getY()-QTL_R2_HEIGHT_STANDARD/2,
					Math.floor(50*qtl.getR2()),
					QTL_R2_HEIGHT_STANDARD,
					QTLColorManager.instance().getColor(qtl.getTrait()));
				shapeDesR2.info = qtl.toPretty();
				qtlsList.add(shapeDesR2);
			}
		}

		return qtlsList;
	}
	/**
	 * Returns a qtl shapes list. The design is the standard one.
	 * @param qtlsBioList The Qtls list
	 * @return A a qtl shapes list
	 */
	private	List<ShapeDesign>	getQtlsShapes_condensed(List<Qtl> qtlsBioList){
		List<ShapeDesign>		qtlsList	= new ArrayList<ShapeDesign>();
		ShapeDesign				shapeDesQtl	= null;
		Map<String, Integer>	qtlsPositions	= null;
		int						nbQtlCols	= 0;

		if (null == _annealingQtls){
			_annealingQtls = new SimulatedAnnealingQTL(qtlsBioList);
			_annealingQtls.execute();
		}
		qtlsPositions = _annealingQtls.getResult();
		nbQtlCols = _annealingQtls.getNbCols();

		_widthQtls = nbQtlCols*10 + QTL_STANDARD_WIDTH;
		for (int i = 0; i < qtlsBioList.size(); ++i) {
			Qtl qtl = qtlsBioList.get(i);

			if (null != qtlsPositions.get(qtl.getName())){
				shapeDesQtl = new ShapeDesign(
				ShapeDesign.FILLED_ROUND_RECTANGLE,
				10*qtlsPositions.get(qtl.getName()),
				position_data_to_pxl(qtl.getPositionStart()).getY(),
				QTL_STANDARD_WIDTH,
				position_data_to_pxl(qtl.getPositionEnd()).getY() - position_data_to_pxl(qtl.getPositionStart()).getY(),
				QTLColorManager.instance().getColor(qtl.getTrait()));
				qtlsList.add(shapeDesQtl);
			}
		}

		return qtlsList;
	}

		/**
	 * Returns a qtl shapes list. The design is the standard one.
	 * @param qtlsBioList The Qtls list
	 * @return A qtl shapes list
	 */
	private	List<ShapeDesign>	getQtlsShapes_condensed_MetaQTL(List<Qtl> qtlsBioList){
		List<ShapeDesign>		qtlsList		= new ArrayList<ShapeDesign>();
		ShapeDesign				shapeDesQtl		= null;
		Map<String, Integer>	qtlsPositions	= null;
		int						nbQtlCols		= 0;

		if (null == _annealingQtls){
			_annealingQtls = new SimulatedAnnealingQTL(qtlsBioList);
			_annealingQtls.execute();
		}
		qtlsPositions = _annealingQtls.getResult();
		nbQtlCols = _annealingQtls.getNbCols();

		_widthQtls = nbQtlCols*10 + QTL_STANDARD_WIDTH;
		for (int i = 0; i < qtlsBioList.size(); ++i) {
			Qtl		qtl			= qtlsBioList.get(i);
			double	posStartBelong = qtl.getPositionStart();

			for (Iterator<MetaQtl> it = _META_MODEL.iterator(); it.hasNext();) {
				MetaQtl	metaQtl	= it.next();
				Double	belong	= metaQtl.getBelonging(qtl.getName());
				double	posEndBelong = posStartBelong + (qtl.getPositionEnd()-qtl.getPositionStart())*belong;

				shapeDesQtl = new ShapeDesign(
						ShapeDesign.FILLED_RECTANGLE,
						10*qtlsPositions.get(qtl.getName()),
						position_data_to_pxl(posStartBelong).getY(),
						QTL_STANDARD_WIDTH,
						position_data_to_pxl(posEndBelong).getY() - position_data_to_pxl(posStartBelong).getY(),
						QTLColorManager.instance().getColor(metaQtl.getTrait()));
				shapeDesQtl.info = qtl.toPretty();
				qtlsList.add(shapeDesQtl);
				posStartBelong = posEndBelong;
			}
		}

		return qtlsList;
	}

	/**
	 * Returns a qtl shapes list. The design is the standard one.
	 * @param qtlsBioList The Qtls list
	 * @return A a qtl shapes list
	 */
	private	List<ShapeDesign>		getQtlsShapes_curved(List<Qtl> qtlsBioList){
		List<ShapeDesign>			qtlsList	= new ArrayList<ShapeDesign>();
		List<Map<Color, Integer>>	density		= new ArrayList<Map<Color, Integer>>((int)Math.round(getMaxPxlSize()));
		Map<Color, Integer>			hashMap		= null;
		Color						color		= null;
		int							incr		= 1;

		for (int y = 0; y < (int)Math.round(getMaxPxlSize()); y++) {
			density.add(new HashMap<Color, Integer>());
		}
		for (Iterator<Qtl> it = qtlsBioList.iterator(); it.hasNext();){
			Qtl	qtl = it.next();

			for (	int y = (int)position_data_to_pxl(qtl.getPositionStart()).getY();
					y < (int)position_data_to_pxl(qtl.getPositionEnd()).getY();
					++y){
				hashMap = density.get(y);
				color = QTLColorManager.instance().getColor(qtl.getTrait());

				if (hashMap.containsKey(color)){
					hashMap.put(color, hashMap.get(color)+incr);
				}else{
					hashMap.put(color, new Integer(incr));
				}
			}
		}// for
		_widthQtls = 50;//nbQtlCols;//*2;
		for (int y = 0; y < density.size(); ++y) {
			hashMap = density.get(y);
			for (Iterator<Color> itColors = hashMap.keySet().iterator(); itColors.hasNext();) {
				color = itColors.next();
				Integer	value = hashMap.get(color);
				if (value > 0){
					ShapeDesign	shape = new ShapeDesign(ShapeDesign.LINE, _widthQtls - value*2, y, 0, 0, color);
					qtlsList.add(shape);
				}
			}
		}

		return qtlsList;
	}

	/**
	 * Returns a qtl shapes list. The design is the standard one.
	 * @param qtlsBioList The Qtls list
	 * @return A a qtl shapes list
	 */
	private	List<ShapeDesign>	getQtlsShapes_overview(List<Qtl> qtlsBioList){
		List<ShapeDesign>		qtlsList	= new ArrayList<ShapeDesign>();
		Integer[]				qtlDensity	= null;
		int						maxDensity	= 0;
		int						coef		= 1;
		double					step		= 0.5;
		double					si			= 0;
		double					fx1			= 0;
		double					fx2			= 0;
		double					area		= 0;

		qtlDensity = new Integer[(int)((_MAX_BIOPOS-_MIN_BIOPOS)/step)];
		for (int i = 0; i < qtlDensity.length; ++i){
			qtlDensity[i] = new Integer(0);
		}
		for (int i = 0; i < qtlDensity.length; ++i){
			area = 0;
			double pos_cM = _MIN_BIOPOS + i*step;
			for (Iterator<Qtl> it = qtlsBioList.iterator(); it.hasNext();){
				Qtl qtl = it.next();

				si = (qtl.getPositionEnd()-qtl.getPositionStart())/(2*1.96);
				fx1 = NumericalUtilities.normalDistrib(pos_cM,		qtl.getPosition(), si);
				fx2 = NumericalUtilities.normalDistrib(pos_cM+step,	qtl.getPosition(), si);
				area += step*(fx1+fx2)/2;
			}// for
			qtlDensity[i] = (int)(Math.round(area*100));
		}
		maxDensity = 0;
		for (int i = 0; i < qtlDensity.length; ++i){
			maxDensity = Math.max(maxDensity, qtlDensity[i]);
		}
		_widthQtls = maxDensity;
		for (int i = 0; i < qtlDensity.length; ++i){
			int pos_pxl = (int)Math.round(position_data_to_pxl(_MIN_BIOPOS + i*step).getY());

			if (qtlDensity[i] >= 0){
				ShapeDesign	shape = new ShapeDesign(ShapeDesign.LINE,
													coef*(maxDensity - qtlDensity[i]),
													pos_pxl,
													0,
													0,
													Color.BLUE);
				qtlsList.add(shape);
			}
		}

		return qtlsList;
	}

	/**
	 * Returns a qtl shapes list. The design is the standard one.
	 * @param qtlsBioList The Qtls list
	 * @return A a qtl shapes list
	 */
	private	List<ShapeDesign>		getQtlsShapes_curvedR2(List<Qtl> qtlsBioList){
		List<ShapeDesign>			qtlsList	= new ArrayList<ShapeDesign>();
		List<Map<Color, Integer>>	density		= new ArrayList<Map<Color, Integer>>((int)getHeight());
		Map<Color, Integer>			hashMap		= null;
		Color						color		= null;
		int							incr		= 1;

		for (int y = 0; y < (int)getHeight(); y++) {
			density.add(new HashMap<Color, Integer>());
		}
		for (Iterator<Qtl> it = qtlsBioList.iterator(); it.hasNext();){
			Qtl	qtl = it.next();

			incr = (int)Math.round(qtl.getR2()*10);
			for (	int y = (int)position_data_to_pxl(qtl.getPositionStart()).getY();
					y < (int)position_data_to_pxl(qtl.getPositionEnd()).getY();
					++y){
				hashMap = density.get(y);
				color = QTLColorManager.instance().getColor(qtl.getTrait());

				if (hashMap.containsKey(color)){
					hashMap.put(color, hashMap.get(color)+incr);
				}else{
					hashMap.put(color, new Integer(incr));
				}
			}
		}// for
		_widthQtls = 50;//nbQtlCols;//*2;
		for (int y = 0; y < density.size(); ++y) {
			hashMap = density.get(y);
			for (Iterator<Color> itColors = hashMap.keySet().iterator(); itColors.hasNext();) {
				color = itColors.next();
				Integer	value = hashMap.get(color);
				if (value > 0){
					ShapeDesign	shape = new ShapeDesign(ShapeDesign.LINE, _widthQtls - value*2, y, 0, 0, color);
					qtlsList.add(shape);
				}
			}
		}

		return qtlsList;
	}

	private	void	filterOutSideQtls(List<Qtl> qtlsBioList){
		for (Iterator<Qtl> it = qtlsBioList.iterator(); it.hasNext();) {
			Qtl qtl = it.next();

			if (	qtl.getPositionStart() > super._windowEnd
				||	qtl.getPositionEnd() < super._windowStart){
				it.remove();
			}

		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////////////////////////// META QTL DESIGN //////////////////////////////
	/**
	 * Sets the markers list with metaQtl ShapeDesigns from the super's
	 * linkage group. (between the class positions)
	 * @param qtlWidthPxl The qtl width in pixels
	 */
	private	void			setMetaQtlsShapeDesigns(double qtlWidthPxl){
		List<MetaQtl>		metaQtlsBioList	= null;
		List<ShapeDesign>	metaQtlsList	= null;
		ShapeDesign			shapeDes		= null;
		double				posStart		= 0;
		double				posEnd			= 0;

		if (null != _META_MODEL){
			metaQtlsList = new ArrayList<ShapeDesign>();
			metaQtlsBioList	= Controller.getElements(_META_MODEL, MetaQtl.class);
			for (Iterator<MetaQtl> it = metaQtlsBioList.iterator(); it.hasNext();) {
				MetaQtl metaQtl = it.next();

				posStart = position_data_to_pxl(metaQtl.getPositionStart()).getY();
				posEnd = position_data_to_pxl(metaQtl.getPositionEnd()).getY();

				shapeDes = new ShapeDesign(	ShapeDesign.FILLED_RECTANGLE,
											qtlWidthPxl+1,
											posStart,
											WIDTH_LINKAGE_GROUP-1,
											posEnd - posStart,
											QTLColorManager.instance().getTransparentColor(metaQtl.getTrait()));
				metaQtlsList.add(shapeDes);
			}
			_elements.put(META_QTL, metaQtlsList);
		}
	}

	/**
	 * Removes all qtl with not selected traits (through the MainFrame).
	 * If a meta analysis is persent, this method also removes all QTLs if they
	 * are not present in the meta analysis used QTLs.
	 * @param qtls The QTLs list.
	 */
	private	void	filterMarkers(List<Marker> markers){
		for (Iterator<Marker> it = markers.iterator(); it.hasNext();) {
			Marker marker = it.next();

			if (!MarkerColorManager.instance().isSelected(marker.getType())){
				it.remove();
			}
		}
	}

	/**
	 * Removes all qtl with not selected traits (through the MainFrame).
	 * If a meta analysis is persent, this method also removes all QTLs if they
	 * are not present in the meta analysis used QTLs.
	 * @param qtls The QTLs list.
	 */
	private	void	filterQtls(List<Qtl> qtls){
		for (Iterator<Qtl> it = qtls.iterator(); it.hasNext();) {
			Qtl qtl = it.next();

			if (!QTLColorManager.instance().isSelected(qtl.getTrait())){
				it.remove();
			}
		}
		if (null != _META_ANALYSIS){
			for (Iterator<Qtl> it = qtls.iterator(); it.hasNext();) {
				Qtl qtl = it.next();
				if (!_META_ANALYSIS.containsQtl(qtl)){
					it.remove();
				}
			}
		}
	}

	/**
	 * Returns the point with pixels coordinates corresponding to the given
	 * biological position.
	 * @param position The biological position
	 * @return the point with pixels coordinates corresponding to the given
	 * biological position
	 */
	private	double	position_data_to_mini_lkg_pxl(double position) {
		double		pxl = (position - super._MIN_BIOPOS)*MINI_CHROMOSOME_WIDTH/(super._MAX_BIOPOS - super._MIN_BIOPOS);

		if (pxl < 0){
			pxl = 0;
		}else if (pxl > MINI_CHROMOSOME_WIDTH){
			pxl = MINI_CHROMOSOME_WIDTH;
		}

		return pxl;
	}

	/**
	 * Sets the misc list filled with ShapeDesigns.(such as chromosome's edge)
	 * @param qtlWidthPxl The qtl width in pixels
	 */
	private	void			setMiscShapeDesigns(double qtlWidthPxl){
		List<ShapeDesign>	miscList	= new ArrayList<ShapeDesign>();
		ShapeDesignNamed	shapeName	= null;
		ShapeDesign			shape		= null;
		int					heightPos	= 0;
		int					arcWidth	= WIDTH_LINKAGE_GROUP;
		int					arcHeight	= 15;

		// Linkage main drawing
		miscList.add(new ShapeDesign(ShapeDesign.LINE, qtlWidthPxl, 0, 0, super.getMaxPxlSize(), Color.BLACK));
		miscList.add(new ShapeDesign(ShapeDesign.LINE, qtlWidthPxl+WIDTH_LINKAGE_GROUP, 0, 0, super.getMaxPxlSize(), Color.BLACK));

		// Top of LKG
		if (isLkgTopSeen()){
			miscList.add(new ShapeDesign(ShapeDesign.ARC_UP, qtlWidthPxl, -arcHeight/2, arcWidth, arcHeight, Color.BLACK));
		}else{
			for (int y = 2; y <= 2*arcHeight/3; y += 3){
				miscList.add(new ShapeDesign(ShapeDesign.LINE, qtlWidthPxl, -y, 0, 1, Color.BLACK));
				miscList.add(new ShapeDesign(ShapeDesign.LINE, qtlWidthPxl+WIDTH_LINKAGE_GROUP, -y, 0, 1, Color.BLACK));
			}
		}

		// Bottom of LKG
		if (isLkgBottomSeen()){
			miscList.add(new ShapeDesign(ShapeDesign.ARC_DOWN, qtlWidthPxl, super.getMaxPxlSize()-arcHeight/2, arcWidth, arcHeight, Color.BLACK));
		}else{
			for (int y = 2; y <= 2*arcHeight/3; y += 3){
				miscList.add(new ShapeDesign(ShapeDesign.LINE, qtlWidthPxl, y+super.getMaxPxlSize(), 0, 1, Color.BLACK));
				miscList.add(new ShapeDesign(ShapeDesign.LINE, qtlWidthPxl+WIDTH_LINKAGE_GROUP, y+super.getMaxPxlSize(), 0, 1, Color.BLACK));
			}
		}

		// Map name
		if (showMapName()){
			heightPos += GAP_NAMES;
			shapeName = new ShapeDesignNamed(ShapeDesign.NONE, "", 0, 0, 0, 0, Color.BLACK);
			shapeName.position = ShapeDesignNamed.HORIZONTAL;
			shapeName.str = _LKG.getMapName();
			shapeName.strX = qtlWidthPxl - FontTools.getWidth(shapeName.str)/2 + WIDTH_LINKAGE_GROUP*4;
			shapeName.strY = super.getMaxPxlSize() + heightPos;
			miscList.add(shapeName);
		}

		// Chromosome name
		if (showChrName()){
			heightPos += GAP_NAMES;
			shapeName = new ShapeDesignNamed(ShapeDesign.NONE, "", 0, 0, 0, 0, Color.BLACK);
			shapeName.position = ShapeDesignNamed.HORIZONTAL;
			shapeName.str = _LKG.getChrName();
			shapeName.strX = qtlWidthPxl - FontTools.getWidth(shapeName.str)/2 + WIDTH_LINKAGE_GROUP*4;
			shapeName.strY = super.getMaxPxlSize() + heightPos;
			miscList.add(shapeName);
		}

		// Linkage group name
		if (showLkgName()){
			heightPos += GAP_NAMES;
			shapeName = new ShapeDesignNamed(ShapeDesign.NONE, "", 0, 0, 0, 0, Color.BLACK);
			shapeName.position = ShapeDesignNamed.HORIZONTAL;
			shapeName.str = _LKG.getName();
			shapeName.strX = qtlWidthPxl - FontTools.getWidth(shapeName.str)/2 + WIDTH_LINKAGE_GROUP*4;
			shapeName.strY = super.getMaxPxlSize() + heightPos;
			miscList.add(shapeName);
		}

		// Mini linkage group
		if (showMiniLkg()){
			double	x		= qtlWidthPxl + WIDTH_LINKAGE_GROUP/2 - MINI_CHROMOSOME_WIDTH/2;
			double	xStart	= position_data_to_mini_lkg_pxl(super._windowStart) + WIDTH_LINKAGE_GROUP*4;
			double	xEnd	= position_data_to_mini_lkg_pxl(super._windowEnd);

			//  Mini linkage group shape
			heightPos += GAP_MINI_CHROMOSOME;
			shape = new ShapeDesign(ShapeDesign.ROUND_RECTANGLE,
									x + xStart + 4,
									super.getMaxPxlSize() + heightPos,
									MINI_CHROMOSOME_WIDTH - WIDTH_LINKAGE_GROUP*3 + 2,
									MINI_CHROMOSOME_HEIGHT,
									Color.BLACK);
			miscList.add(shape);

			//  Mini cursor
			shape = new ShapeDesign(ShapeDesign.ROUND_RECTANGLE,
									x + xStart,
									super.getMaxPxlSize() + heightPos - (MINI_CHROMOSOME_SCROLLER_HEIGHT-MINI_CHROMOSOME_HEIGHT)/2,
									xEnd - xStart + 20,
									MINI_CHROMOSOME_SCROLLER_HEIGHT,
									new Color(100, 100, 100, 200));
			miscList.add(shape);

			heightPos += MINI_CHROMOSOME_HEIGHT + GAP_MINI_CHROMOSOME;
		}


		_elements.put(MISC, miscList);
	}

	/**
	 * Returns if the given point is close to the linkage group design.
	 * @param p The point
	 * @return if the given point is close to the linkage group design.
	 */
	@Override
	public	boolean	containsCloseToLkg(Point p, int margin){
		return	p.x > _widthQtls - margin &&
				p.x < _widthQtls + WIDTH_LINKAGE_GROUP + margin &&
				p.y > 0 &&
				p.y < getHeight();
	}

	@Override
	public	double	getWidth(){
		return	LEFT_MARGIN
				+ _widthQtls
				+ WIDTH_LINKAGE_GROUP
				+ GAP_LINK
				+ LINK_WIDTH
				+ GAP_LINK
				+ FontTools.getWidth(_longestMarkername)
				+ RIGHT_MARGIN;
	}

	@Override
	public	double	getHeight(){
		return TOP_MARGIN
				+ super.getMaxPxlSize()
				+ (showMiniLkg()?GAP_MINI_CHROMOSOME*2+MINI_CHROMOSOME_HEIGHT:0)
				+ (showMapName()?GAP_NAMES:0)
				+ (showChrName()?GAP_NAMES:0)
				+ (showLkgName()?GAP_NAMES:0)
				+ BOTTOM_MARGIN;
	}

	@Override
	public ShapeDesign		onWhichElt(Point p) {
		List<ShapeDesign>	list	= null;
		ShapeDesign			shape	= null;

		if (null != (list = _elements.get(QTL))){
			shape = onWhichEltInList(p, list);
		}
		if (null == shape && (null != (list = _elements.get(META_QTL)))){
			shape = onWhichEltInList(p, list);
		}

		return shape;
	}

	private	ShapeDesign		onWhichEltInList(Point p, List<ShapeDesign> list) {
		ShapeDesign			shape	= null;
		int					thresh	= 1;

		if (null != list){
			for (int i = 0; null == shape && i < list.size(); ++i){
				ShapeDesign shapeCur = list.get(i);

				if (p.x > shapeCur.x-thresh && p.x < shapeCur.x + shapeCur.w + 2*thresh &&
					p.y > shapeCur.y-thresh && p.y < shapeCur.y + shapeCur.h + 2*thresh)
					shape = shapeCur;
			}
		}

		return shape;
	}

	/**
	 * Used to check if the mini linkage group should be displayed.
	 * @return if the mini linkage group should be displayed.
	 */
	private	boolean	showMiniLkg(){
		return (Boolean)Session.instance().getValue(Session.DISPLAY_MINI_LKG);
	}

	/**
	 * Used to check if the map name should be displayed.
	 * @return if the map name should be displayed.
	 */
	private	boolean	showMapName(){
		return (Boolean)Session.instance().getValue(Session.DISPLAY_MAP_NAME);
	}

	/**
	 * Used to check if the chromosome name should be displayed.
	 * @return if the chromosome name should be displayed.
	 */
	private	boolean	showChrName(){
		return (Boolean)Session.instance().getValue(Session.DISPLAY_CHR_NAME);
	}

	/**
	 * Used to check if the linkage group name should be displayed.
	 * @return if the linkage group name should be displayed.
	 */
	private	boolean	showLkgName(){
		return (Boolean)Session.instance().getValue(Session.DISPLAY_LKG_NAME);
	}

	/**
	 * Returns the meta analysis associated to this LinkageGroupDesignerGenetic.
	 * @return the meta analysis associated to this LinkageGroupDesignerGenetic.
	 */
	public	MetaAnalysis	getMetaAnalysis(){
		return _META_ANALYSIS;
	}

	/**
	 * Setter for the SimulatedAnnealingQTL.
	 * @param annealing The SimulatedAnnealingQTL
	 */
	public	void	setAnnealing(SimulatedAnnealingQTL annealing){
		_annealingQtls = annealing;
	}

	/**
	 * Getter for the SimulatedAnnealingQTL.
	 * @return The SimulatedAnnealingQTL.
	 */
	public	SimulatedAnnealingQTL	getAnnealing(){
		return _annealingQtls;
	}

	@Override
	public double getScrollerX() {
		return _widthQtls-((_scroller.getWidth()-WIDTH_LINKAGE_GROUP)/2);
	}

	@Override
	public	double	getLeftAreaWidth(){
		return _widthQtls;
	}


	private			SimulatedAnnealingQTL			_annealingQtls		= null;
	private			double							_widthQtls			= 0;
	private			String							_longestMarkername	= null;
	private			Map<String, ShapeDesignLinked>	_names				= null;
	private			double							_minSpace			= 15;
	private			int								_idxNoSpace			= 0;


	private	final	static	double	QTL_STANDARD_WIDTH		= 2;
	private	final	static	double	QTL_STANDARD_GAP		= 10;
	private	final	static	double	QTL_R2_HEIGHT_STANDARD	= 1;
	private	final	static	double	LINK_WIDTH				= 60;
	private	final	static	double	GAP_LINK				= 5;
	private	final	static	int		GAP_NB_SPACES_POS_NAME	= 5;

	protected	final	MetaAnalysis	_META_ANALYSIS;
}

