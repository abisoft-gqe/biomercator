/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 13-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.graphic.utils;


import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sosnowski
 */
public class ElementColorManager {
	protected ElementColorManager(){
		_eltColors = new HashMap<String, Color>();
		_eltSelected = new HashMap<String, Boolean>();
	}

	/**
	 * Returns if the given key is selected. An non-present key is by default
	 * selected.
	 * @param key
	 * @return If the given key is selected.
	 */
	public	Boolean	isSelected(String key){
		Boolean		res = Boolean.TRUE;

		if (_eltSelected.containsKey(key)){
			res = _eltSelected.get(key);
		}

		return res;
	}

	/**
	 * Sets the given key to the given state.
	 * @param key
	 * @param selected
	 */
	public	void	setSelected(String key, Boolean selected){
		_eltSelected.put(key, selected);
	}

	/**
	 * Sets the color for the given key.
	 * @param key
	 * @param color
	 */
	public	void	setColor(String key, Color color){
		_eltColors.put(key, color);
	}

	/**
	 * Returns the color (for the class specified) if defined; creates a new one
	 * otherwise.
	 * @param trait
	 * @return the trait Color
	 */
	public Color	getColor(String key){
		if (!_eltColors.containsKey(key)){
			if (_indexColors >= _listColors.size()){
				_indexColors = 0;
			}
			_eltColors.put(key, _listColors.get(_indexColors++));
		}

		return _eltColors.get(key);
	}

	/**
	 * Returns the color (for the class specified) if defined; creates a new one
	 * otherwise. The color will be transparent.
	 * @param trait
	 * @return the trait transparent color.
	 */
	public Color	getTransparentColor(String key){
		Color		c = this.getColor(key);

		return new Color(c.getRed(), c.getGreen(), c.getBlue(), 142);
	}

	private					Map<String, Color>		_eltColors		= null;
	private					Map<String, Boolean>	_eltSelected	= null;
	private					int						_indexColors	= 0;
	private static	final	List<Color>				_listColors		= new ArrayList<Color>();
	static{
		_listColors.add(new Color(153,51,0));		//53
		_listColors.add(new Color(51,51,0));		//52
		_listColors.add(new Color(255,102,0));		//46
		_listColors.add(new Color(0,0,255));		//55
		_listColors.add(new Color(128,0,0));		//9
		_listColors.add(new Color(51,51,51));		//56
		_listColors.add(new Color(255,0,0));		//3
		_listColors.add(new Color(0,0,128));		//47
		_listColors.add(new Color(255,153,0));		//45
		_listColors.add(new Color(0,51,0));			//51
		_listColors.add(new Color(153,204,0));		//43
		_listColors.add(new Color(0,51,102));		//49
		_listColors.add(new Color(51,51,153));		//16
		_listColors.add(new Color(128,128,0));		//12
		_listColors.add(new Color(0,128,0));		//10
		_listColors.add(new Color(22,131,131));		//14
		_listColors.add(new Color(102,102,153));	//56
		_listColors.add(new Color(128,128,128));	//48
		_listColors.add(new Color(52,153,102));		//50 ?
		_listColors.add(new Color(51,204,204));		//42
		_listColors.add(new Color(51,102,255));		//41
		_listColors.add(new Color(128,0,128));		//50 ?
		_listColors.add(new Color(149,149,149));	//16
		_listColors.add(new Color(255,0,255));		//7
		_listColors.add(new Color(255,204,0));		//44
		_listColors.add(new Color(255,255,0));		//6
		_listColors.add(new Color(0,255,0));		//35
		_listColors.add(new Color(0,255,255));		//8
		_listColors.add(new Color(0,204,255));		//33
		_listColors.add(new Color(153,51,102));		//54
		_listColors.add(new Color(192,192,192));	//15
		_listColors.add(new Color(255,153,204));	//38
		_listColors.add(new Color(255,204,153));	//40
		_listColors.add(new Color(255,255,153));	//36
		_listColors.add(new Color(204,255,204));	//4
		_listColors.add(new Color(204,255,255));	//34
		_listColors.add(new Color(153,204,255));	//37
		_listColors.add(new Color(204,153,255));	//39
	}
}
