/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.converters;

import fr.inra.moulon.starter.fileTools.utils.FileTools;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FromBioM_QTL_V2 extends FileConverter{
	public FromBioM_QTL_V2(File file, String outputName, String mapName){
		super(file, outputName);
		_writer	= new SAXWriter(super.getNewFileName());
		_file	= file;
	}

	@Override
	public void			execute() {
		Boolean			res				= Boolean.TRUE;
		BufferedReader	buf				= null;
		String			line			= null;
		String[]		tokens			= new String[10];
		String			mapName			= null;
		String			chrName			= null;
		boolean			first_map		= true;
		boolean			first_chr		= true;

		try{
		    _writer.startDocument();
		    _writer.startElement(XMLStandardTagIds.MAPS);
		    buf = new BufferedReader(new FileReader(_file));
		    line = buf.readLine();
		    line = line.trim();    
        while (res && null != (line = buf.readLine())) {
				if (10 == FileTools.split(line,"\t",tokens)){ 
					if (!tokens[0].equals(mapName)){
						mapName.trim();
						mapName = tokens[0];
						if (!first_map){
							endMap();
						}
						first_map = false;
						chrName = null;
						startMap(mapName);
					}
					if (!tokens[2].equals(chrName)){
						chrName = tokens[2];
						if (!first_chr){
							endChr();
						}
						first_chr = false;
						startChr(chrName);
					}
					_writer.startElement(	XMLStandardTagIds.QTL,
											XMLStandardTagIds.NAME,
											tokens[1]);
					_writer.addElement(XMLStandardTagIds.QTL_TRAIT,			tokens[3]);
					_writer.addElement(XMLStandardTagIds.QTL_TRAIT_OID,		"");
					_writer.addElement(XMLStandardTagIds.QTL_EXP_PLACE,		"");
					_writer.addElement(XMLStandardTagIds.QTL_EXP_DATE,		"");
					_writer.addElement(XMLStandardTagIds.QTL_LOD,			tokens[4]);
					_writer.addElement(XMLStandardTagIds.QTL_R2,			tokens[5]);
					_writer.addElement(XMLStandardTagIds.QTL_LOCATION,		tokens[7]);
					_writer.addElement(XMLStandardTagIds.QTL_CI_FROM,		tokens[8]);
					_writer.addElement(XMLStandardTagIds.QTL_CI_TO,			tokens[9]);
					_writer.addElement(XMLStandardTagIds.QTL_CROSS_NAME,	"");
					_writer.addElement(XMLStandardTagIds.QTL_CROSS_SIZE,	"");
					_writer.addElement(XMLStandardTagIds.QTL_CROSS_TYPE,	"");
					_writer.endElement(XMLStandardTagIds.QTL);
				}
			}
			if (!first_chr){
				_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
				_writer.endElement(XMLStandardTagIds.CHROMOSOME);
			}
			if (!first_map){
				_writer.endElement(XMLStandardTagIds.MAP);
			}
			_writer.endElement(XMLStandardTagIds.MAPS);
			_writer.endDocument();
			buf.close();
		}catch(IOException e){
			System.out.println();
		}
	}

	private	void	startMap(String mapName){
		_writer.startElement(XMLStandardTagIds.MAP,	XMLStandardTagIds.NAME, mapName);
	}

	private	void	endMap(){
		_writer.endElement(XMLStandardTagIds.MAP);
	}

	private	void	startChr(String chrName){
		_writer.startElement(	XMLStandardTagIds.CHROMOSOME,
								XMLStandardTagIds.NAME,
								chrName);
		_writer.startElement(	XMLStandardTagIds.LINKAGE_GROUP,
								XMLStandardTagIds.NAME,
								chrName);
	}

	private	void	endChr(){
		_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
		_writer.endElement(XMLStandardTagIds.CHROMOSOME);
	}

	private SAXWriter					_writer		= null;
	private	File						_file		= null;
}
