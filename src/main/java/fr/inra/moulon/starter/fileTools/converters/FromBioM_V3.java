/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.converters;

import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.fileTools.utils.FileTools;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class FromBioM_V3 extends FileConverter{
	public FromBioM_V3(File file, String outputName, String mapName){
		super(file, outputName);
		_writer	= new SAXWriter(super.getNewFileName());
		_file	= file;
	}

	public void				executeOLD() {
		Boolean				res				= Boolean.TRUE;
		BufferedReader		buf				= null;
		String				line			= null;
		String				mapName			= null;
		String[]			map_attribute	= new String[2];
		String[]			locus_attribute	= new String[4];
		double				position_previous	= 0;
		double				position		= 0;
		String				locus_type		= null;
		boolean				first_chr		= true;
		String				fieldName		= null;

		try{
			_writer.startDocument();
			_writer.startElement(XMLStandardTagIds.MAPS);
			buf = new BufferedReader(new FileReader(_file));
			line = buf.readLine();
			mapName = line.replaceFirst("mapName=", "");
			_writer.startElement(XMLStandardTagIds.MAP,	XMLStandardTagIds.NAME, mapName);
			while (res && null != (line = buf.readLine())) {
				line = line.trim();
				locus_type = null;
				switch (FileTools.split(line, "\t", locus_attribute)){
					case 1:
						break;
					case 4:
						locus_type = locus_attribute[3];
					case 3:
						try{
							position = Double.valueOf(locus_attribute[2]);
						} catch (NumberFormatException ex){
							locus_type = locus_attribute[2];
						}
					case 2:
						_writer.startElement(	XMLStandardTagIds.MARKER,
												XMLStandardTagIds.NAME,
												locus_attribute[1]);
//						System.out.println("Passage dans executeOLD");
						if (MapGene.LOCUS_LOCATION_ABSOLUTE == _locusLocation){
							_writer.addElement(XMLStandardTagIds.MARKER_LOCATION, String.valueOf(position));
						}else{
							position += position_previous;
							_writer.addElement(XMLStandardTagIds.MARKER_LOCATION, String.valueOf(position));
							position_previous = position;
						}
						_writer.addElement(XMLStandardTagIds.MARKER_ALIASE, "");
						_writer.addElement(XMLStandardTagIds.MARKER_TYPE, locus_type);
						_writer.endElement(XMLStandardTagIds.MARKER);
						//position += position_next;
						break;
					default:
						res = Boolean.FALSE;
				}
				switch (FileTools.split(line, "=", map_attribute)){
					case 1:
						break;
					case 2:
						if (_CHROMOSOME.equals(map_attribute[0])){
							if (!first_chr){
								_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
								_writer.endElement(XMLStandardTagIds.CHROMOSOME);
							}
							first_chr = false;
							position = 0;
							_writer.startElement(	XMLStandardTagIds.CHROMOSOME,
													XMLStandardTagIds.NAME,
													map_attribute[1]);
						}else if (_LINKAGE_GROUP.equals(map_attribute[0])){
							_writer.startElement(	XMLStandardTagIds.LINKAGE_GROUP,
													XMLStandardTagIds.NAME,
													map_attribute[1]);
						}else{
							fieldName = convertField(map_attribute[0]);
							if (_GENUS.equals(fieldName)){
								_fieldID = 1;
							}else if (_SPECIES.equals(fieldName)){
								if (1 != _fieldID){
									_writer.addElement(XMLStandardTagIds.GENUS, MapGene.DEFAULT_GENUS);
								}
								_fieldID = 2;
							}else {
								switch (_fieldID){
									case 0:
										_writer.addElement(XMLStandardTagIds.GENUS, MapGene.DEFAULT_GENUS);
										_writer.addElement(XMLStandardTagIds.SPECIES, MapGene.DEFAULT_SPECIES);
										break;
									case 1:
										_writer.addElement(XMLStandardTagIds.SPECIES, MapGene.DEFAULT_SPECIES);
										break;
								}

							}
							_writer.addElement(fieldName, map_attribute[1]);
						}
						break;
//					default:
//						res = Boolean.FALSE;
//						break;
				}
			}
			if (!first_chr){
				_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
				_writer.endElement(XMLStandardTagIds.CHROMOSOME);
			}
			_writer.endElement(XMLStandardTagIds.MAP);
			_writer.endElement(XMLStandardTagIds.MAPS);
			_writer.endDocument();
			buf.close();
		}catch(IOException e){
			System.out.println();
		}
	}

	@Override
	public void				execute() {
		BufferedReader		buf				= null;
		String				line			= null;
		String				mapName			= null;
		String[]			map_attribute	= new String[2];
		String[]			locus_attribute	= new String[4];
		double				position_previous	= 0;
		double				position		= 0;
		String				locus_type		= null;
		boolean				first_chr		= true;
		boolean				prev_lkg		= false;

		try{
			_writer.startDocument();
			_writer.startElement(XMLStandardTagIds.MAPS);
			buf = new BufferedReader(new FileReader(_file));
			line = buf.readLine().trim();
			mapName = line.replaceFirst("mapName=", "");
			_writer.startElement(XMLStandardTagIds.MAP,	XMLStandardTagIds.NAME, mapName);
			while (null != (line = buf.readLine())) {
				line=line.trim();
				//	Attributes in the BioMercator V3 text file (ie : "field=value")
				switch (FileTools.split(line, "=", map_attribute)){
					case 2:
						String field	= convertField(map_attribute[0]);
						String value	= map_attribute[1];

						if (XMLStandardTagIds.CHROMOSOME.equals(field)){
							if (!first_chr){
								_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
								_writer.endElement(XMLStandardTagIds.CHROMOSOME);
							}
							first_chr = false;
							prev_lkg = false;
							position = 0;
							_writer.startElement(	XMLStandardTagIds.CHROMOSOME,
													XMLStandardTagIds.NAME,
													map_attribute[1]);
						}else if (XMLStandardTagIds.LINKAGE_GROUP.equals(field)){
							if (prev_lkg){
								_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
							}
							_writer.startElement(	XMLStandardTagIds.LINKAGE_GROUP,
													XMLStandardTagIds.NAME,
													map_attribute[1]);
							prev_lkg = true;
							position_previous = 0;
						}else if (XMLStandardTagIds.MARKER_LOCATION.equals(field)){
							try{
								_locusLocation = Integer.valueOf(value);
							}catch(NumberFormatException e){}
							
						}else{
							_writer.addElement(field, value);
						}
						break;
				}

				//	Marker line in the BioMercator V3 text file
				locus_type = null;
				switch (FileTools.split(line, "\t", locus_attribute)){
					case 4:
						locus_type = locus_attribute[3];
					case 3:
						try{
							position = Double.valueOf(locus_attribute[2]);
						} catch (NumberFormatException ex){
							locus_type = locus_attribute[2];
						}
					case 2:
						_writer.startElement(	XMLStandardTagIds.MARKER,
												XMLStandardTagIds.NAME,
												locus_attribute[1]);
//						System.out.println("Passage dans execute");
//						System.out.println(_locusLocation);
						if (MapGene.LOCUS_LOCATION_ABSOLUTE == _locusLocation){
							_writer.addElement(XMLStandardTagIds.MARKER_LOCATION, String.valueOf(position));
						}else{
							position += position_previous;
							_writer.addElement(XMLStandardTagIds.MARKER_LOCATION, String.valueOf(position));
							position_previous = position;
						}
						_writer.addElement(XMLStandardTagIds.MARKER_ALIASE, "");
						_writer.addElement(XMLStandardTagIds.MARKER_TYPE, locus_type);
						_writer.endElement(XMLStandardTagIds.MARKER);
						//position += position_next;
						break;
				}
			}
			if (!first_chr){
				_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
				_writer.endElement(XMLStandardTagIds.CHROMOSOME);
			}
			_writer.endElement(XMLStandardTagIds.MAP);
			_writer.endElement(XMLStandardTagIds.MAPS);
			_writer.endDocument();
			buf.close();
		}catch(IOException e){
			System.out.println();
		}
	}

	private	static	String	convertField(String field){
		String				field_std = null;

		if (null == (field_std = _mapTags.get(field))){
			field_std = XMLStandardTagIds.UNKNOWN;
		}

		return field_std;
	}

	// Attribute for adding default genus and species to the XML (mandatory and first of <map> elements).
	private	int	_fieldID	= 0;


	private	static	String	_GENUS				= "Organism Genus";
	private	static	String	_SPECIES			= "Organism Species";
	private	static	String	_PARENT				= "Parent";
	private	static	String	_CROSS_TYPE			= "crossType";
	private	static	String	_POP_SIZE			= "popSize";
	private	static	String	_MAPPING_CROSS_TYPE	= "mappingCrossType";
	private	static	String	_MAPPING_FUNCTION	= "mappingFunction";
	private	static	String	_MAP_NAME			= "mapName";
	private	static	String	_MAP_UNIT			= "mapUnit";
	private	static	String	_MAP_EXPANSION		= "mapExpansion";
	private	static	String	_MAP_QUALITY		= "mapQuality";
	private	static	String	_LOCUS_LOCATION		= "locusLocation";
	private	static	String	_CHROMOSOME			= "chr";
	private	static	String	_LINKAGE_GROUP		= "lg";


	private SAXWriter					_writer			= null;
	private	File						_file			= null;
	private	int							_locusLocation	= MapGene.LOCUS_LOCATION_RELATIVE;
	private	static Map<String, String>	_mapTags		=
			new HashMap(){{
				put(_GENUS,					XMLStandardTagIds.GENUS);
				put(_SPECIES,				XMLStandardTagIds.SPECIES);
				put(_PARENT,				XMLStandardTagIds.PARENT_NAME);
				put(_CROSS_TYPE,			XMLStandardTagIds.CROSSTYPE);
				put(_POP_SIZE,				XMLStandardTagIds.POPSIZE);
				put(_MAPPING_CROSS_TYPE,	XMLStandardTagIds.MAPPING_CROSSTYPE);
				put(_MAPPING_FUNCTION,		XMLStandardTagIds.MAPPING_FUNCTION);
				put(_MAP_UNIT,				XMLStandardTagIds.MAP_UNIT);
				put(_MAP_EXPANSION,			XMLStandardTagIds.MAP_EXPANSION);
				put(_MAP_QUALITY,			XMLStandardTagIds.MAP_QUALITY);
				put(_LOCUS_LOCATION,		XMLStandardTagIds.MARKER_LOCATION);
				put(_CHROMOSOME,			XMLStandardTagIds.CHROMOSOME);
				put(_LINKAGE_GROUP,			XMLStandardTagIds.LINKAGE_GROUP);
			}};
}
