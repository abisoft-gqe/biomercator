/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.utils;


public class FileTools {
	/**
	 * Cuts, using the separator, the given string into substrings, and stores
	 * them into the given array. The array will contain at maximum as much
	 * elements as its length. Returns the filled array.
	 * This function is faster than the String.split() function.
	 * @param str The string to cut
	 * @param separator The separator
	 * @param array The array where to store the substrings
	 * @return The number of substrings found.
	 */
	public	static	int	split(String str, String separator, String[] array){
		int				start	= 0;
		int				end		= 0;
		int				index	= 0;

		while (index < array.length && -1 != (end = str.indexOf(separator, start))){
			array[index++] = str.substring(start, end);
			start = end + separator.length();
		}
		if (index < array.length && start < str.length()){
			array[index++] = str.substring(start, str.length());
		}

		return index;
	}
}
