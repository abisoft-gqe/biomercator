/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.verifyers;

import java.util.Set;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 *
 * @author sosnowski
 */
public class XmlVerifyer  implements ContentHandler {
	public XmlVerifyer(XMLTree	root){
		super();
		_tree = root;
		_verified = Boolean.TRUE;
		_allowedTags = root.getAllowedTags();
	}

	@Override
	public void setDocumentLocator(Locator value) {
	}

	@Override
	public void startDocument() throws SAXException {
	}

	@Override
	public void endDocument() throws SAXException {
	}

	@Override
	public void startPrefixMapping(String prefix, String URI) throws SAXException {
	}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException {
	}

	@Override
	public void	startElement(	String		nameSpaceURI,
								String		localName,
								String		rawName,
								Attributes	attributs) throws SAXException {
		if (null != _tree && _tree.canMoveDown(localName)){
			_tree = _tree.moveDown(localName);
		}else if (!_allowedTags.contains(localName)){
//			System.out.println("Unknown tag : " + localName);
			_verified = Boolean.FALSE;
		}
	}

	@Override
	public void endElement(String nameSpaceURI, String localName, String rawName) throws SAXException {
		//_tree = _tree._dad;
		if (null != _tree){
//			for (Iterator<XMLTree> it = _tree.getMandatorySonsUnchecked().iterator(); it.hasNext();) {
//				XMLTree uncheckedSon = it.next();

				//System.out.println("In <" + _tree + ">, son <" + uncheckedSon + "> is unchecked !");
				//_verified = Boolean.FALSE;
//			}
			if (!_allowedTags.contains(localName)){
				_tree = _tree.moveUp();
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int end) throws SAXException {}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int end) throws SAXException {}

	@Override
	public void processingInstruction(String target, String data) throws SAXException {}

	@Override
	public void skippedEntity(String arg0) throws SAXException {
		System.out.println("THERE IS A MASSIVE PROBLEM !!!");
	}

	/**
	 * Returns if the XML file corresponds to the given XML tree.
	 * @return if the XML file corresponds to the given XML tree.
	 */
	public	Boolean	isFileVerified(){
		return _verified;
	}

	private	XMLTree		_tree			= null;
	private	Boolean		_verified		= null;
	private	Set<String>	_allowedTags	= null;
}
