/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.inra.moulon.starter.fileTools.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class is a file parsing logger; ie it stocks the elements loaded count
 * for each map in the file.
 * The methods should be called during every parsing as it stores only the last
 * one.
 * @author sosnowski
 */
public class Summary {
    private Summary() {
	}

	public static Summary instance(){
		if (null == _instance){
			synchronized (Summary.class){
				if (null == _instance){
					_instance = new Summary();
				}
			}
		}
		return _instance;
	}

	/**
	 * Reset the logging for a new parsing.
	 */
	public	void	newSummary(){
		_maps = new Node();
	}

	/**
	 * This method should be called when a new map is found.
	 * @param mapName The map name
	 */
	public	void	newMap(String mapName){
		_map = new Node();
		_maps.node.put(mapName, _map);
	}

	/**
	 * This method should be called when a new chromosome is found.
	 * @param mapName The chromosome name
	 */
	public	void	newChromosome(String chrName){
		_chr = new Node();
		_map.node.put(chrName, _chr);
	}

	/**
	 * This method should be called when a new linkage group is found.
	 * @param mapName The linkage group name
	 */
	public	void	newLinkageGroup(String lkgName){
		_lkg = new Leaf();
		_chr.node.put(lkgName, _lkg);
	}

	/**
	 * This method should be called when a new marker is found.
	 */
	public	void	addMarkerCount(){
		_lkg.nbMarkers++;
	}

	/**
	 * This method should be called when a new QTL is found.
	 */
	public	void	addQTLCount(){
		_lkg.nbQTLs++;
	}

	/**
	 * Returns an iterator on maps summary. (Each element corresponds to a map)
	 * @return An iterator on maps summary.
	 */
	public	Iterator<String>	iterator(){
		List<String>			infos		= new ArrayList<String>();
		String					mapName		= null;
		String					chrName		= null;
		String					lkgName		= null;
		String					str			= null;
		Node					map			= null;
		Node					chr			= null;
		Leaf					lkg			= null;
		int						nbChr		= 0;
		int						nbMarkers	= 0;
		int						nbQTLs		= 0;

		for (Iterator<String> itMap = _maps.node.keySet().iterator(); itMap.hasNext();) {
			mapName = itMap.next();
			map = (Node)_maps.node.get(mapName);
			nbChr = 0;
			nbMarkers = 0;
			nbQTLs = 0;
			for (Iterator<String> itChr = map.node.keySet().iterator(); itChr.hasNext();) {
				chrName = itChr.next();
				chr = (Node)map.node.get(chrName);
				for (Iterator<String> itLkg = chr.node.keySet().iterator(); itLkg.hasNext();) {
					lkgName = itLkg.next();
					lkg = (Leaf)chr.node.get(lkgName);
					nbMarkers += lkg.nbMarkers;
					nbQTLs += lkg.nbQTLs;
				}
				++nbChr;
			}
			str = "<html>"
					+ mapName + " contains :"
					+ "<ul>"
					+ " <li>" + nbChr		+ " chromosome" + (nbChr > 1 ? "s" : "")
					+ " </li>"
					+ " <li>" + nbMarkers	+ " locus"
					+ " </li>"
					+ " <li>" + nbQTLs		+ " QTL" + (nbQTLs > 1 ? "s" : "")
					+ " </li>"
					+ " </ul>"
					+ "</html>";


//			str = mapName		+ " contains "
//					+ nbChr		+ " chromosome" + (nbChr > 1 ? "s" : "")
//					+ nbMarkers	+ " locus"
//					+ nbQTLs	+ " QTL" + (nbQTLs > 1 ? "s" : "");
			infos.add(str);
		}

		return infos.iterator();
	}

	private	Node	_maps	= null;
	private	Node	_map	= null;
	private	Node	_chr	= null;
	private	Leaf	_lkg	= null;


	// Classes for the simple "Tree" implementation
	private	interface AbstractNode{
	}

	private	class Node implements AbstractNode{
		Map<String, AbstractNode>	node = new HashMap<String, AbstractNode>();
	}

	private	class Leaf implements AbstractNode{
		int	nbMarkers	= 0;
		int	nbQTLs		= 0;
//		int	nbMetaQTLs	= 0;
	}

	// Attribute for singleton
	private	volatile	static	Summary	_instance;
 }
