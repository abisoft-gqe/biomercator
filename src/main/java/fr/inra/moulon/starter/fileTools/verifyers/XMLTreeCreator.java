/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 21-Mar-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.verifyers;

import fr.inra.moulon.starter.fileTools.utils.XMLMCQTLTagIds;
import fr.inra.moulon.starter.fileTools.utils.XMLMetaQTLTagIds;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;

public class XMLTreeCreator {
	public	static	XMLTree	createXMLStandardTree(){
		XMLTree		root				= new XMLTree("root");
		XMLTree		maps				= new XMLTree(XMLStandardTagIds.MAPS);
		XMLTree		map					= new XMLTree(XMLStandardTagIds.MAP);
		XMLTree		chromosome			= new XMLTree(XMLStandardTagIds.CHROMOSOME);
		XMLTree		lkg					= new XMLTree(XMLStandardTagIds.LINKAGE_GROUP);
		XMLTree		marker				= new XMLTree(XMLStandardTagIds.MARKER);
		XMLTree		qtl					= new XMLTree(XMLStandardTagIds.QTL);
		XMLTree		qtlEffect			= new XMLTree(XMLStandardTagIds.QTL_EFFECT);
		XMLTree		metaAnalysis		= new XMLTree(XMLStandardTagIds.META_A);
		XMLTree		mqtl				= new XMLTree(XMLStandardTagIds.MQTL);
		XMLTree		mqtlEffect			= new XMLTree(XMLStandardTagIds.MQTL_EFFECT);
		XMLTree		membership			= new XMLTree(XMLStandardTagIds.MQTL_MEMBERSHIP);

		root.addSon(maps);
		maps.addSon(map);

		map.addSon(new String[]{XMLStandardTagIds.GENUS,
								XMLStandardTagIds.SPECIES,
								XMLStandardTagIds.PARENT_NAME,
								XMLStandardTagIds.CROSSTYPE,
								XMLStandardTagIds.POPSIZE,
								XMLStandardTagIds.MAPPING_CROSSTYPE,
								XMLStandardTagIds.MAPPING_FUNCTION,
								XMLStandardTagIds.MAP_UNIT,
								XMLStandardTagIds.MAP_EXPANSION,
								XMLStandardTagIds.MAP_QUALITY,
								});
		map.addSon(chromosome);
		chromosome.addSon(lkg);
		lkg.addSon(marker);

		marker.addSon(new String[]{	XMLStandardTagIds.MARKER_ALIASE,
									XMLStandardTagIds.MARKER_LOCATION,
									XMLStandardTagIds.MARKER_TYPE
									});

		lkg.addSon(qtl);

		qtl.addSon(new String[]{XMLStandardTagIds.QTL_TRAIT,
								XMLStandardTagIds.QTL_TRAIT_OID,
								XMLStandardTagIds.QTL_EXP_PLACE,
								XMLStandardTagIds.QTL_EXP_DATE,
								XMLStandardTagIds.QTL_LOD,
								XMLStandardTagIds.QTL_R2,
								XMLStandardTagIds.QTL_LOCATION,
								XMLStandardTagIds.QTL_CI_FROM,
								XMLStandardTagIds.QTL_CI_TO,
								XMLStandardTagIds.QTL_CROSS_NAME,
								XMLStandardTagIds.QTL_CROSS_SIZE,
								XMLStandardTagIds.QTL_CROSS_TYPE
								});

		qtl.addSon(qtlEffect);
		qtlEffect.addSon(new String[]{	XMLStandardTagIds.QTL_EFFECT_UNIT,
										XMLStandardTagIds.QTL_EFFECT_UNIT_OID,
										XMLStandardTagIds.QTL_PARENT
										});

		lkg.addSon(metaAnalysis);
		metaAnalysis.addSon(new String[]{	XMLStandardTagIds.META_A_METHOD,
											XMLStandardTagIds.META_A_USED_QTL_ID
											});
		//metaAnalysis.addSon(usedQtlId);
		metaAnalysis.addSon(mqtl);


		mqtl.addSon(new String[]{	XMLStandardTagIds.MQTL_TRAIT,
									XMLStandardTagIds.MQTL_TRAIT_OID,
									XMLStandardTagIds.MQTL_EXP_PLACE,
									XMLStandardTagIds.MQTL_EXP_DATE,
									XMLStandardTagIds.MQTL_LOD,
									XMLStandardTagIds.MQTL_R2,
									XMLStandardTagIds.MQTL_LOCATION,
									XMLStandardTagIds.MQTL_CI_FROM,
									XMLStandardTagIds.MQTL_CI_TO,
									XMLStandardTagIds.MQTL_CROSS_NAME,
									XMLStandardTagIds.MQTL_CROSS_SIZE,
									XMLStandardTagIds.MQTL_CROSS_TYPE,
									});
		mqtl.addSon(mqtlEffect);

		mqtlEffect.addSon(new String[]{	XMLStandardTagIds.MQTL_EFFECT_UNIT,
										XMLStandardTagIds.MQTL_EFFECT_UNIT_OID,
										XMLStandardTagIds.MQTL_PARENT});
		mqtl.addSon(membership);

		return root;
	}

	/**
	 * Creates an d returns a generic XML tree corresponding to a metaQTL XML
	 * format.
	 * @return The tree
	 */
	public	static	XMLTree	createXMLMetaQTLTree(){
		XMLTree		root				= new XMLTree("root");
		XMLTree		maps				= new XMLTree(XMLMetaQTLTagIds.GENOME_MAP);
		XMLTree		property_map		= new XMLTree(XMLMetaQTLTagIds.PROPERTY);
		XMLTree		lkg					= new XMLTree(XMLMetaQTLTagIds.LINKAGE_GROUP);
		XMLTree		property_lkg		= new XMLTree(XMLMetaQTLTagIds.PROPERTY);
		XMLTree		locus				= new XMLTree(XMLMetaQTLTagIds.LOCUS);
		XMLTree		property_locus		= new XMLTree(XMLMetaQTLTagIds.PROPERTY);


		root.addSon(maps);
		maps.addSon(property_map);
		maps.addSon(lkg);
		lkg.addSon(locus);
		lkg.addSon(property_lkg);
		locus.addSon(property_locus);

		return root;
	}

	/**
	 * Creates and returns a generic XML tree corresponding to a MCQTL XML
	 * format.
	 * @return The tree
	 */
	public	static	XMLTree	createMCQTLTree(){
		XMLTree		root				= new XMLTree("root");
		XMLTree		trait				= new XMLTree(XMLMCQTLTagIds.TRAIT);
		XMLTree		genetic_map			= new XMLTree(XMLMCQTLTagIds.GENETIC_MAP);
		XMLTree		chromosome			= new XMLTree(XMLMCQTLTagIds.CHROMOSOME);

		XMLTree		marker				= new XMLTree(XMLMCQTLTagIds.MARKER);
		XMLTree		qtl					= new XMLTree(XMLMCQTLTagIds.QTL);
		XMLTree		lod					= new XMLTree(XMLMCQTLTagIds.QTL_LOD_SUPPORT);
		XMLTree		ci_start			= new XMLTree(XMLMCQTLTagIds.QTL_INFERIOR_POSITION);
		XMLTree		ci_end				= new XMLTree(XMLMCQTLTagIds.QTL_SUPERIOR_POSITION);

		root.addSon(trait);
		trait.addSon(genetic_map);
		genetic_map.addSon(chromosome);
		chromosome.addSon(marker);
		chromosome.addSon(qtl);
		qtl.addSon(lod);
		lod.addSon(ci_start);
		lod.addSon(ci_end);

		// For tags not important but present
		root.addAllowedTag(XMLMCQTLTagIds.QTL_R2);
		root.addAllowedTag(XMLMCQTLTagIds.QTL_TEST);
		root.addAllowedTag(XMLMCQTLTagIds.QTL_NUMERATOR);
		root.addAllowedTag(XMLMCQTLTagIds.QTL_DENOMINATOR);
		root.addAllowedTag(XMLMCQTLTagIds.ESTIMATION);
		root.addAllowedTag(XMLMCQTLTagIds.RESIDUAL_VARIANCE);
		root.addAllowedTag(XMLMCQTLTagIds.R2_GLOBAL);
		root.addAllowedTag(XMLMCQTLTagIds.CROSS_EFFECT);
		root.addAllowedTag(XMLMCQTLTagIds.EFFECTS);
		root.addAllowedTag(XMLMCQTLTagIds.QTL_EFFECT);

		return root;
	}
}
