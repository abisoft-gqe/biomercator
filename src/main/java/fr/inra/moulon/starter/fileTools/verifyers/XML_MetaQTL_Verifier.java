/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 6, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.verifyers;

import fr.inra.moulon.starter.fileTools.FileManager.FileFormat;

import java.io.FileInputStream;
import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * This class is designed to verify a file is XML MetaQTL format.
 * @author sosnowski
 */
public class XML_MetaQTL_Verifier implements Verifier{
	@Override
	public FileFormat getFormatId() {
		return FileFormat.XML_METAQTL;
	}

	@Override
	public Boolean	verifyFile(String filePath) {
		XMLTree		xmlTree		= XMLTreeCreator.createXMLMetaQTLTree();
		XmlVerifyer	handler		= null;
		XMLReader	saxReader	= null;
		Boolean		res			= Boolean.FALSE;

		try {
			handler = new XmlVerifyer(xmlTree);
			saxReader = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
			//saxReader = XMLReaderFactory.createXMLReader("javax.xml.parsers.SAXParser");
			saxReader.setContentHandler(handler);
			FileInputStream stream = new FileInputStream(filePath);
			saxReader.parse(new InputSource(stream));
			//saxReader.parse(filePath);
			res = handler.isFileVerified();
		} catch (SAXException ex) {
			System.out.println(ex.getMessage());
		} catch (IOException ex){
			System.out.println(ex.getMessage());
		}

		return res;

	}
}
