/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 15-Jun-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.logging;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Parameters {
	public	Parameters(){
		_map = new LinkedHashMap<String, List<String>>();
	}

	public	void		addParam(String field, String value){
		List<String>	values	= new ArrayList<String>();

		values.add(value);

		addParam(field, values);
	}

	public	void		addParam(String field, Set<String> values){
		addParam(field, new ArrayList<String>(values));
	}


	public	void	addParam(String field, List<String> values){
		if (!_map.containsKey(field)){
			_map.put(field, new ArrayList<String>());
		}
		_map.get(field).addAll(values);
	}

	public	Iterator<String>	iterator(){
		return _map.keySet().iterator();
	}

	public	List<String>	getValues(String field){
		return _map.get(field);
	}

	private Map<String, List<String>>	_map = null;
}
