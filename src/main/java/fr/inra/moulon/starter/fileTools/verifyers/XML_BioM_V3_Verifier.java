/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 6, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.verifyers;

import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.fileTools.FileManager.FileFormat;
import fr.inra.moulon.starter.utils.Session;
import java.net.URL;
import javax.xml.XMLConstants;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.InputSource;

/**
 * This class is designed to verify a file is XML BioMercator V3 format.
 * @author sosnowski
 */
public class XML_BioM_V3_Verifier implements Verifier{
	@Override
	public FileFormat getFormatId() {
		return FileFormat.XML_BIOMERATOR_V3;
	}

	private	static	Schema	loadSchema(URL url) {
		Schema				schema = null;
		try {
			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
			SchemaFactory factory = SchemaFactory.newInstance(language);
			schema = factory.newSchema(url);
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return schema;
	}

	@Override
	public Boolean	verifyFile(String filePath) {
		Boolean		res = Boolean.TRUE;
		Schema		schema = loadSchema(Session.instance().getResource(FileManager.RESOURCES_CONFIG_BMV3_XSD));

		try {
			// creating a Validator instance
			Validator validator = schema.newValidator();

			// preparing the XML file as a SAX source
			SAXSource source = new SAXSource(
				new InputSource(new java.io.FileInputStream(filePath)));

			// validating the SAX source against the schema
			validator.validate(source);
		} catch (Exception e) {
			System.out.println("File isn't XML BioMercatorV3 : " + e.toString());
			res = false;
		}

		return res;
	}
}
