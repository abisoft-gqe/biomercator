/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.converters;

import fr.inra.moulon.starter.fileTools.utils.FileTools;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;
import fr.inra.moulon.starter.fileTools.verifyers.CSV_BioM_V3_QTL_Verifier;
import fr.inra.moulon.starter.utils.StringTools;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FromBioM_QTL_V3 extends FileConverter{
	public FromBioM_QTL_V3(File file, String outputName, String mapName){
		super(file, outputName);
		_writer	= new SAXWriter(super.getNewFileName());
		_file	= file;
	}

	@Override
	public void			execute() {
		Boolean			res				= Boolean.TRUE;
		BufferedReader	buf				= null;
		String			line			= null;
		int				nbFields		= 12;
		String[]		tokens			= new String[nbFields];
		String			mapName			= null;
		String			chrName			= null;
		String			lkgName			= null;
		boolean			first_chr		= true;
		boolean			first_lkg		= true;
		int				nbCurFields		= 0;

		try{
			_writer.startDocument();
			_writer.startElement(XMLStandardTagIds.MAPS);
			buf = new BufferedReader(new FileReader(_file));

			// Map name get
			if (null != (line = buf.readLine())){
				line = line.trim();
				mapName = line.replaceFirst(CSV_BioM_V3_QTL_Verifier.MAPNAME, "");
				startMap(mapName);
			}else{
				res = false;
			}

			// All QTLs get
			while (res && null != (line = buf.readLine())) {
				line = line.trim();
				line = StringTools.removeComment(line);
				System.out.print("suis ici");
				nbCurFields = FileTools.split(line, "\t", tokens);
				if (nbFields == nbCurFields){
					if (!tokens[5].equals(chrName)){
						chrName = tokens[5];
						if (!first_chr){
							endLkg();
							endChr();
						}
						first_chr = false;
						first_lkg = true;
						lkgName = null;
						startChr(chrName);
					}
					if (!tokens[6].equals(lkgName)){
						lkgName = tokens[6];
						if (!first_lkg){
							endLkg();
						}
						first_lkg = false;
						startLkg(lkgName);
					}
					_writer.startElement(	XMLStandardTagIds.QTL,
											XMLStandardTagIds.NAME,
											tokens[0]);
					_writer.addElement(XMLStandardTagIds.QTL_TRAIT,			tokens[1]);
					_writer.addElement(XMLStandardTagIds.QTL_TRAIT_OID,		tokens[2]);
					_writer.addElement(XMLStandardTagIds.QTL_EXP_PLACE,		tokens[3]);
					_writer.addElement(XMLStandardTagIds.QTL_EXP_DATE,		tokens[4]);
					_writer.addElement(XMLStandardTagIds.QTL_LOD,			tokens[7]);
					_writer.addElement(XMLStandardTagIds.QTL_R2,			tokens[8]);
					_writer.addElement(XMLStandardTagIds.QTL_LOCATION,		tokens[9]);
					_writer.addElement(XMLStandardTagIds.QTL_CI_FROM,		tokens[10]);
					_writer.addElement(XMLStandardTagIds.QTL_CI_TO,			tokens[11]);
					_writer.addElement(XMLStandardTagIds.QTL_CROSS_NAME,	"");
					_writer.addElement(XMLStandardTagIds.QTL_CROSS_SIZE,	"");
					_writer.addElement(XMLStandardTagIds.QTL_CROSS_TYPE,	"");
					_writer.endElement(XMLStandardTagIds.QTL);
//				}else if (1 == nbCurFields){
//					mapName = tokens[0].replaceFirst(CSV_BioM_V3_QTL_Verifier.MAPNAME, "");
//					if (!first_map){
//						endMap();
//					}else{
//						first_map = false;
//					}
//					chrName = null;
//					lkgName = null;
//					startMap(mapName);
				}
			}
			if (!first_lkg){
				endLkg();
			}
			if (!first_chr){
				endChr();
			}
			endMap();
			_writer.endElement(XMLStandardTagIds.MAPS);
			_writer.endDocument();
			buf.close();
		}catch(IOException e){
			System.out.println();
		}
	}

	private	void	startMap(String mapName){
		_writer.startElement(XMLStandardTagIds.MAP,	XMLStandardTagIds.NAME, mapName);
	}

	private	void	endMap(){
		_writer.endElement(XMLStandardTagIds.MAP);
	}

	private	void	startChr(String chrName){
		_writer.startElement(	XMLStandardTagIds.CHROMOSOME,
								XMLStandardTagIds.NAME,
								chrName);
	}

	private	void	endChr(){
		_writer.endElement(XMLStandardTagIds.CHROMOSOME);
	}

	private	void	startLkg(String lkgName){
		_writer.startElement(	XMLStandardTagIds.LINKAGE_GROUP,
								XMLStandardTagIds.NAME,
								lkgName);
	}

	private	void	endLkg(){
		_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
	}

	private SAXWriter	_writer	= null;
	private	File		_file	= null;
}
