/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.utils;

/**
 *
 * @author sosnowski
 */
public class XMLMCQTLTagIds {
	public static final String		TRAIT					= "TRAIT";
	public static final String		GENETIC_MAP				= "GENETIC_MAP";
	public static final String		CHROMOSOME				= "CHROMOSOME";
	public static final String		MARKER					= "MARKER";
	public static final String		QTL						= "QTL";
	public static final String		QTL_R2					= "R2";
	public static final String		QTL_NUMERATOR			= "NUMERATOR_ddl";
	public static final String		QTL_DENOMINATOR			= "DENOMINATOR_ddl";
	public static final String		QTL_TEST				= "TEST";
	public static final String		QTL_LOD_SUPPORT			= "LOD_SUPPORT";
	public static final String		QTL_INFERIOR_POSITION	= "INFERIOR_POSITION";
	public static final String		QTL_SUPERIOR_POSITION	= "SUPERIOR_POSITION";

	public static final String		ESTIMATION				= "ESTIMATION";
	public static final String		RESIDUAL_VARIANCE		= "RESIDUAL_VARIANCE";
	public static final String		R2_GLOBAL				= "R2_GLOBAL";
	public static final String		CROSS_EFFECT			= "CROSS_EFFECT";
	public static final String		EFFECTS					= "EFFECTS";
	public static final String		QTL_EFFECT				= "QTL_EFFECT";


	public static final String		PROP_VALUE				= "value";
	public static final String		PROP_NAME				= "name";
	public static final String		PROP_POSITION			= "position";
}
