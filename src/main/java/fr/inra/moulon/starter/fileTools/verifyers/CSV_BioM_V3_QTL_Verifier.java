/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.verifyers;

import fr.inra.moulon.starter.fileTools.FileManager.FileFormat;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class CSV_BioM_V3_QTL_Verifier implements Verifier{
	public	static	final	String	MAPNAME		= "mapName=";
	public	static	final	int		NBFIELDS	= 12;

	@Override
	public FileFormat getFormatId() {
		return FileFormat.CSV_BIOMERATOR_QTL_V3;
	}

	@Override
	public Boolean verifyFile(String filePath) {
		Boolean			res		= Boolean.TRUE;
		BufferedReader	buf		= null;
		String			line	= null;
		String[]		tokens	= null;

		try{
			buf = new BufferedReader(new FileReader(new File(filePath)));
			// Checking first line
			if (null == (line = buf.readLine()) || !line.startsWith(MAPNAME)) {
				res = false;
			}
			// Checking rest of the file
			while (res && null != (line = buf.readLine())) {
				tokens = line.split("\t");
				if (NBFIELDS != tokens.length){
					res = false;
				}
			}
			buf.close();
		}catch (Exception e){
			System.out.println();
		}

		return res;
	}

}
