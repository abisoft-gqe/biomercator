/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.converters;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.File;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;



public class FromMetaQTLXml extends FileConverter{
	public FromMetaQTLXml(File file, String outputName, String mapName){
		super(file, outputName);
		_mapName = mapName;
	}

	private void readerMetaQTL() throws SAXException, IOException{
		MetaQTLXMLRW handler = new MetaQTLXMLRW(_outputName, _mapName);
				
		XMLReader saxReader = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
		//XMLReader saxReader = XMLReaderFactory.createXMLReader("javax.xml.parsers.SAXParser");
		saxReader.setContentHandler(handler);
		FileInputStream stream = new FileInputStream(_fileName);
		saxReader.parse(new InputSource(stream));
		//saxReader.parse(_fileName);
	}

	@Override
	public void	execute(){
		if (null == _fileName){
            System.out.println("Error : file name to load is null");
        }
		try {
			readerMetaQTL();
		} catch (Throwable t) {
			t.printStackTrace(System.out);
			System.out.println(t.getMessage());
		}
	}

	private	String	_mapName	= null;
}
