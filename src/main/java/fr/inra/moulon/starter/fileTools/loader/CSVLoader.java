/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 05-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.loader;

import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.database.Field;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoundedRangeModel;


public class CSVLoader {
	public	static	int	load(	File				file,
								String				sep,
								String				statementInsertUnique,
								String				statementInsertAll,
								List<Field>			correspondance,
								BoundedRangeModel	boundedModel){
		Connection			connection	= null;
		PreparedStatement	prep		= null;
		Statement			stat		= null;
		ResultSet			rs			= null;
		BufferedReader		buf			= null;
		String				line		= null;
		int					indexPrev	= 0;
		int					index		= 0;
		int					batchIndex	= 0;
		int					batchSize	= 100;
		List<String>		fields		= new ArrayList<String>();
		int					id			= 0;
		long				fileSize	= file.length();
		long				readSize	= 0;
		int					percent		= 0;

		try {
			connection	= DatabaseManager.instance().getConnection();
			connection.setAutoCommit(false);

			stat = connection.createStatement();
			stat.executeUpdate(statementInsertUnique);
			rs = stat.executeQuery("select last_insert_rowid();");
			if (rs.next()){
				id = rs.getInt(1);
				prep = connection.prepareStatement(statementInsertAll);
				buf = new BufferedReader(new FileReader(file));
				while (null != (line = buf.readLine())) {
					if (line.length() > 0 && line.charAt(0) != '#'){	// To avoid possible commented lines
						indexPrev = 0;
						fields.clear();
						while (-1 != (index = line.indexOf(sep, indexPrev))){
							fields.add(line.substring(indexPrev, index));
							indexPrev = index+sep.length();
						}
						if (indexPrev < line.length()){
							fields.add(line.substring(indexPrev, line.length()));
						}
						insertToPrep(correspondance, fields, prep);
						prep.setInt(1, id);
						prep.addBatch();
						if (++batchIndex > batchSize){
							prep.executeBatch();
							batchIndex = 0;
						}
					}
					readSize += line.length();
					percent = Math.round(readSize*100/fileSize);
					boundedModel.setValue(percent);
				}
				prep.executeBatch();	//	For last batch
			}
			connection.setAutoCommit(true);
		} catch (Throwable t) {
			t.printStackTrace(System.out);
		}

		return id;
	}

	private	static	void	insertToPrep(	List<Field>			correspondance,
											List<String>		fieldsValue,
											PreparedStatement	prep){
		Field				field			= null;
		String				fieldValue		= null;
		boolean				correctValues	= true;
		Integer				intValue		= null;
		Double				doubleValue		= null;

		
		try{
			for (int i = 0; i < fieldsValue.size(); ++i) {
				fieldValue = fieldsValue.get(i).trim();
				field = correspondance.get(i);
				switch (field.valType){
					case Field.STRING:
						prep.setString(field.index, fieldValue);
						break;
					case Field.INT:
						try{
							intValue = Integer.valueOf(fieldValue);
							prep.setInt(field.index, intValue);
						}catch(NumberFormatException e){
							correctValues = false;
						}
						break;
					case Field.DOUBLE:
						try{
							doubleValue = Double.valueOf(fieldValue);
							prep.setInt(field.index, intValue);
							prep.setDouble(field.index, doubleValue);
						}catch(NumberFormatException e){
							correctValues = false;
						}
						break;
					case Field.NONE:
						break;
				}
				if (!correctValues){
					prep.clearParameters();
				}
			}
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
	}
}