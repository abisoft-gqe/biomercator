/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.inra.moulon.starter.fileTools.verifyers;

import fr.inra.moulon.starter.fileTools.FileManager.FileFormat;

/**
 *
 * @author sosnowski
 */
public interface Verifier {
	/**
	 * Returns the ID corresponding to the file verifier.
	 * @return the ID corresponding to the file verifier.
	 */
	public	FileFormat	getFormatId();

	/**
	 * Launch the verifying process on the given file and returns if the file
	 * format is verified.
	 * @param filePath the file path to check.
	 * @return If the file corresponds to the format checked by the verifier.
	 */
	public	Boolean	verifyFile(String filePath);
}
