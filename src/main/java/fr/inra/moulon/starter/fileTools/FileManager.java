/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: Apr 6, 2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools;

import fr.inra.moulon.starter.controller.Controller;
import fr.inra.moulon.starter.controller.visitors.Visitor;
import fr.inra.moulon.starter.controller.visitors.VisitorSaveMetaQtlXML;
import fr.inra.moulon.starter.controller.visitors.VisitorSaveStandardXML;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Project;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.fileTools.converters.FileConverter;
import fr.inra.moulon.starter.fileTools.converters.FromBioM_QTL_V2;
import fr.inra.moulon.starter.fileTools.converters.FromBioM_QTL_V3;
import fr.inra.moulon.starter.fileTools.converters.FromBioM_V2;
import fr.inra.moulon.starter.fileTools.converters.FromBioM_V3;
import fr.inra.moulon.starter.fileTools.converters.FromMCQTLXml;
import fr.inra.moulon.starter.fileTools.converters.FromMetaQTLXml;
import java.io.File;
import fr.inra.moulon.starter.fileTools.loader.XMLStandardHandler;
import fr.inra.moulon.starter.fileTools.logging.AnalysisLogger;
import fr.inra.moulon.starter.fileTools.logging.Parameters;
import fr.inra.moulon.starter.fileTools.verifyers.CSV_BioM_V2_QTL_Verifier;
import fr.inra.moulon.starter.fileTools.verifyers.CSV_BioM_V2_Verifier;
import fr.inra.moulon.starter.fileTools.verifyers.CSV_BioM_V3_QTL_Verifier;
import fr.inra.moulon.starter.fileTools.verifyers.CSV_BioM_V3_Verifier;
import fr.inra.moulon.starter.fileTools.verifyers.Verifier;
import fr.inra.moulon.starter.fileTools.verifyers.XML_BioM_V3_Verifier;
import fr.inra.moulon.starter.fileTools.verifyers.XML_MCQTL_Verifier;
import fr.inra.moulon.starter.fileTools.verifyers.XML_MetaQTL_Verifier;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import org.apache.commons.io.IOUtils;


public class FileManager {
	public	static	void	initDirectories(){
		createDirectoryIfNeeded(BIOMERCATOR_DIR);
		createDirectoryIfNeeded(DIR_SOFT);
		createDirectoryIfNeeded(DIR_SOFT_MAP);
		createDirectoryIfNeeded(DIR_SOFT_TMP);
		createDirectoryIfNeeded(DIR_SOFT_CONFIG);
		createDirectoryIfNeeded(DIR_RES_ANALYSES);
	}

	/**
	 * This method loads the WOrkspace's XML Standard BioMercator V3 files into
	 * the data model.
	 */
	public	static	void	loadWorkspace(){
		File				dir				= new File(DIR_SOFT_MAP);
		File[]				listProjects	= null;

		if (dir.isDirectory()){
			if (null != (listProjects = dir.listFiles())){
				for (int i = 0; i < listProjects.length; i++){
					loadProject(listProjects[i]);
				}
			}
        } 
	}

	/**
	 * This method loads the directory's XML Standard BioMercator V3 files,
	 * tabulated files and image files into the data model.
	 */
	private	static	void	loadProject(File dirProject){
		File				subFile			= null;
		File[]				listFiles		= null;
		File[]				listSubFiles	= null;
		List<File>			listMetaFiles	= null;

		if (dirProject.isDirectory()){
			if (null != (listFiles = dirProject.listFiles())){
				listMetaFiles = new ArrayList<File>();
				for (int i = 0; i < listFiles.length; i++){
					File file = listFiles[i];

					if (file.isDirectory()){
						listMetaFiles.add(file);
					}else if (file.canRead()){
						if (file.getName().endsWith(".xml")){
							loadDirect(file, dirProject.getName());
						}else{
							Controller.addFileToProject(file, dirProject.getName());
						}
					}
				}
				for (int i = 0; i < listMetaFiles.size(); i++) {
					File metaFile = listMetaFiles.get(i);

					if (null != (listSubFiles = metaFile.listFiles())){
						for (int iSub = 0; iSub < listSubFiles.length; iSub++){
							subFile = listSubFiles[iSub];
							Controller.addMetaFileToProject(subFile, dirProject.getName(), metaFile.getName());
						}
					}
				}

			}
		}
	}

	/**
	 * Returns the format id of the given file.
	 * @param f The input file.
	 * @return The format id of the file. (Values are the static int of the class)
	 */
	public	static	FileFormat	findFormat(File f){
		String					filePath	= null;
		FileFormat				formatId	= FileFormat.UNKNOWN;

		if (null != f && f.isFile() && f.canRead()){
			filePath = f.getAbsolutePath();

			for (int i = 0; FileFormat.UNKNOWN == formatId && i < VERIFYERS.length; i++) {
				Verifier verifier = VERIFYERS[i];

				if (verifier.verifyFile(filePath)){
					formatId = verifier.getFormatId();
				}
			}

		}else{
			System.out.println("File opening error");
		}

		return formatId;
	}

	/**
	 * Converts a file into the XML Standard BioMercator V3 format.
	 * If the file is already in the XML Standard BioMercator V3 format, nothing
	 * is done to the file.
	 * The converted file is returned.
	 * @param input The file to convert
	 * @param formatId The file format id
	 * @param projectName The project in which store the file
	 * @param mapName The map name if not null
	 * @return The file converted to XML Standard BioMercator V3 format.
	 */
	public	static	File	convert(File input, FileFormat formatId, String projectName, String mapName){
		FileConverter		converter	= null;
		String				fileName	= (null == mapName)?input.getName():mapName;
		String				outputName	=	DIR_SOFT_TMP +
											projectName + "/" +
											fileName + ".xml";

		createDirectoryIfNeeded(DIR_SOFT_TMP + projectName);
		switch (formatId){
			case XML_METAQTL:
				converter = new FromMetaQTLXml(input, outputName, mapName);
				break;
			case XML_MCQTL:
				converter = new FromMCQTLXml(input, outputName, mapName);
				break;
			case XML_BIOMERATOR_V3:
				File outputFile = copyFileIntoProject(input, projectName);
				outputName = outputFile.getAbsolutePath();
				break;
			case CSV_BIOMERATOR_V3:
				converter = new FromBioM_V3(input, outputName, mapName);
				break;
			case CSV_BIOMERATOR_V2:
				converter = new FromBioM_V2(input, outputName, mapName);
				break;
			case CSV_BIOMERATOR_QTL_V3:
				converter = new FromBioM_QTL_V3(input, outputName, mapName);
				break;
			case CSV_BIOMERATOR_QTL_V2:
				converter = new FromBioM_QTL_V2(input, outputName, mapName);
				break;
		}
		if (null != converter){
			converter.execute();
		}

		return new File(outputName);
	}

	/**
	 * Creates if necessary the directory.
	 * @param directoryName The project name
	 */
	public	static	void	createDirectoryIfNeeded(String directoryName){
		System.out.println(directoryName);
		File				dir = new File(directoryName);

		if (!dir.exists()){
			dir.mkdir();
		}
	}

	/**
	 * Returns if the given project name exists in the workspace.
	 * @return If the given project name exists in the workspace.
	 */
	public	static	boolean	existsProject(String projectName){
		File				dir = new File(DIR_SOFT_MAP + projectName);

		return dir.exists();
	}

	/**
	 * Returns if the given map name exists in the given project name.
	 * @return If the given map name exists in the given project name.
	 */
	public	static	boolean	existsMap(String projectName, String mapName){
		File				fileMap = new File(DIR_SOFT_MAP + projectName + "/" + mapName + ".xml");

		return fileMap.exists();
	}

	/**
	 * Returns if the given map name exists in the given project name.
	 * @return If the given map name exists in the given project name.
	 */
	public	static	boolean	existsFile(String projectName, String fileName){
		File				file = new File(DIR_SOFT_MAP + projectName + "/" + fileName + ".xml");

		return file.exists();
	}

	/**
	 * Returns a file from given URL
	 * @param url
	 * @param index An unique index for file differentiation
	 */
	public	static	File	fileFromURL(URL url, int index){
		File				file = new File(DIR_SOFT_TMP + "file_from_url_" + index + ".xml");
		Writer				writer = null;

		try {
			writer = new FileWriter(file);
			IOUtils.copy(url.openStream(), writer);
			writer.flush();
		} catch (IOException ex) {
			Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
		}

		return file;
	}

	public	static	void	loadFromDistant(File file, String projectName){
		createDirectoryIfNeeded(DIR_SOFT_MAP + "/" + projectName);
		load(file, FileFormat.XML_BIOMERATOR_V3, projectName, null);
	}

	/**
	 * This method loads a XML Standard BioMercator V3 file into the data model,
	 * in the specified project. It also copies the given file into the correct
	 * directory.
	 * @param f The file to load
	 * @param projectName The project in which load the file
	 * @return A filled project
	 */
	public	static	Project	loadDirect(File file, String projectName){
		List<MapGene>		maps	= null;
		Project				project	= null;

		maps = readMapsFromXmlFile(file);
		if (null != maps){
			if (null == (project = Controller.getProject(projectName))){
				project = new Project(projectName);
				Controller.addProject(project);
			}
			for (Iterator<MapGene> it = maps.iterator(); it.hasNext();) {
				MapGene	mapGene = it.next();

				project.add(mapGene);
				mapGene.setBelongingProjectName(projectName);
			}
		}

		return project;
	}

	/**
	 * Reads the given file a returns a maps list.
	 * @param file The XML file to read
	 * @return
	 */
	public	static	List<MapGene>	readMapsFromXmlFile(File file){
		List<MapGene>				maps		= null;
		String						fileName	= file.getAbsolutePath();
		XMLStandardHandler			handler		= null;

		try {
			XMLReader		saxReader = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
			//XMLReader		saxReader = XMLReaderFactory.createXMLReader("javax.xml.parsers.SAXParser");

			handler = new XMLStandardHandler();
			saxReader.setContentHandler(handler);
			FileInputStream stream = new FileInputStream(fileName);
			saxReader.parse(new InputSource(stream));
			//saxReader.parse(fileName);
			maps = handler.getMaps();
		} catch (Exception e) {
//			File fileTMP = new File(fileName);
//			fileTMP.delete();
			System.out.println("readMapsFromXmlFile : "+e.getMessage());
			e.printStackTrace();
		}

		return maps;
	}

	/**
	 * Loads a file into the data model by converting it before into the XML
	 * Standard BioMercator V3 format.
	 * (If the file is already in the XML Standard BioMercator V3 format,
	 * nothing is done to the file)
	 * The given mapName should be null if one wants to use the mapName given in
	 * the file; otherwise, the resulting map name will be sets to the not null
	 * value.
	 * @param input The file to convert
	 * @param formatId The file format id
	 * @param projectName The project in which load the file
	 * @param mapName The wanted map name (null if the mapName is in the file)
	 */
	public	static	void	load(File input, FileFormat formatId, String projectName, String mapName){
		File				stdFile = convert(input, formatId, projectName, mapName);
		Visitor				v		= null;
		Project				project	= null;

		System.out.println(input.getAbsolutePath());
		System.out.println(formatId);
		System.out.println(stdFile.getAbsolutePath());
		createDirectoryIfNeeded(DIR_SOFT_MAP + "/" + projectName);
		if (null != stdFile && stdFile.canRead()){
			project = loadDirect(stdFile, projectName);
			System.out.println("project name "+projectName);
			v = new VisitorSaveStandardXML(DIR_SOFT_MAP + projectName + "/");
			project.accept(v);
		}
		//input.deleteOnExit();
		//stdFile.deleteOnExit();
	}

	/**
	 * Copies the given directory into the given project. The files won't be
	 * copied; the files list will be return; if no file is present, an empty
	 * list will be returned.
	 * @param dir The directory to copy
	 * @param projectName The project name where to put the directory copy
	 * @return The list of files contained by the directory.
	 */
	public	static	List<File>	copyDirIntoProject(File dir, String projectName){
		List<File>				files		= new ArrayList<File>();
		File[]					filesArray	= null;

		try {
			createDirectoryIfNeeded(DIR_SOFT_MAP + "/" + projectName);
			createDirectoryIfNeeded(DIR_SOFT_MAP + "/" + projectName + "/" + dir.getName());
			if (dir.isDirectory()){
				filesArray = dir.listFiles();
				for (int iFile = 0; iFile < filesArray.length; iFile++) {
					File file = filesArray[iFile];

					files.add(file);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return files;
	}

	/**
	 * Copies the given file into the given project.
	 * @param file The file to copy
	 * @param projectName The project name where to put the file copy
	 * @return The copied file
	 */
	public	static	File	copyFileIntoProject(File file, String projectName){
		return copyFileIntoProject(file, projectName, null);
	}

	/**
	 * Copies the given file into the given project.
	 * @param file The file to copy
	 * @param projectName The project name where to put the file copy
	 * @param subDirName the sub directory where the file should be copied
	 * @return The copied file
	 */
	public	static	File	copyFileIntoProject(File file, String projectName, String subDirName){
		File				fileCopy	= null;
		FileChannel			in			= null;
		FileChannel			out			= null;
		String				dirsPath	= projectName + "/";

		dirsPath += (null != subDirName)?subDirName + "/":"";
		try {
			createDirectoryIfNeeded(DIR_SOFT_MAP + "/" + projectName);
			createDirectoryIfNeeded(DIR_SOFT_MAP + "/" + dirsPath);

			fileCopy = new File(DIR_SOFT_MAP + "/" + dirsPath + file.getName());
			in = new FileInputStream(file).getChannel();
			out = new FileOutputStream(fileCopy).getChannel();

			in.transferTo(0, in.size(), out);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (null != in) {
				try {
				in.close();
				} catch (IOException e) {}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {}
			}
		}

		return fileCopy;
	}

	/**
	 * Returns a human readable string of the given format file id.
	 * @param formatId The format file id to convert
	 * @return a String of the given format file id
	 */
	public static	String	getFormatTxt(FileFormat formatId){
		String				res = null;

		switch (formatId){
			case UNKNOWN:
				res = UNKNOWN_TXT;
				break;
			case XML_BIOMERATOR_V3:
				res = XML_BIOMERATOR_V3_TXT;
				break;
			case XML_METAQTL:
				res = XML_METAQTL_TXT;
				break;
			case XML_MCQTL:
				res = XML_MCQTL_TXT;
				break;
			case CSV_BIOMERATOR_V2:
				res = CSV_BIOMERATOR_V2_TXT;
				break;
			case CSV_BIOMERATOR_QTL_V2:
				res = CSV_BIOMERATOR_QTL_V2_TXT;
				break;
			case CSV_BIOMERATOR_V3:
				res = CSV_BIOMERATOR_V3_TXT;
				break;
			case CSV_BIOMERATOR_QTL_V3:
				res = CSV_BIOMERATOR_QTL_V3_TXT;
				break;
		}

		return res;
	}

	/**
	 * Writes the given map into the MetaQTL XML format file.
	 * @param mapGene The map to write
	 * @param fileName The fileName
	 * @return The created file
	 */
	public	static	File		writeToMetaQTLXML(MapGene mapGene, String fileName){
		File					output = null;
		VisitorSaveMetaQtlXML	v = new VisitorSaveMetaQtlXML(fileName);

		mapGene.accept(v);

		return output;
	}

	/**
	 * Writes the given map into the MetaQTL XML format file.
	 * @param mapGene The map to write
	 * @param fileName The fileName
	 * @return The created file
	 */
	public	static	File		writeToMetaQTLXML(	MapGene		mapGene,
													String		fileName,
													List<Qtl>	dubious){
		File					output = null;
		VisitorSaveMetaQtlXML	v = new VisitorSaveMetaQtlXML(fileName, dubious);

		mapGene.accept(v);

		return output;
	}

//	/**
//	 * Writes the given map into the Standard XML format file.
//	 * @param mapGene The map to write
//	 * @param projectName the project where to store the file
//	 * @return The created file
//	 */
//	public	static	File		writeMapIntoProject(MapGene mapGene, String projectName){
//		File					output	= null;
//		VisitorSaveStandardXML	v		= null;
//
//		createDirectoryIfNeeded(DIR_SOFT_MAP + projectName);
//		v = new VisitorSaveStandardXML(DIR_SOFT_MAP + projectName + "/" + mapGene.getName() + ".xml");
//		mapGene.accept(v);
//
//		return output;
//	}

	/**
	 * Writes the given map into the Standard XML format file.
	 * @param mapGene The map to write
	 * @param projectName the project where to store the file
	 * @return The created file
	 */
	public	static	File		writeMap(MapGene mapGene, String projectName){
		File					output	= null;
		VisitorSaveStandardXML	v		= null;

		createDirectoryIfNeeded(DIR_SOFT_MAP + projectName);
		v = new VisitorSaveStandardXML(DIR_SOFT_MAP + projectName + "/");
		mapGene.accept(v);

		return output;
	}

	/**
	 * Empties a directory or a file.
	 * @param path The file to empty
	 * @return if the function succeeded
	 */
	static public boolean	emptyDirectory(File path) {
		boolean				resultat	= true;
		File[]				files		= null;
		//we have to call the garbage collector manually because of a bug in Java / Windows version
		System.gc();
		
		if(path.exists()) {
			files = path.listFiles();
			if (files != null) {
				for(int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						resultat &= deleteDirectory(files[i]);
					}else {
						System.out.println("Deleting "+files[i].toString());
						resultat &= files[i].delete();
					}
				}
			}
		}
		System.out.println("Resultat : "+resultat);
		return resultat;
	}

	/**
	 * Deletes a directory or a file.
	 * @param path The file to suppress
	 * @return if the suppression has been done
	 */
	static public boolean	deleteDirectory(File path) {
        boolean				res		= true;
		File[]				files	= null;

		if (path.exists()) {
			files = path.listFiles();
			for(int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					res &= deleteDirectory(files[i]);
				} else {
					res &= files[i].delete();
				}
			}
		}
		res &= path.delete();

		return res;
	}

	static public void cleanEmptyAndTmpDirectories() {
		
		//clean empty projects
		File mapdir = new File(DIR_SOFT_MAP);
		File[] projects = mapdir.listFiles();
		for (int i=0;i < projects.length;i++) {
			if (((File)projects[i]).isDirectory() && ((File)projects[i]).list().length == 0) {
				((File)projects[i]).delete();
			}
		}
		
		//clean Tmp dir
		File tmpdir = new File(DIR_SOFT_TMP);
		File[] tmps = tmpdir.listFiles();
		for (int i=0;i < tmps.length;i++) {
			emptyDirectory(tmps[i]);
		}
	}
	
	/**
	 * Deletes a map file.
	 * @param projectName The project name
	 * @param mapName The map name
	 * @return if the suppression has been done
	 */
	static public boolean	deleteMap(String projectName, String mapName) {
        boolean				res			= true;
		File				projectDir	= new File(DIR_SOFT_MAP + projectName);
		File				mapFile		= null;

		if (projectDir.exists()) {
			mapFile = new File(DIR_SOFT_MAP + projectName + "/" + mapName + ".xml");
			if (mapFile.exists()){
				res = mapFile.delete();
			}
		}

		return res;
	}

	public	static	void	analysisLoggingStart(String projectName){
		_logger.loggingStart(projectName);
	}

	public	static	void	analysisLoggingNew(String analysisName){
		_logger.loggingNewAnalysis(analysisName);
	}

	public	static	void	logAttributes(String loggingPart, Parameters params){
		_logger.logAttributes(loggingPart, params);
	}

	public	static	void	analysisLoggingEnd(){
		_logger.loggingEnd();
	}

	private	static	final	AnalysisLogger	_logger = new AnalysisLogger();

	public	static	enum FileFormat {	UNKNOWN,
										XML_BIOMERATOR_V3,
										XML_METAQTL,
										XML_MCQTL,
										CSV_BIOMERATOR_V2,
										CSV_BIOMERATOR_QTL_V2,
										CSV_BIOMERATOR_V3,
										CSV_BIOMERATOR_QTL_V3
										};

/*	public	static	final	int			UNKNOWN						= 0;
	public	static	final	int			XML_BIOMERATOR_V3			= 1;
	public	static	final	int			XML_METAQTL					= 2;
	public	static	final	int			XML_MCQTL					= 3;
	public	static	final	int			CSV_BIOMERATOR_V2			= 4;
	public	static	final	int			CSV_BIOMERATOR_QTL_V2		= 5;
	public	static	final	int			CSV_BIOMERATOR_V3			= 6;
	public	static	final	int			CSV_BIOMERATOR_QTL_V3		= 7;
*/
	private static  final   String      BIOMERCATOR_DIR             = System.getProperty("user.home") + "/.biomercator/";
	private	static	final	String		UNKNOWN_TXT					= "Unknown";
	private	static	final	String		XML_BIOMERATOR_V3_TXT		= "XML standard BioMercator V3";
	private	static	final	String		XML_METAQTL_TXT				= "XML MetaQTL";
	private	static	final	String		XML_MCQTL_TXT				= "XML MCQTL";
	private	static	final	String		CSV_BIOMERATOR_V2_TXT		= "BioMercator V2 - Map file";
	private	static	final	String		CSV_BIOMERATOR_QTL_V2_TXT	= "BioMercator V2 - Qtls file";
	private	static	final	String		CSV_BIOMERATOR_V3_TXT		= "BioMercator V3 - Map file";
	private	static	final	String		CSV_BIOMERATOR_QTL_V3_TXT	= "BioMercator V3 - Qtls file";

	public	static	final	String		DIR_SOFT					= BIOMERCATOR_DIR + "SoftwareFiles/";

	public	static	final	String		DIR_SOFT_CONFIG				= DIR_SOFT + "config/";
	public	static	final	String		CONFIG_FILE_LOCAL			= DIR_SOFT_CONFIG + "biomercator.config";
	public	static	final	String		CONFIG_FILE_PAR				= DIR_SOFT_CONFIG + "default.par";
	public	static	final	String		DIR_SOFT_MAP				= DIR_SOFT + "MapFiles/";
	public	static	final	String		DIR_SOFT_TMP				= DIR_SOFT + "Tmp/";
	public	static	final	String		DIR_SOFT_URGI				= DIR_SOFT + "urgiTalend/";
	public	static	final	String		DIR_SOFT_URGI_JOB			= DIR_SOFT_URGI + "BioMercatorMetaQTLToGnpMapMetaQTL/";
	public	static	final	String		FILE_SOFT_URGI_JOB_SH		= DIR_SOFT_URGI_JOB + "BioMercatorMetaQTLToGnpMapMetaQTL_run.sh";
	public	static	final	String		DIR_RES_ANALYSES			= BIOMERCATOR_DIR + "analyses/";
	public	static	final	String		GENE_ONTOLOGY_OBO_FILE		= DIR_SOFT + "gene_ontology_ext.obo";

	private	static	final	String		DIR_RESOURCES				= "";
	private	static	final	String		DIR_IMAGES					= DIR_RESOURCES + "images/";
	public	static	final	String		RESOURCES_IMAGE_ANCHOR		= DIR_IMAGES + "anchor.png";
	public	static	final	String		RESOURCES_IMAGE_MAP			= DIR_IMAGES + "icon_map.gif";
	public	static	final	String		RESOURCES_IMAGE_MAP_W_META	= DIR_IMAGES + "icon_map_with_meta.gif";
	public	static	final	String		RESOURCES_IMAGE_CHR			= DIR_IMAGES + "icon_chr.gif";
	public	static	final	String		RESOURCES_IMAGE_CHR_W_META	= DIR_IMAGES + "icon_chr_with_meta.gif";
	public	static	final	String		RESOURCES_IMAGE_LKG			= DIR_IMAGES + "icon_lkg.gif";
	public	static	final	String		RESOURCES_IMAGE_LKG_W_META	= DIR_IMAGES + "icon_lkg_with_meta.gif";
	public	static	final	String		RESOURCES_IMAGE_JPG			= DIR_IMAGES + "icon_jpg.gif";
	public	static	final	String		RESOURCES_IMAGE_TXT			= DIR_IMAGES + "icon_txt.gif";
	public	static	final	String		RESOURCES_IMAGE_LOGO_UMRGV	= DIR_IMAGES + "logoUMRGV.jpg";
	public  static  final   String      RESOURCES_IMAGE_HELP        = DIR_IMAGES + "help.png";

	public	static	final	String		RESOURCES_CONFIG			= DIR_RESOURCES + "config/";
	public	static	final	String		RESOURCES_CONFIG_FILE_LOCAL	= RESOURCES_CONFIG + "biomercator.config";
	public	static	final	String		RESOURCES_CONFIG_FILE_PAR	= RESOURCES_CONFIG + "default.par";
	public	static	final	String		RESOURCES_CONFIG_BMV3_XSD	= RESOURCES_CONFIG + "BioMercator_Standard.xsd";
	public	static	final	String		RESOURCES_GENE_ONTOLOGY_OBO_FILE	= DIR_RESOURCES + "gene_ontology_ext.obo";
	public  static  final   String      RESOURCES_PROJECT           = DIR_RESOURCES + "project.properties";

	private	static	final	Verifier[]	VERIFYERS			= {	new XML_BioM_V3_Verifier(),
																new XML_MetaQTL_Verifier(),
																new XML_MCQTL_Verifier(),
																new CSV_BioM_V3_Verifier(),
																new CSV_BioM_V3_QTL_Verifier(),
																new CSV_BioM_V2_Verifier(),
																new CSV_BioM_V2_QTL_Verifier()
																};

}
