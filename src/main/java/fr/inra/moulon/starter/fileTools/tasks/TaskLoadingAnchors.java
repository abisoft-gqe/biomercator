/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 04-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.tasks;

import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.gui.wizards.WizardGenomeVersion;
import fr.inra.moulon.starter.utils.PhysicDataTypes;
import java.io.File;
import java.util.List;


public class TaskLoadingAnchors extends TaskLoadingFile{
	public	TaskLoadingAnchors(	WizardGenomeVersion wizard,
								File				file,
								List<Field>			correspondance,
								String				separator){
		super(wizard, file, correspondance, separator, PhysicDataTypes.ANCHORS);
	}

	@Override
	public String getStatementInsertUnique() {
		return DatabaseManager.getInsertStatementAnchorsList(_file.getName());
	}

	@Override
	public String getStatementInsertAll() {
		return DatabaseManager.getInsertStatementAnchors();
	}
}
