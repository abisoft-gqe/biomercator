package fr.inra.moulon.starter.fileTools.tasks;

import java.io.File;
import java.util.List;

import fr.inra.moulon.starter.database.DatabaseManager;
import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.gui.wizards.WizardGenomeVersion;
import fr.inra.moulon.starter.utils.PhysicDataTypes;

public class TaskLoadingDBLinks extends TaskLoadingFile {

	public TaskLoadingDBLinks(
			WizardGenomeVersion wizard,
			File file,
			List<Field> correspondance,
			String separator) {
		super(wizard, file, correspondance, separator, PhysicDataTypes.DBLINKS);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getStatementInsertAll() {
		return DatabaseManager.getInsertStatementDBLinks();
	}

	@Override
	public String getStatementInsertUnique() {
		return DatabaseManager.getInsertStatementDBLinksList(_file.getName());
	}

}
