/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 15-Jun-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.logging;

import fr.inra.moulon.starter.fileTools.FileManager;
import fr.inra.moulon.starter.utils.StringTools;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public	class AnalysisLogger {
	public	void	loggingStart(String projectName){
		String		prolog	= "\n\n";

		_output = new File(FileManager.DIR_RES_ANALYSES + projectName + "_analyses" + ".txt");
		try {
			if (!_output.exists()){
				prolog	= "";
			}
			_buf = new BufferedWriter(new FileWriter(_output, true));
			_buf.write(prolog);
		} catch (IOException ex) {
			Logger.getLogger(AnalysisLogger.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public	void	loggingNewAnalysis(String analysisName){
		String		line	= null;
		int			maxLen	= 0;

		try{
			if (null != _buf && null != analysisName){
				maxLen = analysisName.length();
				line = StringTools.repeatString("#", maxLen + 2*EXT.length() + 2*SPACE.length());
				_buf.write(	line + "\n"
							+ EXT + SPACE + analysisName + StringTools.repeatString(" ", maxLen-analysisName.length()) + SPACE + EXT + "\n"
							+ line + "\n");
			}
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}

	public	void		logAttributes(String loggingPart, Parameters params){
		String			date		= DATEFORMAT.format(new Date());
		String			field		= null;
		List<String>	values		= null;

		if (null != _buf){
			try{
				_buf.write(EXT + SPACE + loggingPart + SPACE + EXT + "\n");
				_buf.write("Time" + SEPARATOR + "\n");
				_buf.write(INDENT + date + "\n");

				for (Iterator<String> it = params.iterator(); it.hasNext();) {
					field = it.next();
					values = params.getValues(field);
					_buf.write(field + SEPARATOR + "\n");
					for (Iterator<String> itValues = values.iterator(); itValues.hasNext();) {
						String value = itValues.next();
						_buf.write(INDENT + value + "\n");
					}
				}
			} catch (IOException e){
				System.out.println(e.getMessage());
			}
		}
	}

	public	void	loggingEnd(){
		if (null != _buf){
			try{
				_buf.close();
			} catch (IOException e){
				System.out.println(e.getMessage());
			}
		}
	}

	private	static	String			SEPARATOR	= ":";
	private	static	String			INDENT		= "\t";
	private	static	String			EXT			= "##";
	private	static	String			SPACE		= " ";

	private			File			_output		= null;
	private			BufferedWriter	_buf		= null;
	private	static	DateFormat		DATEFORMAT	= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
}