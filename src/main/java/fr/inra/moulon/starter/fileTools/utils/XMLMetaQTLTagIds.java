/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.utils;

/**
 *
 * @author sosnowski
 */
public class XMLMetaQTLTagIds {
	public static final String		GENOME_MAP				= "genome-map";
	public static final String		LINKAGE_GROUP			= "linkage-group";
	public static final String		LOCUS					= "locus";
	public static final String		NAME					= "name";
	public static final String		VALUE					= "value";
	public static final String		TYPE					= "type";
	public static final String		POSITION				= "position";
	public static final String		PROPERTY				= "property";

	public static final String		PROP_CROSS_SIZE			= "cross.size";
	public static final String		PROP_CROSS_TYPE			= "cross.type";
	public static final String		PROP_MAPPING_UNIT		= "mapping.unit";
	public static final String		PROP_MAPPING_FUN		= "mapping.function";
	public static final String		PROP_QTL_R2				= "qtl.rsquare";
	public static final String		PROP_QTL_TRAIT			= "qtl.trait.name";
	public static final String		PROP_QTL_CI_TO			= "qtl.ci.to";
	public static final String		PROP_QTL_CI_FROM		= "qtl.ci.from";
	public static final String		PROP_QTL_LOD			= "qtl.ci.lod.decrease";
	public static final String		PROP_QTL_CROSS_SIZE		= "qtl.cross.size";
	public static final String		PROP_QTL_CROSS_NAME		= "qtl.cross.name";
	public static final String		PROP_QTL_CROSS_TYPE		= "qtl.cross.type";
	public static final String		PROP_QTL_EFFECT_ADD		= "qtl.effect.additive";
}
