/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.verifyers;

import fr.inra.moulon.starter.fileTools.utils.FileTools;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public	abstract	class CSV_BioM_Verifier implements Verifier{
	protected	abstract	String[]	getMandatoryFields();

	@Override
	public 	Boolean			verifyFile(String filePath) {
		Boolean				res				= Boolean.TRUE;
		BufferedReader		buf				= null;
		String				line			= null;
		String[]			map_attribute	= new String[2];
		String[]			locus_attribute	= new String[3];
		Map<String, String>	map_attributes	= new HashMap<String, String>();

		try{
			buf = new BufferedReader(new FileReader(new File(filePath)));
			while (res && null != (line = buf.readLine())) {
				line = line.trim();
				switch (FileTools.split(line, " ", locus_attribute)){
					case 0:
					case 1:
						break;
					case 4:
					case 3:
					case 2:
						break;
					default:
						res = Boolean.FALSE;
				}
				switch (FileTools.split(line, "=", map_attribute)){
					case 0:
					case 1:
						break;
					case 2:
						map_attributes.put(map_attribute[0], map_attribute[1]);
						break;
					default:
						res = Boolean.FALSE;
						break;
				}
			}
			buf.close();
		}catch(IOException e){
			e.printStackTrace();
		}

		return res && verify(map_attributes, getMandatoryFields());
	}

	protected	Boolean	verify(	Map<String, String>	map_attributes,
								String[]			mandatoryFields){
		Boolean			res = Boolean.TRUE;

		for (int i = 0; res && i < mandatoryFields.length; ++i){
			res &= map_attributes.containsKey(mandatoryFields[i]);
		}

		return res;
	}
}
