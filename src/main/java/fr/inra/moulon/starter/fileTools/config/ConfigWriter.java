/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 31-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.config;

import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.utils.Session;
import java.util.Locale;
import java.util.ResourceBundle;


public class ConfigWriter {
	public	static	void	writeConfigFile(String path){
		SAXWriter			writer;
		ResourceBundle		bundle	= (ResourceBundle)Session.instance().getValue(Session.RESSOURCE_BUNDLE);
		Locale				locale	= null;
		
		System.out.println("State of bundle : "+bundle);
		writer = new SAXWriter(path);
		if (null != bundle){
			locale = bundle.getLocale();
		}else{
			System.out.println("Using default locale");
			locale = Locale.getDefault();
		}
		
		System.out.println("Locale : "+locale.getCountry()+"-"+locale.getLanguage());
		
		writer.startDocument();
		writer.startElement(XMLConfigTagIds.CONFIG);
		writer.startElement(XMLConfigTagIds.LOCALE);
		writer.addElement(XMLConfigTagIds.LOCALE_LANGUAGE, locale.getLanguage());
		writer.addElement(XMLConfigTagIds.LOCALE_COUNTRY, locale.getCountry());
		writer.endElement(XMLConfigTagIds.LOCALE);
		writer.endElement(XMLConfigTagIds.CONFIG);
		writer.endDocument();
	}
}
