/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.loader;

import fr.inra.moulon.starter.datamodel.entities.Chromosome;
import fr.inra.moulon.starter.datamodel.entities.LinkageGroup;
import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.datamodel.entities.Marker;
import fr.inra.moulon.starter.datamodel.entities.MetaAnalysis;
import fr.inra.moulon.starter.datamodel.entities.MetaModel;
import fr.inra.moulon.starter.datamodel.entities.MetaQtl;
import fr.inra.moulon.starter.datamodel.entities.Qtl;
import fr.inra.moulon.starter.fileTools.utils.Summary;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.LocatorImpl;


public class XMLStandardHandler implements ContentHandler {
	private	static final	String	TMP = "tmp";

	public XMLStandardHandler(){
		super();
        _locator = new LocatorImpl();
		_maps = new ArrayList<MapGene>();
	}

	/**
	 * Returns the maps read from the file.
	 * @return The list of the maps read from the file.
	 */
	public	List<MapGene>	getMaps(){
		return _maps;
	}

	/**
	 * Definition du locator qui permet a tout moment pendant l'analyse, de localiser
	 * le traitement dans le flux. Le locator par defaut indique, par exemple, le numero
	 * de ligne et le numero de caractere sur la ligne.
	 * @author smeric
	 * @param value le locator a utiliser.
	 * @see org.xml.sax.ContentHandler#setDocumentLocator(org.xml.sax.Locator)
	 */
	@Override
	public void setDocumentLocator(Locator value) {
		_locator =  value;
	}

	/**
	 * Evenement envoye au demarrage du parse du flux xml.
	 * @throws SAXException en cas de probleme quelquonque ne permettant pas de
	 * se lancer dans l'analyse du document.
	 * @see org.xml.sax.ContentHandler#startDocument()
	 */
	@Override
	public void startDocument() throws SAXException {
		Summary.instance().newSummary();
	}

	/**
	 * Evenement envoye a la fin de l'analyse du flux xml.
	 * @throws SAXException en cas de probleme quelquonque ne permettant pas de
	 * considerer l'analyse du document comme etant complete.
	 * @see org.xml.sax.ContentHandler#endDocument()
	 */
	@Override
	public void endDocument() throws SAXException {
//		Workspace.instance().add(_project);
	}

	/**
	 * Debut de traitement dans un espace de nommage.
	 * @param prefixe utilise pour cet espace de nommage dans cette partie de l'arborescence.
	 * @param URI de l'espace de nommage.
	 * @see org.xml.sax.ContentHandler#startPrefixMapping(java.lang.String, java.lang.String)
	 */
	@Override
	public void startPrefixMapping(String prefix, String URI) throws SAXException {
		//System.out.println("Traitement de l'espace de nommage : " + URI + ", prefixe choisi : " + prefix);
	}
	
	/**
	 * Fin de traitement de l'espace de nommage.
	 * @param prefixe le prefixe choisi a l'ouverture du traitement de l'espace nommage.
	 * @see org.xml.sax.ContentHandler#endPrefixMapping(java.lang.String)
	 */
	@Override
	public void endPrefixMapping(String prefix) throws SAXException {
		//System.out.println("Fin de traitement de l'espace de nommage : " + prefix);
	}

	/**
	 * Evenement recu a chaque fois que l'analyseur rencontre une balise xml ouvrante.
	 * @param nameSpaceURI l'url de l'espace de nommage.
	 * @param localName le nom local de la balise.
	 * @param rawName nom de la balise en version 1.0 <code>nameSpaceURI + ":" + localName</code>
	 * @throws SAXException si la balise ne correspond pas a ce qui est attendu,
	 * comme par exemple non respect d'une dtd.
	 * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	
	
	
	
	public void	startElement(String nameSpaceURI, String localName, String rawName, Attributes attributs) throws SAXException {
		String NameLkgGroup = null;
		if (localName.equalsIgnoreCase(XMLStandardTagIds.MAP)){
			
			/// Eliminer les espaces et tabulations du nom de la carte 
			String mapName = null;
			mapName = attributs.getValue(XMLStandardTagIds.NAME);
			mapName = mapName.trim();
			
			// Replace the "%" by "_"
			String substr = "_";
			String regexp = "%";
			mapName = mapName.replaceAll(regexp, substr);
			_map = new MapGene(mapName);
			Summary.instance().newMap(_map.getName());
		
	   	
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.CHROMOSOME)){
				// Eliminer les espaces pour chromosome
				String chromoName = null;
				chromoName = attributs.getValue(XMLStandardTagIds.NAME);
				chromoName = chromoName.trim();
				String substr = "_";
				String regexp = "%";
				chromoName = chromoName.replaceAll(regexp, substr);
				_chromosome = new Chromosome(chromoName);
				Summary.instance().newChromosome(_chromosome.getName());
		
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.USED_MAP)){
				_map.addMapUsed(attributs.getValue(XMLStandardTagIds.NAME));
		
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.LINKAGE_GROUP)){
				//... pour linkageGroup
				String mapName = null;
			    mapName = attributs.getValue(XMLStandardTagIds.NAME);
				mapName = mapName.trim();
				String substr = "_";
				String regexp = "%";
				mapName = mapName.replaceAll(regexp, substr); 
				_linkageGroup = new LinkageGroup(mapName);
				_linkageGroup.setMapName(_map.getName());
				_linkageGroup.setBelongingMap(_map);
				_linkageGroup.setChrName(_chromosome.getName());
				Summary.instance().newLinkageGroup(_linkageGroup.getName());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MARKER)){
			//... pour marker
			String marker = null;
			marker = attributs.getValue(XMLStandardTagIds.NAME);
			marker = marker.trim();
			_marker = new Marker(marker);
			Summary.instance().addMarkerCount();
		
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL)){
			//... pour qtl
			String qtl = null;
			qtl = attributs.getValue(XMLStandardTagIds.NAME);
			qtl =qtl.trim();
			_qtl = new Qtl(qtl, TMP);
			Summary.instance().addQTLCount();
		
        
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.META_A)){
			//... pour metaAnalysis
			String metaAnalysis = null;
			metaAnalysis = attributs.getValue(XMLStandardTagIds.NAME);
			metaAnalysis = metaAnalysis.trim();
			_metaAnalysis = new MetaAnalysis(metaAnalysis, TMP);
			
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.META_MODEL)){
			//... pour metamodel
			String metaModel = null;
			metaModel = attributs.getValue(XMLStandardTagIds.NAME);
			metaModel = metaModel.trim();
			_metaModel = new MetaModel(metaModel);
		
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL)){
			//... pour metaqtl
			String metaQtl = null;
			metaQtl = attributs.getValue(XMLStandardTagIds.NAME);
			metaQtl = metaQtl.trim();
			_metaQtl = new MetaQtl(metaQtl, TMP);
		
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_MEMBERSHIP)){
			//... pour mtqtl_membership
			String mqtlMembership = null;
			mqtlMembership = attributs.getValue(XMLStandardTagIds.NAME);
			mqtlMembership = mqtlMembership.trim();
			String belonging = null;
			belonging = attributs.getValue(XMLStandardTagIds.BELONGING);
			belonging = belonging.trim();
		    _metaQtl.addMembership(	mqtlMembership, strToDouble(belonging));
		
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.META_A_USED_QTL_ID)){
			try{
				Qtl qtl = (Qtl)_linkageGroup.get(attributs.getValue(XMLStandardTagIds.NAME));
				if (null != qtl){
					_metaAnalysis.addUsedQtls(qtl);
				}
			}catch(Exception e){}
		}
		resetStr();
	}

	/**
	 * Evenement recu a chaque fermeture de balise.
	 * @see org.xml.sax.ContentHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public	void	endElement(String nameSpaceURI, String localName, String rawName) throws SAXException {
		int			intValue;

		if (localName.equalsIgnoreCase(XMLStandardTagIds.MAP)){
			_map.sort();
			_maps.add(_map);
			_map = null;
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.GENUS)){
			_map.setGenus(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.SPECIES)){
			_map.setSpecies(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.PARENT_NAME)){

		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.CROSSTYPE)){
			_map.setCrossType(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.POPSIZE)){
			Integer popSize = null;
			try{
				popSize = Integer.valueOf(getStr());
			}catch (NumberFormatException e){
				popSize = 0;
			}
			_map.setCrossSize(popSize);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MAPPING_CROSSTYPE)){
			_map.setMappingCrossType(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MAPPING_FUNCTION)){
			_map.setMappingFunction(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MAP_UNIT)){
			_map.setMappingUnit(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MAP_EXPANSION)){
			try{
				intValue = Integer.valueOf(getStr());
			}catch (NumberFormatException e){
				intValue = MapGene.DEFAULT_EXPANSION;
			}
			_map.setMapExpansion(intValue);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MAP_QUALITY)){
			try{
				intValue = Integer.valueOf(getStr());
			}catch (NumberFormatException e){
				intValue = MapGene.LOCUS_LOCATION_ABSOLUTE;
			}
			_map.setMapQuality(intValue);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.CHROMOSOME)){
			_map.add(_chromosome);
			_chromosome = null;
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.LINKAGE_GROUP)){
			_linkageGroup.sort();
			_chromosome.add(_linkageGroup);
			_linkageGroup = null;
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MARKER)){
			_linkageGroup.add(_marker);
			_marker = null;
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MARKER_ALIASE)){
			
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MARKER_LOCATION)){
			_marker.setPosition(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MARKER_TYPE)){
			_marker.setType(getStr());
		}
		////////////////////////////// QTL //////////////////////////////
		else if(localName.equalsIgnoreCase(XMLStandardTagIds.QTL)){
			_linkageGroup.add(_qtl);
			_qtl = null;
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_TRAIT)){
			_qtl.setTrait(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_ORIGIN_MAP)){
			_qtl.setOriginMap(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_TRAIT_OID)){
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_EXP_PLACE)){
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_EXP_DATE)){
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_LOD)){
			_qtl.setLodscore(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_R2)){
			double	r2 = Double.valueOf(getStr());
			if (r2 >= 1){
				r2 /= 100;
			}
			_qtl.setR2(r2);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_LOCATION)){
			_qtl.setPosition(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_CI_FROM)){
			_qtl.setPositionStart(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_CI_TO)){
			_qtl.setPositionEnd(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_CROSS_NAME)){
			_qtl.setCrossName(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_CROSS_SIZE)){
			Integer crossSize = null;
			try {
				crossSize = Integer.valueOf(getStr());
			}catch (NumberFormatException e){
				crossSize = 0;
			}
			if (0 == crossSize || null == crossSize){
				crossSize = _map.getCrossSize();
			}
			_qtl.setCrossSize(crossSize);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_EFFECT_UNIT)){
			Double effectAddictive = null;
			try {
				effectAddictive = Double.valueOf(getStr());
			}catch (NumberFormatException e){
				effectAddictive = 0.0;
			}
			_qtl.setEffectAddictive(effectAddictive);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_CROSS_TYPE)){
			_qtl.setCrossType(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.QTL_EFFECT)){
		}
		////////////////////////////// MANALYSIS //////////////////////////////
		else if (localName.equalsIgnoreCase(XMLStandardTagIds.META_A)){
			_linkageGroup.addMetaAnalysis(_metaAnalysis);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.META_A_METHOD)){
			_metaAnalysis.setMethod(getStr());
//		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.META_A_USED_QTL_ID)){
//			try{
//				_metaAnalysis.addUsedQtls((Qtl)_linkageGroup.get(getStr()));
//			}catch(Exception e){}
		}
		//////////////////////////// MODEL - MQTL //////////////////////////////
		else if (localName.equalsIgnoreCase(XMLStandardTagIds.META_MODEL)){
			_metaAnalysis.add(_metaModel);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL)){
			_metaModel.add(_metaQtl);
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_TRAIT)){
			_metaQtl.setTrait(getStr());
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_TRAIT_OID)){
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_EXP_PLACE)){
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_EXP_DATE)){
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_LOD)){
			_metaQtl.setLodscore(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_R2)){
			_metaQtl.setR2(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_LOCATION)){
			_metaQtl.setPosition(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_CI_FROM)){
			_metaQtl.setPositionStart(Double.valueOf(getStr()));
		}else if (localName.equalsIgnoreCase(XMLStandardTagIds.MQTL_CI_TO)){
			_metaQtl.setPositionEnd(Double.valueOf(getStr()));
		}
	}

	/**
	 * Evenement recu a chaque fois que l'analyseur rencontre des caracteres (entre
	 * deux balises).
	 * @param ch les caracteres proprement dits.
	 * @param start le rang du premier caractere a traiter effectivement.
	 * @param length le nombre de caracteres a traiter effectivement.
	 * @see org.xml.sax.ContentHandler#characters(char[], int, int)
	 */
	@Override
	public void	characters(char[] ch, int start, int length) throws SAXException {
		String	strTmp = new String(ch, start, length);
		//System.out.println("#PCDATA : " + new String(ch, start, length));
		if (null !=_str){
			_str += strTmp;
		}else{
			_str = strTmp;
		}
		_str = _str.replaceAll("\n", "");
		if (_str.length() == 0){
			_str = null;
		}
	}

	private void	resetStr(){
		_str = null;
	}

	private String	getStr(){
		String	res = _str;

		_str = null;

		return res;
	}
	/**
	 * Recu chaque fois que des caracteres d'espacement peuvent etre ignores au sens de
	 * XML. C'est a dire que cet evenement est envoye pour plusieurs espaces se succedant,
	 * les tabulations, et les retours chariot se succedants ainsi que toute combinaison de ces
	 * trois types d'occurrence.
	 * @param ch les caracteres proprement dits.
	 * @param start le rang du premier caractere a traiter effectivement.
	 * @param end le rang du dernier caractere a traiter effectivement
	 * @see org.xml.sax.ContentHandler#ignorableWhitespace(char[], int, int)
	 */
	@Override
	public void ignorableWhitespace(char[] ch, int start, int end) throws SAXException {}

	/**
	 * Rencontre une instruction de fonctionnement.
	 * @param target la cible de l'instruction de fonctionnement.
	 * @param data les valeurs associees a cette cible. En general, elle se presente sous la forme 
	 * d'une serie de paires nom/valeur.
	 * @see org.xml.sax.ContentHandler#processingInstruction(java.lang.String, java.lang.String)
	 */
	@Override
	public void processingInstruction(String target, String data) throws SAXException {}

	/**
	 * Recu a chaque fois qu'une balise est evitee dans le traitement a cause d'un
	 * probleme non bloque par le parser.
	 * @see org.xml.sax.ContentHandler#skippedEntity(java.lang.String)
	 */
	@Override
	public void skippedEntity(String arg0) throws SAXException {}

	private Double	strToDouble(String str){
		Double		res = new Double(0);

		if (null != str){
			try{
				res = Double.valueOf(str);
			}catch (NumberFormatException e){}
		}

		return res;
	}

	private Locator			_locator		= null;
	private List<MapGene>	_maps			= null;
	private MapGene			_map			= null;
	private Chromosome		_chromosome		= null;
	private LinkageGroup	_linkageGroup	= null;
	private Marker			_marker			= null;
	private Qtl				_qtl			= null;
	private MetaAnalysis	_metaAnalysis	= null;
	private MetaModel		_metaModel		= null;
	private MetaQtl			_metaQtl		= null;
	private String			_str			= null;
}
