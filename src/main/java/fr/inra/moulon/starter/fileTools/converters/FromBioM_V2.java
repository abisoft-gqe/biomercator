/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.converters;

import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.fileTools.utils.FileTools;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class FromBioM_V2 extends FileConverter{
	public FromBioM_V2(File file, String outputName, String mapName){
		super(file, outputName);
		_writer	= new SAXWriter(super.getNewFileName());
		_file	= file;
	}

	@Override
	public void				execute() {
		Boolean				res				= Boolean.TRUE;
		BufferedReader		buf				= null;
		String				line			= null;
		String				mapName			= "TMP";
		String[]			locus_attribute	= new String[4]; // results of the split method
		double				position_next	= 0;
		double				position		= 0;
		boolean				first_chr		= true;

		try{
			buf = new BufferedReader(new FileReader(_file));
			line = buf.readLine();
			//line = line.trim();	// Read the mapname
			if (null != line && line.startsWith("mapname=")){
				mapName = line.substring(line.indexOf("=")+1);
			    mapName = mapName.trim(); 
				
		
			}
			_writer.startDocument();
			_writer.startElement(XMLStandardTagIds.MAPS);
			_writer.startElement(XMLStandardTagIds.MAP,	XMLStandardTagIds.NAME, mapName);
			_writer.addElement(XMLStandardTagIds.GENUS, MapGene.DEFAULT_GENUS);
			_writer.addElement(XMLStandardTagIds.SPECIES, MapGene.DEFAULT_SPECIES);

			while (res && null != (line = buf.readLine())) {
				
				line = line.trim();
				/*line = line.replaceAll("[\t]+", " ");
				line = line.replaceAll("[ ]+", " ");
				line = line.replaceAll(" $", "");
				line = line.replaceAll("^ ", "");*/
				switch (FileTools.split(line, " ", locus_attribute)){
					case 0:
					case 1:
						break;
					case 2:
						if (_CHROMOSOME.equals(locus_attribute[0])){
							if (!first_chr){
								_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
								_writer.endElement(XMLStandardTagIds.CHROMOSOME);
							}
							first_chr = false;
							_writer.startElement(	XMLStandardTagIds.CHROMOSOME,
													XMLStandardTagIds.NAME,
													locus_attribute[1]);
							_writer.startElement(	XMLStandardTagIds.LINKAGE_GROUP,
													XMLStandardTagIds.NAME,
													locus_attribute[1]);
						}
						position = 0;
						break;
					case 3:
					case 4:
						try{
							position_next = Double.valueOf(locus_attribute[2]);
						} catch (NumberFormatException ex){	}
						_writer.startElement(	XMLStandardTagIds.MARKER,
												XMLStandardTagIds.NAME,
												locus_attribute[1]);
						_writer.addElement(XMLStandardTagIds.MARKER_LOCATION, String.valueOf(position));
						_writer.addElement(XMLStandardTagIds.MARKER_ALIASE, "");
						_writer.addElement(XMLStandardTagIds.MARKER_TYPE, "");
						_writer.endElement(XMLStandardTagIds.MARKER);
						position += position_next;
						break;
					default:
						res = Boolean.FALSE;
				}
			}
			if (!first_chr){
				_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
				_writer.endElement(XMLStandardTagIds.CHROMOSOME);
			}
			_writer.endElement(XMLStandardTagIds.MAP);
			_writer.endElement(XMLStandardTagIds.MAPS);
			_writer.endDocument();
			buf.close();
		}catch(IOException e){
			System.out.println();
		}
	}

	private	static	String	convertField(String field){
		String				field_std = null;

		if (null == (field_std = _mapTags.get(field))){
			field_std = XMLStandardTagIds.UNKNOWN;
		}

		return field_std;
	}

	//
	private	static	String	_POP_TYPE			= "poptype";
	private	static	String	_MAP_NAME			= "mapname";
	private	static	String	_CHROMOSOME			= "chromosome";

	//
	private SAXWriter					_writer		= null;
	private	File						_file		= null;
	private	static Map<String, String>	_mapTags	= new HashMap(){{
				put(_POP_TYPE,	XMLStandardTagIds.CROSSTYPE);
			}};
}
