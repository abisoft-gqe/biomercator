/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 21-Mar-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.verifyers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class	XMLTree{
	public	XMLTree(String tag){
		_sons			= new HashMap<String, XMLTree>();
		_allowedTags	= new HashSet<String>();
		_properties		= new ArrayList<String>();
		_checked		= Boolean.FALSE;
		_mandatory		= Boolean.FALSE;
		_tag			= tag;
	}

	public	void	addSon(XMLTree son){
		_sons.put(son._tag, son);
		son._dad = this;
	}

	public	void	addSon(String[] sonsName){
		XMLTree		son = null;

		if (null != sonsName){
			for (int i = 0; i < sonsName.length; i++) {
				son = new XMLTree(sonsName[i]);
				_sons.put(sonsName[i], son);
				son._dad = this;
			}
		}
	}

	public	XMLTree	moveUp(){
		for (Iterator<String> it = _sons.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			_sons.get(key)._checked = Boolean.FALSE;
		}

		return _dad;
	}

	public	Boolean	canMoveDown(String tag){
		return _sons.containsKey(tag);
	}

	public	XMLTree	moveDown(String tag){
		XMLTree		son = _sons.get(tag);

		son._checked = Boolean.TRUE;

		return son;
	}

	/**
	 * Creates and returns a list of unvisited children if any. In the latest
	 * case, returns an empty list.
	 * @return a not null list of unvisited children
	 */
	public	List<XMLTree>	getMandatorySonsUnchecked(){
		List<XMLTree>		list = new ArrayList<XMLTree>();

		for (Iterator<String> it = _sons.keySet().iterator(); it.hasNext();) {
			XMLTree son = _sons.get(it.next());

			if (!son._checked){
				list.add(son);
			}
		}

		return list;
	}

	/**
	 * Returns if the XML file corresponds to the given XML tree.
	 * @return if the XML file corresponds to the given XML tree.
	 */
	public	void	addAllowedTag(String tag){
		_allowedTags.add(tag);
	}

	public	Set<String>	getAllowedTags(){
		return _allowedTags;
	}

	@Override
	public	String	toString(){
		return _tag;
	}

	private	Map<String, XMLTree>	_sons			= null;
	private	List<String>			_properties		= null;
	private	Boolean					_checked		= null;
	private	Boolean					_mandatory		= null;
	private	XMLTree					_dad			= null;
	private	String					_tag			= null;
	private	Set<String>				_allowedTags	= null;
};

