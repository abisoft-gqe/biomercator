/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 12-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.converters;

import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.fileTools.utils.FileTools;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class FromCSV extends FileConverter{
	public FromCSV(	File		file,
					String		outputName,
					String		mapName,
					List<Field>	fields){
		super(file, outputName);
		_writer	= new SAXWriter(super.getNewFileName());
		_file	= file;
		_fields	= fields;
	}

	@Override
	public void			execute() {
		BufferedReader	buf				= null;
		String			line			= null;
		String[]		locus_attribute	= new String[_fields.size()];
		double			position		= 0;
		String			chrName			= null;
		int				chrIndex		= 0;
		int				locusNameIndex	= 0;
		int				locusTypeIndex	= 0;
		int				locusPosIndex	= 0;
		boolean			first_chr		= true;

		// Indexes setting
		for (int i = 0; i < _fields.size(); i++) {
			Field field = _fields.get(i);

			if (field.name.equals("chr name")){
				chrIndex = i;
			}else if (field.name.equals("locus name")){
				locusNameIndex = i;
			}else if (field.name.equals("locus type")){
				locusTypeIndex = i;
			}else if (field.name.equals("position")){
				locusPosIndex = i;
			}
		}
		try{
			_writer.startDocument();
			_writer.startElement(XMLStandardTagIds.MAPS);
			_writer.startElement(XMLStandardTagIds.MAP,	XMLStandardTagIds.NAME, "test");
			//	Map Tags
			_writer.addElement(XMLStandardTagIds.POPSIZE, "42");
			_writer.addElement(XMLStandardTagIds.CROSSTYPE, "RI1");
			_writer.addElement(XMLStandardTagIds.MAP_UNIT, "cM");
			_writer.addElement(XMLStandardTagIds.MAPPING_FUNCTION, "haldane");

			buf = new BufferedReader(new FileReader(_file));
			while (null != (line = buf.readLine())) {
				line = line.trim();
				FileTools.split(line, "\t", locus_attribute);
				if (!locus_attribute[chrIndex].equalsIgnoreCase(chrName)){
					chrName = locus_attribute[chrIndex];
					if (!first_chr){
						_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
						_writer.endElement(XMLStandardTagIds.CHROMOSOME);
					}
					first_chr = false;
					_writer.startElement(	XMLStandardTagIds.CHROMOSOME,
											XMLStandardTagIds.NAME,
											locus_attribute[chrIndex]);
					_writer.startElement(	XMLStandardTagIds.LINKAGE_GROUP,
											XMLStandardTagIds.NAME,
											locus_attribute[chrIndex]);
				}
				try{
					position = Double.valueOf(locus_attribute[locusPosIndex]);
				} catch (NumberFormatException ex){	}
				_writer.startElement(	XMLStandardTagIds.MARKER,
										XMLStandardTagIds.NAME,
										locus_attribute[locusNameIndex]);
				_writer.addElement(XMLStandardTagIds.MARKER_LOCATION, String.valueOf(position));
				_writer.addElement(XMLStandardTagIds.MARKER_ALIASE, "");
				_writer.addElement(XMLStandardTagIds.MARKER_TYPE, locus_attribute[locusTypeIndex]);
				_writer.endElement(XMLStandardTagIds.MARKER);
			}
			if (!first_chr){
				_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
				_writer.endElement(XMLStandardTagIds.CHROMOSOME);
			}
			_writer.endElement(XMLStandardTagIds.MAP);
			_writer.endElement(XMLStandardTagIds.MAPS);
			_writer.endDocument();
			buf.close();
		}catch(IOException e){
			System.out.println();
		}
	}

	private	List<Field>	_fields		= null;
	private SAXWriter	_writer		= null;
	private	File		_file		= null;
}
