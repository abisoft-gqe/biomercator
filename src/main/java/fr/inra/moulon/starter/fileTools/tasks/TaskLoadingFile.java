/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 04-May-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.tasks;

import fr.inra.moulon.starter.database.Field;
import fr.inra.moulon.starter.fileTools.loader.CSVLoader;
import fr.inra.moulon.starter.gui.wizards.WizardGenomeVersion;
import fr.inra.moulon.starter.utils.PhysicDataTypes;
import java.io.File;
import java.util.List;
import javax.swing.BoundedRangeModel;
import javax.swing.SwingWorker;


public abstract class TaskLoadingFile extends SwingWorker<Void, Void>{
	public	TaskLoadingFile(WizardGenomeVersion	wizard,
							File				file,
							List<Field>			correspondance,
							String				separator,
							PhysicDataTypes		physicDataType){
		_wizard			= wizard;
		_file			= file;
		_correspondance	= correspondance;
		_separator		= separator;
		_physicDataType	= physicDataType;
	}

	/**
	 * Returns a SQLite of insertion statement : "insert into <i>table</i>
	 * values ([?,]*?;");
	 * @return
	 */
	public	abstract	String	getStatementInsertAll();

	/**
	 * Returns a SQLite of insertion statement : "insert into <i>table</i>
	 * values ([?,]*?;");
	 * @return
	 */
	public	abstract	String	getStatementInsertUnique();

	@Override
	protected	Void	doInBackground() throws Exception {
		try{
			_dtbId = CSVLoader.load(_file,
									_separator,
									getStatementInsertUnique(),
									getStatementInsertAll(),
									_correspondance,
									_boundedModel);
		}catch (Throwable t){
			t.printStackTrace(System.out);
		}
		return null;
	}

	public	void	setBoundedModel(BoundedRangeModel boundedModel){
		_boundedModel = boundedModel;
	}

	@Override
	public	void	done(){
		System.out.println("Done !");
		_wizard.taskIsDone(_physicDataType, _dtbId);
	}

	private		WizardGenomeVersion	_wizard			= null;
	protected	File				_file			= null;
	private		List<Field>			_correspondance	= null;
	private		String				_separator		= null;
	private		BoundedRangeModel	_boundedModel	= null;
	private		int					_dtbId			= 0;
	private		PhysicDataTypes		_physicDataType	= null;
}
