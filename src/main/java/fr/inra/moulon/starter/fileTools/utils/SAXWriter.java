/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.utils;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

/**
 * This class allows to write into an XML file.
 * @author sosnowski
 */
public class SAXWriter {
	/**
	 * Creates the writer and opens the given file.
	 * @param fileName The file name where to write.
	 */
	public SAXWriter(String path){
		_tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
		_hd = createHD(path);
	}

	/**
	 * Creates a TransformerHandler from the transformer factory and set the
	 * fileName as the result file
	 * @param fileName the file name
	 * @param tf the SAXTransformerFactory
	 * @return
	 */
	private TransformerHandler	createHD(String path){
		TransformerHandler		hd = null;

		try {
			hd = _tf.newTransformerHandler();
			Transformer serializer = hd.getTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			hd.setResult(new StreamResult(path));
		} catch (TransformerConfigurationException ex) {
			Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
		}

		return hd;
	}

	/**
	 * Method to call when starting a document.
	 */
	public	void startDocument(){
		try {
			_hd.startDocument();
		} catch (SAXException ex) {
			Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Method to call when ending a document.
	 */
	public	void endDocument(){
		try {
			_hd.endDocument();
		} catch (SAXException ex) {
			Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Opens a new XML tag.
	 * @param name The tag name.
	 */
	public	void startElement(String name){
		try {
			_hd.startElement("", "", name, null);
		} catch (SAXException ex) {
			Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Opens a new XML tag with the given attribute/value.
	 * @param name The tag name.
	 * @param field The attribute name.
	 * @param value The value.
	 */
	public	void startElement(String name, String field, String value){
		AttributesImpl	att = new AttributesImpl();

		att.addAttribute("", "", field, "", value);

		try {
			_hd.startElement("", "", name, att);
		} catch (SAXException ex) {
			Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Opens a new XML tag with the given attribute/value.
	 * @param name The tag name.
	 * @param field The attribute name.
	 * @param value The value.
	 */
	public	void startElement(String name, String[] fields, String[] values){
		AttributesImpl	att = new AttributesImpl();

		for (int i = 0; i < fields.length; i++) {
			String field = fields[i];
			String value = values[i];
			att.addAttribute("", "", field, "", value);
		}

		try {
			_hd.startElement("", "", name, att);
		} catch (SAXException ex) {
			Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Opens a new XML tag, sets a text and closes the tag right after.
	 * @param name The tag name
	 * @param txt The text (can be null).
	 */
	public	void	addElement(String name, String txt){
		if (null != txt){
			try {
				_hd.startElement("", "", name, null);
				_hd.characters(txt.toCharArray(), 0, txt.length());
				_hd.endElement("", "", name);
			} catch (SAXException ex) {
				Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}


	/**
	 * Opens a new XML tag, sets a text and closes the tag right after, only if
	 * all fields/values are not null.
	 * @param name The tag name
	 * @param fields The fields array
	 * @param txt The values array
	 */
	public	void	addElement(String name, String[] fields, String[] values){
		AttributesImpl	att = new AttributesImpl();

		for (int i = 0; null != att && i < fields.length; i++) {
			String field = fields[i];
			String value = values[i];
			if (null != field && null != value){
				att.addAttribute("", "", field, "", value);
			}else{
				att = null;
			}
		}

		if (null != att){
			try {
				_hd.startElement("", "", name, att);
				_hd.endElement("", "", name);
			} catch (SAXException ex) {
				Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	/**
	 * Closes a XML tag.
	 * @param name The tag name.
	 */
	public	void endElement(String name){
		try {
			_hd.endElement("", "", name);
		} catch (SAXException ex) {
			Logger.getLogger(SAXWriter.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private	TransformerHandler		_hd = null;
	private SAXTransformerFactory	_tf = null;
}
