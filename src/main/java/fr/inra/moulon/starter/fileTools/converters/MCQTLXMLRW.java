/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.converters;


import fr.inra.moulon.starter.datamodel.entities.MapGene;
import fr.inra.moulon.starter.fileTools.utils.SAXWriter;
import fr.inra.moulon.starter.fileTools.utils.XMLMCQTLTagIds;
import fr.inra.moulon.starter.fileTools.utils.XMLStandardTagIds;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.LocatorImpl;


public class MCQTLXMLRW implements ContentHandler {
	public MCQTLXMLRW(String fileName, String mapName){
		super();
        _locator = new LocatorImpl();
		_writer = new SAXWriter(fileName);
		_mapName = mapName;
	}

	/**
	 * Definition du locator qui permet a tout moment pendant l'analyse, de localiser
	 * le traitement dans le flux. Le locator par defaut indique, par exemple, le numero
	 * de ligne et le numero de caractere sur la ligne.
	 * @author smeric
	 * @param value le locator a utiliser.
	 * @see org.xml.sax.ContentHandler#setDocumentLocator(org.xml.sax.Locator)
	 */
	@Override
	public void setDocumentLocator(Locator value) {
		_locator =  value;
	}

	/**
	 * Evenement envoye au demarrage du parse du flux xml.
	 * @throws SAXException en cas de probleme quelquonque ne permettant pas de
	 * se lancer dans l'analyse du document.
	 * @see org.xml.sax.ContentHandler#startDocument()
	 */
	@Override
	public void startDocument() throws SAXException {
		_writer.startDocument();
		_writer.startElement(XMLStandardTagIds.MAPS);

	}

	/**
	 * Evenement envoye a la fin de l'analyse du flux xml.
	 * @throws SAXException en cas de probleme quelquonque ne permettant pas de
	 * considerer l'analyse du document comme etant complete.
	 * @see org.xml.sax.ContentHandler#endDocument()
	 */
	@Override
	public void endDocument() throws SAXException {
		_writer.endElement(XMLStandardTagIds.MAPS);
		_writer.endDocument();
	}

	/**
	 * Debut de traitement dans un espace de nommage.
	 * @param prefixe utilise pour cet espace de nommage dans cette partie de l'arborescence.
	 * @param URI de l'espace de nommage.
	 * @see org.xml.sax.ContentHandler#startPrefixMapping(java.lang.String, java.lang.String)
	 */
	@Override
	public void startPrefixMapping(String prefix, String URI) throws SAXException {}
	
	/**
	 * Fin de traitement de l'espace de nommage.
	 * @param prefixe le prefixe choisi a l'ouverture du traitement de l'espace nommage.
	 * @see org.xml.sax.ContentHandler#endPrefixMapping(java.lang.String)
	 */
	@Override
	public void endPrefixMapping(String prefix) throws SAXException {}

	/**
	 * Evenement recu a chaque fois que l'analyseur rencontre une balise xml ouvrante.
	 * @param nameSpaceURI l'url de l'espace de nommage.
	 * @param localName le nom local de la balise.
	 * @param rawName nom de la balise en version 1.0 <code>nameSpaceURI + ":" + localName</code>
	 * @throws SAXException si la balise ne correspond pas a ce qui est attendu,
	 * comme par exemple non respect d'une dtd.
	 * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void		startElement(	String		nameSpaceURI,
									String		localName,
									String		rawName,
									Attributes	attributs) throws SAXException {
		String		mapName = null;
		resetStr();
		if (localName.equalsIgnoreCase(XMLMCQTLTagIds.TRAIT)){
			_traitName = attributs.getValue(XMLMCQTLTagIds.PROP_NAME);
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.GENETIC_MAP)){
			_tag = MAP;
			if (null != _mapName){
				mapName = _mapName;
			}else{
				mapName = attributs.getValue(XMLMCQTLTagIds.PROP_NAME);
			}
			_writer.startElement(XMLStandardTagIds.MAP,	XMLStandardTagIds.NAME, mapName);
			_writer.addElement(XMLStandardTagIds.GENUS, MapGene.DEFAULT_GENUS);
			_writer.addElement(XMLStandardTagIds.SPECIES, MapGene.DEFAULT_SPECIES);
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.CHROMOSOME)){
			_tag = CHR;
			_writer.startElement(	XMLStandardTagIds.CHROMOSOME,
									XMLStandardTagIds.NAME,
									attributs.getValue(XMLMCQTLTagIds.PROP_NAME));

			_writer.startElement(	XMLStandardTagIds.LINKAGE_GROUP,
									XMLStandardTagIds.NAME,
									attributs.getValue(XMLMCQTLTagIds.PROP_NAME));
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.MARKER)){
			_tag = MARKER;
			_position = attributs.getValue(XMLMCQTLTagIds.PROP_POSITION);
			_writer.startElement(	XMLStandardTagIds.MARKER,
									XMLStandardTagIds.NAME,
									attributs.getValue(XMLMCQTLTagIds.PROP_NAME));
			_writer.addElement(XMLStandardTagIds.MARKER_ALIASE, "");
			_writer.addElement(XMLStandardTagIds.MARKER_LOCATION, _position);
			_writer.addElement(XMLStandardTagIds.MARKER_TYPE, "");
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.QTL)){
			_tag = QTL;
			_position = attributs.getValue(XMLMCQTLTagIds.PROP_POSITION);
			_writer.startElement(	XMLStandardTagIds.QTL,
									XMLStandardTagIds.NAME,
									_traitName+"-"+_position);
			_writer.addElement(XMLStandardTagIds.QTL_TRAIT, _traitName);
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.QTL_INFERIOR_POSITION)){
			try{
				_ciFrom = Double.valueOf(attributs.getValue(XMLMCQTLTagIds.PROP_VALUE));
			}catch (Exception e){
				_ciFrom = 0;
			}
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.QTL_SUPERIOR_POSITION)){
			try{
				_ciTo = Double.valueOf(attributs.getValue(XMLMCQTLTagIds.PROP_VALUE));
			}catch (Exception e){
				_ciTo = 0;
			}
		}
	}

	/**
	 * Evenement recu a chaque fermeture de balise.
	 * @see org.xml.sax.ContentHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void		endElement(String nameSpaceURI, String localName, String rawName) throws SAXException {
		if(localName.equalsIgnoreCase(XMLMCQTLTagIds.GENETIC_MAP)) {
			_writer.endElement(XMLStandardTagIds.MAP);
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.CHROMOSOME)){
			_writer.endElement(XMLStandardTagIds.LINKAGE_GROUP);
			_writer.endElement(XMLStandardTagIds.CHROMOSOME);
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.MARKER)){
			_writer.endElement(XMLStandardTagIds.MARKER);
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.QTL)) {
			_writer.addElement(XMLStandardTagIds.QTL_R2, String.valueOf(_r2));
			_writer.addElement(XMLStandardTagIds.QTL_LOCATION, String.valueOf(_position));
			_writer.addElement(XMLStandardTagIds.QTL_CI_FROM, String.valueOf(_ciFrom));
			_writer.addElement(XMLStandardTagIds.QTL_CI_TO, String.valueOf(_ciTo));
			_writer.endElement(XMLStandardTagIds.QTL);
		}else if (localName.equalsIgnoreCase(XMLMCQTLTagIds.QTL_R2)) {
			try{
				_r2 = Double.valueOf(getStr());
			}catch (NumberFormatException e){
				_r2 = 0;
			}
		}
	}

	/**
	 * Evenement recu a chaque fois que l'analyseur rencontre des caracteres (entre
	 * deux balises).
	 * @param ch les caracteres proprement dits.
	 * @param start le rang du premier caractere a traiter effectivement.
	 * @param length le nombre de caracteres a traiter effectivement.
	 * @see org.xml.sax.ContentHandler#characters(char[], int, int)
	 */
	@Override
	public void	characters(char[] ch, int start, int length) throws SAXException {
		String	strTmp = new String(ch, start, length);
		//System.out.println("#PCDATA : " + new String(ch, start, length));
		if (null !=_str){
			_str += strTmp;
		}else{
			_str = strTmp;
		}
		_str = _str.replaceAll("\n", "");
		if (_str.length() == 0){
			_str = null;
		}
	}

	private void	resetStr(){
		_str = null;
	}

	private String	getStr(){
		String	res = _str;

		//resetStr();

		return res;
	}

	/**
	 * Recu chaque fois que des caracteres d'espacement peuvent etre ignores au sens de
	 * XML. C'est a dire que cet evenement est envoye pour plusieurs espaces se succedant,
	 * les tabulations, et les retours chariot se succedants ainsi que toute combinaison de ces
	 * trois types d'occurrence.
	 * @param ch les caracteres proprement dits.
	 * @param start le rang du premier caractere a traiter effectivement.
	 * @param end le rang du dernier caractere a traiter effectivement
	 * @see org.xml.sax.ContentHandler#ignorableWhitespace(char[], int, int)
	 */
	@Override
	public void ignorableWhitespace(char[] ch, int start, int end) throws SAXException {}

	/**
	 * Rencontre une instruction de fonctionnement.
	 * @param target la cible de l'instruction de fonctionnement.
	 * @param data les valeurs associees a cette cible. En general, elle se presente sous la forme 
	 * d'une serie de paires nom/valeur.
	 * @see org.xml.sax.ContentHandler#processingInstruction(java.lang.String, java.lang.String)
	 */
	@Override
	public void processingInstruction(String target, String data) throws SAXException {}

	/**
	 * Recu a chaque fois qu'une balise est evitee dans le traitement a cause d'un
	 * probleme non bloque par le parser. Pour ma part je ne pense pas que vous
	 * en ayez besoin dans vos traitements.
	 * @see org.xml.sax.ContentHandler#skippedEntity(java.lang.String)
	 */
	@Override
	public void skippedEntity(String arg0) throws SAXException {
		System.out.println("THERE IS A MASSIVE PROBLEM !!!");
	}

	private Locator		_locator	= null;
	private SAXWriter	_writer		= null;
	private	String		_position	= null;
	private	int			_tag		= 0;
	private	double		_popSize	= -1;
	private	double		_ciFact		= 530;

	private String		_traitName	= null;
	private	String		_mapName	= null;
	private	double		_ciFrom		= -1;
	private	double		_ciTo		= -1;
	private	double		_r2			= -1;
	private	String		_str		= null;

	private	static	final	int	MAP		= 1;
	private	static	final	int	CHR		= 2;
	private	static	final	int	MARKER	= 3;
	private	static	final	int	QTL		= 4;

}
