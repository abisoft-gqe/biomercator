/**
 *
 * $Author: Olivier SOSNOWSKI, Johann JOETS
 * $Date: 03-Feb-2011
 * $Version: 3.2
 *
 *
 * Copyright (C) 2011-2012  Olivier SOSNOWSKI, Johann JOETS, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 */

package fr.inra.moulon.starter.fileTools.utils;

/**
 *
 * @author sosnowski
 */
public class XMLStandardTagIds {
	public static final String		UNKNOWN					= "unknow";
	public static final String		NAME					= "name";
	public static final String		MAPS					= "maps";
	public static final String		MAP						= "map";
	public static final String		GENUS					= "genus";
	public static final String		SPECIES					= "species";
	public static final String		CROSSTYPE				= "crossType";
	public static final String		POPSIZE					= "popSize";
	public static final String		MAPPING_CROSSTYPE		= "mappingCrossType";
	public static final String		MAPPING_FUNCTION		= "mappingFunction";
	public static final String		PARENT_NAME				= "parentName";
	public static final String		MAP_UNIT				= "mapUnit";
	public static final String		MAP_EXPANSION			= "mapExpansion";
	public static final String		MAP_QUALITY				= "mapQuality";
	public static final String		USED_MAPS				= "usedMaps";
	public static final String		USED_MAP				= "usedMap";
	public static final String		CHROMOSOME				= "chromosome";
	public static final String		LINKAGE_GROUP			= "linkageGroup";
	public static final String		BELONGING				= "belonging";
	// LOCUS FIELDS
	public static final String		MARKER					= "locus";
	public static final String		MARKER_ALIASE			= "locusAliase";
	public static final String		MARKER_LOCATION			= "locusLocation";
	public static final String		MARKER_TYPE				= "locusType";
	// QTL FIELDS
	public static final String		QTL						= "qtl";
	public static final String		QTL_TRAIT				= "qtlTrait";
	public static final String		QTL_ORIGIN_MAP			= "qtlOriginMap";
	public static final String		QTL_TRAIT_OID			= "qtlTraitOntologyId";
	public static final String		QTL_EXP_PLACE			= "qtlExperimentPlace";
	public static final String		QTL_EXP_DATE			= "qtlExperimentDate";
	public static final String		QTL_LOD					= "qtlLodScore";
	public static final String		QTL_R2					= "qtlR2";
	public static final String		QTL_LOCATION			= "qtlLocation";
	public static final String		QTL_CI_FROM				= "qtlCiFrom";
	public static final String		QTL_CI_TO				= "qtlCiTo";
	public static final String		QTL_CROSS_NAME			= "qtlCrossName";
	public static final String		QTL_CROSS_SIZE			= "qtlCrossSize";
	public static final String		QTL_CROSS_TYPE			= "qtlCrossType";
	public static final String		QTL_EFFECT				= "qtlEffect";
	public static final String		QTL_EFFECT_UNIT			= "qtlEffectUnit";
	public static final String		QTL_EFFECT_UNIT_OID		= "qtlEffectUnitOntologyId";
	public static final String		QTL_PARENT				= "qtlParent";
	// MQTL FIELDS
	public static final String		META_A					= "metaAnalysis";
	public static final String		META_A_METHOD			= "metaAnalysisMethod";
	public static final String		META_A_MEMERBSHIPS		= "mqtlMemberships";
	public static final String		META_A_USED_QTL_IDS		= "metaAnalysisUsedQtlIds";
	public static final String		META_A_USED_QTL_ID		= "metaAnalysisUsedQtlId";
	public static final String		META_MODEL				= "metaModel";
	public static final String		MQTL					= "mqtl";
	public static final String		MQTL_QTL_ID				= "mqtlQtlId";
	public static final String		MQTL_TRAIT				= "mqtlTrait";
	public static final String		MQTL_TRAIT_OID			= "mqtlTraitOntologyId";
	public static final String		MQTL_EXP_PLACE			= "mqtlExperimentPlace";
	public static final String		MQTL_EXP_DATE			= "mqtlExperimentDate";
	public static final String		MQTL_LOD				= "mqtlLodScore";
	public static final String		MQTL_R2					= "mqtlR2";
	public static final String		MQTL_LOCATION			= "mqtlLocation";
	public static final String		MQTL_CI_FROM			= "mqtlCiFrom";
	public static final String		MQTL_CI_TO				= "mqtlCiTo";
	public static final String		MQTL_CROSS_NAME			= "mqtlCrossName";
	public static final String		MQTL_CROSS_SIZE			= "mqtlCrossSize";
	public static final String		MQTL_CROSS_TYPE			= "mqtlCrossType";
	public static final String		MQTL_EFFECT				= "mqtlEffect";
	public static final String		MQTL_EFFECT_UNIT		= "mqtlEffectUnit";
	public static final String		MQTL_EFFECT_UNIT_OID	= "mqtlEffectUnitOntologyId";
	public static final String		MQTL_PARENT				= "mqtlParent";
	public static final String		MQTL_MEMBERSHIP			= "mqtlMembership";
}
